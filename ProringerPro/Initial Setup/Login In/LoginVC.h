//
//  LoginVC.h
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginVC : ProRingerProBaseVC <CLLocationManagerDelegate> {
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
}

@property (weak, nonatomic) IBOutlet UITextField *txtEmailId;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UIView *viewAccessory;

@property (strong, nonatomic) NSString *strTimeZone;

- (IBAction)LoginButtonPressed:(id)sender;
- (IBAction)CreateAccount:(id)sender;
- (IBAction)ForgotPassword:(id)sender;

@end

NS_ASSUME_NONNULL_END
