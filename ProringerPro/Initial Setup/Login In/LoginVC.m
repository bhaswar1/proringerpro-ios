//
//  LoginVC.m
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "LoginVC.h"

@interface LoginVC () {
    
    BOOL isLoginPressed;
}

@end

@implementation LoginVC
@synthesize txtEmailId, txtPassword, viewAccessory, strTimeZone;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.view.backgroundColor = [UIColor clearColor];
    
    isLoginPressed = NO;
    
    [self currentLocationIdentifier];
    [self getCurrentTimeZone];
    
//    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = @"LOG IN";
    
    txtPassword.delegate = self;
    txtEmailId.delegate = self;
    
    [self paddingForTextField:txtEmailId];
    [self paddingForTextField:txtPassword];
    
    [self shadowForTextField:txtEmailId];
    [self shadowForTextField:txtPassword];
    
    txtEmailId.inputAccessoryView = txtPassword.inputAccessoryView = viewAccessory;
    
//    txtEmailId.text = @"subhadeep.roy@esolzmail.com"; // @"atanu.sahoo@esolzmail.com";// @"subhadeep.roy@esolzmail.com"; //
//    txtPassword.text = @"123456";
    
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = NO;
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

-(void)getCurrentTimeZone {
    
    [YXSpritesLoadingView show];
    
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    
    strTimeZone = timeZone.name;
    
    NSLog(@"Name : %@", timeZone);
    
    if (isLoginPressed) {
        [self connectionForLogin];
    }
    
    [YXSpritesLoadingView dismiss];
}


-(void)currentLocationIdentifier {
    
    [YXSpritesLoadingView show];
    
    //---- For getting current gps location
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    //------
    [YXSpritesLoadingView dismiss];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    [YXSpritesLoadingView show];
    
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
         if (!(error)) {
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
//             NSLog(@"\nCurrent Location Detected\n");
//             NSLog(@"placemark %@",placemark);
//             NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
//             NSString *Address = [[NSString alloc]initWithString:locatedAt];
//             NSString *Area = [[NSString alloc]initWithString:placemark.locality];
//             NSString *Country = [[NSString alloc]initWithString:placemark.country];
//             NSString *CountryArea = [NSString stringWithFormat:@"%@, %@", Area,Country];
//             NSLog(@"%@",CountryArea);
//             NSLog(@"TIME ZONE:::...%@", placemark.timeZone.name);
             self->strTimeZone = placemark.timeZone.name;
//             NSLog(@"Timezone -%@", placemark.timeZone.abbreviation);
         } else {
             NSLog(@"Geocode failed with error %@", error);
             NSLog(@"\nCurrent Location Not Detected\n");
             //return;
//             CountryArea = NULL;
         }
         /*---- For more results
          placemark.region);
          placemark.country);
          placemark.locality);
          placemark.name);
          placemark.ocean);
          placemark.postalCode);
          placemark.subLocality);
          placemark.location);
          ------*/
        [YXSpritesLoadingView dismiss];
     }];
}

//#pragma mark - UITextField Design
//-(void) paddingForTextField:(UITextField *)textField {
//    
//    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(30, 0, 10, 20)];
//    textField.leftView = paddingView;
//    textField.leftViewMode = UITextFieldViewModeAlways;
//    
//    textField.layer.masksToBounds = NO;
//    textField.layer.shadowOffset = CGSizeMake(0.0, 0.0);
//    textField.layer.shadowRadius = 1;
//    textField.layer.shadowOpacity = 0.2;
//    textField.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
//    
//    textField.layer.borderWidth = 1.0;
//    textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
//}
//
//-(void)shadowForTextField:(UITextField *)textfield {
//    
//    UIColor *color = [UIColor blackColor];
//    textfield.layer.shadowColor = [color CGColor];
//    textfield.layer.shadowRadius = 1.5f;
//    textfield.layer.shadowOpacity = 0.2;
//    textfield.layer.shadowOffset = CGSizeZero;
//    textfield.layer.masksToBounds = NO;
//}
//
//-(void)textFieldDidBeginEditing:(UITextField *)textField {
//    
//    textField.layer.borderColor = COLOR_THEME.CGColor;
//    textField.layer.borderWidth=1.5f;
//    
//}
//
//- (void)textFieldDidEndEditing:(UITextField *)textField {
//    
//    [self paddingForTextField:textField];
//    [self shadowForTextField:textField];
//}
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    
//    [textField resignFirstResponder];
//    return YES;
//}

#pragma mark - UITextField Validation
-(BOOL)validateTextField {
    
    NSString *strAlert = @"";
    BOOL isValid = YES;
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtEmailId.text]) {
        
        strAlert = @"Please enter account email";
        isValid = NO;
        [txtEmailId becomeFirstResponder];
    } else if (txtEmailId.text.length > 0) {
        
        if (![self validateEmail:txtEmailId.text]) {
            strAlert = @"Please enter a valid email format";
            isValid = NO;
        }
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtPassword.text]) {
        strAlert = @"Please enter your password";
        isValid = NO;
        [txtPassword becomeFirstResponder];
    }
    
    if (strAlert.length > 0) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strAlert];
    }
    return isValid;
}

-(BOOL) validateEmail:(NSString*) emailString {
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    }
    else
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Web Service
-(void) connectionForLogin {
    [YXSpritesLoadingView show];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *deviceToken = [NSString stringWithFormat:@"%@", [userDefault objectForKey:@"deviceToken"]];
    
    if ([deviceToken isEqualToString:@"(null)"]) {
        deviceToken = @"cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e";
    }
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_LOGIN];
    
    NSDictionary *params = @{@"email": txtEmailId.text,
                             @"password": txtPassword.text,
                             @"device_type": @"i",
                             @"user_type": @"C",
                             @"device_token": deviceToken,
                             @"user_timezone": strTimeZone
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];

    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"RESPONSE:::...%@", response);
        
        if ([[response objectForKey:@"response"] boolValue]) {
            
            [userDefault setObject:[[response objectForKey:@"info_array"] objectForKey:@"email"] forKey:@"email"];
            [userDefault setObject:[[response objectForKey:@"info_array"] objectForKey:@"first_name"] forKey:@"firstName"];
            [userDefault setObject:[[response objectForKey:@"info_array"] objectForKey:@"last_name"] forKey:@"lastName"];
            [userDefault setObject:[[response objectForKey:@"info_array"] objectForKey:@"user_id"] forKey:@"userId"];
            
            self.strUserId = [[response objectForKey:@"info_array"] objectForKey:@"user_id"];
            NSLog(@"USER ID FROM STRING:::...%@", self.strUserId);
            NSLog(@"USER ID FROM USERDEFAULT:::...%@", [userDefault objectForKey:@"user_id"]);
            
            [userDefault setBool:YES forKey:@"isLoggedIn"];
            [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"footer"];
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
//            DashboardVC *tbvc = [storyboard instantiateViewControllerWithIdentifier:@"DashboardVC"];
//            [self.navigationController pushViewController:tbvc animated:YES];
            [self footerButtonTap:1];
        } else {
            self->isLoginPressed = NO;
            [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:[response objectForKey:@"message"]];
        }
    }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              [YXSpritesLoadingView dismiss];
              NSLog(@"ERROR:::...%@", task.error);
          }];
}

#pragma mark - UIButton Action
- (IBAction)LoginButtonPressed:(id)sender {
    if ([self validateTextField]) {
        if (strTimeZone.length == 0 || strTimeZone == nil) {
            isLoginPressed = YES;
            [self getCurrentTimeZone];
        } else {
            [self connectionForLogin];
        }
    }
}

- (IBAction)CreateAccount:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignUpVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SignUpVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

- (IBAction)ForgotPassword:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ForgotPasswordVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordVC"];
    [self.navigationController pushViewController:svc animated:YES];
}
@end
