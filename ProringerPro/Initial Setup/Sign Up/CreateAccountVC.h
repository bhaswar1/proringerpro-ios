//
//  CreateAccountVC.h
//  ProringerPro
//
//  Created by Soma Halder on 30/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CreateAccountVC : ProRingerProBaseVC //<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (strong, nonatomic) IBOutlet UIView *viewAccessory;

@property (weak, nonatomic) IBOutlet UITableView *tblSignUp;
@property (weak, nonatomic) IBOutlet UITableView *tblServiceList;

@property (weak, nonatomic) IBOutlet UITextField *txtBusinessName;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtState;
@property (weak, nonatomic) IBOutlet UITextField *txtZip;
@property (weak, nonatomic) IBOutlet UITextField *txtBusinessPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPrimaryService;
@property (weak, nonatomic) IBOutlet UITextField *txtServiceArea;

@property (strong, nonatomic) NSString *strFirstName, *strLastName, *strProEmailId, *strProPhoneNumber, *strPassword;
@property (weak, nonatomic) IBOutlet FRHyperLabel *lblLink;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constPickerHeight;

- (IBAction)CompleteAccount:(id)sender;

@end

NS_ASSUME_NONNULL_END
