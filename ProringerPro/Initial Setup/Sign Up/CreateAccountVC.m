//
//  CreateAccountVC.m
//  ProringerPro
//
//  Created by Soma Halder on 30/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "CreateAccountVC.h"
#import "IQKeyboardManager.h"

@interface CreateAccountVC ()
{
    NSMutableArray *arrPrimaryServiceList, *arrSearch, *arrTemp;
    NSString *strTimeZone, *strZipCode, *strCountry, *strCity,  *strState,  *strStreet,  *strLat,  *strLan, *strPhoneNumber;;
    NSMutableDictionary *dictService;
    BOOL area, isSelected, isStartEditing;
    CGFloat fScreenHeight;
}

@end

@implementation CreateAccountVC
@synthesize txtZip, txtCity, txtEmail, txtState, txtAddress, txtBusinessPhoneNumber, txtServiceArea, txtBusinessName, txtPrimaryService, viewHeader, tblServiceList, constPickerHeight, strLastName, strPassword, strFirstName, strProEmailId, strProPhoneNumber, lblLink, tblSignUp, viewAccessory;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Create Account";
    self.navigationController.navigationBar.hidden = NO;
    
    arrPrimaryServiceList = [[NSMutableArray alloc] init];
    arrSearch = [[NSMutableArray alloc] init];
    arrTemp = [[NSMutableArray alloc] init];
    
    constPickerHeight.constant = 0;
    CGRect frame = viewHeader.frame;
    fScreenHeight = frame.size.height = 660;
    viewHeader.frame = frame;
    
    isSelected = isStartEditing = NO;
    
    tblServiceList.dataSource = self;
    tblServiceList.delegate = self;
    tblServiceList.hidden = YES;
    
    arrPrimaryServiceList = [[NSMutableArray alloc] init];//WithObjects:@"1", @"11", @"111", @"1111", @"11111", @"111111", @"1111111", @"11111111", @"111111111", @"1111111111", nil];
    
    tblServiceList.backgroundColor = [UIColor whiteColor];
    
    txtZip.delegate = self;
    txtCity.delegate = self;
    txtEmail.delegate = self;
    txtState.delegate = self;
    txtAddress.delegate = self;
    txtServiceArea.delegate = self;
    txtBusinessPhoneNumber.delegate = self;
    txtBusinessName.delegate = self;
    txtPrimaryService.delegate = self;
    
    [self paddingForTextField:txtPrimaryService];
    [self paddingForTextField:txtBusinessName];
    [self paddingForTextField:txtCity];
    [self paddingForTextField:txtState];
    [self paddingForTextField:txtZip];
    [self paddingForTextField:txtBusinessPhoneNumber];
    [self paddingForTextField:txtEmail];
    [self paddingForTextField:txtServiceArea];
    [self paddingForTextField:txtAddress];
    
    [self shadowForTextField:txtPrimaryService];
    [self shadowForTextField:txtBusinessName];
    [self shadowForTextField:txtCity];
    [self shadowForTextField:txtState];
    [self shadowForTextField:txtZip];
    [self shadowForTextField:txtBusinessPhoneNumber];
    [self shadowForTextField:txtEmail];
    [self shadowForTextField:txtServiceArea];
    [self shadowForTextField:txtAddress];
    
//    txtPrimaryService.inputAccessoryView = viewAccessory;
    
//    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:NO];
    
    NSString *strMessage = @"By signing up with ProRinger you agree with our Terms of Use and Privacy Policy";
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor darkGrayColor],NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]};
    lblLink.attributedText = [[NSAttributedString alloc]initWithString:strMessage attributes:attributes];
    
    void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
        if ([substring isEqualToString:@"Terms of Use"]) {
            [self navigateToTermsOfUse];
        } else if ([substring isEqualToString:@"Privacy Policy"]) {
            [self navigateToPrivacyPolicy];
        }
    };
    
    //Step 3: Add link substrings
    [lblLink setLinksForSubstrings:@[@"Terms of Use", @"Privacy Policy"] withLinkHandler:handler];

//    [[IQKeyboardManager sharedManager] setEnable:NO];
//    [self connectionForServiceTypeList:@""];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[IQKeyboardManager sharedManager] setEnable:YES];
}

-(void)navigateToTermsOfUse {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TermsAndConditionVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditionVC"];
    svc.strTitle = @"TERMS OF SERVICES";
    svc.strApi = @"app_term";
    [self.navigationController pushViewController:svc animated:YES];
}

-(void)navigateToPrivacyPolicy {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TermsAndConditionVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditionVC"];
    svc.strTitle = @"Privacy Policy";
    svc.strApi = @"app_privacy_policy";
    [self.navigationController pushViewController:svc animated:YES];
}

#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    textField.layer.borderColor = COLOR_THEME.CGColor;
    textField.layer.borderWidth = 1.5f;
    
    if (textField == txtPrimaryService) {
        
        isSelected = NO;
        
//        [self scrollToBottom];
        
        dictService = [[NSMutableDictionary alloc] init];
        
    } else if (textField == txtAddress) {
        isStartEditing = NO;
        [textField resignFirstResponder];
        area = NO;
        [self performSegueWithIdentifier:@"zipCode" sender:nil];
    } else if (textField == txtServiceArea) {
        [textField resignFirstResponder];
        area = YES;
        isStartEditing = NO;
        [self performSegueWithIdentifier:@"zipCode" sender:nil];
    } else {
        isStartEditing = isSelected = NO;
        [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == txtBusinessPhoneNumber) {
        NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        BOOL deleting = [newText length] < [textField.text length];
        
        NSString *stripppedNumber = [newText stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [newText length])];
        NSUInteger digits = [stripppedNumber length];
        strPhoneNumber = stripppedNumber;
        if (digits > 10)
            stripppedNumber = [stripppedNumber substringToIndex:10];
        
        UITextRange *selectedRange = [textField selectedTextRange];
        NSInteger oldLength = [textField.text length];
        
        if (digits == 0)
            textField.text = @"";
        else if (digits < 3 || (digits == 3 && deleting))
            textField.text = [NSString stringWithFormat:@"(%@", stripppedNumber];
        else if (digits < 6 || (digits == 6 && deleting))
            textField.text = [NSString stringWithFormat:@"(%@) %@", [stripppedNumber substringToIndex:3], [stripppedNumber substringFromIndex:3]];
        else
            textField.text = [NSString stringWithFormat:@"(%@) %@-%@", [stripppedNumber substringToIndex:3], [stripppedNumber substringWithRange:NSMakeRange(3, 3)], [stripppedNumber substringFromIndex:6]];
        
        UITextPosition *newPosition = [textField positionFromPosition:selectedRange.start offset:[textField.text length] - oldLength];
        UITextRange *newRange = [textField textRangeFromPosition:newPosition toPosition:newPosition];
        [textField setSelectedTextRange:newRange];
        
        return NO;
    } else if (textField == txtPrimaryService) {
        
        NSString *keyWord = [NSString stringWithFormat:@"%@%@",textField.text,string];
        
        if ([string isEqualToString:@""]) {
            if (keyWord.length > 0) {
                keyWord = [keyWord substringToIndex:[keyWord length] - 1];
            }
        } else if ([NSString stringWithFormat:@"%@%@", textField.text, string].length > 0) {
            keyWord = [NSString stringWithFormat:@"%@%@", textField.text, string];
        } else {
            keyWord = @"";
        }
        
        NSLog(@"KEYWORD:::...%@\nTEXTFIELD:::...%@\nSTRING:::...%@", keyWord, textField.text, string);
        
        [self connectionForServiceTypeList:keyWord];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self paddingForTextField:textField];
    [self shadowForTextField:textField];
    
    if (textField == txtPrimaryService) {
        
        if (isStartEditing) {
            if (isSelected) {
                constPickerHeight.constant = 0;
                CGRect frame = viewHeader.frame;
                frame.size.height = fScreenHeight;
                viewHeader.frame = frame;
                [tblSignUp scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
                tblServiceList.hidden = YES;
                [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            } /* else {
                [self connectionForServiceTypeList:txtPrimaryService.text];
                [txtPrimaryService becomeFirstResponder];
            } */
        } else {
            constPickerHeight.constant = 0;
            CGRect frame = viewHeader.frame;
            frame.size.height = fScreenHeight;
            viewHeader.frame = frame;
            [tblSignUp scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
            tblServiceList.hidden = YES;
            [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        }
        
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
//    isSelected = NO;
    
    if (textField == txtPrimaryService) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidShowNotification object:nil];
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {

    if (textField == txtPrimaryService) {
        
        constPickerHeight.constant = 0;
        CGRect frame = viewHeader.frame;
        frame.size.height = fScreenHeight;
        viewHeader.frame = frame;
        
        tblServiceList.hidden = YES;
        
//        if (isStartEditing) {
//            if (isSelected) {
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
//            } else {
//                [txtPrimaryService becomeFirstResponder];
//            }
//        } else {
//            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
//        }
    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    }
    return YES;
}

// 875081994

- (void)keyboardDidShow:(NSNotification *)notification {
    // Assign new frame to your view
//    [self.view setFrame:CGRectMake(0, -170, self.view.frame.size.width, self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
//    [self.view endEditing:NO];
}

-(void)keyboardDidHide:(NSNotification *)notification {
    [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    [self.view endEditing:NO];
}

- (void)scrollToBottom {
    
    CGFloat yOffset = 0;
    
    if (tblSignUp.contentSize.height > tblSignUp.bounds.size.height) {
        yOffset = tblSignUp.contentSize.height - tblSignUp.bounds.size.height;
    }
    [tblSignUp setContentOffset:CGPointMake(0, yOffset) animated:NO];
}

#pragma mark - UITextField Validation
-(BOOL)validateTextField {
    
    NSString *strAlert = @"";
    BOOL isValid = YES;
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtBusinessName.text]) {
        [txtBusinessName becomeFirstResponder];
        strAlert = @"Please enter business name";
        isValid = NO;
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtAddress.text]) {
        [txtAddress becomeFirstResponder];
        strAlert = @"Please enter address";
        isValid = NO;
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtBusinessPhoneNumber.text]) {
        [txtBusinessPhoneNumber becomeFirstResponder];
        strAlert = @"Please enter business phone number";
        isValid = NO;
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtEmail.text]) {
        [txtEmail becomeFirstResponder];
        strAlert = @"Please enter your business email address";
        isValid = NO;
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtPrimaryService.text]) {
        [txtPrimaryService becomeFirstResponder];
        strAlert = @"please select primary service";
        isValid = NO;
    } else if (dictService == nil || [dictService count] == 0 || [dictService isEqual:[NSNull null]] || [[dictService objectForKey:@"id"] isEqualToString:@""]) {
        [txtServiceArea becomeFirstResponder];
        strAlert = @"Please enter service area";
        isValid = NO;
    }
    
    if (txtBusinessPhoneNumber.text.length > 0) {
        if (![[AppManager sharedDataAccess] isValidPhoneNumber:strPhoneNumber]) {
            [txtBusinessPhoneNumber becomeFirstResponder];
            isValid = NO;
            strAlert = @"Please enter correct phone number";
        }
    }
    if (txtEmail.text.length > 0) {
        if (![[AppManager sharedDataAccess] validateEmailWithString:txtEmail.text]) {
            [txtEmail becomeFirstResponder];
            strAlert = @"Please enter a valid email";
            isValid = NO;
        }
    }
    
    if (strAlert.length > 0) {
        [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strAlert];
    }
    return isValid;
}

#pragma mark - UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrPrimaryServiceList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellPrimaryService";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.textLabel.text = [[arrPrimaryServiceList objectAtIndex:indexPath.row] objectForKey:@"category_name"];
    cell.textLabel.textColor = COLOR_DARK_BACK;
    
    return cell;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    isSelected = YES;
    
    txtPrimaryService.text = [[arrPrimaryServiceList objectAtIndex:indexPath.row] objectForKey:@"category_name"];
    
    dictService = [[NSMutableDictionary alloc] init];
    dictService = [[arrPrimaryServiceList objectAtIndex:indexPath.row] mutableCopy];
    
    if ([[dictService objectForKey:@"id"] length] > 0) {
        isSelected = YES;
        [txtPrimaryService resignFirstResponder];
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    txtPrimaryService.text = [[arrPrimaryServiceList objectAtIndex:indexPath.row] objectForKey:@"category_name"];
    
    dictService = [[NSMutableDictionary alloc] init];
    dictService = [[arrPrimaryServiceList objectAtIndex:indexPath.row] mutableCopy];
    
    if ([[dictService objectForKey:@"id"] length] > 0) {
        isSelected = YES;
        [txtPrimaryService resignFirstResponder];
    }
    
    return nil;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"zipCode"]) {
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        SearchZipVC *szvc = segue.destinationViewController;
        self.providesPresentationContextTransitionStyle = YES;
        self.definesPresentationContext = YES;
        szvc.deleagte = self;
        [szvc setModalPresentationStyle:UIModalPresentationCurrentContext];
        
        if (area) {
            szvc.strNavigationTitle = @"Service Area";
            szvc.strAddressTitle = @"City/Zip code";
            szvc.strPlaceHolder = @"City / zip";
            szvc.strApi = @"(regions)";
            szvc.strSearchText = @"postal_code";
            szvc.strSearchComponent = @"short_name";
        } else {
            szvc.strNavigationTitle = @"Business Address";
            szvc.strAddressTitle = @"Business Street Address";
            szvc.strPlaceHolder = @"Street address";
            szvc.strApi = @"geocode";
            szvc.strSearchText = @"postal_code";
            szvc.strRoute = @"route";
            szvc.strSearchComponent = @"short_name";
        }
    }
}

#pragma mark - Protocol Method
-(void)sendAddressToUserInfo:(NSDictionary *)dictAddress {
    
    NSArray *arrAddressComponent = [dictAddress objectForKey:@"address_components"];
    
    NSLog(@"Address::...%@", dictAddress);
    
    strZipCode = strCountry = strState = strCity = strStreet = @"";
    NSString *strStreetNumber;
    
    for (int i = 0; i < arrAddressComponent.count; i++) {
        NSDictionary *dictComponent = [arrAddressComponent objectAtIndex:i];
        NSArray *arrTypes = [dictComponent objectForKey:@"types"];
        for (int iTypes = 0; iTypes < arrTypes.count; iTypes++) {
            if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"postal_code"]) {
                strZipCode = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"country"]) {
                strCountry = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"administrative_area_level_1"]) {
                strState = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"locality"] || [[arrTypes objectAtIndex:iTypes] isEqualToString:@"sublocality"]) {
                strCity = [dictComponent objectForKey:@"long_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"route"]) {
                strStreet = [dictComponent objectForKey:@"long_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"street_number"]) {
                strStreetNumber = [dictComponent objectForKey:@"long_name"];
            } else if (strStreet.length == 0) {
                if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"administrative_area_level_2"]) {
                    strStreet = [dictComponent objectForKey:@"long_name"];
                }
            }
        }
    }
    
    NSLog(@"\npostal_code:::...%@\ncountry:::...%@\nstate:::...%@\ncity:::...%@\nstreet:::...%@", strZipCode, strCountry, strState, strCity, strStreet);
        
        if (strZipCode.length == 0 || strStreet.length == 0) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Enter a valid street address" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                ;
            }];
            
            [alertController addAction:okAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        } else {
            
            if (!area) {
                
                if (strStreetNumber.length > 0) {
                    strStreet = [NSString stringWithFormat:@"%@ %@", strStreetNumber, strStreet];
                }
                txtAddress.text = strStreet;
                txtCity.text = strCity;
                txtState.text = strState;
                txtZip.text = strZipCode;
                if (strCity.length > 0) {
                    txtServiceArea.text = [NSString stringWithFormat:@"%@, %@", strCity, strState];
                } else {
                    txtServiceArea.text = strState;
                }
                
                
                strLat = [[[[dictAddress objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"] stringValue];
                strLan = [[[[dictAddress objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"] stringValue];
                
                double latitude = [strLat doubleValue];
                double longitude = [strLan doubleValue];
                
                [self findTimeZoneFromSelectedAddressWithLat:latitude andLon:longitude];
            } else {
                txtServiceArea.text = [NSString stringWithFormat:@"%@, %@", strCity, strState];
            }
    }
}

-(void)findTimeZoneFromSelectedAddressWithLat:(double)latitude andLon:(double)longitude {
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        
        self->strTimeZone = placemark.timeZone.name;
    }];
}

#pragma mark - UIButton Action
- (IBAction)CompleteAccount:(id)sender {
    NSLog(@"SIGN UP DETAILS:::...\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@", strFirstName, strLastName, strProEmailId, strProPhoneNumber, strPassword, txtZip.text, txtCity.text, txtEmail.text, txtState.text, txtAddress.text, txtBusinessPhoneNumber.text, txtServiceArea.text, txtBusinessName.text, txtPrimaryService.text);
    
    if ([self validateTextField]) {
        
        [self connectionForSignUp];
        
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Tabbar" bundle:nil];
//        TabBarVC *tbvc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarVC"];
//        [self.navigationController pushViewController:tbvc animated:YES];
    }
}

#pragma mark - Web Service
-(void)connectionForSignUp {
    [YXSpritesLoadingView show];
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_SIGN_UP];
    
    NSDictionary *params = @{@"contact_f_name": strFirstName,
                            @"contact_l_name": strLastName,
                            @"pro_email": strProEmailId,
                            @"pro_phone": strProPhoneNumber,
                            @"pro_password": strPassword,
                            @"com_name": txtBusinessName.text,
                            @"com_address": txtAddress.text,
                            @"city": txtCity.text,
                            @"state": txtState.text,
                            @"zipcode": txtZip.text,
                            @"country": strCountry,
                            @"alt_phone": txtBusinessPhoneNumber.text,
                            @"com_email": txtEmail.text,
                            @"primary_category": [dictService objectForKey:@"id"],
                            @"service_area": txtServiceArea.text,
                            @"latitude": strLat,
                            @"longitude": strLan,
                            @"device_type": @"I",
                            @"user_timezone": strTimeZone
                        };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];

    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        NSLog(@"RESPONSE:::...%@", response);
        [YXSpritesLoadingView dismiss];
        if ([[response objectForKey:@"response"] boolValue] == YES) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success" message:[response objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                SignUpCompleteVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SignUpCompleteVC"];
                svc.strEmail = self->strProEmailId;
                [self.navigationController pushViewController:svc animated:YES];
                
            }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        } else {
            
            [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:[response objectForKey:@"message"]];
        }
    }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              [YXSpritesLoadingView dismiss];
              NSLog(@"ERROR:::...%@", task.error);
          }];
}

-(void)connectionForServiceTypeList:(NSString *)keyWord {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_CATEGORY_LIST];
    
    NSDictionary *param = @{@"category_search": keyWord};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
//        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [self->arrPrimaryServiceList removeAllObjects];
            [self->arrPrimaryServiceList addObjectsFromArray:[responseObject objectForKey:@"info_array"]];
//            [self->pickerServiceList reloadAllComponents];
            
            if (self->arrPrimaryServiceList.count > 0) {
//                self->constPickerHeight.constant = 150;
                CGRect frame = self->viewHeader.frame;
//                frame.size.height = 780;
                
                if (self->arrPrimaryServiceList.count == 1) {
                    self->constPickerHeight.constant = 50;
                } else if (self->arrPrimaryServiceList.count == 2) {
                    self->constPickerHeight.constant = 100;
                } else if (self->arrPrimaryServiceList.count >= 3) {
                    self->constPickerHeight.constant = 150;
                }
                
//                frame.size.height = self->fScreenHeight +self->constPickerHeight.constant;
                frame.size.height = self->viewHeader.frame.size.height +self->constPickerHeight.constant;
                self->viewHeader.frame = frame;
                self->isSelected = NO;
                self->isStartEditing = YES;
                self->tblServiceList.hidden = NO;
                
                [self.view setFrame:CGRectMake(0, -self->constPickerHeight.constant, self.view.frame.size.width, self.view.frame.size.height)];
//                [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                
                [self->tblServiceList reloadData];
            } else {
                self->isStartEditing = YES;
            }
            
//            for (int i = 0; i < self->arrPrimaryServiceList.count; i++) {
//                NSString *strTemp = [[self->arrPrimaryServiceList objectAtIndex:i] objectForKey:@"category_name"];
//                [self->arrTemp addObject:strTemp];
//            }
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

@end
