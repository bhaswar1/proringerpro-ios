//
//  SignUpCompleteVC.m
//  ProringerPro
//
//  Created by Soma Halder on 09/07/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "SignUpCompleteVC.h"

@interface SignUpCompleteVC ()

@end

@implementation SignUpCompleteVC
@synthesize lblEmail, lblContactUs, lblConfirmLink, lblResendRequest, strEmail, lblNote;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"";
    
    lblEmail.text = strEmail;
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName: COLOR_DARK_BACK,NSFontAttributeName: [UIFont fontWithName:@"OpenSans" size:13.0]};
    
    //CONFORM LINK
    NSString *strConfirmLink = @" Check your email and \n click the confirm link to \n activate your account.";
    NSDictionary *attributesConfLink = @{NSForegroundColorAttributeName: COLOR_DARK_BACK, NSFontAttributeName: [UIFont fontWithName:lblConfirmLink.font.fontName size:lblConfirmLink.font.pointSize]};
    lblConfirmLink.attributedText = [[NSAttributedString alloc] initWithString:strConfirmLink attributes:attributesConfLink];
    void(^handlerConfLink)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
        if ([substring isEqualToString:@"click the confirm link"]) {
            ;
        }
    };
    [lblConfirmLink setLinksForSubstrings:@[@"click the confirm link"] withLinkHandler:handlerConfLink];
    
    //REQUEST RESENT
    NSString *strResentRequest = @"If you do not received confirmation email within 30 minutes you can request it to be resent.";
    NSDictionary *attributesResentRequest = @{NSForegroundColorAttributeName: COLOR_DARK_BACK, NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote]};
    lblResendRequest.attributedText = [[NSAttributedString alloc] initWithString:strResentRequest attributes:attributesResentRequest];
    void(^handlerResentRequest)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
        if ([substring isEqualToString:@"request it to be resent"]) {
            [self navigateToRequestResent];
        }
    };
    [lblResendRequest setLinksForSubstrings:@[@"request it to be resent"] withLinkHandler:handlerResentRequest];
    
    //CONTACT US
    NSString *strContactUs = @"If you already request your confirmation email to be reset and you have not received that email within 30 minutes and do not see it in your Spam/Junk folder please contact us and include a phone number.";
    
    lblContactUs.attributedText = [[NSAttributedString alloc] initWithString:strContactUs attributes:attributes];
    void(^handlerContactUs)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
        if ([substring isEqualToString:@"contact us"]) {
            [self navigateToContactUs];
        }
    };
    [lblContactUs setLinksForSubstrings:@[@"contact us"] withLinkHandler:handlerContactUs];
    
    //NOTE
    NSString *strNote = @"  If you are using Gmail, Yahoo or similar services you may have to log into that account via account (may not show up  in an email client) and check the spam folder from there.";
    lblNote.attributedText = [[NSAttributedString alloc] initWithString:strNote attributes:attributes];
//    void(^handlerNote)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
//        if ([substring isEqualToString:@"NOTE:"]) {
//        }
//    };
//    [lblNote setLinksForSubstrings:@[@"NOTE:"] withLinkHandler:handlerNote];
}

//-(void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//
//    self.navigationController.navigationBar.hidden = YES;
//}
//
//-(void)viewWillDisappear:(BOOL)animated {
//    self.navigationController.navigationBar.hidden = NO;
//    [super viewWillDisappear:animated];
//}

-(void)navigateToContactUs {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContactUsVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"ContactUsVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

-(void)navigateToRequestResent {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ResendConfVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"ResendConfVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

-(void)rightBarButtonPressed:(id)sender {
    [self loginProRinger];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Login:(id)sender {
    [self loginProRinger];
}

- (IBAction)RightBarButtonItem:(id)sender {
    [self loginProRinger];
}
@end
