//
//  SignUpCompleteVC.h
//  ProringerPro
//
//  Created by Soma Halder on 09/07/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SignUpCompleteVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UILabel *lblEmail;

@property (weak, nonatomic) IBOutlet FRHyperLabel *lblConfirmLink;
@property (weak, nonatomic) IBOutlet FRHyperLabel *lblResendRequest;
@property (weak, nonatomic) IBOutlet FRHyperLabel *lblContactUs;
@property (weak, nonatomic) IBOutlet FRHyperLabel *lblNote;

@property (strong, nonatomic) NSString *strEmail;

- (IBAction)Login:(id)sender;
- (IBAction)RightBarButtonItem:(id)sender;


@end

NS_ASSUME_NONNULL_END
