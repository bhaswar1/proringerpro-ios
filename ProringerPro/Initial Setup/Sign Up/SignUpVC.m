//
//  SignUpVC.m
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "SignUpVC.h"

@interface SignUpVC ()
{
    NSMutableArray *arrSignup;
    NSString *strPhoneNumber;
}

@end

@implementation SignUpVC
@synthesize txtFirstName, txtLastName, txtEmail, txtPhoneNumber, txtPassword, txtConfEmail, txtConfPassword, viewHeader, viewAccessory;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Create Pro Account";
    self.navigationController.navigationBar.hidden = NO;
    
    txtFirstName.delegate = self;
    txtLastName.delegate = self;
    txtEmail.delegate = self;
    txtPhoneNumber.delegate = self;
    txtPassword.delegate = self;
    txtConfPassword.delegate = self;
    txtEmail.delegate = self;
    txtConfEmail.delegate = self;
    
    [self paddingForTextField:txtFirstName];
    [self paddingForTextField:txtLastName];
    [self paddingForTextField:txtEmail];
    [self paddingForTextField:txtConfEmail];
    [self paddingForTextField:txtPhoneNumber];
    [self paddingForTextField:txtPassword];
    [self paddingForTextField:txtConfPassword];
    
    [self shadowForTextField:txtFirstName];
    [self shadowForTextField:txtLastName];
    [self shadowForTextField:txtEmail];
    [self shadowForTextField:txtConfEmail];
    [self shadowForTextField:txtPhoneNumber];
    [self shadowForTextField:txtPassword];
    [self shadowForTextField:txtConfPassword];
    
    txtFirstName.inputAccessoryView = viewAccessory;
    txtLastName.inputAccessoryView = viewAccessory;
    txtEmail.inputAccessoryView = viewAccessory;
    txtConfEmail.inputAccessoryView = viewAccessory;
    txtPhoneNumber.inputAccessoryView = viewAccessory;
    txtPassword.inputAccessoryView = viewAccessory;
    txtConfPassword.inputAccessoryView = viewAccessory;
    
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

//#pragma mark - UITextField Design
//-(void) paddingForTextField:(UITextField *)textField {
//    
//    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(30, 0, 10, 20)];
//    textField.leftView = paddingView;
//    textField.leftViewMode = UITextFieldViewModeAlways;
//    
//    textField.layer.masksToBounds = NO;
//    textField.layer.shadowOffset = CGSizeMake(0.0, 0.0);
//    textField.layer.shadowRadius = 1;
//    textField.layer.shadowOpacity = 0.2;
//    textField.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
//    
//    textField.layer.borderWidth = 1.0;
//    textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
//}
//
//-(void)shadowForTextField:(UITextField *)textfield {
//    
//    UIColor *color = [UIColor blackColor];
//    textfield.layer.shadowColor = [color CGColor];
//    textfield.layer.shadowRadius = 1.5f;
//    textfield.layer.shadowOpacity = 0.2;
//    textfield.layer.shadowOffset = CGSizeZero;
//    textfield.layer.masksToBounds = NO;
//}
//
#pragma mark - UITextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == txtPhoneNumber) {
        NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        BOOL deleting = [newText length] < [textField.text length];
        
        NSString *stripppedNumber = [newText stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [newText length])];
        NSUInteger digits = [stripppedNumber length];
        strPhoneNumber = stripppedNumber;
        if (digits > 10)
            stripppedNumber = [stripppedNumber substringToIndex:10];
        
        UITextRange *selectedRange = [textField selectedTextRange];
        NSInteger oldLength = [textField.text length];
        
        if (digits == 0)
            textField.text = @"";
        else if (digits < 3 || (digits == 3 && deleting))
            textField.text = [NSString stringWithFormat:@"(%@", stripppedNumber];
        else if (digits < 6 || (digits == 6 && deleting))
            textField.text = [NSString stringWithFormat:@"(%@) %@", [stripppedNumber substringToIndex:3], [stripppedNumber substringFromIndex:3]];
        else
            textField.text = [NSString stringWithFormat:@"(%@) %@-%@", [stripppedNumber substringToIndex:3], [stripppedNumber substringWithRange:NSMakeRange(3, 3)], [stripppedNumber substringFromIndex:6]];
        
        UITextPosition *newPosition = [textField positionFromPosition:selectedRange.start offset:[textField.text length] - oldLength];
        UITextRange *newRange = [textField textRangeFromPosition:newPosition toPosition:newPosition];
        [textField setSelectedTextRange:newRange];
        
        return NO;
    }
    return YES;
}

//-(void)textFieldDidBeginEditing:(UITextField *)textField {
//    
//    textField.layer.borderColor = COLOR_THEME.CGColor;
//    textField.layer.borderWidth=1.5f;
//    
//}
//
//- (void)textFieldDidEndEditing:(UITextField *)textField {
//    
//    [self paddingForTextField:textField];
//    [self shadowForTextField:textField];
//}
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    
//    [textField resignFirstResponder];
//    return YES;
//}

#pragma mark - UITextField Validation
-(BOOL)validateTextField {
    
    NSString *strAlert = @"";
    BOOL isValid = YES;
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtFirstName.text]) {
        [txtFirstName becomeFirstResponder];
        isValid = NO;
        strAlert = @"Please enter First Name";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtLastName.text]) {
        [txtLastName becomeFirstResponder];
        isValid = NO;
        strAlert = @"Please enter Last Name";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtEmail.text]) {
        [txtEmail becomeFirstResponder];
        strAlert = @"Please enter your email address";
        isValid = NO;
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtPhoneNumber.text]) {
        [txtPhoneNumber becomeFirstResponder];
        isValid = NO;
        strAlert = @"Please enter a phone number";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtPassword.text]) {
        [txtPassword becomeFirstResponder];
        strAlert = @"Please enter a password";
        isValid = NO;
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtConfPassword.text]) {
        [txtConfPassword becomeFirstResponder];
        strAlert = @"Please confirm your password";
        isValid = NO;
    }
    if (![self validateEmail:txtEmail.text]) {
        
        [txtEmail becomeFirstResponder];
        strAlert = @"Please enter a valid email.";
        isValid = NO;
    }
    /* else if (txtEmail.text.length == 0) {
      
        isValid = NO;
        strAlert = @"Please enter a valid email id";
    }
     else if ([[AppManager sharedDataAccess] validateEmailWithString:txtEmail.text]) {
        
        isValid = NO;
        strAlert = @"Please enter a valid email id";
    } */
    if (![txtConfEmail.text isEqualToString:txtEmail.text]) {
        [txtConfEmail becomeFirstResponder];
        isValid = NO;
        strAlert = @"The email addresses entered do not match. Please reconfirm your email address.";
    }
    if (![txtConfPassword.text isEqualToString:txtPassword.text]) {
        [txtConfPassword becomeFirstResponder];
        isValid = NO;
        strAlert = @"The passwords entered do not match. Please try again.";
    }
    
    if (txtPhoneNumber.text.length > 0) {
        if (![[AppManager sharedDataAccess] isValidPhoneNumber:strPhoneNumber]) {
            [txtPhoneNumber becomeFirstResponder];
            isValid = NO;
            strAlert = @"Please enter correct phone number";
        }
    }
    
    if (strAlert.length > 0) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strAlert];
    }
    return isValid;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIButton Action
- (IBAction)Next:(id)sender {
//    if ([self validateTextField]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CreateAccountVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"CreateAccountVC"];
        svc.strFirstName = txtFirstName.text;
        svc.strLastName = txtLastName.text;
        svc.strProEmailId = txtEmail.text;
        svc.strProPhoneNumber = txtPhoneNumber.text;
        svc.strPassword = txtPassword.text;
        svc.strProEmailId = txtEmail.text;
        [self.navigationController pushViewController:svc animated:YES];
//    }
}

- (IBAction)LoginButtonPressed:(id)sender {
    
    [self loginProRinger];
}
@end
