//
//  TermsAndConditionVC.m
//  ProringerPro
//
//  Created by Soma Halder on 13/07/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "TermsAndConditionVC.h"
#import "NSMutableAttributedString+Color.h"
#import "LTLinkTextView.h"

@interface TermsAndConditionVC ()

@end

@implementation TermsAndConditionVC
@synthesize strTitle, strApi, webView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"isLoggedIn"] boolValue] == YES) {
        [self connectionForGetDashboardData];
    }
    
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = strTitle;
    
    webView.hidden = YES;
    webView.delegate = self;
    
    if ([strTitle isEqualToString:@"Faq"]) {
        webView.hidden = NO;
        NSString *urlAddress = [NSString stringWithFormat:@"%@%@", ROOT_URL, strApi];
        NSURL *url = [NSURL URLWithString:urlAddress];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [webView loadRequest:requestObj];
        webView.scrollView.bounces = NO;
    } else {
        [self connectionForCreateWebView];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    if (self.iPremiumStatus == 0) {
        [self setUpBanner];
    }
}

-(void) setUpBanner {
    
    if ([strTitle isEqualToString:@"Faq"]) {
        if (self.arrAdvViewStatus.count > 0) {
            for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
                NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
                if ([[dict objectForKey:@"title"] isEqualToString:@"FAQ"]) {
                    if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                        [self addBannerViewToView:self.bannerView];
                    } else {
                        [self.bannerView removeFromSuperview];
                    }
                }
            }
        } else {
            [self.bannerView removeFromSuperview];
        }
    } else if ([strTitle isEqualToString:@"TERMS OF SERVICES"]) {
        if (self.arrAdvViewStatus.count > 0) {
            for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
                NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
                if ([[dict objectForKey:@"title"] isEqualToString:@"Terms & Conditions"]) {
                    if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                        [self addBannerViewToView:self.bannerView];
                    } else {
                        [self.bannerView removeFromSuperview];
                    }
                }
            }
        } else {
            [self.bannerView removeFromSuperview];
        }
    } else if ([strTitle isEqualToString:@"Privacy Policy"]) {
        if (self.arrAdvViewStatus.count > 0) {
            for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
                NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
                if ([[dict objectForKey:@"title"] isEqualToString:@"Privacy Policy"]) {
                    if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                        [self addBannerViewToView:self.bannerView];
                    } else {
                        [self.bannerView removeFromSuperview];
                    }
                }
            }
        } else {
            [self.bannerView removeFromSuperview];
        }
    }
}

#pragma mark - AD Banner Delegate
- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.bottomLayoutGuide
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:0],
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
    //    bannerView.layer.zPosition = -1;
    //    table_view.layer.zPosition = -2;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
    [YXSpritesLoadingView show];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [YXSpritesLoadingView dismiss];
}

-(void)connectionForCreateWebView {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, strApi];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            self.navigationItem.title = [responseObject objectForKey:@"page_title"];
            
            NSMutableString *htmlString = [NSMutableString stringWithString: @"<html><head><title></title></head><body style=\"background:transparent;\">"];
            
            //continue building the string
            [htmlString appendString:[responseObject valueForKey:@"page_content"]];
            [htmlString appendString:@"</body></html>"];
            
            //instantiate the web view
//            UIWebView *webView = [[UIWebView alloc] init];
            self->webView.frame = CGRectMake(0,110, self.view.frame.size.width, self.view.frame.size.height -110);
            self->webView.opaque = NO;
//            self->webView.hidden = NO;
            UILabel * lbl = [[UILabel alloc] init];
            lbl.frame = CGRectMake(10, 70, self.view.frame.size.width, 40);
            lbl.text = [responseObject valueForKey:@"Effective_date"];
//             lbl.backgroundColor = [UIColor redColor];
            [self.view addSubview:lbl];
            
            //make the background transparent
            [self->webView setBackgroundColor:[UIColor colorWithRed:243.0f/255 green:245.0f/255 blue:249.0f/255 alpha:1]];
            
            //pass the string to the webview
            [self->webView loadHTMLString:[htmlString description] baseURL:nil];
            
            UITextView *textView = [[UITextView alloc] init];
            textView.frame = CGRectMake(0,110, self.view.frame.size.width, self.view.frame.size.height -110);
            textView.backgroundColor = [UIColor clearColor];
            textView.opaque = NO;
            textView.editable = NO;
            textView.delegate = self;
            textView.userInteractionEnabled = YES;
//            textView.hidden = NO;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding] options: @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes: nil error: nil];
            
//            NSAttributedString* attrString = [[NSAttributedString alloc] initWithString:@"a clickable word" attributes:@{ @"myCustomTag" : @(YES) }];
//            [attributedString appendAttributedString:attrString];
            
            textView.attributedText = attributedString;
            
            FRHyperLabel *lblWeb = [[FRHyperLabel alloc] init];
            lblWeb.frame = CGRectMake(0,110, self.view.frame.size.width, self.view.frame.size.height -110);
            lblWeb.opaque = NO;
            lblWeb.hidden = YES;
            lblWeb.textAlignment = NSTextAlignmentLeft;
            lblWeb.numberOfLines = 0;
            lblWeb.attributedText = attributedString;
            
            void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring) {
                if ([substring isEqualToString:@"Changes to Terms."]) {
                    [self navigateToContactUs];
                }
            };
            [lblWeb setLinksForSubstrings:@[@"Changes to Terms."] withLinkHandler:handler];
            
            //add it to the subview
            [self.view addSubview:textView];
//            [self.view addSubview:lblWeb];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)navigateToContactUs {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContactUsVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"ContactUsVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange interaction:(UITextItemInteraction)interaction {
    NSLog(@"URL SCHEME:::...%@ \n and url:::...%@", URL.scheme, URL);
    if ([URL.absoluteString isEqualToString:@"https://www.proringer.com/contact"]) {
        // Launch View controller
        NSLog(@"Launch View controller");
        [self navigateToContactUs];
        return NO;
    }
    return YES;
}

- (void)textTapped:(UITapGestureRecognizer *)recognizer {
    
    UITextView *textView = (UITextView *)recognizer.view;
    
    // Location of the tap in text-container coordinates
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    // Find the character that's been tapped on
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:NULL];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        id value = [textView.attributedText attribute:@"myCustomTag" atIndex:characterIndex effectiveRange:&range];
        
        // Handle as required...
        
        NSLog(@"%@, %d, %d", value, range.location, range.length);
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
