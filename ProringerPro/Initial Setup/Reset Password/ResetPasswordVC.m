//
//  ResetPasswordVC.m
//  ProringerPro
//
//  Created by Soma Halder on 19/08/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ResetPasswordVC.h"

@interface ResetPasswordVC ()

@end

@implementation ResetPasswordVC
@synthesize txtPassword, txtResetCode, txtConfPassword, lblContactUs;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"RESET PASSWORD";
    self.navigationController.navigationBar.hidden = NO;
    
    [self paddingForTextField:txtConfPassword];
    [self paddingForTextField:txtResetCode];
    [self paddingForTextField:txtPassword];
    
    [self shadowForTextField:txtResetCode];
    [self shadowForTextField:txtPassword];
    [self shadowForTextField:txtConfPassword];
    
    NSString *strMessage = @"Having trouble loggin in? Contact Us for help";
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName: COLOR_TEXT, NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote]};
    lblContactUs.attributedText = [[NSAttributedString alloc]initWithString:strMessage attributes:attributes];
    
    void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
        if ([substring isEqualToString:@"Contact Us"]) {
            [self navigateToContactUs];
        }
    };
    
    //Step 3: Add link substrings
    [lblContactUs setLinksForSubstrings:@[@"Contact Us"] withLinkHandler:handler];
}

-(void)navigateToContactUs {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContactUsVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"ContactUsVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextField Validation
-(BOOL)validateTextField {
    
    BOOL isValid = YES;
    NSString *strMsg = @"";
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtResetCode.text]) {
        strMsg = @"please enter request code sent to email-address.";
        isValid = NO;
        [txtResetCode becomeFirstResponder];
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtPassword.text]) {
        strMsg = @"please enter new password";
        isValid = NO;
        [txtPassword becomeFirstResponder];
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtConfPassword.text]) {
        strMsg = @"please enter confirm password";
        isValid = NO;
        [txtConfPassword becomeFirstResponder];
    } else if (![txtConfPassword.text isEqualToString:txtPassword.text]) {
        strMsg = @"Password and Connfirm password doesn't matched";
        isValid = NO;
        [txtConfPassword becomeFirstResponder];
    }
    
    if (!isValid) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strMsg];
    }
    
    return isValid;
}

#pragma mark - UIButton Action
-(IBAction)Submit:(id)sender {
    if ([self validateTextField]) {
        [self connectionForResetPassword];
    }
}

#pragma mark - Web Service
-(void)connectionForResetPassword {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_RESET_PASSWORD];
    NSDictionary *params = @{@"user_reset_code": txtResetCode.text,
                             @"new_password": txtConfPassword.text
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        NSLog(@"RESPONSE:::...%@", response);
        [YXSpritesLoadingView dismiss];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[response objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

            for (UIViewController *controller in self.navigationController.viewControllers) {
                
                if ([[response objectForKey:@"response"] boolValue] == YES) {
                    
                    if ([controller isKindOfClass:[LoginVC class]]) {
                        [self.navigationController popToViewController:controller animated:YES];
                        return;
                    }
                }
            }
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        [alertController.view setTintColor:COLOR_THEME];
    }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              [YXSpritesLoadingView dismiss];
              NSLog(@"ERROR:::...%@", task.error);
          }];
}

@end
