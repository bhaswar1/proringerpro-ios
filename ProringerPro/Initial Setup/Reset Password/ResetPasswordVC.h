//
//  ResetPasswordVC.h
//  ProringerPro
//
//  Created by Soma Halder on 19/08/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ResetPasswordVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITextField *txtResetCode;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfPassword;

@property (weak, nonatomic) IBOutlet FRHyperLabel *lblContactUs;

-(IBAction)Submit:(id)sender;
@end

NS_ASSUME_NONNULL_END
