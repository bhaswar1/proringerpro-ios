//
//  ViewController.m
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ViewController.h"
#import "HomeVC.h"

@interface ViewController ()
{
    NSTimer *timer;
    AppDelegate *appDelegate;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.navigationController.navigationBar.hidden = YES;
    
//    [YXSpritesLoadingView show];
    
//    timer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(addTimmer) userInfo:nil repeats:YES];
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.arrKhistiList = [[NSMutableArray alloc] init];
    
    [self connectionForGetKhistiList];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSLog(@"SHOW ME THE BOOL VALUE:::...%@", [userDefault objectForKey:@"isLoggedIn"]);
    
    if ([[userDefault objectForKey:@"isLoggedIn"] boolValue] == YES) {
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"footer"];
        [self footerButtonTap:1];
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
//        DashboardVC *tbvc = [storyboard instantiateViewControllerWithIdentifier:@"DashboardVC"];
//        [self.navigationController pushViewController:tbvc animated:YES];
    } else {
        
        [self connectionForGetWelcomeList];
//        NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_HOME_SCREEN];
//        
//        [[AppManager sharedDataAccess] connectionForWebServiceForGETMethodWithParameters:nil andUrl:url];
    }
}

-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"isLoggedIn"] boolValue] == YES) {
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
//        DashboardVC *tbvc = [storyboard instantiateViewControllerWithIdentifier:@"DashboardVC"];
//        [self.navigationController pushViewController:tbvc animated:YES];
//        [self footerButtonTap:1];
//    } else {
    
//        [self connectionForGetWelcomeList];
//    }
//    timer = [NSTimer scheduledTimerWithTimeInterval:6.0 target:self selector:@selector(addTimmer) userInfo:nil repeats:YES];
}

-(void)viewWillDisappear:(BOOL)animated {
    [timer invalidate];
    [super viewWillDisappear:animated];
}

-(void)viewDidDisappear:(BOOL)animated {
    [timer invalidate];
}

#pragma mark CollectionView AutoScroll


- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

-(void)addTimmer {
    
    [timer invalidate];
    [YXSpritesLoadingView dismiss];
}

#pragma mark - Web Service
-(void) connectionForGetWelcomeList {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_HOME_SCREEN];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            HomeVC *wvc = [storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
            wvc.arrWelcome = [[NSMutableArray alloc] init];
            [wvc.arrWelcome removeAllObjects];
            [wvc.arrWelcome addObjectsFromArray:[responseObject objectForKey:@"info_array"]];
            
            [self.navigationController pushViewController:wvc animated:YES];
        } else {
            [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:@"Please check your internet connection"];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:@"Please check your internet connection"];
    }];
}

-(void)connectionForGetKhistiList {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_KHISTI_LIST];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [self->appDelegate.arrKhistiList removeAllObjects];
//            [self->appDelegate.arrKhistiList addObjectsFromArray:[[[responseObject objectForKey:@"info_array"] objectAtIndex:0] objectForKey:@"keywords"]];
            NSString *strKhisti = [NSString stringWithFormat:@"%@", [[[responseObject objectForKey:@"info_array"] objectAtIndex:0] objectForKey:@"keywords"]];
            [self->appDelegate.arrKhistiList addObjectsFromArray:[strKhisti componentsSeparatedByString:@","]];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

@end
