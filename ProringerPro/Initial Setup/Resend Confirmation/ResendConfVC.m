//
//  ResendConfVC.m
//  ProringerPro
//
//  Created by Soma Halder on 01/08/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ResendConfVC.h"

@interface ResendConfVC ()

@end

@implementation ResendConfVC
@synthesize txtAccountEmail, lblContactUs, lblFollowSteps;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Resend Confirmation";
    self.navigationController.navigationBar.hidden = NO;
    
    NSString *strMessage = @"Check your inbox and spam/junk mail folders. If you can not locate it, feel free to Contact Us";
    NSDictionary *attributes = @{NSForegroundColorAttributeName: COLOR_TEXT, NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote]};
    lblContactUs.attributedText = [[NSAttributedString alloc]initWithString:strMessage attributes:attributes];
    
    void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
        if ([substring isEqualToString:@"Contact Us"]) {
            [self navigateToContactUs];
        }
    };
    
    //Step 3: Add link substrings
    [lblContactUs setLinksForSubstrings:@[@"Contact Us"] withLinkHandler:handler];
    
    NSString *strFollowSteps = @"If you have not received the confirmation email within 30 minutes please follow these steps.";
    lblFollowSteps.attributedText = [[NSAttributedString alloc] initWithString:strFollowSteps attributes:attributes];
    void(^handlerFollowSteps)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
        if ([substring isEqualToString:@"Contact Us"]) {
            [self navigateToContactUs];
        }
    };
    [lblFollowSteps setLinksForSubstrings:@[@"Contact Us"] withLinkHandler:handlerFollowSteps];
    
    txtAccountEmail.delegate = self;
    
    [self paddingForTextField:txtAccountEmail];
    [self shadowForTextField:txtAccountEmail];
}

-(void) navigateToContactUs {
    NSLog(@"Contact Us");
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContactUsVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"ContactUsVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

-(void)rightBarButtonPressed:(id)sender {
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}

-(BOOL)validateTextFields {
    
    BOOL isValid = YES;
    NSString *strMsg = @"";
    if (txtAccountEmail.text.length > 0) {
        if (![[AppManager sharedDataAccess] validateEmailWithString:txtAccountEmail.text]) {
            [txtAccountEmail becomeFirstResponder];
            strMsg = @"Please enter valid email";
            isValid = NO;
        }
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtAccountEmail.text]) {
        strMsg = @"Please enter email";
        [txtAccountEmail becomeFirstResponder];
        isValid = NO;
    }
    
    if (strMsg.length > 0) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strMsg];
    }
    return isValid;
}

-(void)connectionForResendEmail {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_RESEND_CONFIRMATION];
    
    NSDictionary *params = @{@"user_email": txtAccountEmail.text,
                             @"user_type": @"C"
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        //        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
        
        [[[UIAlertView alloc] initWithTitle:@"" message:[responseObject objectForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        //        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Submit:(id)sender {
    if ([self validateTextFields]) {
        [self connectionForResendEmail];
    }
}
@end
