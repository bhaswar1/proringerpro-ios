//
//  ResendConfVC.h
//  ProringerPro
//
//  Created by Soma Halder on 01/08/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ResendConfVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITextField *txtAccountEmail;
@property (weak, nonatomic) IBOutlet FRHyperLabel *lblContactUs;
@property (weak, nonatomic) IBOutlet FRHyperLabel *lblFollowSteps;


- (IBAction)Submit:(id)sender;

@end

NS_ASSUME_NONNULL_END
