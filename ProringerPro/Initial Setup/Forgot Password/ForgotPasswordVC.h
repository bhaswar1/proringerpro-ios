//
//  ForgotPasswordVC.h
//  ProringerPro
//
//  Created by Soma Halder on 19/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface ForgotPasswordVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;

- (IBAction)Submit:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnResendConfirmation;
@property (strong, nonatomic) IBOutlet UIView *viewAccessory;

- (IBAction)ResendConfirmation:(id)sender;

@end

NS_ASSUME_NONNULL_END
