//
//  ForgotPasswordVC.m
//  ProringerPro
//
//  Created by Soma Halder on 19/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ForgotPasswordVC.h"

@interface ForgotPasswordVC ()

@end

@implementation ForgotPasswordVC
@synthesize txtEmail, btnResendConfirmation, viewAccessory;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Forgot Password";
    self.navigationController.navigationBar.hidden = NO;
    
    [self paddingForTextField:txtEmail];
    [self shadowForTextField:txtEmail];
    
    txtEmail.inputAccessoryView = viewAccessory;
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

-(void)rightBarButtonPressed:(id)sender {
    
    NSLog(@"Right button pressed from Dashboard class");
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}

/*
#pragma mark - UITextField Design
-(void) paddingForTextField:(UITextField *)textField {
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(30, 0, 10, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    textField.layer.masksToBounds = NO;
    textField.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    textField.layer.shadowRadius = 1;
    textField.layer.shadowOpacity = 0.2;
    textField.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    
    textField.layer.borderWidth = 1.0;
    textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)shadowForTextField:(UITextField *)textfield {
    
    UIColor *color = [UIColor blackColor];
    textfield.layer.shadowColor = [color CGColor];
    textfield.layer.shadowRadius = 1.5f;
    textfield.layer.shadowOpacity = 0.2;
    textfield.layer.shadowOffset = CGSizeZero;
    textfield.layer.masksToBounds = NO;
}
*/
//#pragma mark - UITextField Delegate
//-(void)textFieldDidBeginEditing:(UITextField *)textField {
//    
//    textField.layer.borderColor = COLOR_THEME.CGColor;
//    textField.layer.borderWidth=1.5f;
//    
//}
//
//- (void)textFieldDidEndEditing:(UITextField *)textField {
//    
//    [self paddingForTextField:textField];
//    [self shadowForTextField:textField];
//}
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    
//    [textField resignFirstResponder];
//    return YES;
//}

#pragma mark - UITextField Validation
-(BOOL)validateTextField {
    
    NSString *strAlert = @"";
    BOOL isValid = YES;
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtEmail.text]) {
        [txtEmail becomeFirstResponder];
        strAlert = @"Please enter a valid account email";
        isValid = NO;
    } else if (txtEmail.text.length > 0) {
        if (![[AppManager sharedDataAccess] validateEmailWithString:txtEmail.text]) {
            [txtEmail becomeFirstResponder];
            strAlert = @"Please enter a valid email";
            isValid = NO;
        }
    }
    if (strAlert.length > 0) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strAlert];
    }
    return isValid;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Web Service
-(void)connectionForRequestPassword {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_FORGOT_PW];
    
    NSDictionary *params = @{@"user_email": txtEmail.text};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        NSLog(@"RESPONSE:::...%@", response);
        [YXSpritesLoadingView dismiss];
        
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[response objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    if ([[response objectForKey:@"response"] boolValue] == YES) {
                        
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        ResetPasswordVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"ResetPasswordVC"];
                        [self.navigationController pushViewController:svc animated:YES];
                    } else {
                        for (UIViewController *controller in self.navigationController.viewControllers) {
                        if ([controller isKindOfClass:[LoginVC class]]) {
                            [self.navigationController popToViewController:controller animated:YES];
                            return;
                        }
                    }
                }
            }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
    }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              [YXSpritesLoadingView dismiss];
              NSLog(@"ERROR:::...%@", task.error);
          }];
}

#pragma mark - UIButton Action
- (IBAction)Submit:(id)sender {
    
    if ([self validateTextField]) {
        [self connectionForRequestPassword];
    }
}

- (IBAction)ResendConfirmation:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ResendConfVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"ResendConfVC"];
    [self.navigationController pushViewController:svc animated:YES];
}
@end
