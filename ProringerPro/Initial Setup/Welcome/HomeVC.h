//
//  HomeVC.h
//  ProringerPro
//
//  Created by Soma Halder on 17/07/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeVC : ProRingerProBaseVC <UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) NSMutableArray *arrImage;
@property (strong, nonatomic) NSMutableArray *arrTitle;
@property (strong, nonatomic) NSMutableArray *arrWelcome;

@property (weak, nonatomic) IBOutlet UICollectionView *collImages;
@property (weak, nonatomic) IBOutlet UICollectionView *collTitle;

@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

@property (weak, nonatomic) IBOutlet UIPageControl *pageController;

- (IBAction)SignUpBusiness:(id)sender;
- (IBAction)Login:(id)sender;
- (IBAction)ChangeCurrentPage:(id)sender;
//- (IBAction)PageControllerButtonPressed:(id)sender;

@end

NS_ASSUME_NONNULL_END
