//
//  HomeVC.m
//  ProringerPro
//
//  Created by Soma Halder on 17/07/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "HomeVC.h"

@interface HomeVC ()
{
    NSInteger iCurrentPageIndex;
    NSTimer *timer;
    NSIndexPath *indexPath;
}

@end

@implementation HomeVC
@synthesize arrImage, arrTitle, collTitle, collImages, pageController, arrWelcome, btnLogin;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.hidden = YES;
    
//    NSLog(@"IMAGE ARRAY:::...%@\nTITLE ARRAY:::...%@\nWELCOME ARRAY:::...%@", arrImage, arrTitle, arrWelcome);
    
    collImages.dataSource = self;
    collImages.delegate = self;
    collTitle.dataSource = self;
    collTitle.delegate = self;
    collImages.pagingEnabled = YES;
    collTitle.pagingEnabled = YES;
    
    btnLogin.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    btnLogin.layer.shadowOffset = CGSizeMake(0, 3);
    btnLogin.layer.shadowOpacity = 1;
    btnLogin.layer.shadowRadius = 1.0;
    btnLogin.layer.masksToBounds = NO;
    
    pageController.transform = CGAffineTransformMakeScale(1.5, 1.5);
    pageController.currentPage = 0;
    iCurrentPageIndex = -1;
    
    if (arrWelcome.count == 0 || arrWelcome == nil) {
        arrWelcome = [[NSMutableArray alloc] init];
        [self connectionForGetWelcomeList];
    } else {
        timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(changeIndexPathOfUICollectionView) userInfo:nil repeats:NO];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
    
    if (arrWelcome.count == 0 || arrWelcome == nil) {
        arrWelcome = [[NSMutableArray alloc] init];
        [self connectionForGetWelcomeList];
    } else {
        timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(changeIndexPathOfUICollectionView) userInfo:nil repeats:NO];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self stopTimer];
    
    self.navigationController.navigationBar.hidden = NO;
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self stopTimer];
    
    self.navigationController.navigationBar.hidden = NO;
}

- (void) stopTimer {
    [timer invalidate];
    timer = nil;
}

#pragma mark - UICollectionView DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    pageController.numberOfPages = arrWelcome.count;
    return arrWelcome.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dictWelcome = [arrWelcome objectAtIndex:indexPath.item];
    
//    iCurrentPageIndex = indexPath.item;
    
    if (collectionView == collImages) {
        static NSString *cellIdentifier = @"cellImage";
        
        WelcomeCollectionViewCell *cell = (WelcomeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        [cell.imgWelcome sd_setImageWithURL:[NSURL URLWithString:[dictWelcome objectForKey:@"image"]]];
        
        return cell;
    } else if (collectionView == collTitle) {
        
        static NSString *cellIdentifier = @"cellTitle";
        
        WelcomeCollectionViewCell *cell = (WelcomeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
//        cell.contentView.backgroundColor = [UIColor cyanColor];
        cell.lblTitle.text = [dictWelcome objectForKey:@"title"];
        NSString *strDesc = [dictWelcome objectForKey:@"description"];
        cell.lblDescription.text = [strDesc stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
        
        return cell;
    } else
        return nil;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {

//    iCurrentPageIndex = indexPath.item;
}

#pragma mark -  CollectionView Delegate
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == collTitle) {
        UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout*)collTitle.collectionViewLayout;
        CGFloat availableWidthForCells = CGRectGetWidth(collTitle.frame) - flowLayout.sectionInset.left - flowLayout.sectionInset.right - flowLayout.minimumInteritemSpacing * (1 - 1);
        
        CGFloat cellWidth = availableWidthForCells / 1;
        
        flowLayout.itemSize = CGSizeMake(cellWidth, collTitle.frame.size.height);
        
        return flowLayout.itemSize;
    } else {
        
        return CGSizeMake(CGRectGetWidth(collectionView.frame), (CGRectGetHeight(collectionView.frame)));
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}

#pragma mark - Auto Scroll
-(void)changeIndexPathOfUICollectionView {
    
    if ((arrWelcome.count -1) > iCurrentPageIndex) {
        iCurrentPageIndex = iCurrentPageIndex +1;
        
    } else {
        indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        iCurrentPageIndex = 0;
    }
    
    indexPath = [NSIndexPath indexPathForItem:iCurrentPageIndex inSection:0];
    
    [collTitle scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    //    [collImages scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
    pageController.currentPage = iCurrentPageIndex;
}

#pragma mark - ScrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    NSInteger currentIndex = collTitle.contentOffset.x / collTitle.frame.size.width;
    
//    NSLog(@"CURRENT INDEX:::...%ld\n CURRENT PAGE:::...%ld", (long)currentIndex, (long)iCurrentPageIndex);
    
    iCurrentPageIndex = pageController.currentPage = currentIndex;
    
    indexPath = [NSIndexPath indexPathForItem:currentIndex inSection:0];
    
//    [collImages scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    [self collectionView:collTitle layout:flowLayout sizeForItemAtIndexPath:indexPath];
    
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(changeIndexPathOfUICollectionView) userInfo:nil repeats:NO];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {

    [collImages scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(changeIndexPathOfUICollectionView) userInfo:nil repeats:NO];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {

    [collImages scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(changeIndexPathOfUICollectionView) userInfo:nil repeats:NO];
}

#pragma mark - Web Service
-(void) connectionForGetWelcomeList {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_HOME_SCREEN];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [self->arrWelcome removeAllObjects];
            [self->arrWelcome addObjectsFromArray:[responseObject objectForKey:@"info_array"]];
            
            self->pageController.currentPage = 0;
            self->iCurrentPageIndex = -1;
            [self->collImages reloadData];
            [self->collTitle reloadData];
            self->timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(changeIndexPathOfUICollectionView) userInfo:nil repeats:YES];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIButton Action
- (IBAction)SignUpBusiness:(id)sender {
    
    [self stopTimer];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignUpVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SignUpVC"];
    [self.navigationController pushViewController:svc animated:YES];
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreateAccountVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"CreateAccountVC"];
//    [self.navigationController pushViewController:svc animated:YES];
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    SignUpCompleteVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SignUpCompleteVC"];
//    [self.navigationController pushViewController:svc animated:YES];
}

- (IBAction)Login:(id)sender {
    
    [self stopTimer];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoginVC *lvc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)ChangeCurrentPage:(id)sender {
    
    UIPageControl *pager = sender;
    NSInteger iPager = pager.currentPage;
    
    pageController.currentPage = iPager;
    
    indexPath = [NSIndexPath indexPathForItem:iPager inSection:0];
    
    [collTitle scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(changeIndexPathOfUICollectionView) userInfo:nil repeats:NO];
}

@end
