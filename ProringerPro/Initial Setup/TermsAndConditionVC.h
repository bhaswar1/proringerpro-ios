//
//  TermsAndConditionVC.h
//  ProringerPro
//
//  Created by Soma Halder on 13/07/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TermsAndConditionVC : ProRingerProBaseVC <UIWebViewDelegate>

@property (strong, nonatomic) NSString *strTitle;
@property (strong, nonatomic) NSString *strApi;

@property (weak, nonatomic) IBOutlet UIWebView *webView;


@end

NS_ASSUME_NONNULL_END
