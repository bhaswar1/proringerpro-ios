//
//  ContactUsVC.m
//  ProringerPro
//
//  Created by Soma Halder on 01/08/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ContactUsVC.h"

@interface ContactUsVC ()
{
    NSMutableArray *arrRestrictedWordsFound;
}

@end

@implementation ContactUsVC
@synthesize txtFirstName, txtLastName, txtEmail, txtPhoneNumber, txtComment, lblAddComment, viewAccessory, scrollView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Contact Us";
    self.navigationController.navigationBar.hidden = NO;
    
    arrRestrictedWordsFound = [[NSMutableArray alloc] init];
    
    txtFirstName.delegate = self;
    txtLastName.delegate = self;
    txtEmail.delegate = self;
    txtPhoneNumber.delegate = self;
    txtComment.delegate = self;
    
    [self paddingForTextField:txtFirstName];
    [self paddingForTextField:txtLastName];
    [self paddingForTextField:txtPhoneNumber];
    [self paddingForTextField:txtEmail];
    
    [self shadowForTextField:txtFirstName];
    [self shadowForTextField:txtLastName];
    [self shadowForTextField:txtEmail];
    [self shadowForTextField:txtPhoneNumber];
    
//    [self padingForTextView:txtComment];
    [self shadowForTextView:txtComment];
    
//    txtEmail.inputAccessoryView = txtComment.inputAccessoryView = txtLastName.inputAccessoryView = txtFirstName.inputAccessoryView = txtPhoneNumber.inputAccessoryView = viewAccessory;
    txtComment.inputAccessoryView = viewAccessory;
    
//    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];

}

#pragma mark - UITextView Design
-(void) padingForTextView:(UITextView *)textView {
    
    textView.contentInset = UIEdgeInsetsMake(3, 5, 10, 5);
}

#pragma mark - UITextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == txtPhoneNumber) {
        NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        BOOL deleting = [newText length] < [textField.text length];
        
        NSString *stripppedNumber = [newText stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [newText length])];
        NSUInteger digits = [stripppedNumber length];
        
        if (digits > 10)
            stripppedNumber = [stripppedNumber substringToIndex:10];
        
        UITextRange *selectedRange = [textField selectedTextRange];
        NSInteger oldLength = [textField.text length];
        
        if (digits == 0)
            textField.text = @"";
        else if (digits < 3 || (digits == 3 && deleting))
            textField.text = [NSString stringWithFormat:@"(%@", stripppedNumber];
        else if (digits < 6 || (digits == 6 && deleting))
            textField.text = [NSString stringWithFormat:@"(%@) %@", [stripppedNumber substringToIndex:3], [stripppedNumber substringFromIndex:3]];
        else
            textField.text = [NSString stringWithFormat:@"(%@) %@-%@", [stripppedNumber substringToIndex:3], [stripppedNumber substringWithRange:NSMakeRange(3, 3)], [stripppedNumber substringFromIndex:6]];
        
        UITextPosition *newPosition = [textField positionFromPosition:selectedRange.start offset:[textField.text length] - oldLength];
        UITextRange *newRange = [textField textRangeFromPosition:newPosition toPosition:newPosition];
        [textField setSelectedTextRange:newRange];
        
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITextView Delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
//    NSRange resultRange = [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet] options:NSBackwardsSearch];
//    if ([text length] == 1 && resultRange.location != NSNotFound) {
//        [textView resignFirstResponder];
//        return NO;
//    }
//    return YES;
    
    if([text isEqualToString:@""]) {
        
        NSLog(@"%@",textView.text);
        
        if (textView.text.length==1) {
            lblAddComment.hidden = NO;
        }
    } else if ([NSString stringWithFormat:@"%@%@",textView.text,text].length > 0) {
        
        lblAddComment.hidden = YES;
    } else {
        lblAddComment.hidden = NO;
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    [self paddingForTextView:txtComment];
    [self shadowForTextView:txtComment];
    
    [arrRestrictedWordsFound removeAllObjects];
    
    NSString *strText = [textView.text stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    NSArray *arrNewWords = [strText componentsSeparatedByString:@" "];
    NSLog(@"WORDS ARRAY FROM TEXTVIEW:::...%@", arrNewWords);
    arrRestrictedWordsFound = [self doArraysContainTheSameObjects:arrNewWords];
    
    if (![txtComment hasText]) {
        lblAddComment.hidden = NO;
    }
}

- (void) textViewDidChange:(UITextView *)textView {
    
    if(![textView hasText]) {
        lblAddComment.hidden = NO;
    }
    else{
        lblAddComment.hidden = YES;
    }
}

#pragma mark - Validate UITextField
-(BOOL)validateTextField {
    
    BOOL isValid = YES;
    
    NSString *strMsg = @"";
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtFirstName.text]) {
        [txtFirstName becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please enter first name";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtLastName.text]) {
        [txtLastName becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please enter last name";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtEmail.text]) {
        [txtEmail becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please enter valid email";
    } else if (![[AppManager sharedDataAccess] validateEmailWithString:txtEmail.text]) {
        [txtEmail becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please enter valid email";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtPhoneNumber.text]) {
        [txtPhoneNumber becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please enter phone number";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtComment.text]) {
        [txtComment becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please add a comment";
    } else if (txtPhoneNumber.text.length > 0 && txtPhoneNumber.text.length < 10) {
        [txtPhoneNumber becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please enter valid phone number";
    } else if (arrRestrictedWordsFound.count > 0) {
        NSString *strKhisti = [arrRestrictedWordsFound componentsJoinedByString:@","];
        strMsg = [NSString stringWithFormat:@"Invalid term: %@ - Please be respectful of others. Remove and try again", strKhisti];
        [txtComment becomeFirstResponder];
        isValid = NO;
    } else if ([[AppManager sharedDataAccess] detectType:txtComment.text]) {
        strMsg = self.strRestrictionMsg;
        isValid = NO;
    }
    
    if (strMsg.length > 0) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strMsg];
    }
    
    return isValid;
}

- (BOOL)validatePhone:(NSString *)phoneNumber {
    
//    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
//    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
//
//    return [phoneTest evaluateWithObject:phoneNumber];
    
    NSString *phoneRegex = @"[789][0-9]{9}";
    // OR below for advanced type
    //NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}

//-(BOOL) validateEmail:(NSString*) emailString {
//    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
//    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
//    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
//    NSLog(@"%lu", (unsigned long)regExMatches);
//    if (regExMatches == 0) {
//        return NO;
//    }
//    else
//        return YES;
//}

#pragma mark - WebService
-(void) connectionForContactAdmin {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_CONTACT_US];
    
    NSDictionary *params = @{@"fname": txtFirstName.text,
                             @"lname": txtLastName.text,
                             @"email": txtEmail.text,
                             @"phonenumber": txtPhoneNumber.text,
                             @"contact_info": txtComment.text};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)ContactUs:(id)sender {
    if ([self validateTextField]) {
        NSLog(@"text field validate");
        [self connectionForContactAdmin];
    }
}
@end
