//
//  ContactUsVC.h
//  ProringerPro
//
//  Created by Soma Halder on 01/08/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContactUsVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextView *txtComment;

@property (weak, nonatomic) IBOutlet UILabel *lblAddComment;

@property (strong, nonatomic) IBOutlet UIView *viewAccessory;

- (IBAction)ContactUs:(id)sender;

@end

NS_ASSUME_NONNULL_END
