//
//  TransactionHistoryVC.m
//  ProringerPro
//
//  Created by Soma Halder on 30/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "TransactionHistoryVC.h"

@interface TransactionHistoryVC ()
{
    int iStartForm, iNextData, iCampaign;
    
    NSMutableArray *arrTransactionHistory;
}

@end

@implementation TransactionHistoryVC
@synthesize lblCurrentCampaign, btnManageCampaign, btnManagePayment, tblTransactionHistory, description;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    self.navigationItem.title = @"TRANSACTION HISTORY";
    
//    self.tabBarController.tabBar.hidden = NO;
    
    iStartForm = iNextData = iCampaign = 0;
    
    tblTransactionHistory.dataSource = self;
    tblTransactionHistory.delegate = self;
    
    arrTransactionHistory = [[NSMutableArray alloc] init];
    
    [self connectionForGetTransactionHistory];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Transaction"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Transaction"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrTransactionHistory.count;
//    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellTransactionHistory";
    
    NSDictionary *dict = [arrTransactionHistory objectAtIndex:indexPath.row];
    
    TransactionHistoryTableViewCell *cell = (TransactionHistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell configureCellWith:dict];
    
    if (indexPath.row +1 == arrTransactionHistory.count) {
        if (iNextData == 1) {
            [self connectionForGetTransactionHistory];
        }
    }
    
    return cell;
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    if (indexPath.row +1 == arrTransactionHistory.count) {
//        [self connectionForGetTransactionHistory];
//    }
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60;
}
/*
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    // NSLog(@"offset: %f", offset.y);
    // NSLog(@"content.height: %f", size.height);
    // NSLog(@"bounds.height: %f", bounds.size.height);
    // NSLog(@"inset.top: %f", inset.top);
    // NSLog(@"inset.bottom: %f", inset.bottom);
    // NSLog(@"pos: %f of %f", y, h);
    
    float reload_distance = 10;
    if(y > h + reload_distance) {
        NSLog(@"load more rows");
        [self connectionForGetTransactionHistory];
    }
}
*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)connectionForGetTransactionHistory {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_TRANSACTION_GET];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"start_from":[NSString stringWithFormat:@"%d", iStartForm],
                             @"per_page": @"10"};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        NSLog(@"response from TRANSACTION HISTORY:::...%@", response);
        [YXSpritesLoadingView dismiss];
        if ([[response objectForKey:@"response"] boolValue] == YES) {
            
            self->lblCurrentCampaign.text = [[response objectForKey:@"info_array"] objectForKey:@"current_campaign"];
            if ([[[response objectForKey:@"info_array"] objectForKey:@"current_campaign_id"] isEqualToString:@""]) {
                [self->btnManageCampaign setTitle:@"GO PREMIUM" forState:UIControlStateNormal];
            } else {
                [self->btnManageCampaign setTitle:@"MANAGE CAMPAIGN" forState:UIControlStateNormal];
            }
            self->iCampaign = [[[response objectForKey:@"info_array"] objectForKey:@"current_campaign_id"] intValue];
            
            if (self->iStartForm == 0) {
                [self->arrTransactionHistory removeAllObjects];
                [self->arrTransactionHistory addObjectsFromArray:[[response objectForKey:@"info_array"] objectForKey:@"info"]];
            } else {
                [self->arrTransactionHistory addObject:[[response objectForKey:@"info_array"] objectForKey:@"info"]];
            }
            self->iNextData = [[[response objectForKey:@"info_array"] objectForKey:@"next_data"] intValue];
            if (self->iNextData == 1) {
                self->iStartForm = self->iStartForm + 10;
            }
            
            [self->tblTransactionHistory reloadData];
        }
    }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             NSLog(@"ERROR:::...%@", task.error);
             [YXSpritesLoadingView dismiss];
         }];
}

- (IBAction)ManageCampaign:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    
//    if (iCampaign == 0) {
//        PremiumVC *pvc = [storyboard instantiateViewControllerWithIdentifier:@"PremiumVC"];
//        [self.navigationController pushViewController:pvc animated:YES];
//    } else {
        CampaignSummaryVC *csvc = [storyboard instantiateViewControllerWithIdentifier:@"CampaignSummaryVC"];
        [self.navigationController pushViewController:csvc animated:YES];
//    }
}

- (IBAction)ManagePayment:(id)sender {
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
//    CampaignUpgradeVC *csvc = [storyboard instantiateViewControllerWithIdentifier:@"CampaignUpgradeVC"];
////    csvc.strPlanId = [[NSUserDefaults standardUserDefaults] objectForKey:@"plan_id"];
//    csvc.viewHeader.hidden = YES;
//    csvc.viewPaymentMethod.hidden = NO;
//    csvc.strPlanId = [NSString stringWithFormat:@"%d", iCampaign];
//    [self.navigationController pushViewController:csvc animated:YES];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    CampaignUpgradeVC *cuvc = (CampaignUpgradeVC *)[storyboard instantiateViewControllerWithIdentifier:@"CampaignUpgradeVC"];
    cuvc.isEditPayment = YES;
    cuvc.strPlanId = [self.dictInfoArray objectForKey:@"plan_id"];
    [self.navigationController pushViewController:cuvc animated:YES];
}

@end
