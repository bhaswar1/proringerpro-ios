//
//  TransactionHistoryTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 29/05/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "TransactionHistoryTableViewCell.h"

@implementation TransactionHistoryTableViewCell
@synthesize lblAmount, lblTransaction, lblTransactionDate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
/*
 {
 amount = "$40.00";
 date = "03/28/19";
 method = "Credit Card";
 transaction = "Premium Monthly";
 }
 */

-(void)configureCellWith:(NSDictionary *)dict {
    
    lblTransactionDate.text = [dict objectForKey:@"date"];
    lblTransaction.text = [dict objectForKey:@"transaction"];
    lblAmount.text = [dict objectForKey:@"amount"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
