//
//  TransactionHistoryVC.h
//  ProringerPro
//
//  Created by Soma Halder on 30/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TransactionHistoryVC : ProRingerProBaseVC <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblCurrentCampaign;

@property (weak, nonatomic) IBOutlet UIButton *btnManageCampaign;
@property (weak, nonatomic) IBOutlet UIButton *btnManagePayment;

@property (weak, nonatomic) IBOutlet UITableView *tblTransactionHistory;

- (IBAction)ManageCampaign:(id)sender;
- (IBAction)ManagePayment:(id)sender;

@end

NS_ASSUME_NONNULL_END
