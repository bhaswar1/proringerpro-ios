//
//  TransactionHistoryTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 29/05/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TransactionHistoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTransaction;
@property (weak, nonatomic) IBOutlet UILabel *lblTransactionDate;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount;

-(void)configureCellWith:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
