//
//  VerifyPendingVC.m
//  ProringerPro
//
//  Created by Soma Halder on 11/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "VerifyPendingVC.h"

@interface VerifyPendingVC ()

@end

@implementation VerifyPendingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self progressBarWidth];
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationController.navigationBar.backItem.hidesBackButton = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)GoToDashboard:(id)sender {
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        if ([controller isKindOfClass:[DashboardVC class]]) {
            
            [self.navigationController popToViewController:controller animated:YES];
            return;
        }
    }
}
@end
