//
//  VerifyPhoneVC.h
//  ProringerPro
//
//  Created by Soma Halder on 03/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTLinkTextView.h"

NS_ASSUME_NONNULL_BEGIN

@interface VerifyPhoneVC : ProRingerProBaseVC 

@property (weak, nonatomic) IBOutlet UIImageView *imgVerificationType;
@property (weak, nonatomic) IBOutlet FRHyperLabel *lblVerificationDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblVerifyTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNumber;

@property (weak, nonatomic) IBOutlet LTLinkTextView *viewLink;

@property (strong, nonatomic) NSString *strVerifyTitle, *strDetails, *strButtonTitle;
@property (assign, nonatomic) BOOL isCall;

@property (weak, nonatomic) IBOutlet UIButton *btnCallOrText;

- (IBAction)CallOrTextButtonPressed:(id)sender;

@end

NS_ASSUME_NONNULL_END
