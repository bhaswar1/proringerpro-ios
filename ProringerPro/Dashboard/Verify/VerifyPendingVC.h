//
//  VerifyPendingVC.h
//  ProringerPro
//
//  Created by Soma Halder on 11/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ProRingerProBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface VerifyPendingVC : ProRingerProBaseVC

- (IBAction)GoToDashboard:(id)sender;

@end

NS_ASSUME_NONNULL_END
