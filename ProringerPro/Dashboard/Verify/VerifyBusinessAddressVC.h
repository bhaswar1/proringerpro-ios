//
//  VerifyBusinessAddressVC.h
//  ProringerPro
//
//  Created by Soma Halder on 04/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VerifyBusinessAddressVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITextField *txtBusinessAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

- (IBAction)SubmitButtonPressed:(id)sender;

@end

NS_ASSUME_NONNULL_END
