//
//  VerifyBusinessInfoVC.m
//  ProringerPro
//
//  Created by Soma Halder on 04/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "VerifyBusinessInfoVC.h"

@interface VerifyBusinessInfoVC ()
{
    BOOL isLegal, isNumber, isLicense, isInsured, isSubmitEnable, isAddNewLicense, isAddNewLegal, isAddNewInsured;
    NSData *dataLicensedCertified, *dataBusinessFiled, *dataBusinessInsured;
    UIImage *chosenImage;
    int iTag;
}

@end

@implementation VerifyBusinessInfoVC
@synthesize txtCbnNumber, btnSubmit, btnNoNumber, btnNoInsured, btnNoLicense, btnYesNumber, btnYesInsured, btnYesLicense, btnNoLegalName, btnYesLegalName, btnDocumentLegal, btnDocumentInsured, btnDocumentLicence, imgLicense, constLicense, constRemoveLicense, constHeaderHeight, constLegalHeight, constInsuredHeight, constRemoveLegalWidth, constRemoveInsureWidth, imgLegal, imgInsured;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self progressBarWidth];
    
    txtCbnNumber.delegate = self;
    
    iTag = 0;
    
    [self paddingForTextField:txtCbnNumber];
    [self shadowForTextField:txtCbnNumber];
    
    constLicense.constant = constRemoveLicense.constant = constRemoveInsureWidth.constant = constRemoveLegalWidth.constant = constInsuredHeight.constant = constLegalHeight.constant = 0.0;
    constHeaderHeight.constant = btnDocumentInsured.frame.origin.y + 60;
    
    NSLog(@"SHOW ME THE VIEW HEIGHT :::...%f", constHeaderHeight.constant);
    
    isAddNewLicense = isAddNewLegal = isAddNewInsured = isLegal = isNumber = isLicense = isInsured = YES;
    
//    [btnNoLegalName setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
//    [btnNoLicense setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
//    [btnNoInsured setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
//    [btnNoNumber setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
//
//    [btnYesLegalName setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
//    [btnYesLicense setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
//    [btnYesInsured setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
//    [btnYesNumber setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
    
    [btnYesLegalName setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
    [btnNoLegalName setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
    
    [btnDocumentLegal setBackgroundColor:COLOR_THEME];
    [btnDocumentLegal setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDocumentLegal.layer.borderWidth = 0.0;
    btnDocumentLegal.userInteractionEnabled = YES;
    
    [btnYesNumber setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
    [btnNoNumber setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
    
    [btnYesLicense setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
    [btnNoLicense setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
    
    [btnDocumentLicence setBackgroundColor:COLOR_THEME];
    [btnDocumentLicence setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDocumentLicence.layer.borderWidth = 0.0;
    btnDocumentLicence.userInteractionEnabled = YES;
    
    [btnYesInsured setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
    [btnNoInsured setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
    
    [btnDocumentInsured setBackgroundColor:COLOR_THEME];
    [btnDocumentInsured setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDocumentInsured.layer.borderWidth = 0.0;
    btnDocumentInsured.userInteractionEnabled = YES;
    
    txtCbnNumber.userInteractionEnabled = YES;
    
    [self designForSubmitButton];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    [[NSUserDefaults standardUserDefaults] setInteger:5 forKey:@"progressWidth"];
    [self progressBarWidth];
}

-(BOOL)setDocumentButtonDesignNew {
    
    BOOL isActive = YES;
    
    if (isNumber) {
        if (txtCbnNumber.text.length < 10)
            isActive = NO;
    }
    
    if (isLegal) {
        if (dataBusinessFiled.length == 0)
            isActive = NO;
    }
    if (isLicense) {
        if (dataLicensedCertified.length == 0)
            isActive = NO;
    }
    if (isInsured) {
        if (dataBusinessInsured.length == 0)
            isActive = NO;
    }
    
    return isActive;
}

-(void)designForSubmitButton {
    /*
    if (isNumber) {
        if (txtCbnNumber.text.length > 9) {
            isSubmitEnable = YES;
        } else {
            isSubmitEnable = NO;
        }
    } else if (isLegal) {
        if (dataBusinessFiled.length > 0) {
            isSubmitEnable = YES;
        } else {
            isSubmitEnable = NO;
        }
    } else if (isInsured) {
        if (dataBusinessInsured.length > 0) {
            isSubmitEnable = YES;
        } else {
            isSubmitEnable = NO;
        }
    } else if (isLicense) {
        if (dataLicensedCertified.length > 0) {
            isSubmitEnable = YES;
        } else {
            isSubmitEnable = NO;
        }
    }
    */
    if ([self setDocumentButtonDesignNew]) {
        [btnSubmit setBackgroundColor:COLOR_THEME];
        [btnSubmit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnSubmit.layer.borderWidth = 0.0;
        btnSubmit.userInteractionEnabled = YES;
    } else {
        btnSubmit.layer.borderWidth = 1.0;
        btnSubmit.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
        [btnSubmit setTitleColor:COLOR_TABLE_HEADER forState:UIControlStateNormal];
        [btnSubmit setBackgroundColor:[UIColor clearColor]];
        btnSubmit.userInteractionEnabled = NO;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == txtCbnNumber) {
        NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        BOOL deleting = [newText length] < [textField.text length];
        
        NSString *stripppedNumber = [newText stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [newText length])];
        NSUInteger digits = [stripppedNumber length];
        
        if (digits > 9)
            stripppedNumber = [stripppedNumber substringToIndex:9];
        
        UITextRange *selectedRange = [textField selectedTextRange];
        NSInteger oldLength = [textField.text length];
        
        if (digits == 0)
            textField.text = @"";
        else if (digits < 2 || (digits == 2 && deleting))
            textField.text = [NSString stringWithFormat:@"%@", stripppedNumber];
        else if (digits < 6 || (digits == 6 && deleting))
            textField.text = [NSString stringWithFormat:@"%@-%@", [stripppedNumber substringToIndex:2], [stripppedNumber substringFromIndex:2]];
        else
            textField.text = [NSString stringWithFormat:@"%@-%@", [stripppedNumber substringToIndex:2], [stripppedNumber substringFromIndex:2]];
        
        UITextPosition *newPosition = [textField positionFromPosition:selectedRange.start offset:[textField.text length] - oldLength];
        UITextRange *newRange = [textField textRangeFromPosition:newPosition toPosition:newPosition];
        [textField setSelectedTextRange:newRange];
        
        return NO;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
//    if (textField.text.length == 9) {
//        isSubmitEnable = YES;
//    } else {
//        isSubmitEnable = NO;
//    }
    [self designForSubmitButton];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Web Service
-(void)connectionForUploadLicenseData {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_VERIFY_DOCUMENT];
    
    NSDictionary *params = @{@"user_id": [userDefault objectForKey:@"user_id"],
                             @"ein_no": txtCbnNumber.text,
//                             @"licensed_certified": @"",
//                             @"business_filed": @"",
//                             @"business_insured": @"",
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentType = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentType addObject:@"text/html"];
    [contentType addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentType;
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (self->dataLicensedCertified.length > 0) {
            [formData appendPartWithFileData:self->dataLicensedCertified
                                        name:@"licensed_certified"
                                    fileName:@"LicensedCertifiedImage.png"
                                    mimeType:@"image/png"];
        }
        if (self->dataBusinessInsured.length > 0) {
            [formData appendPartWithFileData:self->dataBusinessInsured
                                        name:@"business_insured"
                                    fileName:@"BusinessInsuredImage.png"
                                    mimeType:@"image/png"];
        }
        if (self->dataBusinessFiled.length > 0) {
            [formData appendPartWithFileData:self->dataBusinessFiled
                                        name:@"business_filed"
                                    fileName:@"BusinessFiledImage.png"
                                    mimeType:@"image/png"];
        }
        
    } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [YXSpritesLoadingView dismiss];
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [[NSUserDefaults standardUserDefaults] setInteger:5 forKey:@"progressWidth"];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
            VerifyBusinessAddressVC *hvc = [storyboard instantiateViewControllerWithIdentifier:@"VerifyBusinessAddressVC"];
            [self.navigationController pushViewController:hvc animated:YES];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", task.error);
        [YXSpritesLoadingView dismiss];
    }];
}

-(void)connectionForVerifyAddress {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSLog(@"USER ID FROM USERDEFAULT:::...%@", [userDefault objectForKey:@"user_id"]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_VERIFY_PINCODE];
    
    NSDictionary *params = @{@"user_id": [userDefault objectForKey:@"user_id"],
                             @"ein_no": txtCbnNumber.text
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE VERIFY PINCODE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            if (self->dataLicensedCertified.length > 0) {
                [self connectionForUploadLicenseData];
            }
        }
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}



#pragma mark - UIButton Action
- (IBAction)YesNumber:(id)sender {
    
    isNumber = YES;
    
    [btnYesNumber setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
    [btnNoNumber setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
    
    txtCbnNumber.userInteractionEnabled = YES;
    
    txtCbnNumber.text = @"";
    
    [self designForSubmitButton];
}

- (IBAction)NoNumber:(id)sender {
    
    isNumber = NO;
    
    [btnYesNumber setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
    [btnNoNumber setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
    
    txtCbnNumber.userInteractionEnabled = NO;
    
    txtCbnNumber.text = @"";
    
    [self designForSubmitButton];
}

- (IBAction)YesLicense:(id)sender {
    
    isLicense = YES;
    
    [btnYesLicense setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
    [btnNoLicense setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
    
    [btnDocumentLicence setBackgroundColor:COLOR_THEME];
    [btnDocumentLicence setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDocumentLicence.layer.borderWidth = 0.0;
    btnDocumentLicence.userInteractionEnabled = YES;
    
    dataLicensedCertified = [[NSData alloc] init];
    
    [self designForSubmitButton];
}

- (IBAction)NoLicense:(id)sender {
    
    isLicense = NO;
    
    [btnYesLicense setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
    [btnNoLicense setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
    
    btnDocumentLicence.layer.borderWidth = 1.0;
    btnDocumentLicence.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
    [btnDocumentLicence setTitleColor:COLOR_TABLE_HEADER forState:UIControlStateNormal];
    [btnDocumentLicence setBackgroundColor:[UIColor clearColor]];
    btnDocumentLicence.userInteractionEnabled = NO;
    
    [self designForSubmitButton];
}

- (IBAction)YesLegal:(id)sender {
    
    isLegal = YES;
    
    [btnYesLegalName setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
    [btnNoLegalName setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
    
    [btnDocumentLegal setBackgroundColor:COLOR_THEME];
    [btnDocumentLegal setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDocumentLegal.layer.borderWidth = 0.0;
    btnDocumentLegal.userInteractionEnabled = YES;
    
    dataBusinessFiled = [[NSData alloc] init];
    
    [self designForSubmitButton];
}

- (IBAction)NoLegal:(id)sender {
    
    isLegal = NO;
    
    [btnYesLegalName setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
    [btnNoLegalName setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
    
    btnDocumentLegal.layer.borderWidth = 1.0;
    btnDocumentLegal.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
    [btnDocumentLegal setTitleColor:COLOR_TABLE_HEADER forState:UIControlStateNormal];
    [btnDocumentLegal setBackgroundColor:[UIColor clearColor]];
    btnDocumentLegal.userInteractionEnabled = NO;
    
    [self designForSubmitButton];
}

- (IBAction)YesInsured:(id)sender {
    
    isInsured = YES;
    
    [btnYesInsured setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
    [btnNoInsured setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
    
    [btnDocumentInsured setBackgroundColor:COLOR_THEME];
    [btnDocumentInsured setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDocumentInsured.layer.borderWidth = 0.0;
    btnDocumentInsured.userInteractionEnabled = YES;
    
    dataBusinessInsured = [[NSData alloc] init];
    
    [self designForSubmitButton];
}

- (IBAction)NoInsured:(id)sender {
    
    isInsured = NO;
    
    [btnYesInsured setImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
    [btnNoInsured setImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
    
    btnDocumentInsured.layer.borderWidth = 1.0;
    btnDocumentInsured.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
    [btnDocumentInsured setTitleColor:COLOR_TABLE_HEADER forState:UIControlStateNormal];
    [btnDocumentInsured setBackgroundColor:[UIColor clearColor]];
    btnDocumentInsured.userInteractionEnabled = NO;
    
    [self designForSubmitButton];
}

- (IBAction)AddDocumentLicense:(id)sender {
    
    iTag = 1;
    
    [self openImagePicker];
}

- (IBAction)AddDocumentLegal:(id)sender {
    
    iTag = 2;
    
    [self openImagePicker];
}

- (IBAction)AddDocumentInsured:(id)sender {
    
    iTag = 3;
    
    [self openImagePicker];
}

- (IBAction)RemoveLicenseImage:(id)sender {
    
    dataLicensedCertified = [[NSData alloc] init];
    isAddNewLicense = YES;
    constLicense.constant = constRemoveLicense.constant = 0.0;
    constHeaderHeight.constant = constHeaderHeight.constant - 90;
    
    [self designForSubmitButton];
}

- (IBAction)RemoveLegalImage:(id)sender {
    
    dataBusinessFiled = [[NSData alloc] init];
    isAddNewLegal = YES;
    constLegalHeight.constant = constRemoveLegalWidth.constant = 0.0;
    constHeaderHeight.constant = constHeaderHeight.constant - 90;
    
    [self designForSubmitButton];
}

- (IBAction)RemoveInsuredImage:(id)sender {
    
    dataBusinessInsured = [[NSData alloc] init];
    isAddNewInsured = YES;
    constInsuredHeight.constant = constRemoveInsureWidth.constant = 0.0;
    constHeaderHeight.constant = constHeaderHeight.constant - 90;
    
    [self designForSubmitButton];
}

- (IBAction)SubmitButtonPressed:(id)sender {
    
    [self connectionForUploadLicenseData];
}

#pragma mark - UIImage Picker
-(void)openImagePicker {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"License image" message:@"Please choose image source type" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"CAMERA" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:nil];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Your device has no camera or camera is not supported" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }];
    UIAlertAction *photo = [UIAlertAction actionWithTitle:@"PHOTOS" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
    }];
    
    [alertController addAction:camera];
    [alertController addAction:photo];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
    alertController.view.tintColor = COLOR_THEME;
}

#pragma mark - UIImagePicker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    // output image
    chosenImage = info[UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [self openEditor];
    }];
}

-(void)openEditor {
    
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = chosenImage;
    controller.cropAspectRatio = self.view.frame.size.width / self.view.frame.size.height;
    controller.keepingCropAspectRatio = YES;
    controller.cropAspectRatio = 5.0f / 3.0f;
//    [controller setRotationEnabled:NO];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navigationController animated:NO completion:nil];
}

/*
-(void)openEditor {
    self.cropController.image = chosenImage;
    self.cropController.keepingCropAspectRatio = NO;
    self.cropController.cropAspectRatio = 1.0;

    CGFloat ratio = 3.0f / 4.0f;
    CGRect cropRect = self.cropController.cropView.cropRect;
    CGFloat widthCrop = CGRectGetWidth(cropRect);
    cropRect.size = CGSizeMake(widthCrop, widthCrop * ratio);
    self.cropController.cropView.cropRect = cropRect;

    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.cropController];
    
//    PECropViewController *controller = [[PECropViewController alloc] init];
//    controller.delegate = self;
//    controller.image = chosenImage;
//
//    UIImage *image = chosenImage;
//    CGFloat width = image.size.width;
//    CGFloat height = image.size.height;
//    CGFloat length = MIN(width, height);
//    controller.imageCropRect = CGRectMake((width - length) / 2,
//                                          (height - length) / 2,
//                                          length,
//                                          length);
//    [self.cropController clickedButtonAtIndex:4];
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];

    [self presentViewController:navigationController animated:YES completion:^{
        [self.cropController clickedButtonAtIndex:4];
    }];
}

- (UIImage *)cropImage:(UIImage *)image outPutRect:(CGRect) outputRect
{
    
    CGImageRef takenCGImage = image.CGImage;
    size_t width = CGImageGetWidth(takenCGImage);
    size_t height = CGImageGetHeight(takenCGImage);
    CGRect cropRect = CGRectMake(outputRect.origin.x * width, outputRect.origin.y * height,
                                 outputRect.size.width * width, outputRect.size.height * height);
    
    CGImageRef cropCGImage = CGImageCreateWithImageInRect(takenCGImage, cropRect);
    image = [UIImage imageWithCGImage:cropCGImage scale:1 orientation:image.imageOrientation];
    CGImageRelease(cropCGImage);
    
    return image;
}
*/
 
- (void)cancelButtonDidPush:(id)sender {
    
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - PECropViewControllerDelegate methods
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
    
    chosenImage = croppedImage;
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
    chosenImage = croppedImage;
    
    if (isLicense) {
        
        if (iTag == 1) {
            [self resizeImage:chosenImage];
            imgLicense.image = chosenImage;
            
            dataLicensedCertified = UIImagePNGRepresentation(chosenImage);
            dataLicensedCertified = [NSData dataWithData:dataLicensedCertified];
            
            constLicense.constant = 90.0;
            constRemoveLicense.constant = 20.0;
            if (isAddNewLicense) {
                constHeaderHeight.constant = constHeaderHeight.constant +90;
                isAddNewLicense = NO;
            }
        }
    }
    if (isLegal) {
        
        if (iTag == 2) {
            [self resizeImage:chosenImage];
            imgLegal.image = chosenImage;
            
            dataBusinessFiled = UIImagePNGRepresentation(chosenImage);
            dataBusinessFiled = [NSData dataWithData:dataBusinessFiled];
            
            constLegalHeight.constant = 90.0;
            constRemoveLegalWidth.constant = 20.0;
            if (isAddNewLegal) {
                constHeaderHeight.constant = constHeaderHeight.constant +90;
                isAddNewLegal = NO;
            }
        }
    }
    if (isInsured) {
        
        if (iTag == 3) {
            [self resizeImage:chosenImage];
            imgInsured.image = chosenImage;
            
            dataBusinessInsured = UIImagePNGRepresentation(chosenImage);
            dataBusinessInsured = [NSData dataWithData:dataBusinessInsured];
            
            constInsuredHeight.constant = 90.0;
            constRemoveInsureWidth.constant = 20.0;
            if (isAddNewInsured) {
                constHeaderHeight.constant = constHeaderHeight.constant +90;
                isAddNewInsured = NO;
            }
        }
    }
    [self designForSubmitButton];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:^{
//        self->imgLicence.image = self->chosenImage;
    }];
}

- (void)resizeImage:(UIImage*)image {
    
    NSData *finalData = nil;
    NSData *unscaledData = UIImagePNGRepresentation(image);
    
    if (unscaledData.length > 5000.0f ) {
        
        
        //if image size is greater than 5KB dividing its height and width maintaining proportions
        
        UIImage *scaledImage = [self imageWithImage:image andWidth:image.size.width/2 andHeight:image.size.height/2];
        finalData = UIImagePNGRepresentation(scaledImage);
        
        if (finalData.length > 500000.0f ) {
            
            [self resizeImage:scaledImage];
        } else {
            //scaled image will be your final image
//            dataLicenseImage = UIImagePNGRepresentation(scaledImage);
            chosenImage = scaledImage;
        }
    }
}

- (UIImage*)imageWithImage:(UIImage*)image andWidth:(CGFloat)width andHeight:(CGFloat)height {
    
    UIGraphicsBeginImageContext( CGSizeMake(width, height));
    [image drawInRect:CGRectMake(0,0,width,height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext() ;
    return newImage;
}

@end
