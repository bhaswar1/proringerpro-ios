//
//  VerifyMethodVC.h
//  ProringerPro
//
//  Created by Soma Halder on 03/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VerifyMethodVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UIImageView *imgSms;
@property (weak, nonatomic) IBOutlet UIImageView *imgCall;

@property (weak, nonatomic) IBOutlet UILabel *lblSms;
@property (weak, nonatomic) IBOutlet UILabel *lblCall;

- (IBAction)SmsButtonPressed:(id)sender;
- (IBAction)CallButtonPressed:(id)sender;

@end

NS_ASSUME_NONNULL_END
