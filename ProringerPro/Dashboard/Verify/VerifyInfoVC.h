//
//  VerifyInfoVC.h
//  ProringerPro
//
//  Created by Soma Halder on 30/09/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VerifyInfoVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UIButton *btnVerificationProcess;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckBox;


- (IBAction)CheckBoxButtonPressed:(id)sender;
- (IBAction)VerificationProcess:(id)sender;

@end

NS_ASSUME_NONNULL_END
