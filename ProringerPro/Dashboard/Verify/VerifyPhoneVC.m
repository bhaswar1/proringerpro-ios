//
//  VerifyPhoneVC.m
//  ProringerPro
//
//  Created by Soma Halder on 03/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "VerifyPhoneVC.h"
#import "LTLinkTextView.h"

@interface VerifyPhoneVC () <LTLinkTextViewDelegate>
{
    NSString *strPhoneNumber;
    BOOL isChecked;
}

@end

@implementation VerifyPhoneVC
@synthesize lblVerificationDetails, imgVerificationType, txtPhoneNumber, strVerifyTitle, strDetails, isCall, lblVerifyTitle, strButtonTitle, btnCallOrText, viewLink;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitleAndImage];
    
    txtPhoneNumber.delegate = self;
    [self shadowForTextField:txtPhoneNumber];
    [self paddingForTextField:txtPhoneNumber];
    
    btnCallOrText.layer.borderWidth = 1.0;
    btnCallOrText.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
    [btnCallOrText setTitleColor:COLOR_TABLE_HEADER forState:UIControlStateNormal];
    [btnCallOrText setBackgroundColor:[UIColor clearColor]];
    btnCallOrText.userInteractionEnabled = NO;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"progressWidth"];
    [self progressBarWidth];
}

-(void)textMessage {
    
    isCall = NO;
    [self setTitleAndImage];
}

-(void)phoneCall {
    
    isCall = YES;
    [self setTitleAndImage];
}

-(void) setTitleAndImage {
    
    if (isCall) {
        imgVerificationType.image = [UIImage imageNamed:@"Call_Selected"];
        lblVerifyTitle.text = @"Verify via Phone Call";
        lblVerificationDetails.text = strDetails = @"You will receive a phone call at the number you enter below. If you want to verify with a text message you should select verify by Text Message.";
        [btnCallOrText setTitle:@"CALL NOW" forState:UIControlStateNormal];
    } else {
        imgVerificationType.image = [UIImage imageNamed:@"Sms_Selected"];
        lblVerifyTitle.text = @"Verify with a Text Message";
        lblVerificationDetails.text = strDetails = @"You will receive a text message at the number you enter below. If you want to verify a land line you should select verify by Phone Call.";
        [btnCallOrText setTitle:@"SEND TEXT" forState:UIControlStateNormal];
    }
    
    [self setupLinkViewForPhoneCall];
}

-(void)setupLinkViewForPhoneCall {
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    UIColor *highlightedColor = COLOR_THEME;
    UIFont *customFont = [UIFont fontWithName:@"OpenSans" size:13.0];
    
    NSDictionary *textAttributes = @{LTTextStringParameterKey : NSLocalizedString(strDetails, @""),
                                     NSParagraphStyleAttributeName : paragraphStyle,
                                     NSFontAttributeName : customFont,
                                     NSForegroundColorAttributeName : [UIColor grayColor]};
    
    if (isCall) {
        NSArray *buttonsAttributes = @[@{LTTextStringParameterKey : NSLocalizedString(@"Text Message.", @""),
                                         NSFontAttributeName : customFont,
                                         NSUnderlineStyleAttributeName : @(NSUnderlineStyleNone),
                                         NSForegroundColorAttributeName : highlightedColor
                                         },
                                       
                                       ];
        [viewLink setStringAttributes:textAttributes withButtonsStringsAttributes:buttonsAttributes];
    } else {
        NSArray *buttonsAttributes = @[@{LTTextStringParameterKey : NSLocalizedString(@"Phone Call.", @""),
                                         NSFontAttributeName : customFont,
                                         NSUnderlineStyleAttributeName : @(NSUnderlineStyleNone),
                                         NSForegroundColorAttributeName : highlightedColor
                                         },
                                       
                                       ];
        [viewLink setStringAttributes:textAttributes withButtonsStringsAttributes:buttonsAttributes];
    }
    
    viewLink.delegate  = self;
    viewLink.tag = 6;
}

- (void)linkTextView:(LTLinkTextView*)termsTextView didSelectButtonWithIndex:(NSUInteger)buttonIndex title:(NSString*)title {
    
    if (isCall) {
        [self textMessage];
    } else {
        [self phoneCall];
    }
}

#pragma mark - UITextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    BOOL deleting = [newText length] < [textField.text length];
    
    NSString *stripppedNumber = [newText stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [newText length])];
    NSUInteger digits = [stripppedNumber length];
    strPhoneNumber = stripppedNumber;
    
    if (strPhoneNumber.length == 10) {
        [btnCallOrText setBackgroundColor:COLOR_THEME];
        [btnCallOrText setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnCallOrText.layer.borderWidth = 0.0;
        btnCallOrText.userInteractionEnabled = YES;
    } else if (strPhoneNumber.length > 10) {
        [self textFieldShouldReturn:txtPhoneNumber];
        return YES;
    } else {
        btnCallOrText.layer.borderWidth = 1.0;
        btnCallOrText.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
        [btnCallOrText setTitleColor:COLOR_TABLE_HEADER forState:UIControlStateNormal];
        [btnCallOrText setBackgroundColor:[UIColor clearColor]];
        btnCallOrText.userInteractionEnabled = NO;
    }
    
    if (digits > 10)
        stripppedNumber = [stripppedNumber substringToIndex:10];
    
    UITextRange *selectedRange = [textField selectedTextRange];
    NSInteger oldLength = [textField.text length];
    
    if (digits == 0)
        textField.text = @"";
    else if (digits < 3 || (digits == 3 && deleting))
        textField.text = [NSString stringWithFormat:@"(%@", stripppedNumber];
    else if (digits < 6 || (digits == 6 && deleting))
        textField.text = [NSString stringWithFormat:@"(%@) %@", [stripppedNumber substringToIndex:3], [stripppedNumber substringFromIndex:3]];
    else
        textField.text = [NSString stringWithFormat:@"(%@) %@-%@", [stripppedNumber substringToIndex:3], [stripppedNumber substringWithRange:NSMakeRange(3, 3)], [stripppedNumber substringFromIndex:6]];
    
    UITextPosition *newPosition = [textField positionFromPosition:selectedRange.start offset:[textField.text length] - oldLength];
    UITextRange *newRange = [textField textRangeFromPosition:newPosition toPosition:newPosition];
    [textField setSelectedTextRange:newRange];
    
    return NO;
}

#pragma mark - Web Service
-(void)connectionForVerifyPhone {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_PHONE_NUMBER];
    NSString *verificationType;
    if (isCall) {
        verificationType = @"call";
    } else {
        verificationType = @"msg";
    }
    
    NSLog(@"USER ID FROM STRING:::...%@", self.strUserId);
    NSLog(@"USER ID FROM USERDEFAULT:::...%@", [userDefault objectForKey:@"user_id"]);
    
    NSDictionary *param = @{@"user_id": [userDefault objectForKey:@"user_id"],
                            @"pros_ph_no": txtPhoneNumber.text,
                            @"verificatin_type": verificationType};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE VERIFY PHONE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"progressWidth"];
            
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
            VerifyPinVC *vpvc = [sb instantiateViewControllerWithIdentifier:@"VerifyPinVC"];
            vpvc.strPhoneNumber = self->txtPhoneNumber.text;
            vpvc.strVerificationType = verificationType;
            vpvc.isCall = self->isCall;
            [self.navigationController pushViewController:vpvc animated:YES];
        } else {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)CallOrTextButtonPressed:(id)sender {
    
    if (strPhoneNumber.length > 0) {
        [self connectionForVerifyPhone];
    }
}
@end
