//
//  VerifyPinVC.h
//  ProringerPro
//
//  Created by Soma Halder on 03/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VerifyPinVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITextField *txtPinCode;

@property (weak, nonatomic) IBOutlet UIButton *btnConform;
@property (weak, nonatomic) IBOutlet UIButton *btnResendPin;

@property (strong, nonatomic) NSString *strPhoneNumber, *strVerificationType;
@property (nonatomic, assign) BOOL isCall;

- (IBAction)ConfirmButtonPressed:(id)sender;
- (IBAction)ResendPinButtonPressed:(id)sender;
- (IBAction)RestartProcessButtonPressed:(id)sender;

@end

NS_ASSUME_NONNULL_END
