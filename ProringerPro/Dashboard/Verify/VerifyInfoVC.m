//
//  VerifyInfoVC.m
//  ProringerPro
//
//  Created by Soma Halder on 30/09/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "VerifyInfoVC.h"

@interface VerifyInfoVC ()
{
    BOOL isChecked;
    int iBarWidth;
}

@end

@implementation VerifyInfoVC
@synthesize imgCheckBox, btnVerificationProcess;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    isChecked = NO;
    btnVerificationProcess.layer.borderWidth = 1.0;
    btnVerificationProcess.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
    [btnVerificationProcess setTitleColor:COLOR_TABLE_HEADER forState:UIControlStateNormal];
    [btnVerificationProcess setBackgroundColor:[UIColor clearColor]];
    imgCheckBox.image = [UIImage imageNamed:@"CheckBoxEmpty"];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    isChecked = NO;
    btnVerificationProcess.layer.borderWidth = 1.0;
    btnVerificationProcess.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
    [btnVerificationProcess setTitleColor:COLOR_TABLE_HEADER forState:UIControlStateNormal];
    [btnVerificationProcess setBackgroundColor:[UIColor clearColor]];
    imgCheckBox.image = [UIImage imageNamed:@"CheckBoxEmpty"];
    
//    [[NSUserDefaults standardUserDefaults] setInteger:iBarWidth forKey:@"progressWidth"];
    [self progressBarWidth];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)CheckBoxButtonPressed:(id)sender {
    
    if (isChecked) {
        isChecked = NO;
        [btnVerificationProcess setBackgroundColor:COLOR_LIGHT_BACK];
        [btnVerificationProcess setTitleColor:COLOR_TABLE_HEADER forState:UIControlStateNormal];
        btnVerificationProcess.layer.borderWidth = 1.0;
        imgCheckBox.image = [UIImage imageNamed:@"CheckBoxEmpty"];
        iBarWidth = 0;
    } else {
        isChecked = YES;
        [btnVerificationProcess setBackgroundColor:COLOR_THEME];
        [btnVerificationProcess setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnVerificationProcess.layer.borderWidth = 0.0;
        imgCheckBox.image = [UIImage imageNamed:@"CheckBoxFull"];
        iBarWidth = 1;
    }
}

- (IBAction)VerificationProcess:(id)sender {
    
    if (isChecked) {
        
        self.viewProgress.hidden = self.lblProgress.hidden = NO;
//        self.lblProgress.frame = CGRectMake(0, 0, self.viewProgress.frame.size.width /5, self.viewProgress.frame.size.height);
        [[NSUserDefaults standardUserDefaults] setInteger:iBarWidth forKey:@"progressWidth"];

        [self progressBarWidth];
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
        VerifyMethodVC *vmvc = [sb instantiateViewControllerWithIdentifier:@"VerifyMethodVC"];
        [self.navigationController pushViewController:vmvc animated:YES];
    } else {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:@"Check the verify box to continue"];
    }
}
@end
