//
//  VerifyBusinessInfoVC.h
//  ProringerPro
//
//  Created by Soma Halder on 04/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VerifyBusinessInfoVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UIView *viewHeader;

@property (weak, nonatomic) IBOutlet UIButton *btnYesNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnNoNumber;

@property (weak, nonatomic) IBOutlet UIButton *btnYesLicense;
@property (weak, nonatomic) IBOutlet UIButton *btnNoLicense;

@property (weak, nonatomic) IBOutlet UIButton *btnYesLegalName;
@property (weak, nonatomic) IBOutlet UIButton *btnNoLegalName;

@property (weak, nonatomic) IBOutlet UIButton *btnYesInsured;
@property (weak, nonatomic) IBOutlet UIButton *btnNoInsured;

@property (weak, nonatomic) IBOutlet UITextField *txtCbnNumber;

@property (weak, nonatomic) IBOutlet UIButton *btnDocumentLicence;
@property (weak, nonatomic) IBOutlet UIButton *btnDocumentLegal;
@property (weak, nonatomic) IBOutlet UIButton *btnDocumentInsured;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property (weak, nonatomic) IBOutlet UIImageView *imgLicense;
@property (weak, nonatomic) IBOutlet UIImageView *imgLegal;
@property (weak, nonatomic) IBOutlet UIImageView *imgInsured;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constLicense;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constRemoveLicense;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constHeaderHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constLegalHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constRemoveLegalWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constInsuredHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constRemoveInsureWidth;

- (IBAction)YesNumber:(id)sender;
- (IBAction)NoNumber:(id)sender;
- (IBAction)YesLicense:(id)sender;
- (IBAction)NoLicense:(id)sender;
- (IBAction)YesLegal:(id)sender;
- (IBAction)NoLegal:(id)sender;
- (IBAction)YesInsured:(id)sender;
- (IBAction)NoInsured:(id)sender;

- (IBAction)AddDocumentLicense:(id)sender;
- (IBAction)AddDocumentLegal:(id)sender;
- (IBAction)AddDocumentInsured:(id)sender;

- (IBAction)RemoveLicenseImage:(id)sender;
- (IBAction)RemoveLegalImage:(id)sender;
- (IBAction)RemoveInsuredImage:(id)sender;

- (IBAction)SubmitButtonPressed:(id)sender;

@end

NS_ASSUME_NONNULL_END
