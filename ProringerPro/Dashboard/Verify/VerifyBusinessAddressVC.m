//
//  VerifyBusinessAddressVC.m
//  ProringerPro
//
//  Created by Soma Halder on 04/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "VerifyBusinessAddressVC.h"

@interface VerifyBusinessAddressVC ()
{
    NSString *strZipCode, *strCountry, *strState, *strCity, *strStreet, *strLat, *strLan, *strTimeZone;
}

@end

@implementation VerifyBusinessAddressVC
@synthesize txtBusinessAddress, btnSubmit;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    [YXSpritesLoadingView show];
    
    txtBusinessAddress.delegate = self;
    
    [self paddingForTextField:txtBusinessAddress];
    [self shadowForTextField:txtBusinessAddress];
    
    btnSubmit.layer.borderWidth = 1.0;
    btnSubmit.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
    [btnSubmit setTitleColor:COLOR_TABLE_HEADER forState:UIControlStateNormal];
    [btnSubmit setBackgroundColor:[UIColor clearColor]];
    btnSubmit.userInteractionEnabled = NO;
    
//    address, city, state zipcode, country_name
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    txtBusinessAddress.text = [NSString stringWithFormat:@"%@, %@, %@ %@, %@", [userDefault objectForKey:@"address"], [userDefault objectForKey:@"city"], [userDefault objectForKey:@"state"], [userDefault objectForKey:@"zipcode"], [userDefault objectForKey:@"country_name"]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    [[NSUserDefaults standardUserDefaults] setInteger:6 forKey:@"progressWidth"];
    [self progressBarWidth];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    strZipCode = [userDefault objectForKey:@"zipcode"];
    strStreet = [userDefault objectForKey:@"address"];
    strCity = [userDefault objectForKey:@"city"];
    strState = [userDefault objectForKey:@"state"];
    strCountry = [userDefault objectForKey:@"country_name"];
    strLat = [userDefault objectForKey:@"latitude"];
    strLan = [userDefault objectForKey:@"longitude"];
    
    txtBusinessAddress.text = [NSString stringWithFormat:@"%@, %@, %@ %@, %@", strStreet, strCity, strState, strZipCode, strCountry];
    
    double latitude = [strLat doubleValue];
    double longitude = [strLan doubleValue];
    
    [self findTimeZoneFromSelectedAddressWithLat:latitude andLon:longitude];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [textField resignFirstResponder];
    [self performSegueWithIdentifier:@"zipCode" sender:nil];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"zipCode"]) {
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        SearchZipVC *szvc = segue.destinationViewController;
        self.providesPresentationContextTransitionStyle = YES;
        self.definesPresentationContext = YES;
        szvc.deleagte = self;
        [szvc setModalPresentationStyle:UIModalPresentationCurrentContext];
        
        szvc.strNavigationTitle = @"Business Address";
        szvc.strAddressTitle = @"Business Street Address";
        szvc.strPlaceHolder = @"Street address";
        szvc.strApi = @"geocode";
        szvc.strSearchText = @"postal_code";
        szvc.strSearchComponent = @"short_name";
    }
}

#pragma mark - Protocol Method
-(void)sendAddressToUserInfo:(NSDictionary *)dictAddress {
    
    NSArray *arrAddressComponent = [dictAddress objectForKey:@"address_components"];
    
    NSLog(@"Address::...%@", dictAddress);
    
    strZipCode = strCountry = strState = strCity = strStreet = @"";
    NSString *strStreetNumber;
    
    for (int i = 0; i < arrAddressComponent.count; i++) {
        NSDictionary *dictComponent = [arrAddressComponent objectAtIndex:i];
        NSArray *arrTypes = [dictComponent objectForKey:@"types"];
        for (int iTypes = 0; iTypes < arrTypes.count; iTypes++) {
            if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"postal_code"]) {
                strZipCode = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"country"]) {
                strCountry = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"administrative_area_level_1"]) {
                strState = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"locality"] || [[arrTypes objectAtIndex:iTypes] isEqualToString:@"sublocality"]) {
                strCity = [dictComponent objectForKey:@"long_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"route"]) {
                strStreet = [dictComponent objectForKey:@"long_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"street_number"]) {
                strStreetNumber = [dictComponent objectForKey:@"long_name"];
            } else if (strStreet.length == 0) {
                if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"administrative_area_level_2"]) {
                    strStreet = [dictComponent objectForKey:@"long_name"];
                }
            }
        }
    }
    NSLog(@"\npostal_code:::...%@\ncountry:::...%@\nstate:::...%@\ncity:::...%@\nstreet:::...%@", strZipCode, strCountry, strState, strCity, strStreet);
    
    
    
    if (strZipCode.length == 0 || strStreet.length == 0) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Enter a valid street address" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            ;
        }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        
        if (strStreetNumber.length > 0) {
            strStreet = [NSString stringWithFormat:@"%@ %@", strStreetNumber, strStreet];
        }
        strLat = [[[[dictAddress objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"] stringValue];
        strLan = [[[[dictAddress objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"] stringValue];
        
        double latitude = [strLat doubleValue];
        double longitude = [strLan doubleValue];
        
        [self findTimeZoneFromSelectedAddressWithLat:latitude andLon:longitude];
        
        txtBusinessAddress.text = [dictAddress objectForKey:@"formatted_address"];
    }
}

-(void)findTimeZoneFromSelectedAddressWithLat:(double)latitude andLon:(double)longitude {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        
        self->strTimeZone = placemark.timeZone.name;
        if (self->strCountry.length == 0) {
            self->strCountry = placemark.ISOcountryCode;
//            self->txtBusinessAddress.text = [NSString stringWithFormat:@"%@, %@, %@ %@, %@", self->strStreet, self->strCity, self->strState, self->strZipCode, self->strCountry];
        }
        
        if (self->strCountry.length == 0) {
            self->strCountry = [placemark.addressDictionary objectForKey:@"CountryCode"];
            
        }
        
        if (self->strCountry.length > 0) {
            [userDefault setObject:self->strCountry forKey:@"country_name"];
            self->txtBusinessAddress.text = [NSString stringWithFormat:@"%@, %@, %@ %@, %@", self->strStreet, self->strCity, self->strState, self->strZipCode, self->strCountry];
        }
        
        [self designSubmitButton];
        
        [YXSpritesLoadingView dismiss];
    }];
}

-(void)designSubmitButton {
    
    [btnSubmit setBackgroundColor:COLOR_THEME];
    [btnSubmit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSubmit.layer.borderWidth = 0.0;
    btnSubmit.userInteractionEnabled = YES;
}

#pragma mark - Web Service
-(void)connectionForVerifyAddress {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_VERIFY_ADDRESS];
    
    NSDictionary *params = @{@"user_id": [userDefault objectForKey:@"user_id"],
                             @"verify_address": strStreet,
                             @"verify_country": strCountry,
                             @"verify_city": strCity,
                             @"verify_state": strState,
                             @"verify_zip": strZipCode,
                             @"verify_latitude": strLat,
                             @"verify_longitude": strLan};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE VERIFY ADDRESS::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [[NSUserDefaults standardUserDefaults] setInteger:6 forKey:@"progressWidth"];
            
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
            VerifyPendingVC *vpvc = [sb instantiateViewControllerWithIdentifier:@"VerifyPendingVC"];
            [self.navigationController pushViewController:vpvc animated:YES];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

- (IBAction)SubmitButtonPressed:(id)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please verify that all the information you have entered is correct and submit now." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *submit = [UIAlertAction actionWithTitle:@"SUBMIT" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (self->strCountry.length == 0) {
            [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:@"Please enter a valid street address"];
        } else {
            [self connectionForVerifyAddress];
        }
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:cancel];
    [alertController addAction:submit];
    [self presentViewController:alertController animated:YES completion:nil];
    alertController.view.tintColor = COLOR_THEME;
    
//    [self connectionForVerifyAddress];
}
@end
