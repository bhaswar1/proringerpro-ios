//
//  VerifyPinVC.m
//  ProringerPro
//
//  Created by Soma Halder on 03/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "VerifyPinVC.h"

@interface VerifyPinVC ()
{
    NSString *strPinCode;
    NSTimer *timer;
}

@end

@implementation VerifyPinVC
@synthesize strPhoneNumber, strVerificationType, btnConform, txtPinCode, isCall, btnResendPin;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self progressBarWidth];
    
    txtPinCode.delegate = self;
    
//    [self paddingForTextField:txtPinCode];
//    [self shadowForTextField:txtPinCode];
    
    txtPinCode.layer.borderWidth = 1.0;
    txtPinCode.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
    [txtPinCode setBackgroundColor:[UIColor whiteColor]];
    
    btnConform.layer.borderWidth = 1.0;
    btnConform.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
    [btnConform setTitleColor:COLOR_TABLE_HEADER forState:UIControlStateNormal];
    [btnConform setBackgroundColor:[UIColor clearColor]];
    btnConform.userInteractionEnabled = NO;
    
    btnResendPin.layer.borderWidth = 1.0;
    btnResendPin.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
    [btnResendPin setTitleColor:COLOR_TABLE_HEADER forState:UIControlStateNormal];
    [btnResendPin setBackgroundColor:[UIColor clearColor]];
    btnResendPin.userInteractionEnabled = NO;
    
    [NSTimer scheduledTimerWithTimeInterval:120.0
                                     target:self
                                   selector:@selector(targetMethod)
                                   userInfo:nil
                                    repeats:NO];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"progressWidth"];
    [self progressBarWidth];
}

-(void)targetMethod {
    
    btnResendPin.userInteractionEnabled = YES;
    [btnResendPin setBackgroundColor:COLOR_THEME];
    [btnResendPin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnResendPin.layer.borderWidth = 0.0;
}

#pragma mark - UITextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (newText.length == 4) {
        [btnConform setBackgroundColor:COLOR_THEME];
        [btnConform setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnConform.layer.borderWidth = 0.0;
        btnConform.userInteractionEnabled = YES;
    } else if (newText.length > 4) {
        [self textFieldShouldReturn:txtPinCode];
    } else {
        btnConform.layer.borderWidth = 1.0;
        btnConform.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
        [btnConform setTitleColor:COLOR_TABLE_HEADER forState:UIControlStateNormal];
        [btnConform setBackgroundColor:[UIColor clearColor]];
        btnConform.userInteractionEnabled = NO;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (textField.text.length == 4) {
        [btnConform setBackgroundColor:COLOR_THEME];
        [btnConform setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnConform.layer.borderWidth = 0.0;
        btnConform.userInteractionEnabled = YES;
    } else {
        btnConform.layer.borderWidth = 1.0;
        btnConform.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
        [btnConform setTitleColor:COLOR_TABLE_HEADER forState:UIControlStateNormal];
        [btnConform setBackgroundColor:[UIColor clearColor]];
        btnConform.userInteractionEnabled = NO;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Web Service
-(void)connectionForVerifyPin {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_VERIFY_PINCODE];
    
    NSDictionary *param = @{@"user_id": [userDefault objectForKey:@"user_id"],
                            @"verify_pin_code": txtPinCode.text};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE VERIFY PINCODE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 0) {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
            if ([[responseObject objectForKey:@"message"] isEqualToString:@"Invaild PIN"]) {
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                }];
                [alert addAction:okAction];
                
            } else {
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"CONTINUE" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
                    VerifyBusinessInfoVC *vbivc = [sb instantiateViewControllerWithIdentifier:@"VerifyBusinessInfoVC"];
                    [self.navigationController pushViewController:vbivc animated:YES];
                }];
                [alert addAction:okAction];
            }
            
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"progressWidth"];
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
            VerifyBusinessInfoVC *vbivc = [sb instantiateViewControllerWithIdentifier:@"VerifyBusinessInfoVC"];
            [self.navigationController pushViewController:vbivc animated:YES];
        }
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForVerifyPhone {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_PHONE_NUMBER];
    NSString *verificationType;
    if (isCall) {
        verificationType = @"call";
    } else {
        verificationType = @"msg";
    }
    
    NSLog(@"USER ID FROM USERDEFAULT:::...%@", [userDefault objectForKey:@"user_id"]);
    
    NSDictionary *param = @{@"user_id": [userDefault objectForKey:@"user_id"],
                            @"pros_ph_no": strPhoneNumber,
                            @"verificatin_type": verificationType};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE VERIFY PHONE::::....%@", responseObject);
        
//        NSString *str = [NSString stringWithFormat:@"Pin has been sent to %@", strPhoneNumber];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:[NSString stringWithFormat:@"Pin has been sent to %@", strPhoneNumber] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if ([[responseObject objectForKey:@"response"] intValue] == 1) {
                [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"progressWidth"];
                self->btnResendPin.layer.borderWidth = 1.0;
                self->btnResendPin.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
                [self->btnResendPin setTitleColor:COLOR_TABLE_HEADER forState:UIControlStateNormal];
                [self->btnResendPin setBackgroundColor:[UIColor clearColor]];
                self->btnResendPin.userInteractionEnabled = NO;
                
                [NSTimer scheduledTimerWithTimeInterval:120.0
                                                 target:self
                                               selector:@selector(targetMethod)
                                               userInfo:nil
                                                repeats:NO];
                [self progressBarWidth];
            }
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

#pragma mark - UIButton Action
- (IBAction)ConfirmButtonPressed:(id)sender {
    
    [self connectionForVerifyPin];
    /*
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    VerifyBusinessInfoVC *vbivc = [sb instantiateViewControllerWithIdentifier:@"VerifyBusinessInfoVC"];
    [self.navigationController pushViewController:vbivc animated:YES];
     */
}

- (IBAction)ResendPinButtonPressed:(id)sender {
    
    [self connectionForVerifyPhone];
}

- (IBAction)RestartProcessButtonPressed:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"progressWidth"];
    
    [self goToRestartProcessHome];
}

-(void) goToRestartProcessHome {
    
    UIViewController *controller;
    BOOL isFound = NO;
    
    for (controller in self.navigationController.viewControllers) {
        
        if ([controller isKindOfClass:[VerifyInfoVC class]]) {
            [self.navigationController popToViewController:controller animated:YES];
            isFound = YES;
            return;
        } else {
            isFound = NO;
        }
    }
    
    if (!isFound) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
        VerifyInfoVC *hvc = [storyboard instantiateViewControllerWithIdentifier:@"VerifyInfoVC"];
        
        CATransition* transition = [CATransition animation];
        transition.duration = 0.65;
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:hvc animated:NO];
    }
}

@end
