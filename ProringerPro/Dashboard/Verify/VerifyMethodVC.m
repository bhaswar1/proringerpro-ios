//
//  VerifyMethodVC.m
//  ProringerPro
//
//  Created by Soma Halder on 03/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "VerifyMethodVC.h"

@interface VerifyMethodVC ()
{
    BOOL isSms, isCall;
}

@end

@implementation VerifyMethodVC
@synthesize imgSms, imgCall, lblSms,lblCall;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.viewProgress.hidden = self.lblProgress.hidden = NO;
    [self progressBarWidth];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self progressBarWidth];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIButton Action
- (IBAction)SmsButtonPressed:(id)sender {
    
    isSms = YES;
    isCall = NO;
    
    imgSms.image = [UIImage imageNamed:@"Sms_Selected"];
    imgCall.image = [UIImage imageNamed:@"Call"];
    
    lblSms.textColor = COLOR_THEME;
    lblCall.textColor = [UIColor darkGrayColor];
    
    [self navigationToVerifyPhone];
}

- (IBAction)CallButtonPressed:(id)sender {
    
    isSms = NO;
    isCall = YES;
    
    imgSms.image = [UIImage imageNamed:@"Sms"];
    imgCall.image = [UIImage imageNamed:@"Call_Selected"];
    
    lblSms.textColor = [UIColor darkGrayColor];
    lblCall.textColor = COLOR_THEME;
    
    [self navigationToVerifyPhone];
}

-(void)navigationToVerifyPhone {
    
    [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"progressWidth"];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    VerifyPhoneVC *vpvc = [sb instantiateViewControllerWithIdentifier:@"VerifyPhoneVC"];
    vpvc.isCall = isCall;
    [self.navigationController pushViewController:vpvc animated:YES];
}

@end
