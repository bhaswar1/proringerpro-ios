//
//  SocialTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 29/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "SocialTableViewCell.h"

@implementation SocialTableViewCell
@synthesize txtSocialMedia, imgSocial;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    [self paddingForTextField:txtSocialMedia];
//    [self shadowForTextField:txtSocialMedia];
    
    [self padding:txtSocialMedia];
    [self shadow:txtSocialMedia];
}

-(void)shadow:(UIView *)subview
{
    UIColor *color = [UIColor blackColor];
    subview.layer.shadowColor = [color CGColor];
    subview.layer.shadowRadius = 1.5f;
    subview.layer.shadowOpacity = 0.2;
    subview.layer.shadowOffset = CGSizeZero;
    subview.layer.masksToBounds = NO;
}


-(void)padding:(UITextField *)textfield
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(20, 0, 45, 20)];
    textfield.leftView = paddingView;
    textfield.leftViewMode = UITextFieldViewModeAlways;
}


-(void)configureCellWith:(NSDictionary *)dict {
    
    txtSocialMedia.text = @"";
    
    imgSocial.image = [UIImage imageNamed:[dict objectForKey:@"image"]];
    
    if ([[dict objectForKey:@"text"] isEqualToString:@""]) {
        txtSocialMedia.placeholder = [dict objectForKey:@"placeHolder"];
    } else {
        txtSocialMedia.text = [dict objectForKey:@"text"];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
