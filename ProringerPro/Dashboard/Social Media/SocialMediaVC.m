//
//  SocialMediaVC.m
//  ProringerPro
//
//  Created by Soma Halder on 27/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "SocialMediaVC.h"

@interface SocialMediaVC ()
{
    NSMutableArray *arrSocial;
    
    NSString *strFacebook, *strTwitter, *strGoogle, *strLinkdin, *strYoutube, *strPinterest, *strInstagram, *strSkype, *strPaypal;
}

@end

@implementation SocialMediaVC
@synthesize tblSocial;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    self.navigationItem.title = @"SOCIAL MEDIA";
    
//    self.tabBarController.tabBar.hidden = NO;
    
    strFacebook = strTwitter = strGoogle = strLinkdin = strYoutube = strPinterest = strInstagram = strSkype = strPaypal = @"";
    
    arrSocial = [[NSMutableArray alloc] initWithObjects:
                 @{@"name": @"Facebook", @"image": @"Facebook", @"placeHolder": @"https://www.facebook.com/yourcompanyname", @"text": strFacebook},
                 @{@"name": @"Twitter", @"image": @"Twitter", @"placeHolder": @"https://www.twitter.com/youraccountname", @"text": strTwitter},
                 @{@"name": @"Google Plus", @"image": @"GooglePlus", @"placeHolder": @"https://www.google.com/yourcompanyname", @"text": strGoogle},
                 @{@"name": @"Linkedin", @"image": @"Linkdin", @"placeHolder": @"https://www.linkedin.com/compny/yourcompanyname", @"text": strLinkdin},
                 @{@"name": @"You Tube", @"image": @"Youtube", @"placeHolder": @"https://www.youtube.com/user/yourcompanyname", @"text": strYoutube},
                 @{@"name": @"Pinterest", @"image": @"Pinterest", @"placeHolder": @"https://www.pinterest.com/yourcompanyname", @"text": strPinterest},
                 @{@"name": @"Instagram", @"image": @"Instagram", @"placeHolder": @"https://www.instagram.com/yourcompanyname", @"text": strInstagram},
                 @{@"name": @"Skype", @"image": @"Skype", @"placeHolder": @"Skype Name", @"text": strSkype},
                 @{@"name": @"PayPal", @"image": @"Paypal", @"placeHolder": @"Paypal Link", @"text": strPaypal},nil];
    
    tblSocial.dataSource = self;
    tblSocial.delegate = self;
    
    [self connectionForGetSocialMediaList];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Social Media"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Social Media"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
}

#pragma mark - NavigationBar Button
-(void)rightBarButtonPressed:(id)sender {
    
    NSLog(@"Right button pressed from Dashboard class");
    [self tapToDismissView];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchProjectVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SearchProjectVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

#pragma mark - UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrSocial.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellSocial";
    
    NSDictionary *dictSocial = [arrSocial objectAtIndex:indexPath.row];
    
    SocialTableViewCell *cell = (SocialTableViewCell *)[tblSocial dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[SocialTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    [cell configureCellWith:dictSocial];
//    cell.txtSocialMedia.text = @"";
//    
//    
//    
//    cell.imgSocial.image = [UIImage imageNamed:[dictSocial objectForKey:@"image"]];
//    
//    if ([[dictSocial objectForKey:@"text"] isEqualToString:@""]) {
//        cell.txtSocialMedia.placeholder = [dictSocial objectForKey:@"placeHolder"];
//    } else {
//        cell.txtSocialMedia.text = [dictSocial objectForKey:@"text"];
//    }
    
    NSLog(@"ROW NUMBER:::...%lu", indexPath.row);
    
    cell.txtSocialMedia.delegate = self;
    cell.txtSocialMedia.tag = indexPath.row;
    [cell.txtSocialMedia addTarget:self action:@selector(textFieldDidEndEditing:) forControlEvents:UIControlEventTouchUpInside];
//    [cell.txtSocialMedia addTarget:self action:@selector(textFieldShouldReturn:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

//- (void)scrollViewWillBeginDragging:(UIScrollView *)activeScrollView
//{
////    tblSocial.dataSource = nil;
//}
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    tblSocial.dataSource = self;
//    [tblSocial reloadData];
//}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60;
}

#pragma mark - UITextField Delegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
//    tblSocial.dataSource = self;
    
    if (textField.tag == 0) {
        strFacebook = textField.text;
    } else if (textField.tag == 1) {
        strTwitter = textField.text;
    } else if (textField.tag == 2) {
        strGoogle = textField.text;
    } else if (textField.tag == 3) {
        strLinkdin = textField.text;
    } else if (textField.tag == 4) {
        strYoutube = textField.text;
    } else if (textField.tag == 5) {
        strPinterest = textField.text;
    } else if (textField.tag == 6) {
        strInstagram = textField.text;
    } else if (textField.tag == 7) {
        strSkype = textField.text;
    } else if (textField.tag == 8) {
        strPaypal = textField.text;
    }
//    [self reloadArray];
}

//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//
//    [textField resignFirstResponder];
//    return YES;
//}

#pragma mark - Web Service
-(void)connectionForGetSocialMediaList {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_GET_SOCIAL_MEDIA];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            self->strFacebook = [[responseObject objectForKey:@"info_array"] objectForKey:@"fb_link"];
            self->strTwitter = [[responseObject objectForKey:@"info_array"] objectForKey:@"twt_link"];
            self->strGoogle = [[responseObject objectForKey:@"info_array"] objectForKey:@"gp+_link"];
            self->strLinkdin = [[responseObject objectForKey:@"info_array"] objectForKey:@"lnkin_link"];
            self->strYoutube = [[responseObject objectForKey:@"info_array"] objectForKey:@"youtb_link"];
            self->strPinterest = [[responseObject objectForKey:@"info_array"] objectForKey:@"pintrst_link"];
            self->strInstagram = [[responseObject objectForKey:@"info_array"] objectForKey:@"instgm_link"];
            self->strSkype = [[responseObject objectForKey:@"info_array"] objectForKey:@"skype_link"];
            self->strPaypal = [[responseObject objectForKey:@"info_array"] objectForKey:@"paypal_link"];
            
            [self reloadArray];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForSaveSocialMedia {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_SAVE_SOCIAL_MEDIA];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"fblink": strFacebook,
                             @"twitterlink": strTwitter,
                             @"googlepluslink": strGoogle,
                             @"linkedinlink": strLinkdin,
                             @"youtubelink": strYoutube,
                             @"pinterestlink": strPinterest,
                             @"instagramlink": strInstagram,
                             @"skypelink": strSkype,
                             @"paypallink": strPaypal};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"ProRinger Pro" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [self.navigationController popViewControllerAnimated:YES];
            }];

            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            alertController.view.tintColor = COLOR_THEME;
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}


#pragma mark - Navigation
/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Configure Array
-(void)reloadArray {
    
//    [arrSocial removeAllObjects];
//
//    [arrSocial insertObject:@{@"name": @"Facebook", @"image": @"Facebook", @"placeHolder": @"https://www.facebook.com/yourcompanyname", @"text": strFacebook} atIndex:0];
//    [arrSocial insertObject:@{@"name": @"Twitter", @"image": @"Twitter", @"placeHolder": @"https://www.twitter.com/youraccountname", @"text": strTwitter} atIndex:1];
//    [arrSocial insertObject:@{@"name": @"Google Plus", @"image": @"GooglePlus", @"placeHolder": @"https://www.google.com/yourcompanyname", @"text": strGoogle} atIndex:2];
//    [arrSocial insertObject:@{@"name": @"Linkedin", @"image": @"Linkdin", @"placeHolder": @"https://www.linkedin.com/compny/yourcompanyname", @"text": strLinkdin} atIndex:3];
//    [arrSocial insertObject:@{@"name": @"You Tube", @"image": @"Youtube", @"placeHolder": @"https://www.youtube.com/user/yourcompanyname", @"text": strYoutube} atIndex:4];
//    [arrSocial insertObject:@{@"name": @"Pinterest", @"image": @"Pinterest", @"placeHolder": @"https://www.pinterest.com/yourcompanyname", @"text": strPinterest} atIndex:5];
//    [arrSocial insertObject:@{@"name": @"Instagram", @"image": @"Instagram", @"placeHolder": @"https://www.instagram.com/yourcompanyname", @"text": strInstagram} atIndex:6];
//    [arrSocial insertObject:@{@"name": @"Skype", @"image": @"Skype", @"placeHolder": @"Skype Name", @"text": strSkype} atIndex:7];
//    [arrSocial insertObject:@{@"name": @"PayPal", @"image": @"Paypal", @"placeHolder": @"Paypal Link", @"text": strPaypal} atIndex:8];

    arrSocial = [[NSMutableArray alloc] initWithObjects:
                 @{@"name": @"Facebook", @"image": @"Facebook", @"placeHolder": @"https://www.facebook.com/yourcompanyname", @"text": strFacebook},
                 @{@"name": @"Twitter", @"image": @"Twitter", @"placeHolder": @"https://www.twitter.com/youraccountname", @"text": strTwitter},
                 @{@"name": @"Google Plus", @"image": @"GooglePlus", @"placeHolder": @"https://www.google.com/yourcompanyname", @"text": strGoogle},
                 @{@"name": @"Linkedin", @"image": @"Linkdin", @"placeHolder": @"https://www.linkedin.com/compny/yourcompanyname", @"text": strLinkdin},
                 @{@"name": @"You Tube", @"image": @"Youtube", @"placeHolder": @"https://www.youtube.com/user/yourcompanyname", @"text": strYoutube},
                 @{@"name": @"Pinterest", @"image": @"Pinterest", @"placeHolder": @"https://www.pinterest.com/yourcompanyname", @"text": strPinterest},
                 @{@"name": @"Instagram", @"image": @"Instagram", @"placeHolder": @"https://www.instagram.com/yourcompanyname", @"text": strInstagram},
                 @{@"name": @"Skype", @"image": @"Skype", @"placeHolder": @"Skype Name", @"text": strSkype},
                 @{@"name": @"PayPal", @"image": @"Paypal", @"placeHolder": @"Paypal Link", @"text": strPaypal},nil];
    
    [tblSocial reloadData];
}

#pragma mark - UIButton Action
- (IBAction)Save:(id)sender {
    
    [self connectionForSaveSocialMedia];
}
@end
