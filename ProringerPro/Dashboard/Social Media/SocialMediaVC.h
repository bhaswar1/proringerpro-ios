//
//  SocialMediaVC.h
//  ProringerPro
//
//  Created by Soma Halder on 27/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SocialMediaVC : ProRingerProBaseVC <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblSocial;

- (IBAction)Save:(id)sender;

@end

NS_ASSUME_NONNULL_END
