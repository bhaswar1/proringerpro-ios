//
//  SocialTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 29/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SocialTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *txtSocialMedia;

@property (weak, nonatomic) IBOutlet UIImageView *imgSocial;

-(void)configureCellWith:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
