//
//  NotificationTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 24/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "NotificationTableViewCell.h"

@implementation NotificationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
