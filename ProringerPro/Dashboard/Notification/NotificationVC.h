//
//  NotificationVC.h
//  ProringerPro
//
//  Created by Soma Halder on 24/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotificationVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITableView *tblNotification;

@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderTitle;
@property (weak, nonatomic) IBOutlet UIView *viewMobileHeader;

@property (weak, nonatomic) IBOutlet UIButton *btnEmailNewsLetter;
@property (weak, nonatomic) IBOutlet UIButton *btnEmailChat;
@property (weak, nonatomic) IBOutlet UIButton *btnEmailTips;
@property (weak, nonatomic) IBOutlet UIButton *btnEmailNewJob;
@property (weak, nonatomic) IBOutlet UIButton *btnEmailReview;
@property (weak, nonatomic) IBOutlet UIButton *btnEmailAccount;


@property (weak, nonatomic) IBOutlet UIButton *btnMobileNewsLetter;
@property (weak, nonatomic) IBOutlet UIButton *btnMobileChat;
@property (weak, nonatomic) IBOutlet UIButton *btnMobileTips;
@property (weak, nonatomic) IBOutlet UIButton *btnMobileNewJob;
@property (weak, nonatomic) IBOutlet UIButton *btnMobileReview;
@property (weak, nonatomic) IBOutlet UIButton *btnMobileAccount;

- (IBAction)EmailNewsLetter:(id)sender;
- (IBAction)EmailChat:(id)sender;
- (IBAction)EmailTips:(id)sender;
- (IBAction)EmailNewJob:(id)sender;
- (IBAction)EmailReview:(id)sender;
- (IBAction)EmailAccount:(id)sender;

- (IBAction)MobileNewsLetter:(id)sender;
- (IBAction)MobileChat:(id)sender;
- (IBAction)MobileTips:(id)sender;
- (IBAction)MobileNewJob:(id)sender;
- (IBAction)MobileReview:(id)sender;
- (IBAction)MobileAccount:(id)sender;

@end

NS_ASSUME_NONNULL_END
