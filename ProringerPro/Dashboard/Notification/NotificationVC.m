//
//  NotificationVC.m
//  ProringerPro
//
//  Created by Soma Halder on 24/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "NotificationVC.h"

@interface NotificationVC ()
{
    NSMutableArray *arrNotification;
    
    NSMutableDictionary *dictEmail, *dictMobile;
}

@end

@implementation NotificationVC
@synthesize tblNotification, viewHeader, lblHeaderTitle, viewMobileHeader, btnEmailChat, btnEmailTips, btnMobileChat, btnMobileTips, btnEmailNewJob, btnEmailReview, btnEmailAccount, btnMobileNewJob, btnMobileReview, btnMobileAccount, btnEmailNewsLetter, btnMobileNewsLetter;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    self.navigationItem.title = @"Notifications";
    
//    self.tabBarController.tabBar.hidden = NO;

    dictEmail = [NSMutableDictionary dictionary];
    dictMobile = [NSMutableDictionary dictionary];
    
//    arrNotification = [[NSMutableArray alloc] initWithObjects:@"Newsletter", @"Chat Messages", @"Tips & Articles", @"New job postings", @"Recive New Reviews", @"Account Achivments", nil];
    
    [self connectionForGetNotificationList];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Notification"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Notification"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }

}

-(void)rightBarButtonPressed:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchProjectVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SearchProjectVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

-(void)notificationSwitchButtonPressed:(UIButton *)sender {
    
    NSLog(@"ROW NUMBER:::...%ld", sender.tag);
    NSLog(@"SECTION NUMBER:::...%@", sender.restorationIdentifier);
    
    UIImage *imgStatus = [sender imageForState:UIControlStateNormal];
    
    if (imgStatus == [UIImage imageNamed:@"Off"]) {
        [sender setImage:[UIImage imageNamed:@"On"] forState:UIControlStateNormal];
    } else {
        [sender setImage:[UIImage imageNamed:@"Off"] forState:UIControlStateNormal];
    }
}

#pragma mark - Web Service
-(void)connectionForGetNotificationList {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_GET_NOTIFICATION];
    
    NSDictionary *param = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager GET:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        [YXSpritesLoadingView dismiss];
        
        NSLog(@"response from update email:::...%@", response);
        
        if ([[response objectForKey:@"response"] boolValue] == YES) {
            
            NSDictionary *dict = [[response objectForKey:@"info_array"] objectAtIndex:0];
            [self->dictEmail addEntriesFromDictionary:[dict objectForKey:@"Email"]];
            [self->dictMobile addEntriesFromDictionary:[dict objectForKey:@"Mobile"]];
            
            [self configureNotificationValues];
        }
    }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              [YXSpritesLoadingView dismiss];
              NSLog(@"ERROR:::...%@", task.error);
          }];
}

-(void)connectionForSaveNotificationValues {
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_SAVE_NOTIFICATION];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"email_newsletter": [dictEmail objectForKey:@"newsletter"],
                             @"email_chat_msg": [dictEmail objectForKey:@"chat_msg"],
                             @"email_tips_article": [dictEmail objectForKey:@"tips_article"],
                             @"email_job_post": [dictEmail objectForKey:@"job_post"],
                             @"email_new_reviews": [dictEmail objectForKey:@"new_reviews"],
                             @"email_acc_achieve": [dictEmail objectForKey:@"account_acheive"],
                             @"mobile_newsletter": [dictMobile objectForKey:@"newsletter"],
                             @"mobile_chat_msg": [dictMobile objectForKey:@"chat_msg"],
                             @"mobile_article": [dictMobile objectForKey:@"tips_article"],
                             @"mobile_job_post": [dictMobile objectForKey:@"job_post"],
                             @"mobile_new_reviews": [dictMobile objectForKey:@"new_reviews"],
                             @"mobile_acc_achieve": [dictMobile objectForKey:@"account_acheive"]
                                 };
    
    NSLog(@"PARAMETERS:::...%@", params);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
        } else {
            [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:[responseObject objectForKey:@"message"]];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:@"Please check your network connection"];
    }];
    
}

#pragma mark - Configure Values
-(void)configureNotificationValues {
    
    UIImage *imgStatus;
    
    if ([[dictEmail objectForKey:@"newsletter"] boolValue]) {
        imgStatus = [UIImage imageNamed:@"On"];
    } else {
        imgStatus = [UIImage imageNamed:@"Off"];
    }
    [btnEmailNewsLetter setImage:imgStatus forState:UIControlStateNormal];
    
    if ([[dictEmail objectForKey:@"chat_msg"] boolValue]) {
        imgStatus = [UIImage imageNamed:@"On"];
    } else {
        imgStatus = [UIImage imageNamed:@"Off"];
    }
    [btnEmailChat setImage:imgStatus forState:UIControlStateNormal];
    
    if ([[dictEmail objectForKey:@"tips_article"] boolValue]) {
        imgStatus = [UIImage imageNamed:@"On"];
    } else {
        imgStatus = [UIImage imageNamed:@"Off"];
    }
    [btnEmailTips setImage:imgStatus forState:UIControlStateNormal];
    
    if ([[dictEmail objectForKey:@"job_post"] boolValue]) {
        imgStatus = [UIImage imageNamed:@"On"];
    } else {
        imgStatus = [UIImage imageNamed:@"Off"];
    }
    [btnEmailNewJob setImage:imgStatus forState:UIControlStateNormal];
    
    if ([[dictEmail objectForKey:@"new_reviews"] boolValue]) {
        imgStatus = [UIImage imageNamed:@"On"];
    } else {
        imgStatus = [UIImage imageNamed:@"Off"];
    }
    [btnEmailReview setImage:imgStatus forState:UIControlStateNormal];
    
    if ([[dictEmail objectForKey:@"account_acheive"] boolValue]) {
        imgStatus = [UIImage imageNamed:@"On"];
    } else {
        imgStatus = [UIImage imageNamed:@"Off"];
    }
    [btnEmailAccount setImage:imgStatus forState:UIControlStateNormal];
    
    //Mobile
    if ([[dictMobile objectForKey:@"newsletter"] boolValue]) {
        imgStatus = [UIImage imageNamed:@"On"];
    } else {
        imgStatus = [UIImage imageNamed:@"Off"];
    }
    [btnMobileNewsLetter setImage:imgStatus forState:UIControlStateNormal];
    
    if ([[dictMobile objectForKey:@"chat_msg"] boolValue]) {
        imgStatus = [UIImage imageNamed:@"On"];
    } else {
        imgStatus = [UIImage imageNamed:@"Off"];
    }
    [btnMobileChat setImage:imgStatus forState:UIControlStateNormal];
    
    if ([[dictMobile objectForKey:@"tips_article"] boolValue]) {
        imgStatus = [UIImage imageNamed:@"On"];
    } else {
        imgStatus = [UIImage imageNamed:@"Off"];
    }
    [btnMobileTips setImage:imgStatus forState:UIControlStateNormal];
    
    if ([[dictMobile objectForKey:@"job_post"] boolValue]) {
        imgStatus = [UIImage imageNamed:@"On"];
    } else {
        imgStatus = [UIImage imageNamed:@"Off"];
    }
    [btnMobileNewJob setImage:imgStatus forState:UIControlStateNormal];
    
    if ([[dictMobile objectForKey:@"new_reviews"] boolValue]) {
        imgStatus = [UIImage imageNamed:@"On"];
    } else {
        imgStatus = [UIImage imageNamed:@"Off"];
    }
    [btnMobileReview setImage:imgStatus forState:UIControlStateNormal];
    
    if ([[dictMobile objectForKey:@"account_acheive"] boolValue]) {
        imgStatus = [UIImage imageNamed:@"On"];
    } else {
        imgStatus = [UIImage imageNamed:@"Off"];
    }
    [btnMobileAccount setImage:imgStatus forState:UIControlStateNormal];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Email Notification
- (IBAction)EmailNewsLetter:(id)sender {
    
//    ;
    
    if ([[dictEmail objectForKey:@"newsletter"] boolValue]) {
        
        [dictEmail setObject:@"FALSE" forKey:@"newsletter"];
    } else {
        [dictEmail setObject:@"TRUE" forKey:@"newsletter"];
    }
    [self configureNotificationValues];
    
    [self connectionForSaveNotificationValues];
}

- (IBAction)EmailChat:(id)sender {
    
//    ;
    
    if ([[dictEmail objectForKey:@"chat_msg"] boolValue]) {
        
        [dictEmail setObject:@"FALSE" forKey:@"chat_msg"];
    } else {
        [dictEmail setObject:@"TRUE" forKey:@"chat_msg"];
    }
    [self configureNotificationValues];
    
    [self connectionForSaveNotificationValues];
}

- (IBAction)EmailTips:(id)sender {
    
//    ;
    
    if ([[dictEmail objectForKey:@"tips_article"] boolValue]) {
        
        [dictEmail setObject:@"FALSE" forKey:@"tips_article"];
    } else {
        [dictEmail setObject:@"TRUE" forKey:@"tips_article"];
    }
    [self configureNotificationValues];
    
    [self connectionForSaveNotificationValues];
}

- (IBAction)EmailNewJob:(id)sender {
    
//    ;
    
    if ([[dictEmail objectForKey:@"job_post"] boolValue]) {
        
        [dictEmail setObject:@"FALSE" forKey:@"job_post"];
    } else {
        [dictEmail setObject:@"TRUE" forKey:@"job_post"];
    }
    [self configureNotificationValues];
    
    [self connectionForSaveNotificationValues];
}

- (IBAction)EmailReview:(id)sender {
    
//    ;
    
    if ([[dictEmail objectForKey:@"new_reviews"] boolValue]) {
        
        [dictEmail setObject:@"FALSE" forKey:@"new_reviews"];
    } else {
        [dictEmail setObject:@"TRUE" forKey:@"new_reviews"];
    }
    [self configureNotificationValues];
    
    [self connectionForSaveNotificationValues];
}

- (IBAction)EmailAccount:(id)sender {
    
//    ;
    
    if ([[dictEmail objectForKey:@"account_acheive"] boolValue]) {
        
        [dictEmail setObject:@"FALSE" forKey:@"account_acheive"];
    } else {
        [dictEmail setObject:@"TRUE" forKey:@"account_acheive"];
    }
    [self configureNotificationValues];
    
    [self connectionForSaveNotificationValues];
}

#pragma mark - Mobile Notification
- (IBAction)MobileNewsLetter:(id)sender {
    
//    ;
    
    if ([[dictMobile objectForKey:@"newsletter"] boolValue]) {
        
        [dictMobile setObject:@"FALSE" forKey:@"newsletter"];
    } else {
        [dictMobile setObject:@"TRUE" forKey:@"newsletter"];
    }
    [self configureNotificationValues];
    
    [self connectionForSaveNotificationValues];
}

- (IBAction)MobileChat:(id)sender {
    
//    ;
    
    if ([[dictMobile objectForKey:@"chat_msg"] boolValue]) {
        
        [dictMobile setObject:@"FALSE" forKey:@"chat_msg"];
    } else {
        [dictMobile setObject:@"TRUE" forKey:@"chat_msg"];
    }
    [self configureNotificationValues];
    
    [self connectionForSaveNotificationValues];
}

- (IBAction)MobileTips:(id)sender {
    
//    ;
    
    if ([[dictMobile objectForKey:@"tips_article"] boolValue]) {
        
        [dictMobile setObject:@"FALSE" forKey:@"tips_article"];
    } else {
        [dictMobile setObject:@"TRUE" forKey:@"tips_article"];
    }
    [self configureNotificationValues];
    
    [self connectionForSaveNotificationValues];
}

- (IBAction)MobileNewJob:(id)sender {
    
//    ;
    
    if ([[dictMobile objectForKey:@"job_post"] boolValue]) {
        
        [dictMobile setObject:@"FALSE" forKey:@"job_post"];
    } else {
        [dictMobile setObject:@"TRUE" forKey:@"job_post"];
    }
    [self configureNotificationValues];
    
    [self connectionForSaveNotificationValues];
}

- (IBAction)MobileReview:(id)sender {
    
//    ;
    
    if ([[dictMobile objectForKey:@"new_reviews"] boolValue]) {
        
        [dictMobile setObject:@"FALSE" forKey:@"new_reviews"];
    } else {
        [dictMobile setObject:@"TRUE" forKey:@"new_reviews"];
    }
    [self configureNotificationValues];
    
    [self connectionForSaveNotificationValues];
}

- (IBAction)MobileAccount:(id)sender {
    
//    ;
    
    if ([[dictMobile objectForKey:@"account_acheive"] boolValue]) {
        
        [dictMobile setObject:@"FALSE" forKey:@"account_acheive"];
    } else {
        [dictMobile setObject:@"TRUE" forKey:@"account_acheive"];
    }
    [self configureNotificationValues];
    
    [self connectionForSaveNotificationValues];
}
@end
