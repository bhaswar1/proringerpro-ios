//
//  NotificationTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 24/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotificationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblNotification;

@property (weak, nonatomic) IBOutlet UIButton *btnNotification;

@end

NS_ASSUME_NONNULL_END
