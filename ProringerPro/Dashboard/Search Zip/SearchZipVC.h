//
//  SearchZipVC.h
//  ProringerPro
//
//  Created by Soma Halder on 25/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol sendAddressDelegate <NSObject>

-(void)sendAddressToUserInfo:(NSDictionary *)dictAddress;

@end

NS_ASSUME_NONNULL_BEGIN

@interface SearchZipVC : ProRingerProBaseVC <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property(assign, nonatomic) id deleagte;

@property (weak, nonatomic) IBOutlet UITextField *txtZipCode;

@property (weak, nonatomic) IBOutlet UITableView *tblAddress;

@property (weak, nonatomic) IBOutlet UINavigationItem *navigationTitle;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnBack;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnDone;

@property (weak, nonatomic) IBOutlet UILabel *lblAddressTitle;

@property (strong, nonatomic) NSString *strNavigationTitle, *strAddressTitle, *strPlaceHolder, *strApi, *strSearchText, *strSubSearchText, *strSearchComponent, *strRoute;

- (IBAction)Done:(id)sender;
- (IBAction)Back:(id)sender;

@end

NS_ASSUME_NONNULL_END
