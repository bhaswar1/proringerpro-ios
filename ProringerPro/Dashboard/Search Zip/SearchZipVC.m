//
//  SearchZipVC.m
//  ProringerPro
//
//  Created by Soma Halder on 25/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "SearchZipVC.h"
#import "AddressTableViewCell.h"

//static NSString* const kApiKey = @"AIzaSyBIGY1ZzRFrtpS8Zb_j2JX7nCQdbw83gJI";
//static NSString* const kApiKey = @"AIzaSyDSYXgSTcdBaf0fZjOrbaC2UEePVrYVNq8";
//static NSString* const kApiKey = @"AIzaSyAHLnD9mtaoO9grq1TlVArjy64jTFbIz7M";
@interface SearchZipVC ()
{
    NSMutableArray *arrAddress;
    int indexPathNumber;
    
    NSString *strZip;
    NSRange previousRange;
    NSDictionary *dictGiocoder;
}

@end

@implementation SearchZipVC
@synthesize tblAddress, txtZipCode, deleagte, btnBack, btnDone, strPlaceHolder, strAddressTitle, strNavigationTitle, lblAddressTitle, navigationTitle, strApi, strSearchText, strSearchComponent, strSubSearchText, strRoute;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = strNavigationTitle;
    self.navigationItem.title = strNavigationTitle;
    navigationTitle.title = strNavigationTitle;
    lblAddressTitle.text = strAddressTitle;
    txtZipCode.placeholder = strPlaceHolder;
    
    [YXSpritesLoadingView show];
    arrAddress = [NSMutableArray array];
    
    strZip = [NSString string];
    
    tblAddress.dataSource = self;
    tblAddress.delegate = self;
    tblAddress.hidden = YES;
    
    txtZipCode.delegate = self;
    
    [self paddingForTextField:txtZipCode];
//    [self shadowForTextField:txtZipCode];
    
//    [txtZipCode becomeFirstResponder];
    [YXSpritesLoadingView dismiss];
}

#pragma mark - UITextField Design
-(void) paddingForTextField:(UITextField *)textField {
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(30, 0, 10, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    textField.layer.masksToBounds = NO;
//    textField.layer.shadowOffset = CGSizeMake(0.0, 0.0);
//    textField.layer.shadowRadius = 1;
//    textField.layer.shadowOpacity = 0.2;
//    textField.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
//
//    textField.layer.borderWidth = 1.0;
//    textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
}
//
//-(void)shadowForTextField:(UITextField *)textfield {
//    
//    UIColor *color = [UIColor blackColor];
//    textfield.layer.shadowColor = [color CGColor];
//    textfield.layer.shadowRadius = 1.5f;
//    textfield.layer.shadowOpacity = 0.2;
//    textfield.layer.shadowOffset = CGSizeZero;
//    textfield.layer.masksToBounds = NO;
//}

#pragma mark - Header button action
-(void)doneButtonPressed {
    
    if (arrAddress.count > 0) {
        if (dictGiocoder == nil) {
            dictGiocoder = [arrAddress objectAtIndex:0];
            txtZipCode.text = [dictGiocoder objectForKey:@"description"];
            
            indexPathNumber = 0;
            txtZipCode.text = [dictGiocoder objectForKey:@"description"];
            [self connectionForGetAddressDetails:[dictGiocoder objectForKey:@"place_id"]];
            
        } else {
            [self dismissViewControllerAnimated:YES completion:^{
                
                [self->deleagte sendAddressToUserInfo:self->dictGiocoder];
            }];
        }
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - UITextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
//    previousRange = range;
//    strZip = [NSString stringWithFormat:@"%@%@", strZip, string];
//    [self connectionForGetAddress:strZip];
    
    NSString *newString = [txtZipCode.text stringByReplacingCharactersInRange:range withString:string];
    
    if (newString.length>0) {
        [self connectionForGetAddress:newString];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrAddress.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellAddress";
    NSDictionary *dictAddress = [arrAddress objectAtIndex:indexPath.row];
    AddressTableViewCell *cell = (AddressTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.lblAddress.text = [dictAddress objectForKey:@"description"];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"DID SELECTED ROW AT INDEXPATH %ld", indexPath.row);
    indexPathNumber = (int)indexPath.row;
    txtZipCode.text = [[arrAddress objectAtIndex:indexPath.row] objectForKey:@"description"];
    [self connectionForGetAddressDetails:[[arrAddress objectAtIndex:indexPath.row] objectForKey:@"place_id"]];
    [txtZipCode resignFirstResponder];
}

#pragma mark - WebService
-(void)connectionForGetAddress:(NSString *)zipCode {
    
    [YXSpritesLoadingView show];
    
    strZip = zipCode;

    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?key=%@&types=%@&components=country:US|country:CA&input=%@", API_GOOGLE_KEY, strApi, zipCode];
    
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSStringEncodingConversionExternalRepresentation];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"GET"];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (!response) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"Connection Error, 1Failed to Connect to the Internet");
    }
    
    //    NSString *respString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    //    NSLog(@"RESPONSE STRING:::: %@", respString);
    
    id dict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    if (dict != nil) {
        NSLog(@"Dict: %@", dict);
        
//        if ([[dict objectForKey:@"status"] isEqualToString:@"OK"]) {
        
        [arrAddress removeAllObjects];
        if (dict[@"predictions"]) {
            [arrAddress addObjectsFromArray:[dict objectForKey:@"predictions"]];
            
            if (arrAddress.count > 0) {
                if (arrAddress.count < 8) {
                    tblAddress.frame = CGRectMake(tblAddress.frame.origin.x, tblAddress.frame.origin.y, tblAddress.frame.size.width, 50 *arrAddress.count);
                } else {
                    tblAddress.frame = CGRectMake(tblAddress.frame.origin.x, tblAddress.frame.origin.y, tblAddress.frame.size.width, self.view.frame.size.height -60);
                }
                [tblAddress setHidden:NO];
                [tblAddress reloadData];
            } else {
                tblAddress.hidden = YES;
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please enter correct address." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
//                    ;
//                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
                [alertController.view setTintColor:COLOR_THEME];
            }
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"Google does not return any value or something went wrong" preferredStyle:UIAlertControllerStyleAlert];
        
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
//                ;
            [self dismissViewControllerAnimated:YES completion:nil];
            }];
        
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        }
//        }
        /* else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[dict objectForKey:@"status"] message:[dict objectForKey:@"error_message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        } */
        [YXSpritesLoadingView dismiss];
    } else {
        [YXSpritesLoadingView dismiss];
        NSLog(@"Error: %@", error);
    }
}

-(void)connectionForGetAddressDetails:(NSString *)placeId {
    
    [YXSpritesLoadingView show];
    
//  
    
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?place_id=%@&key=%@&language=en", placeId, API_GOOGLE_KEY];
    
    NSLog(@"%@", strUrl);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:strUrl parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            self->dictGiocoder = [[responseObject objectForKey:@"results"] objectAtIndex:0];
//            [self doneButtonPressed];
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[responseObject objectForKey:@"status"] message:[responseObject objectForKey:@"error_message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
//                ;
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
    /*
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"GET"];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (!response) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"Connection Error, Failed to Connect to the Internet");
    }
    
    //    NSString *respString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    //    NSLog(@"RESPONSE STRING:::: %@", respString);
    
    id dict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    if (dict != nil) {
        NSLog(@"Dict from response zip: %@", dict);
        
        if ([[dict objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            dictGiocoder = [[dict objectForKey:@"results"] objectAtIndex:0];
//            [self doneButtonPressed];
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[dict objectForKey:@"status"] message:[dict objectForKey:@"error_message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        [YXSpritesLoadingView dismiss];
    } else {
        [YXSpritesLoadingView dismiss];
        NSLog(@"Error: %@", error);
    }
     */
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)Done:(id)sender {
    
    NSArray *arrAddressComponent = [dictGiocoder objectForKey:@"address_components"];

    NSString *strAddress, *strStreetRoute;
    
    for (int i = 0; i < arrAddressComponent.count; i++) {
        NSDictionary *dictComponent = [arrAddressComponent objectAtIndex:i];
        NSArray *arrTypes = [dictComponent objectForKey:@"types"];
        for (int iTypes = 0; iTypes < arrTypes.count; iTypes++) {
            if ([[arrTypes objectAtIndex:iTypes] isEqualToString:strSearchText]) {
                strAddress = [dictComponent objectForKey:strSearchComponent];
            }
        }
    }
    
    for (int i = 0 ; i < arrAddressComponent.count; i++) {
        NSDictionary *dictComponent = [arrAddressComponent objectAtIndex:i];
        NSArray *arrTypes = [dictComponent objectForKey:@"types"];
        for (int iTypes = 0; iTypes < arrTypes.count; iTypes++) {
            if ([[arrTypes objectAtIndex:iTypes] isEqualToString:strRoute]) {
                strStreetRoute = [dictComponent objectForKey:strSearchComponent];
            }
        }
    }
    
    if (strSubSearchText.length > 0) {
        for (int i = 0; i < arrAddressComponent.count; i++) {
            NSDictionary *dictComponent = [arrAddressComponent objectAtIndex:i];
            NSArray *arrTypes = [dictComponent objectForKey:@"types"];
            for (int iTypes = 0; iTypes < arrTypes.count; iTypes++) {
                if ([[arrTypes objectAtIndex:iTypes] isEqualToString:strSearchText] || [[arrTypes objectAtIndex:iTypes] isEqualToString:strSubSearchText]) {
                    strAddress = [dictComponent objectForKey:strSearchComponent];
                }
            }
        }
    }
    
    if (strSearchText.length > 0) {
        if (strRoute.length > 0) {
            if (strAddress.length > 0 && strStreetRoute.length > 0) {
                [self doneButtonPressed];
            } else {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Enter a valid street address" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self->txtZipCode becomeFirstResponder];
                }];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
                alertController.view.tintColor = [UIColor colorWithRed:241/255.0 green:89/255.0 blue:42/255.0 alpha:1];
            }
        } else {
            if (strAddress.length > 0) {
                [self doneButtonPressed];
            } else {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Enter a valid street address" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self->txtZipCode becomeFirstResponder];
                }];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
                alertController.view.tintColor = [UIColor colorWithRed:241/255.0 green:89/255.0 blue:42/255.0 alpha:1];
            }
        }
    } else {
        [self doneButtonPressed];
    }
}

- (IBAction)Back:(id)sender {
//    ;
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
