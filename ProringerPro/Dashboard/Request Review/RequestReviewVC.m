//
//  RequestReviewVC.m
//  ProringerPro
//
//  Created by Soma Halder on 29/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "RequestReviewVC.h"

@interface RequestReviewVC ()
{
    NSMutableArray *arrRestrictedWordsFound;
    CGRect frame;
}

@end

@implementation RequestReviewVC
@synthesize txtFirstNeme, txtLastName, txtEmail, txtConfEmail, txtComments, lblPlaceHolder, viewAccessory, strRequestingReview, constRequestReviewButtonHeight, viewHeader, btnRequestReview;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Request Review";
    
//    self.tabBarController.tabBar.hidden = NO;
    
    lblPlaceHolder.text = [NSString stringWithFormat:@"  Comment \n\n\n\n\n\n"];
    
    [self connectionForGetDashboardData];
    
    [self paddingForTextField:txtFirstNeme];
    [self paddingForTextField:txtLastName];
    [self paddingForTextField:txtEmail];
    [self paddingForTextField:txtConfEmail];
    
    [self shadowForTextField:txtFirstNeme];
    [self shadowForTextField:txtLastName];
    [self shadowForTextField:txtEmail];
    [self shadowForTextField:txtConfEmail];

//    [self paddingForTextView:txtComments];
    [self shadowForTextView:txtComments];
    txtComments.layer.borderWidth = 1.0;
    txtComments.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtComments.backgroundColor = [UIColor clearColor];
    
    frame = viewHeader.frame;
    frame.size.height = 450;
    viewHeader.frame = frame;
    constRequestReviewButtonHeight.constant = 45;
    btnRequestReview.hidden = NO;
    
    txtEmail.inputAccessoryView = txtComments.inputAccessoryView = txtLastName.inputAccessoryView = txtLastName.inputAccessoryView = txtConfEmail.inputAccessoryView = txtFirstNeme.inputAccessoryView = viewAccessory;
    
    arrRestrictedWordsFound = [[NSMutableArray alloc] init];

    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

#pragma mark - AD Banner Delegate
- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.bottomLayoutGuide
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:-0],
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
    //    bannerView.layer.zPosition = -1;
    //    table_view.layer.zPosition = -2;
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Request Review"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Request Review"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
}

//#pragma mark - UITextView Design
//-(void) padingForTextView:(UITextView *)textView {
//
//    textView.contentInset = UIEdgeInsetsMake(3, 5, 10, 5);
//}
//
//-(void)shadowForTextView:(UITextView *)textView {
//
//    UIColor *color = [UIColor blackColor];
//    textView.layer.shadowColor = [color CGColor];
//    textView.layer.shadowRadius = 1.5f;
//    textView.layer.shadowOpacity = 0.2;
//    textView.layer.shadowOffset = CGSizeZero;
//    textView.layer.masksToBounds = NO;
//}

#pragma mark - UITextView Delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@""]) {
        
        NSLog(@"%@",textView.text);
        
        if (textView.text.length == 1) {
            lblPlaceHolder.hidden = NO;
        }
    } else if ([NSString stringWithFormat:@"%@%@",textView.text,text].length > 0) {
        
        lblPlaceHolder.hidden = YES;
    } else {
        lblPlaceHolder.hidden = NO;
    }
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    [textView becomeFirstResponder];
//    lblPlaceHolder.text = @"";
    textView.layer.borderColor = COLOR_THEME.CGColor;
    textView.layer.borderWidth = 1.5f;
    
    frame = viewHeader.frame;
    frame.size.height = 400;
    viewHeader.frame = frame;
    constRequestReviewButtonHeight.constant = 0;
    btnRequestReview.hidden = YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    [self paddingForTextView:textView];
    [self shadowForTextView:textView];
    
    frame = viewHeader.frame;
    frame.size.height = 450;
    viewHeader.frame = frame;
    constRequestReviewButtonHeight.constant = 45;
    btnRequestReview.hidden = NO;
    
    [arrRestrictedWordsFound removeAllObjects];
    
    NSString *strText = [textView.text stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    NSArray *arrNewWords = [strText componentsSeparatedByString:@" "];
    NSLog(@"WORDS ARRAY FROM TEXTVIEW:::...%@", arrNewWords);
    arrRestrictedWordsFound = [self doArraysContainTheSameObjects:arrNewWords];
    
    if ([textView.text isEqualToString:@""]) {
        lblPlaceHolder.hidden = NO;
    } else {
        lblPlaceHolder.hidden = YES;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    textField.layer.borderColor = COLOR_THEME.CGColor;
    textField.layer.borderWidth = 1.5f;
    
    frame = viewHeader.frame;
    frame.size.height = 400;
    viewHeader.frame = frame;
    constRequestReviewButtonHeight.constant = 0;
    btnRequestReview.hidden = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self paddingForTextField:textField];
    [self shadowForTextField:textField];
    
    frame = viewHeader.frame;
    frame.size.height = 450;
    viewHeader.frame = frame;
    constRequestReviewButtonHeight.constant = 45;
    btnRequestReview.hidden = NO;
}

#pragma mark - Web Service
-(void)connectionForRequestReview {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_REQUEST_REVIEW];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"first_name": txtFirstNeme.text,
                             @"last_name": txtLastName.text,
                             @"email": txtEmail.text,
                             @"conf_emailid": txtConfEmail.text,
                             @"comment": txtComments.text,
                             @"project_id":@""};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        NSLog(@"response from update password:::...%@", response);
        [YXSpritesLoadingView dismiss];
        if ([[response objectForKey:@"response"] boolValue] == YES) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[response objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
//                    if (self.interstitial.isReady) {
//                        [self.interstitial presentFromRootViewController:self];
//                    }
//                }
                
                if ([strRequestingReview isEqualToString:@"Requesting a review from request review page"]) {
                    if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
                        if (self.arrInterstitialAd.count > 0) {
                            for (int i = 0; i < self.arrInterstitialAd.count; i++) {
                                NSDictionary *dict = [self.arrInterstitialAd objectAtIndex:i];
                                if ([[dict objectForKey:@"title"] isEqualToString:@"Requesting a review from request review page"]) {
                                    if ([[dict objectForKey:@"ios_interstitial_ads"] intValue] == 1) {
                                        if (self.interstitial.isReady) {
                                            [self.interstitial presentFromRootViewController:self];
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if ([strRequestingReview isEqualToString:@"Requesting a review from Pros profile"]) {
                    if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
                        if (self.arrInterstitialAd.count > 0) {
                            for (int i = 0; i < self.arrInterstitialAd.count; i++) {
                                NSDictionary *dict = [self.arrInterstitialAd objectAtIndex:i];
                                if ([[dict objectForKey:@"title"] isEqualToString:@"Requesting a review from Pros profile"]) {
                                    if ([[dict objectForKey:@"ios_interstitial_ads"] intValue] == 1) {
                                        if (self.interstitial.isReady) {
                                            [self.interstitial presentFromRootViewController:self];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [alertController addAction:okAction];
            [alertController.view setTintColor:COLOR_THEME];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              NSLog(@"ERROR:::...%@", task.error);
              [YXSpritesLoadingView dismiss];
          }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Validation
-(BOOL)validationForAllTextFields {
    
    BOOL isValid = YES;
    
    NSString *strMsg = @"";
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtFirstNeme.text]) {
        [txtFirstNeme becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please enter first name";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtLastName.text]) {
        [txtLastName becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please enter last name";
    } else if (![self validateEmail:txtEmail.text]) {
        [txtEmail becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please enter valid email";
    } else if (![self validateEmail:txtConfEmail.text]) {
        [txtConfEmail becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please enter valid confirm email";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtComments.text]) {
        [txtComments becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please enter some comments";
    } else if (![txtEmail.text isEqualToString:txtConfEmail.text]) {
        [txtConfEmail becomeFirstResponder];
        isValid = NO;
        strMsg = @"Email id does not match!";
    } else if (arrRestrictedWordsFound.count > 0) {
        NSString *strKhisti = [arrRestrictedWordsFound componentsJoinedByString:@","];
        strMsg = [NSString stringWithFormat:@"Invalid term: %@ - Please be respectful of others. Remove and try again", strKhisti];
        [txtComments becomeFirstResponder];
        isValid = NO;
    } else if ([[AppManager sharedDataAccess] detectType:txtComments.text]) {
        isValid = NO;
        strMsg = self.strRestrictionMsg;
        [txtComments becomeFirstResponder];
    }
    
    if (strMsg.length > 0) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strMsg];
    }
    return isValid;
}

//if (txtComment.text.length > 0) {
//    if (arrRestrictedWordsFound.count > 0) {
//        NSString *strKhisti = [arrRestrictedWordsFound componentsJoinedByString:@","];
//        strMsg = [NSString stringWithFormat:@"Invalid term: %@ - Please be respectful of others. Remove and try again", strKhisti];
//        [txtComment becomeFirstResponder];
//        isValid = NO;
//    } else if ([[AppManager sharedDataAccess] detectType:txtComment.text]) {
//        
//        strMsg = self.strRestrictionMsg;
//        isValid = NO;
//    }
//}

-(BOOL) validateEmail:(NSString*) emailString {
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    }
    else
        return YES;
}

#pragma mark - UIButton Action
- (IBAction)RequestReview:(id)sender {
    
    [self textViewDidEndEditing:txtComments];
    
    if ([self validationForAllTextFields]) {
        [self connectionForRequestReview];
    }
}
@end
