//
//  RequestReviewVC.h
//  ProringerPro
//
//  Created by Soma Halder on 29/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RequestReviewVC : ProRingerProBaseVC <UITextViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtFirstNeme;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtConfEmail;
@property (weak, nonatomic) IBOutlet UITextView *txtComments;

@property (strong, nonatomic) NSString *strRequestingReview;

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceHolder;

@property (strong, nonatomic) IBOutlet UIView *viewAccessory;
@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnRequestReview;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constRequestReviewButtonHeight;

- (IBAction)RequestReview:(id)sender;

@end

NS_ASSUME_NONNULL_END
