//
//  DashboardTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "DashboardTableViewCell.h"

@implementation DashboardTableViewCell
@synthesize viewShadow;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIColor *color = [UIColor blackColor];
    viewShadow.layer.shadowColor = [color CGColor];
    viewShadow.layer.shadowRadius = 1.5f;
    viewShadow.layer.shadowOpacity = 0.1;
    viewShadow.layer.cornerRadius = 2.0;
    viewShadow.layer.shadowOffset = CGSizeZero;
    viewShadow.layer.masksToBounds = NO;
    
    viewShadow.layer.shadowColor = COLOR_TABLE_HEADER.CGColor;
    viewShadow.layer.shadowOffset = CGSizeMake(0, 3);
    viewShadow.layer.shadowOpacity = 0.5;
    viewShadow.layer.shadowRadius = 0.6;
    
    viewShadow.layer.cornerRadius = 5.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
