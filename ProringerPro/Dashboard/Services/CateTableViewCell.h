//
//  CateTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 24/05/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CateTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgCheckBox;
@property (weak, nonatomic) IBOutlet UILabel *lblCatName;

@property (weak, nonatomic) IBOutlet UIButton *btnHeaderSelection;
@property (weak, nonatomic) IBOutlet UIButton *btnRowSelection;


@end

NS_ASSUME_NONNULL_END
