//
//  ServicesVC.m
//  ProringerPro
//
//  Created by Soma Halder on 27/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ServicesVC.h"
#import "CateTableViewCell.h"

@interface ServicesVC ()
{
    NSMutableArray *arrServicesLocalList, *arrServicesWebList, *arrCategoryServiceList;
    UIToolbar *toolBar;
    NSString *strServiceName;
    NSMutableDictionary *dictService;
}

@end

@implementation ServicesVC
@synthesize collServicesList, txtServices, viewHeader, pickerServiceList, tblServices;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    self.navigationItem.title = @"SERVICES";
    
//    self.tabBarController.tabBar.hidden = NO;
    
    arrServicesWebList = [[NSMutableArray alloc] init];
    arrServicesLocalList = [[NSMutableArray alloc] init];
    arrCategoryServiceList = [[NSMutableArray alloc] init];
    
    strServiceName = @"";
    
    pickerServiceList.dataSource = self;
    pickerServiceList.delegate = self;
    pickerServiceList.hidden = YES;
    pickerServiceList.backgroundColor = [UIColor whiteColor];
    
    txtServices.delegate = self;
    
    [self paddingForTextField:txtServices];
    [self shadowForTextField:txtServices];
    
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, pickerServiceList.frame.origin.y +20, self.view.frame.size.width, 40)];
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    toolBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor redColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    toolBar.items = [NSArray arrayWithObjects:cancelButton, flexibleSpace, doneButton, nil];
    
    toolBar.hidden = YES;
    
    [self.view addSubview:toolBar];
    
    tblServices.dataSource = self;
    tblServices.delegate = self;
    
    collServicesList.dataSource = self;
    collServicesList.delegate = self;
    
//    [self connectionForServiceTypeList];
    [self connectionForGetServiceList];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Service"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Service"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }

}

#pragma mark - UIToolBar Button
-(void) doneButtonPressed {
//    arrServicesWebList
    
    if (strServiceName.length == 0) {
        strServiceName = [[arrServicesWebList objectAtIndex:0] objectForKey:@"category_name"];
        dictService = [[NSMutableDictionary alloc] init];
        dictService = [[arrServicesWebList objectAtIndex:0] mutableCopy];
    }
    
    txtServices.text = strServiceName;
    strServiceName = @"";
    pickerServiceList.hidden = toolBar.hidden = YES;
    [txtServices resignFirstResponder];
}

-(void) cancelButtonPressed {
    
    txtServices.text = strServiceName = @"";
    pickerServiceList.hidden = toolBar.hidden = YES;
    [txtServices resignFirstResponder];
}

#pragma mark - UITextField Delegate
//- (void)textFieldDidBeginEditing:(UITextField *)textField {
//
//    pickerServiceList.hidden = NO;
//    toolBar.hidden = NO;
//    [textField resignFirstResponder];
//}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    NSLog(@"%@",searchStr);
    
    [self connectionForServiceTypeList:searchStr];
    
    return YES;
}

-(void)updateHeaderSize {
    
    float fHeaderHeight = 190;
    
    if (arrServicesLocalList == nil || arrServicesLocalList.count == 0) {
        viewHeader.frame = CGRectMake(0, 0, self.view.frame.size.width, fHeaderHeight);
    } else {
        fHeaderHeight = fHeaderHeight + 20;
        if ((arrServicesLocalList.count % 2) > 0) {
            fHeaderHeight = (((arrServicesLocalList.count +1) /2) *50) +fHeaderHeight;
            viewHeader.frame = CGRectMake(0, 0, self.view.frame.size.width, fHeaderHeight);
        }
        else {
            fHeaderHeight = ((arrServicesLocalList.count /2) *50) +fHeaderHeight;
            viewHeader.frame = CGRectMake(0, 0, self.view.frame.size.width, fHeaderHeight);
        }
    }
}

#pragma mark - UICollectioView DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrServicesLocalList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellArea";
    
    NSDictionary *dictServices = [arrServicesLocalList objectAtIndex:indexPath.item];
    
    ServiceAreaCollectionViewCell *cell = (ServiceAreaCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.lblAreaName.text = [dictServices objectForKey:@"category_name"];
    cell.btnDelete.tag = indexPath.item;
    [cell.btnDelete addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake((CGRectGetWidth(collectionView.frame) /2) -5,  40);
}

-(void)deleteButtonPressed:(UIButton *)sender {
    
    /*
    [arrServicesLocalList removeObjectAtIndex:sender.tag];
    [collServicesList reloadData];
    [tblServices reloadData];
    [self updateHeaderSize];
    */
    
    NSMutableDictionary *dictHeader = [NSMutableDictionary new];
    dictHeader = [[arrCategoryServiceList objectAtIndex:sender.tag] mutableCopy];
    
    [dictHeader setValue:[NSNumber numberWithInt:0] forKey:@"service_selected"];
    
    NSMutableArray *arrTemp = [NSMutableArray new];
    [arrTemp addObjectsFromArray:[dictHeader objectForKey:@"getSubcategory"]];
    
    for (int i = 0; i < arrTemp.count; i++) {
        NSMutableDictionary *dictTemp = [NSMutableDictionary new];
        dictTemp = [[arrTemp objectAtIndex:i] mutableCopy];
        [dictTemp setValue:[NSNumber numberWithInt:0] forKey:@"cat_selected"];
        [arrTemp replaceObjectAtIndex:i withObject:dictTemp];
    }
    [dictHeader setValue:arrTemp forKey:@"getSubcategory"];
    
    [arrCategoryServiceList replaceObjectAtIndex:sender.tag withObject:dictHeader];
    [tblServices reloadData];
    
    [self SaveServices:nil];
}

#pragma mark - UIPickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return arrServicesWebList.count;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 50)];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRed:241.0f/255 green:89.0f/255 blue:42.0f/255 alpha:1];
    label.font = [UIFont fontWithName:@"OpenSans" size:18];
    label.numberOfLines = 2;
    
    label.text = [[arrServicesWebList objectAtIndex:row] objectForKey:@"category_name"];
    
    return label;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [[arrServicesWebList objectAtIndex:row] objectForKey:@"category_name"];
}

#pragma mark - UIPickerView Delegate
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    
    return 50;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
//    txtServices.text = [[arrServicesWebList objectAtIndex:row] objectForKey:@"category_name"];
    strServiceName = [[arrServicesWebList objectAtIndex:row] objectForKey:@"category_name"];
    
    dictService = [[NSMutableDictionary alloc] init];
    
    dictService = [[arrServicesWebList objectAtIndex:row] mutableCopy];
    
//    [dict setValue:[[arrOptionList objectAtIndex:row] objectForKey:@"value"] forKey:@"describe"];
//    [dict setValue:[[arrOptionList objectAtIndex:row] objectForKey:@"id"] forKey:@"id"];
    
//    pickerServiceList.hidden = YES;
}

#pragma mark - UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return arrCategoryServiceList.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    static NSString *headerIdentifier = @"headerCat";
    
    NSDictionary *dictHeader = [arrCategoryServiceList objectAtIndex:section];
    
    CateTableViewCell *header = (CateTableViewCell *)[tableView dequeueReusableCellWithIdentifier:headerIdentifier];
    
    header.lblCatName.text = [dictHeader objectForKey:@"category_name"];
    
    if ([[dictHeader objectForKey:@"service_selected"] intValue] == 1) {
        [header.imgCheckBox setImage:[UIImage imageNamed:@"CheckBoxFull"]];
    } else {
        [header.imgCheckBox setImage:[UIImage imageNamed:@"CheckBoxEmpty"]];
    }
    
    [header.btnHeaderSelection addTarget:self action:@selector(headerSelectionButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    header.btnHeaderSelection.tag = section;
    
    return header;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[[arrCategoryServiceList objectAtIndex:section] objectForKey:@"getSubcategory"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellCat";
    
    NSDictionary *dict = [[[arrCategoryServiceList objectAtIndex:indexPath.section] objectForKey:@"getSubcategory"] objectAtIndex:indexPath.row];
    
    CateTableViewCell *cell = (CateTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.lblCatName.text = [dict objectForKey:@"category_name"];
    
    if ([[dict objectForKey:@"cat_selected"] intValue] == 1) {
        [cell.imgCheckBox setImage:[UIImage imageNamed:@"CheckBoxFull"]];
    } else {
        [cell.imgCheckBox setImage:[UIImage imageNamed:@"CheckBoxEmpty"]];
    }
    
    [cell.btnRowSelection addTarget:self action:@selector(rowSelectionButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnRowSelection.tag = indexPath.row;
    cell.btnRowSelection.restorationIdentifier = [NSString stringWithFormat:@"%ld", (long)indexPath.section];
    
    return cell;
}

-(void)headerSelectionButtonPressed:(UIButton *)sender {
    
    NSMutableDictionary *dictHeader = [NSMutableDictionary new];
    dictHeader = [[arrCategoryServiceList objectAtIndex:sender.tag] mutableCopy];
    
    if ([[dictHeader objectForKey:@"service_selected"] intValue] == 1) {
        [dictHeader setValue:[NSNumber numberWithInt:0] forKey:@"service_selected"];
        
        NSMutableArray *arrTemp = [NSMutableArray new];
        [arrTemp addObjectsFromArray:[dictHeader objectForKey:@"getSubcategory"]];
        
        for (int i = 0; i < arrTemp.count; i++) {
            NSMutableDictionary *dictTemp = [NSMutableDictionary new];
            dictTemp = [[arrTemp objectAtIndex:i] mutableCopy];
            [dictTemp setValue:[NSNumber numberWithInt:0] forKey:@"cat_selected"];
            [arrTemp replaceObjectAtIndex:i withObject:dictTemp];
        }
        [dictHeader setValue:arrTemp forKey:@"getSubcategory"];
    } else {
        [dictHeader setValue:[NSNumber numberWithInt:1] forKey:@"service_selected"];
        
        NSMutableArray *arrTemp = [NSMutableArray new];
        [arrTemp addObjectsFromArray:[dictHeader objectForKey:@"getSubcategory"]];
        
        for (int i = 0; i < arrTemp.count; i++) {
            NSMutableDictionary *dictTemp = [NSMutableDictionary new];
            dictTemp = [[arrTemp objectAtIndex:i] mutableCopy];
            [dictTemp setValue:[NSNumber numberWithInt:1] forKey:@"cat_selected"];
            [arrTemp replaceObjectAtIndex:i withObject:dictTemp];
        }
        [dictHeader setValue:arrTemp forKey:@"getSubcategory"];
    }
    [arrCategoryServiceList replaceObjectAtIndex:sender.tag withObject:dictHeader];
    [tblServices reloadData];
    [self SaveServices:nil];
}

-(void)rowSelectionButtonPressed:(UIButton *)sender {
    
    int iRestorationIdentifier = [sender.restorationIdentifier intValue];
    
    NSMutableDictionary *dictHeader = [NSMutableDictionary new];
    dictHeader = [[arrCategoryServiceList objectAtIndex:iRestorationIdentifier] mutableCopy];
    
    NSMutableArray *arrRowTemp = [NSMutableArray new];
    [arrRowTemp addObjectsFromArray:[dictHeader objectForKey:@"getSubcategory"]];
    
    NSMutableDictionary *dictTemp = [NSMutableDictionary new];
    dictTemp = [[arrRowTemp objectAtIndex:sender.tag] mutableCopy];
    
    if ([[dictTemp objectForKey:@"cat_selected"] intValue] == 1) {
        [dictTemp setValue:[NSNumber numberWithInt:0] forKey:@"cat_selected"];
    } else {
        [dictTemp setValue:[NSNumber numberWithInt:1] forKey:@"cat_selected"];
        [dictHeader setValue:[NSNumber numberWithInt:1] forKey:@"service_selected"];
    }
    
    [arrRowTemp replaceObjectAtIndex:sender.tag withObject:dictTemp];
    BOOL isFound = YES;
    for (int i = 0; i < arrRowTemp.count; i++) {
        if ([[[arrRowTemp objectAtIndex:i] objectForKey:@"cat_selected"] intValue] == 1) {
            isFound = NO;
        }
    }
    if (isFound) {
        [dictHeader setValue:[NSNumber numberWithInt:0] forKey:@"service_selected"];
    }
    [dictHeader setValue:arrRowTemp forKey:@"getSubcategory"];
    [arrCategoryServiceList replaceObjectAtIndex:iRestorationIdentifier withObject:dictHeader];
    
    [tblServices reloadData];
    
    [self SaveServices:nil];
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 35;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 35;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSLog(@"INDEXPATH NUMBER:::...%lu", indexPath.row);
}

#pragma mark - Navigation
/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Web Service
-(void)connectionForServiceTypeList:(NSString *)keyWord {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_CATEGORY_LIST];
    
    NSDictionary *param = @{@"category_search": keyWord};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
//        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [self->arrServicesWebList removeAllObjects];
            [self->arrServicesWebList addObjectsFromArray:[responseObject objectForKey:@"info_array"]];
            [self->pickerServiceList reloadAllComponents];
            
            self->pickerServiceList.hidden = NO;
            self->toolBar.hidden = NO;
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForGetCategoryServiceList:(NSString *)strCategoryName {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_CATEGORY_SERVICE_LIST];
    
    NSDictionary *param = @{@"parent_category": [dictService objectForKey:@"id"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"GET CTEGORY SERVICE LIST RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
//            [self->arrCategoryServiceList addObjectsFromArray:[responseObject objectForKey:@"info_array"]];
            
            NSArray *arrTemp = [NSArray new];
            arrTemp = [self->arrCategoryServiceList mutableCopy];
            
            NSMutableArray *arrCatService = [[NSMutableArray alloc] init];
            [arrCatService addObjectsFromArray:[responseObject objectForKey:@"info_array"]];
            
            for (int i = 0; i < arrCatService.count; i++) {
                NSMutableDictionary *dictCat = [NSMutableDictionary new];
                dictCat = [[arrCatService objectAtIndex:i] mutableCopy];
                [dictCat setValue:[NSNumber numberWithInt:1] forKey:@"cat_selected"];
                [arrCatService replaceObjectAtIndex:i withObject:dictCat];
            }
            NSLog(@"ARRAY AFTER ADDRED ELEMENT IN IT'S OBJECTS:::...%@", arrCatService);
//            [arrCategoryServiceList addObjectsFromArray:arrCatService];
            
            NSMutableDictionary *dictService = [NSMutableDictionary new];
            [dictService setObject:strCategoryName forKey:@"category_name"];
            [dictService setObject:[NSNumber numberWithInt:1] forKey:@"service_selected"];
            [dictService setObject:arrCatService forKey:@"getSubcategory"];
            [dictService setObject:[self->dictService objectForKey:@"id"] forKey:@"parent_id"];
            
//            [self->arrCategoryServiceList removeAllObjects];
//            [self->arrCategoryServiceList addObjectsFromArray:arrTemp];
            
            [self->arrCategoryServiceList insertObject:dictService atIndex:self->arrCategoryServiceList.count];
            
            NSLog(@"arrCategoryServiceList in category service list:::....%@", self->arrCategoryServiceList);
            
            [self->tblServices reloadData];
            
            self->txtServices.text = @"";
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForSaveServiceList {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    NSString *str;
    NSMutableArray *subcategory = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < arrCategoryServiceList.count; i++) {
        NSDictionary *dictI = [arrCategoryServiceList objectAtIndex:i];
        NSArray *arr = [NSArray new];
        arr = [dictI objectForKey:@"getSubcategory"];
        for (int j = 0; j < arr.count; j++) {
            NSDictionary *dictJ = [NSDictionary new];
            dictJ = [arr objectAtIndex:j];
            if ([[dictJ objectForKey:@"cat_selected"] intValue] == 1) {
                str = [dictJ objectForKey:@"id"];
                [subcategory insertObject:str atIndex:subcategory.count];
            }
        }
    }
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_SAVE_SERVICE];
    
    NSDictionary *param = @{@"user_id": [userDefault objectForKey:@"userId"],
                            @"subcategory": subcategory
                            };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"SAVE SERVICE LIST RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                [self connectionForGetServiceList];
            }];
            
            [alertController addAction:okAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForGetServiceList {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_SERVICE_LIST];
    
    NSDictionary *param = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]
                            };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"GET SERVICE LIST RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [self->arrServicesLocalList removeAllObjects];
            [self->arrServicesLocalList addObjectsFromArray:[responseObject objectForKey:@"info_array"]];
            
            [self->collServicesList reloadData];
            
            NSMutableArray *arrTemp = [NSMutableArray new];
            arrTemp = [[responseObject objectForKey:@"info_array"] mutableCopy];
            
            for (int i = 0; i < arrTemp.count; i++) {
                NSMutableDictionary *dictTemp = [NSMutableDictionary new];
                dictTemp = [[arrTemp objectAtIndex:i] mutableCopy];
                [dictTemp setValue:[NSNumber numberWithInt:1] forKey:@"service_selected"];
                [arrTemp replaceObjectAtIndex:i withObject:dictTemp];
            }
            
            [self->arrCategoryServiceList removeAllObjects];
            [self->arrCategoryServiceList addObjectsFromArray:arrTemp];
            
            [self updateHeaderSize];
            
            [self->tblServices reloadData];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

#pragma mark - UIButton Action
- (IBAction)AddServices:(id)sender {

    if (txtServices.text.length > 0) {
        if (arrServicesLocalList.count > 0) {
            
            NSMutableArray *arrTempServices = [NSMutableArray new];
            for (int i = 0; i < arrServicesLocalList.count; i++) {
                NSDictionary *dict = [arrServicesLocalList objectAtIndex:i];
                if ([[dict objectForKey:@"category_name"] isEqualToString:txtServices.text]) {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"This service already in your list" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    }];
                    
                    [alertController addAction:okAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                    [alertController.view setTintColor:COLOR_THEME];
                } else {
                    [arrTempServices addObject:dict];
                }
            }
            [arrServicesLocalList removeAllObjects];
            [arrServicesLocalList addObjectsFromArray:arrTempServices];
            [arrServicesLocalList insertObject:dictService atIndex:arrServicesLocalList.count];
        } else {
            [arrServicesLocalList addObject:dictService];
        }
        [collServicesList reloadData];
        [self updateHeaderSize];
        [self connectionForGetCategoryServiceList:txtServices.text];
    } else {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:@"Please Select Category name"];
    }
}

- (IBAction)SaveServices:(id)sender {
    [self connectionForSaveServiceList];
}
@end
