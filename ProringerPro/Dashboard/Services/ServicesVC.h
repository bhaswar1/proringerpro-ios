//
//  ServicesVC.h
//  ProringerPro
//
//  Created by Soma Halder on 27/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServicesVC : ProRingerProBaseVC <UICollectionViewDataSource, UICollectionViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collServicesList;
@property (weak, nonatomic) IBOutlet UITableView *tblServices;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerServiceList;

@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UITextField *txtServices;

- (IBAction)AddServices:(id)sender;
- (IBAction)SaveServices:(id)sender;

@end

NS_ASSUME_NONNULL_END
