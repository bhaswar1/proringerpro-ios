//
//  QuickReplyVC.m
//  ProringerPro
//
//  Created by Soma Halder on 25/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "QuickReplyVC.h"

@interface QuickReplyVC ()
{
    NSString *strReplyMsg;
    int iReplyEnable;
    NSMutableArray *arrRestrictedWordsFound;
}

@end

@implementation QuickReplyVC
@synthesize txtQuickReply, btnCheckBox, viewShadow, lblQuickReply, viewAccessory;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"QUICK REPLY";
    
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
//    self.tabBarController.tabBar.hidden = NO;
    
//    NSString *htmlString = @"Hi [first name],<br/><br/> We are very interested in the project that you have posted ProRinger. We would love to have the opportunity to discuss this project in more details. <br/><br/> How soon are you looking in start? <br/><br/> Thanks,<br/>Subhadeep Royy<br/>General Roofing Company"; // [[result objectForKey:@"info_array"] objectForKey:@"placeholder"];
//    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
//    txtQuickReply.text = htmlString;
    txtQuickReply.delegate = self;
    
    txtQuickReply.inputAccessoryView = viewAccessory;
    [self shadow:viewShadow];
    arrRestrictedWordsFound = [[NSMutableArray alloc] init];
    [self connectionForGetQuickReply];
}

-(void)rightBarButtonPressed:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchProjectVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SearchProjectVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

-(void)shadow:(UIView *)subview {
    
    UIColor *color = [UIColor blackColor];
    subview.layer.shadowColor = [color CGColor];
    subview.layer.shadowRadius = 1.5f;
    subview.layer.shadowOpacity = 0.2;
    subview.layer.shadowOffset = CGSizeZero;
    subview.layer.masksToBounds = NO;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
//    textView.layer.borderColor = COLOR_THEME.CGColor;
//    textView.layer.borderWidth = 1.5f;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    [arrRestrictedWordsFound removeAllObjects];
    NSString *strText = [textView.text stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    NSArray *arrNewWords = [strText componentsSeparatedByString:@" "];
//    NSLog(@"WORDS ARRAY FROM TEXTVIEW:::...%@", arrNewWords);
    arrRestrictedWordsFound = [self doArraysContainTheSameObjects:arrNewWords];
    
    NSLog(@"KHISTI IN TEXTVIEW:::...%@", arrRestrictedWordsFound);
    
    if (![txtQuickReply hasText]) {
        lblQuickReply.hidden = NO;
    }
}

-(NSString *)JSONString:(NSString *)aString {
    NSMutableString *s = [NSMutableString stringWithString:aString];
    [s replaceOccurrencesOfString:@"\"" withString:@"\\\"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"/" withString:@"\\/" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\n" withString:@"\\n" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\b" withString:@"\\b" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\f" withString:@"\\f" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\r" withString:@"\\r" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\t" withString:@"\\t" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    return [NSString stringWithString:s];
}

#pragma mark - Web Service
-(void)connectionForGetQuickReply {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_QUICK_REPLY];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        NSLog(@"response from GET QUICK REPLY:::...%@", response);
        [YXSpritesLoadingView dismiss];
        if ([[response objectForKey:@"response"] boolValue] == YES) {
            
            NSString *htmlString = [[response objectForKey:@"info_array"] objectForKey:@"message"];
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\n " withString:@"\n"];
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
            self->txtQuickReply.text = htmlString;
            
            self->iReplyEnable = [[[response objectForKey:@"info_array"] objectForKey:@"reply_enabled"] intValue];
            
            UIImage *btnImage;

            if (self->iReplyEnable == 1) {
                btnImage = [UIImage imageNamed:@"CheckBoxFull"];
            } else {
                btnImage = [UIImage imageNamed:@"CheckBoxEmpty"];
            }
            [self->btnCheckBox setImage:btnImage forState:UIControlStateNormal];
        }
    }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             NSLog(@"ERROR:::...%@", task.error);
             [YXSpritesLoadingView dismiss];
         }];
}

-(void)connectionForSaveQuickReply {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_QUICK_REPLY_SAVE];
    NSString *str = [NSString stringWithFormat:@"%@", txtQuickReply.text];
//    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
//    id jsonOutput = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//    NSString * test = [jsonOutput objectForKey:@"ID"];
    
    [self JSONString:str];
    
//    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"message": [self JSONString:str],
                             @"quick_reply_enabled": [NSString stringWithFormat:@"%d", iReplyEnable]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        NSLog(@"response from QUICK REPLY:::...%@", response);
        [YXSpritesLoadingView dismiss];
        if ([[response objectForKey:@"response"] boolValue] == YES) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[response objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [alertController addAction:okAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        }
    }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              NSLog(@"ERROR:::...%@", task.error);
              [YXSpritesLoadingView dismiss];
          }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIButton Action
- (IBAction)SaveMessage:(id)sender {
    
    if (txtQuickReply.text.length > 0) {
        if ([[AppManager sharedDataAccess] detectType:txtQuickReply.text]) {
            [[[UIAlertView alloc]initWithTitle:@"" message:self.strRestrictionMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            [txtQuickReply becomeFirstResponder];
        } else if (arrRestrictedWordsFound.count > 0) {
            NSString *strKhisti = [arrRestrictedWordsFound componentsJoinedByString:@","];
            strKhisti = [NSString stringWithFormat:@"Invalid term: %@ - Please be respectful of others. Remove and try again", strKhisti];
            
            [[[UIAlertView alloc]initWithTitle:@"" message:strKhisti delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            [txtQuickReply becomeFirstResponder];
        } else {
            [self connectionForSaveQuickReply];
        }
    }
}

- (IBAction)CheckBox:(id)sender {
    
    UIImage *btnImage;
    
    if (iReplyEnable == 1) {
        iReplyEnable = 0;
        btnImage = [UIImage imageNamed:@"CheckBoxEmpty"];
    } else {
        iReplyEnable = 1;
         btnImage = [UIImage imageNamed:@"CheckBoxFull"];
    }
    [btnCheckBox setImage:btnImage forState:UIControlStateNormal];
}
@end
