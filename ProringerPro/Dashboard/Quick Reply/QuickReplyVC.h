//
//  QuickReplyVC.h
//  ProringerPro
//
//  Created by Soma Halder on 25/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QuickReplyVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITextView *txtQuickReply;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (weak, nonatomic) IBOutlet UIView *viewShadow;
@property (weak, nonatomic) IBOutlet UILabel *lblQuickReply;

@property (strong, nonatomic) IBOutlet UIView *viewAccessory;

- (IBAction)SaveMessage:(id)sender;
- (IBAction)CheckBox:(id)sender;

@end

NS_ASSUME_NONNULL_END
