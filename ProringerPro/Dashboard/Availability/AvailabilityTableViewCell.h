//
//  AvailabilityTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 28/05/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AvailabilityTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblAvailabilityDate;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfDays;
@property (weak, nonatomic) IBOutlet UILabel *lblReasonForLeave;

@property (weak, nonatomic) IBOutlet UIButton *btnEditLeave;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteLeave;

-(void)configureCellWith:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
