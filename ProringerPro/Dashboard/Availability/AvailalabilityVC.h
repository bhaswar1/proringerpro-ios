//
//  AvailalabilityVC.h
//  ProringerPro
//
//  Created by Soma Halder on 27/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AvailalabilityVC : ProRingerProBaseVC <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblAvailability;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constSaveButtonHeight;

-(IBAction)AddScheduleForUnAvailability:(id)sender;

@end

NS_ASSUME_NONNULL_END
