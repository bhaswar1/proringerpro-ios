//
//  AvailabilityTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 28/05/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "AvailabilityTableViewCell.h"

@interface AvailabilityTableViewCell ()
{
    NSString *newStartDate, *newEndDate;
}

@end


@implementation AvailabilityTableViewCell
@synthesize lblNumberOfDays, lblReasonForLeave, lblAvailabilityDate, btnEditLeave, btnDeleteLeave;



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)configureCellWith:(NSDictionary *)dict {
    
//    02/20/17 - 02/21/17
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yy"];
    NSDate *startDate = [dateFormatter dateFromString:[dict objectForKey:@"star_date"]];
    NSDate *endDate = [dateFormatter dateFromString:[dict objectForKey:@"end_date"]];
//    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
//    [dateFormatter setTimeZone:gmt];
//    NSDate *date = [dateFormatter dateFromString:[dict objectForKey:@"star_date"]];
//
//    NSLog(@"START DATE:::...%@\nEND DATE:::...%@\nDATE:::...%@", [dateFormatter stringFromDate:startDate], [dateFormatter stringFromDate:endDate], date);
//
//    lblAvailabilityDate.text = [NSString stringWithFormat:@"%@ - %@", startDate, endDate];
    
    lblAvailabilityDate.text = [NSString stringWithFormat:@"%@ - %@", [dict objectForKey:@"star_date"], [dict objectForKey:@"end_date"]];
    lblReasonForLeave.text = [dict objectForKey:@"unavailable_reason"];
    lblNumberOfDays.text = [NSString stringWithFormat:@"%@ days", [dict objectForKey:@"total_day"]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
