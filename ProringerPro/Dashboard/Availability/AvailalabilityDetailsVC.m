//
//  AvailalabilityDetailsVC.m
//  ProringerPro
//
//  Created by Soma Halder on 27/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "AvailalabilityDetailsVC.h"
//#import "DSLCalendarMonthSelectorView.h"
//#import "DSLCalendarMonthView.h"
//#import "NSDateFormatter.h"

@interface AvailalabilityDetailsVC ()
{
    NSDate *startDate, *endDate;
    NSMutableArray *arrUnavailabilityReason, *arrUnavailabilityList, *arrRestrictedWordsFound;
    NSString *strReasonId;
    float y;
    BOOL isStart, dateBool;
    UIToolbar *toolBar;
    int iTag;
    NSMutableDictionary *dictCategory;
    NSDateFormatter *monthFormatter, *yearFormatter, *dateFormatter;
}

@end

@implementation AvailalabilityDetailsVC
@synthesize   txtSelectReason, txtReasonDesc, pickerUnavailabilityReason, isEdited, strTitle, strAvailabilityId, lblPlaceHolder, datePicker, txtSelectEndDate, txtSelectStartDate, strUnavabilityReason, strStartDate, strEndDate, strRestrictionMsg;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [datePicker setMinimumDate:[NSDate date]];
    datePicker.backgroundColor = COLOR_TABLE_HEADER;
    
    isStart = YES;
    
//    self.navigationItem.title = @"Availability";
    self.navigationItem.title = strTitle;
    
    arrUnavailabilityReason = arrUnavailabilityList = [[NSMutableArray alloc] init];
    dictCategory = [[NSMutableDictionary alloc] init];
    
    monthFormatter = [[NSDateFormatter alloc] init];
    [monthFormatter setDateFormat:@"MMM dd"];
    yearFormatter = [[NSDateFormatter alloc] init];
    [yearFormatter setDateFormat:@"yyyy"];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    toolBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor redColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    toolBar.items = [NSArray arrayWithObjects:cancelButton, flexibleSpace, doneButton, nil];
    
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    pickerUnavailabilityReason.dataSource = self;
    pickerUnavailabilityReason.delegate = self;
    pickerUnavailabilityReason.backgroundColor = [UIColor whiteColor];
    
    txtSelectReason.delegate = self;
    txtReasonDesc.delegate = self;
    txtSelectStartDate.delegate = self;
    txtSelectEndDate.delegate = self;
    
    [self shadowForTextField:txtSelectReason];
    [self shadowForTextField:txtSelectStartDate];
    [self shadowForTextField:txtSelectEndDate];
    
    [self paddingForTextField:txtSelectReason];
    [self paddingForTextField:txtSelectStartDate];
    [self paddingForTextField:txtSelectEndDate];
    
    [self shadowForTextView:txtReasonDesc];
    [self paddingForTextView:txtReasonDesc];
    
    
    [txtSelectReason setInputView:pickerUnavailabilityReason];
    [txtSelectStartDate setInputView:datePicker];
    [txtSelectEndDate setInputView:datePicker];
    
    txtSelectStartDate.inputAccessoryView = txtSelectEndDate.inputAccessoryView = txtSelectReason.inputAccessoryView = toolBar;
    
    [self connectionForGetUnavailabilityReason];
    
    if (isEdited) {
        txtSelectReason.text = strUnavabilityReason;
        
        startDate = [dateFormatter dateFromString:strStartDate];
        endDate = [dateFormatter dateFromString:strEndDate];
        
        txtSelectStartDate.text = [NSString stringWithFormat:@"%@th, %@", [monthFormatter stringFromDate:startDate], [yearFormatter stringFromDate:startDate]];
        txtSelectEndDate.text = [NSString stringWithFormat:@"%@th, %@", [monthFormatter stringFromDate:endDate], [yearFormatter stringFromDate:endDate]];

        [self connectionForGetUnavailabilityEditList];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
}

#pragma mark - UITextfield Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    iTag = (int)textField.tag;
    
    if (isEdited) {
        if (textField.tag == 2) {
            [datePicker setMinimumDate:endDate];
        } else if (textField.tag == 1) {
            [datePicker setMinimumDate:startDate];
        }
    } else {
        if (textField.tag == 2) {
            if (startDate != nil) {
                [datePicker setMinimumDate:startDate];
            }
        } else if (textField.tag == 1) {
            [datePicker setMinimumDate:[NSDate date]];
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    
}

#pragma mark - UITextView Delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@""]) {
        
        NSLog(@"%@",textView.text);
        
        if (textView.text.length == 1) {
            lblPlaceHolder.hidden = NO;
        }
    } else if ([NSString stringWithFormat:@"%@%@",textView.text,text].length > 0) {
        
        lblPlaceHolder.hidden = YES;
    } else {
        lblPlaceHolder.hidden = NO;
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    [self paddingForTextView:textView];
    [self shadowForTextView:textView];
    
    [arrRestrictedWordsFound removeAllObjects];
    
    NSString *strText = [textView.text stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    NSArray *arrNewWords = [strText componentsSeparatedByString:@" "];
    NSLog(@"WORDS ARRAY FROM TEXTVIEW:::...%@", arrNewWords);
    arrRestrictedWordsFound = [self doArraysContainTheSameObjects:arrNewWords];
    
    if ([textView.text isEqualToString:@""]) {
        lblPlaceHolder.hidden = NO;
    } else {
        lblPlaceHolder.hidden = YES;
    }
}

#pragma mark - Validation
-(BOOL)validationTextfield {
    
    BOOL isValid = YES;
    NSString *strMsg = @"";
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtSelectStartDate.text]) {
        isValid = NO;
        strMsg = @"Please select start date";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtSelectEndDate.text]) {
        isValid = NO;
        strMsg = @"Please select end date";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtSelectReason.text]) {
        isValid = NO;
        strMsg = @"Please select your reason";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtReasonDesc.text]) {
        isValid = NO;
        strMsg = @"Please enter message";
    } else if ([[AppManager sharedDataAccess] detectType:txtReasonDesc.text]) {
        isValid = NO;
        strMsg = self.strRestrictionMsg;
    } else if (arrRestrictedWordsFound.count > 0) {
        NSString *strKhisti = [arrRestrictedWordsFound componentsJoinedByString:@","];
        strMsg = [NSString stringWithFormat:@"Invalid term: %@ - Please be respectful of others. Remove and try again", strKhisti];
        [txtReasonDesc becomeFirstResponder];
        isValid = NO;
    }
    
    if (strMsg.length > 0) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strMsg];
    }
    
    return isValid;
}

#pragma mark - UIToolBar Button
-(void)doneButtonPressed {
    
    if (iTag == 0) {
        if (dictCategory == nil) {
            dictCategory = [arrUnavailabilityReason objectAtIndex:0];
        }
        strReasonId = [dictCategory objectForKey:@"id"];
        txtSelectReason.text = [dictCategory objectForKey:@"value"];
    } else if (iTag == 1) {
        startDate = [datePicker date];
        txtSelectStartDate.text = [NSString stringWithFormat:@"%@th, %@", [monthFormatter stringFromDate:[datePicker date]], [yearFormatter stringFromDate:[datePicker date]]];
//        txtSelectStartDate.text = [dateFormatter stringFromDate:startDate];
    } else if (iTag == 2) {
        endDate = [datePicker date];
        txtSelectEndDate.text = [NSString stringWithFormat:@"%@th, %@", [monthFormatter stringFromDate:[datePicker date]], [yearFormatter stringFromDate:[datePicker date]]];
//        txtSelectStartDate.text = [dateFormatter stringFromDate:endDate];
    }
    [self cancelButtonPressed];
}

-(void)cancelButtonPressed {

    [txtSelectReason resignFirstResponder];
    [txtSelectStartDate resignFirstResponder];
    [txtSelectEndDate resignFirstResponder];
}

#pragma mark - UIPickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return arrUnavailabilityReason.count;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 50)];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRed:241.0f/255 green:89.0f/255 blue:42.0f/255 alpha:1];
    label.font = [UIFont fontWithName:@"OpenSans" size:18];
    label.numberOfLines = 2;
    
    label.text = [[arrUnavailabilityReason objectAtIndex:row] objectForKey:@"value"];
    
    return label;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [[arrUnavailabilityReason objectAtIndex:row] objectForKey:@"value"];
}

#pragma mark - UIPickerView Delegate
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    
    return 50;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
//    txtSelectReason.text = [[arrUnavailabilityReason objectAtIndex:row] objectForKey:@"value"];
//    strReason = [[arrUnavailabilityReason objectAtIndex:row] objectForKey:@"value"];
//    NSLog(@"%@",strReason);
    dictCategory = [arrUnavailabilityReason objectAtIndex:row];
//    strReasonId = [[arrUnavailabilityReason objectAtIndex:row] objectForKey:@"id"];
//    [txtSelectReason resignFirstResponder];
//    pickerUnavailabilityReason.hidden = YES;
}

#pragma mark - UIButton Action
-(IBAction)AddScheduleForUnAvailability:(id)sender {
    if ([self validationTextfield]) {
        [self connectionForSaveUnavailabilityReason];
    }
}

- (IBAction)DropDownButtonPressed:(id)sender {
    [self textFieldDidBeginEditing:txtSelectReason];
}

- (IBAction)StartDate:(id)sender {
    
    [self textFieldDidBeginEditing:txtSelectStartDate];
}

- (IBAction)EndDate:(id)sender {
    
    [self textFieldDidBeginEditing:txtSelectEndDate];
}

-(void)rightBarButtonPressed:(id)sender {
    
    NSLog(@"Right button pressed from Dashboard class");
    [self tapToDismissView];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchProjectVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SearchProjectVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

#pragma mark - Web Service
-(void)connectionForGetUnavailabilityReason {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_UNAVAILABILITY_REASON];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
//        NSLog(@"response from update password:::...%@", response);
        [YXSpritesLoadingView dismiss];
        if ([[response objectForKey:@"response"] boolValue] == YES) {
            [self->arrUnavailabilityReason removeAllObjects];
            [self->arrUnavailabilityReason addObjectsFromArray:[response objectForKey:@"unavailability_reason"]];
            [self->pickerUnavailabilityReason reloadAllComponents];
        }
    }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              NSLog(@"ERROR:::...%@", task.error);
              [YXSpritesLoadingView dismiss];
          }];
}

-(void)connectionForSaveUnavailabilityReason {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_SAVE_UNAVAILABILITY];
    
    NSLog(@"USER ID:::...%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]);
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"start_date": [dateFormatter stringFromDate:startDate],
                             @"end_date": [dateFormatter stringFromDate:endDate],
                             @"reason": txtReasonDesc.text,
                             @"reason_id": strReasonId,
                             @"availability_id": [NSString stringWithFormat:@"%@", strAvailabilityId]
                                 };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        NSLog(@"response from SAVE AVAIBILITY:::...%@", response);
        [YXSpritesLoadingView dismiss];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[response objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if ([[response objectForKey:@"response"] boolValue] == YES) {
            self->txtReasonDesc.text = @"";
            self->strStartDate = self->strEndDate = @"";
            [self.navigationController popViewControllerAnimated:YES];
            }
        }];
            
        [alertController addAction:okAction];
        [alertController.view setTintColor:COLOR_THEME];
        [self presentViewController:alertController animated:YES completion:nil];
    }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             NSLog(@"ERROR:::...%@", task.error);
             [YXSpritesLoadingView dismiss];
         }];
}

-(void)connectionForGetUnavailabilityEditList {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_UNAVAILABILITY_EDIT_LIST];
    
    NSDictionary *parms = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                            @"availability_id": strAvailabilityId
                            };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager GET:url parameters:parms progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        NSLog(@"response from UNAVAILABLE DETAILS:::...%@", response);
        [YXSpritesLoadingView dismiss];
        if ([[response objectForKey:@"response"] boolValue] == YES) {
            
            self->txtSelectReason.text = [[[[response objectForKey:@"info_array"] objectForKey:@"availability"] objectAtIndex:0] objectForKey:@"unavailable_reason"];
            self->txtReasonDesc.text = [[[[response objectForKey:@"info_array"] objectForKey:@"availability"] objectAtIndex:0] objectForKey:@"reason"];
            self->strReasonId = [[[[response objectForKey:@"info_array"] objectForKey:@"availability"] objectAtIndex:0] objectForKey:@"reason_id"];
            self->strAvailabilityId = [[[[response objectForKey:@"info_array"] objectForKey:@"availability"] objectAtIndex:0] objectForKey:@"availability_id"];
            [self textViewDidEndEditing:self->txtReasonDesc];
        }
    }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             NSLog(@"ERROR:::...%@", task.error);
             [YXSpritesLoadingView dismiss];
         }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
