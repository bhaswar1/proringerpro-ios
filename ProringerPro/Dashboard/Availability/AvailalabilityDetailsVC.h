//
//  AvailalabilityDetailsVC.h
//  ProringerPro
//
//  Created by Soma Halder on 27/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "DSLCalendarView.h"

NS_ASSUME_NONNULL_BEGIN

@interface AvailalabilityDetailsVC : ProRingerProBaseVC

//@property (weak, nonatomic) IBOutlet DSLCalendarView *viewCalendar;
//@property (nonatomic, weak) GLCalendarDateRange *rangeUnderEdit;

@property (weak, nonatomic) IBOutlet UITextField *txtSelectReason;
@property (weak, nonatomic) IBOutlet UITextField *txtSelectStartDate;
@property (weak, nonatomic) IBOutlet UITextField *txtSelectEndDate;
@property (weak, nonatomic) IBOutlet UITextView *txtReasonDesc;

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceHolder;

@property (weak, nonatomic) IBOutlet UIPickerView *pickerUnavailabilityReason;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (strong, nonatomic) NSString *strTitle, *strAvailabilityId, *strStartDate, *strEndDate, *strUnavabilityReason;

@property (assign) BOOL isEdited;

- (IBAction)AddScheduleForUnAvailability:(id)sender;
- (IBAction)DropDownButtonPressed:(id)sender;
- (IBAction)StartDate:(id)sender;
- (IBAction)EndDate:(id)sender;

@end

NS_ASSUME_NONNULL_END
