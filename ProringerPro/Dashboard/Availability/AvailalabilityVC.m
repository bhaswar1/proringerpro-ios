//
//  AvailalabilityVC.m
//  ProringerPro
//
//  Created by Soma Halder on 27/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "AvailalabilityVC.h"

@interface AvailalabilityVC ()
{
    NSMutableArray *arrAvailabilityList;
   // NSString *newStartDate;
    AvailabilityTableViewCell *cell;
    NSString *newStartDate, *newEndDate;
    
}

@end

@implementation AvailalabilityVC
@synthesize tblAvailability, constSaveButtonHeight;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self connectionForGetDashboardData];
    
    self.navigationItem.title = @"AVAILABILITY";
    
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
//    self.tabBarController.tabBar.hidden = NO;
    
    tblAvailability.dataSource = self;
    tblAvailability.delegate = self;
    
    arrAvailabilityList = [[NSMutableArray alloc] init];
    [arrAvailabilityList removeAllObjects];
    
    [self connectionForGetAvailabilityList];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [arrAvailabilityList removeAllObjects];
    [tblAvailability reloadData];
    [self connectionForGetAvailabilityList];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(counterUpdate) name:@"reload_data" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) counterUpdate {
    
    arrAvailabilityList = [[NSMutableArray alloc] init];
    [arrAvailabilityList removeAllObjects];
    
    [self connectionForGetAvailabilityList];
}

-(void)MessageNotificationTap {
    
    [self.appDelegate MessageNotificationTap];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Availability"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Availability"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }

}

#pragma mark - NavigationBar Button
-(void)rightBarButtonPressed:(id)sender {
    
    NSLog(@"Right button pressed from Dashboard class");
    [self tapToDismissView];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchProjectVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SearchProjectVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

#pragma mark - UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrAvailabilityList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellAvailability";
    
    NSDictionary *dictAvailability = [arrAvailabilityList objectAtIndex:indexPath.row];
    
    cell = (AvailabilityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell configureCellWith:dictAvailability];
    
    cell.btnDeleteLeave.tag = indexPath.row;
    cell.btnEditLeave.tag = indexPath.row;
    
    [cell.btnDeleteLeave addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnEditLeave addTarget:self action:@selector(editButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80;
}

#pragma mark - UITableView Button
-(void)editButtonPressed:(UIButton *)sender {
   // NSInteger row = [NSIndexPath.row];
    
    NSLog(@"%ld",sender.tag);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    AvailalabilityDetailsVC *advc = [storyboard instantiateViewControllerWithIdentifier:@"AvailalabilityDetailsVC"];
    advc.isEdited = YES;
    advc.strTitle = @"EDIT AVAILABILITY";
    advc.strStartDate = [[arrAvailabilityList objectAtIndex:sender.tag] objectForKey:@"star_date"];
    advc.strEndDate = [[arrAvailabilityList objectAtIndex:sender.tag] objectForKey:@"end_date"];
    advc.strAvailabilityId = [[arrAvailabilityList objectAtIndex:sender.tag] objectForKey:@"availability_id"];
    advc.strUnavabilityReason = [[arrAvailabilityList objectAtIndex:sender.tag] objectForKey:@"unavailable_reason"];
    [self.navigationController pushViewController:advc animated:YES];
}

-(void)deleteButtonPressed:(UIButton *)sender {
    
    NSDictionary *dictAvailability = [arrAvailabilityList objectAtIndex:sender.tag];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Do you want to delete this from list" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"YES, DELETE IT" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [self connectionForDeleteAvailability:[dictAvailability objectForKey:@"availability_id"]];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [alertController.view setTintColor:COLOR_THEME];
    [self presentViewController:alertController animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Web Service
-(void)connectionForGetAvailabilityList {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_AVAILABILITY_LIST];
    
    NSDictionary *param = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]
                            };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"GET AVAILABILITY LIST RESPONSE::::....%@", responseObject);
        [self->arrAvailabilityList removeAllObjects];
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [self->arrAvailabilityList addObjectsFromArray:[[responseObject objectForKey:@"info_array"] objectForKey:@"availability"]];
            
            if (self->arrAvailabilityList.count > 0) {
                self->constSaveButtonHeight.constant = 45.0;
            } else {
                self->constSaveButtonHeight.constant = 0.0;
            }
        }
        [self->tblAvailability reloadData];
        
        self.appDelegate.MessageNotificationgesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MessageNotificationTap)];
        [self.appDelegate.notificationView addGestureRecognizer:self.appDelegate.MessageNotificationgesture];
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForDeleteAvailability:(NSString *)strAvailabilityId {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_AVAILABILITY_DELETE];
    
    NSDictionary *param = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                            @"availability_id": strAvailabilityId
                            };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"GET AVAILABILITY DELETE RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                
                [self connectionForGetAvailabilityList];
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
            alert.view.tintColor = COLOR_THEME;
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

#pragma mark - UIButton Action
-(IBAction)AddScheduleForUnAvailability:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    AvailalabilityDetailsVC *advc = [storyboard instantiateViewControllerWithIdentifier:@"AvailalabilityDetailsVC"];
    advc.isEdited = NO;
    advc.strTitle = @"ADD AVAILABILITY";
    advc.strAvailabilityId = @"";
    [self.navigationController pushViewController:advc animated:YES];
}

@end
