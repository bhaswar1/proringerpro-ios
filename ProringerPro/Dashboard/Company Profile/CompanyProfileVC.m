//
//  CompanyProfileVC.m
//  ProringerPro
//
//  Created by Soma Halder on 23/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "CompanyProfileVC.h"
#import "IQKeyboardManager.h"

@interface CompanyProfileVC ()
{
    NSMutableArray *arrBusinessType, *arrRestrictedWordsFound;
    
    NSString *strAccName, *strLat, *strLan, *strCountry, *strZipCode, *strCity, *strState, *strStreet, *strTimeZone, *strBusinessTypeId, *strPhoneNumber;
    UIToolbar *toolBar;
    AppDelegate *appDelegate;
    NSMutableDictionary *dictService;
    BOOL isShouldShowAlert;
}

@end

@implementation CompanyProfileVC
@synthesize txtCity, txtEmail, txtState, txtWebsite, txtZipCode, txtCompanyName, txtNumberOfEmp, txtPhoneNumber, txtBusinessDesc, txtStreetAddress, txtTypeOfBusiness, lblPlaceHolder, viewAccessory, pickerBusinessTypes;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    self.navigationItem.title = @"Company Profile";
    
//    self.tabBarController.tabBar.hidden = NO;
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    toolBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor redColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    toolBar.items = [NSArray arrayWithObjects:cancelButton, flexibleSpace, doneButton, nil];
    
    txtCompanyName.delegate = self;
    txtEmail.delegate = self;
    txtWebsite.delegate = self;
    txtPhoneNumber.delegate = self;
    txtTypeOfBusiness.delegate = self;
    txtNumberOfEmp.delegate = self;
    txtStreetAddress.delegate = self;
    txtZipCode.delegate = self;
    txtCity.delegate = self;
    txtState.delegate = self;
    txtBusinessDesc.delegate = self;
    
    [self paddingForTextField:txtTypeOfBusiness];
    [self paddingForTextField:txtStreetAddress];
    [self paddingForTextField:txtPhoneNumber];
    [self paddingForTextField:txtNumberOfEmp];
    [self paddingForTextField:txtCompanyName];
    [self paddingForTextField:txtZipCode];
    [self paddingForTextField:txtWebsite];
    [self paddingForTextField:txtState];
    [self paddingForTextField:txtEmail];
    [self paddingForTextField:txtCity];
    
    [self shadowForTextField:txtTypeOfBusiness];
    [self shadowForTextField:txtStreetAddress];
    [self shadowForTextField:txtPhoneNumber];
    [self shadowForTextField:txtNumberOfEmp];
    [self shadowForTextField:txtCompanyName];
    [self shadowForTextField:txtZipCode];
    [self shadowForTextField:txtWebsite];
    [self shadowForTextField:txtState];
    [self shadowForTextField:txtEmail];
    [self shadowForTextField:txtCity];
    
    [self paddingForTextView:txtBusinessDesc];
    [self shadowForTextView:txtBusinessDesc];
    
    txtBusinessDesc.inputAccessoryView = txtEmail.inputAccessoryView = txtWebsite.inputAccessoryView = txtPhoneNumber.inputAccessoryView = txtNumberOfEmp.inputAccessoryView = txtCompanyName.inputAccessoryView = viewAccessory;
    
    txtTypeOfBusiness.inputView = pickerBusinessTypes;
    txtTypeOfBusiness.inputAccessoryView = toolBar;
    
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    pickerBusinessTypes.dataSource = self;
    pickerBusinessTypes.delegate = self;
    
    arrBusinessType = [[NSMutableArray alloc] init];
    
    lblPlaceHolder.hidden = NO;
    txtBusinessDesc.backgroundColor = [UIColor clearColor];
    
    [self connectionForGetCompanyDetails];
    [self connectionForGetBusinessTypeList];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
//    if (self.iPremiumStatus > 0) {
//        if ([[self.dictInfoArray objectForKey:@"Pro_verified"] isEqualToString:@"A"]) {
////            txtPhoneNumber.userInteractionEnabled = txtStreetAddress.userInteractionEnabled = NO;
//            isShouldShowAlert = YES;
//        } else {
////            txtPhoneNumber.userInteractionEnabled = txtStreetAddress.userInteractionEnabled = YES;
//            isShouldShowAlert = NO;
//        }
//    } else {
////        txtPhoneNumber.userInteractionEnabled = txtStreetAddress.userInteractionEnabled = YES;
//        isShouldShowAlert = NO;
//    }
    
    if ([[self.dictInfoArray objectForKey:@"Pro_verified"] isEqualToString:@"A"]) {
        txtStreetAddress.userInteractionEnabled = NO;
        txtPhoneNumber.userInteractionEnabled = NO;
        isShouldShowAlert = NO;
    } else if ([[self.dictInfoArray objectForKey:@"Pro_verified"] isEqualToString:@"Y"]) {
        isShouldShowAlert = YES;
    } else if ([[self.dictInfoArray objectForKey:@"Pro_verified"] isEqualToString:@"N"]) {
        txtStreetAddress.userInteractionEnabled = YES;
        txtPhoneNumber.userInteractionEnabled = YES;
        isShouldShowAlert = NO;
    }
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Company"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Company"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
}

#pragma mark - UIToolBar Button
-(void) doneButtonPressed {
    
    if (dictService != nil) {
        txtTypeOfBusiness.text = [dictService objectForKey:@"value"];
        [txtTypeOfBusiness resignFirstResponder];
    }
}

-(void) cancelButtonPressed {
    
    [txtTypeOfBusiness resignFirstResponder];
}

#pragma mark - UITextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    textField.layer.borderColor = COLOR_THEME.CGColor;

    if (textField == txtPhoneNumber) {
        if (isShouldShowAlert) {
            [textField resignFirstResponder];
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Are you sure you want to change your phone number?" message:@"Your phone number have been verified. Changing this information on your account will result in losing your verified status. No worries, just complete the verification process with the new information to reinstate your verified status." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self->isShouldShowAlert = NO;
                [textField becomeFirstResponder];
            }];
            UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [textField resignFirstResponder];
            }];
            
            [alertController addAction:yesAction];
            [alertController addAction:noAction];
            [alertController.view setTintColor:COLOR_THEME];
            [self presentViewController:alertController animated:YES completion:nil];
        } else {
            [textField becomeFirstResponder];
        }
        
    } else if (textField == txtStreetAddress) {
        
        if (isShouldShowAlert) {
            [textField resignFirstResponder];
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Are you sure you want to change your address?" message:@"Your address have been verified. Changing this information on your account will result in losing your verified status. No worries, just complete the verification process with the new information to reinstate your verified status." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self->isShouldShowAlert = NO;
                [self performSegueWithIdentifier:@"zipCode" sender:nil];
            }];
            UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            }];
            
            [alertController addAction:yesAction];
            [alertController addAction:noAction];
            [alertController.view setTintColor:COLOR_THEME];
            [self presentViewController:alertController animated:YES completion:nil];
            
            [self performSegueWithIdentifier:@"zipCode" sender:nil];
        } else {
            [self performSegueWithIdentifier:@"zipCode" sender:nil];
        }
    } else {
        [textField becomeFirstResponder];
        [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == txtCompanyName) {
        if (textField.text.length >= 40 && range.length == 0) {
            [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:@"You have reached Maximum length of Characters."];
            return NO; // return NO to not change text
        } else {
            return YES;
        }
    } else if (textField == txtPhoneNumber) {
        NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        BOOL deleting = [newText length] < [textField.text length];
        
        NSString *stripppedNumber = [newText stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [newText length])];
        NSUInteger digits = [stripppedNumber length];
        strPhoneNumber = stripppedNumber;
        if (digits > 10)
            stripppedNumber = [stripppedNumber substringToIndex:10];
        
        UITextRange *selectedRange = [textField selectedTextRange];
        NSInteger oldLength = [textField.text length];
        
        if (digits == 0)
            textField.text = @"";
        else if (digits < 3 || (digits == 3 && deleting))
            textField.text = [NSString stringWithFormat:@"(%@", stripppedNumber];
        else if (digits < 6 || (digits == 6 && deleting))
            textField.text = [NSString stringWithFormat:@"(%@) %@", [stripppedNumber substringToIndex:3], [stripppedNumber substringFromIndex:3]];
        else
            textField.text = [NSString stringWithFormat:@"(%@) %@-%@", [stripppedNumber substringToIndex:3], [stripppedNumber substringWithRange:NSMakeRange(3, 3)], [stripppedNumber substringFromIndex:6]];
        
        UITextPosition *newPosition = [textField positionFromPosition:selectedRange.start offset:[textField.text length] - oldLength];
        UITextRange *newRange = [textField textRangeFromPosition:newPosition toPosition:newPosition];
        [textField setSelectedTextRange:newRange];
        
        return NO;
    } else {
        return YES;
    }
}

#pragma mark - UITextView Delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@""]) {
        
        NSLog(@"%@",textView.text);
        
        if (textView.text.length==1) {
            lblPlaceHolder.hidden = NO;
        }
    } else if ([NSString stringWithFormat:@"%@%@",textView.text,text].length > 0) {
        
        lblPlaceHolder.hidden = YES;
    } else {
        lblPlaceHolder.hidden = NO;
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    [self paddingForTextView:textView];
    [self shadowForTextView:textView];
    
    [arrRestrictedWordsFound removeAllObjects];
    NSString *strText = [textView.text stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    NSArray *arrNewWords = [strText componentsSeparatedByString:@" "];
    NSLog(@"WORDS ARRAY FROM TEXTVIEW:::...%@", arrNewWords);
    arrRestrictedWordsFound = [self doArraysContainTheSameObjects:arrNewWords];
    
    NSLog(@"KHISTI IN TEXTVIEW:::...%@", arrRestrictedWordsFound);
    
    if (![txtBusinessDesc hasText]) {
        lblPlaceHolder.hidden = NO;
    }
}

#pragma mark - UIPickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return arrBusinessType.count;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 50)];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRed:241.0f/255 green:89.0f/255 blue:42.0f/255 alpha:1];
    label.font = [UIFont fontWithName:@"OpenSans" size:18];
    label.numberOfLines = 2;
    
    label.text = [[arrBusinessType objectAtIndex:row] objectForKey:@"value"];
    
    return label;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [[arrBusinessType objectAtIndex:row] objectForKey:@"value"];
}

#pragma mark - UIPickerView Delegate
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 50;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    txtTypeOfBusiness.text = [[arrBusinessType objectAtIndex:row] objectForKey:@"value"];
    
    dictService = [[NSMutableDictionary alloc] init];
    dictService = [[arrBusinessType objectAtIndex:row] mutableCopy];
    
//    [txtTypeOfBusiness resignFirstResponder];
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"zipCode"]) {
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        SearchZipVC *szvc = segue.destinationViewController;
        self.providesPresentationContextTransitionStyle = YES;
        self.definesPresentationContext = YES;
        szvc.deleagte = self;
        [szvc setModalPresentationStyle:UIModalPresentationCurrentContext];
        szvc.strNavigationTitle = @"Business Address";
        szvc.strAddressTitle = @"Business Street Address";
        szvc.strPlaceHolder = @"Street address";
        szvc.strApi = @"geocode";
        szvc.strSearchText = @"postal_code";
        szvc.strSearchComponent = @"short_name";
    }
}

#pragma mark - Protocol Method
-(void)sendAddressToUserInfo:(NSDictionary *)dictAddress {
    
    NSArray *arrAddressComponent = [dictAddress objectForKey:@"address_components"];
    
    NSLog(@"Address::...%@", dictAddress);
    
    strZipCode = strCountry = strState = strCity = strStreet = @"";
    NSString *strStreetNumber;
    
    for (int i = 0; i < arrAddressComponent.count; i++) {
        NSDictionary *dictComponent = [arrAddressComponent objectAtIndex:i];
        NSArray *arrTypes = [dictComponent objectForKey:@"types"];
        for (int iTypes = 0; iTypes < arrTypes.count; iTypes++) {
            if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"postal_code"]) {
                strZipCode = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"country"]) {
                strCountry = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"administrative_area_level_1"]) {
                strState = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"locality"] || [[arrTypes objectAtIndex:iTypes] isEqualToString:@"sublocality"]) {
                strCity = [dictComponent objectForKey:@"long_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"route"]) {
                strStreet = [dictComponent objectForKey:@"long_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"street_number"]) {
                strStreetNumber = [dictComponent objectForKey:@"long_name"];
            } else if (strStreet.length == 0) {
                if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"administrative_area_level_2"]) {
                    strStreetNumber = [dictComponent objectForKey:@"long_name"];
                }
            }
        }
    }
    NSLog(@"\npostal_code:::...%@\ncountry:::...%@\nstate:::...%@\ncity:::...%@\nstreet:::...%@", strZipCode, strCountry, strState, strCity, strStreet);
    
    if (strStreetNumber.length > 0) {
        strStreet = [NSString stringWithFormat:@"%@ %@", strStreetNumber, strStreet];
    }
        
    txtStreetAddress.text = strStreet;
    txtCity.text = strCity;
    txtState.text = strState;
    txtZipCode.text = strZipCode;
    
    if (strZipCode.length > 0 || strCountry.length > 0) {
        strLat = [[[[dictAddress objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"] stringValue];
        strLan = [[[[dictAddress objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"] stringValue];
        
        double latitude = [strLat doubleValue];
        double longitude = [strLan doubleValue];
        
        [self findTimeZoneFromSelectedAddressWithLat:latitude andLon:longitude];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Enter a valid street address" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//            txtZipCode.text = @"";
            [self performSegueWithIdentifier:@"zipCode" sender:nil];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        alertController.view.tintColor = [UIColor colorWithRed:241/255.0 green:89/255.0 blue:42/255.0 alpha:1];
    }
}

-(void)findTimeZoneFromSelectedAddressWithLat:(double)latitude andLon:(double)longitude {
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        
        self->strTimeZone = placemark.timeZone.name;
    }];
}

#pragma mark - Web Service
-(void)connectionForGetCompanyDetails {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_COMPANY_INFO];
    
    NSDictionary *params = @{@"user_id": [userDefault objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            NSDictionary *dictInfoArray = [[responseObject objectForKey:@"info_array"] objectAtIndex:0];
            
            NSString *htmlString = [dictInfoArray objectForKey:@"business_desc"];
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
            
            if (htmlString.length > 0) {
                self->lblPlaceHolder.hidden = YES;
            } else
                self->lblPlaceHolder.hidden = NO;
            
            self->txtBusinessDesc.text = htmlString;
            self->txtCompanyName.text = [dictInfoArray objectForKey:@"company_name"];
            self->txtEmail.text = [dictInfoArray objectForKey:@"contact_email"];
            self->txtWebsite.text = [dictInfoArray objectForKey:@"company_website"];
            self->txtPhoneNumber.text = [dictInfoArray objectForKey:@"company_phone"];
            self->txtTypeOfBusiness.text = [[dictInfoArray objectForKey:@"business_type"] objectForKey:@"value"];
            self->strBusinessTypeId = [[dictInfoArray objectForKey:@"business_type"] objectForKey:@"id"];
            self->txtNumberOfEmp.text = [dictInfoArray objectForKey:@"#employees"];
            self->txtStreetAddress.text = [dictInfoArray objectForKey:@"str_address"];
            self->txtCity.text = [dictInfoArray objectForKey:@"city"];
            self->txtState.text = [dictInfoArray objectForKey:@"state"];
            self->txtZipCode.text = [dictInfoArray objectForKey:@"zipcode"];
            
            self->strAccName = [dictInfoArray objectForKey:@"acc_name"];
            self->strLat = [dictInfoArray objectForKey:@"latitude"];
            self->strLan = [dictInfoArray objectForKey:@"longitude"];
            self->strCountry = [dictInfoArray objectForKey:@"country_name"];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForGetBusinessTypeList {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_BUSINESS_TYPE];
    
//    NSDictionary *params = @{@"user_id": [userDefault objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [self->arrBusinessType removeAllObjects];
            [self->arrBusinessType addObjectsFromArray:[responseObject objectForKey:@"business"]];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForSaveCompanyDetails {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_SAVE_COMP_INFO];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"desc": txtBusinessDesc.text,
                             @"comp_name": txtCompanyName.text,
                             @"comp_email": txtEmail.text,
                             @"website": txtWebsite.text,
                             @"alt_phone": txtPhoneNumber.text,
                             @"bus_type": strBusinessTypeId,
                             @"num_emp": txtNumberOfEmp.text,
                             @"com_address": txtStreetAddress.text,
                             @"city": txtCity.text,
                             @"country": strCountry,
                             @"state": txtState.text,
                             @"zipcode": txtZipCode.text,
                             @"acc_name": strAccName,
                             @"latitude": strLat,
                             @"longitude": strLan
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"SAVE COMPANY RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                self->strStreet = self->strState = self->strCity = self->strZipCode = self->strCountry = self->strLan = self->strLat = @"";
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

/*
#pragma mark - Protocol Method
-(void)sendAddressToUserInfo:(NSDictionary *)dictAddress withOptional:(NSString *)zipCode {
    
    NSLog(@"DICTIONARY FROM PREVIOUS VC:::...%@", dictAddress);
    
//    txtStreetAddress.text = [dictAddress objectForKey:@"formatted_address"];
    //    strCity = [
    strLat = [[[[dictAddress objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"] stringValue];
    strLan = [[[[dictAddress objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"] stringValue];
//
    double latitude = [strLat doubleValue];
    double longitude = [strLan doubleValue];

    if ([[dictAddress objectForKey:@"address_components"] count] == 4) {

        strZipCode = @"";
        strCountry = [NSString stringWithFormat:@"%@", [[[dictAddress objectForKey:@"address_components"] objectAtIndex:3] objectForKey:@"short_name"]];
        strCity = [NSString stringWithFormat:@"%@", [[[dictAddress objectForKey:@"address_components"] objectAtIndex:0] objectForKey:@"short_name"]];
        strState = [NSString stringWithFormat:@"%@", [[[dictAddress objectForKey:@"address_components"] objectAtIndex:2] objectForKey:@"short_name"]];
    } else if ([[dictAddress objectForKey:@"address_components"] count] > 4) {

        strZipCode = [NSString stringWithFormat:@"%@", [[[dictAddress objectForKey:@"address_components"] objectAtIndex:0] objectForKey:@"short_name"]];
        strCountry = [NSString stringWithFormat:@"%@", [[[dictAddress objectForKey:@"address_components"] objectAtIndex:4] objectForKey:@"short_name"]];
        strCity = [NSString stringWithFormat:@"%@", [[[dictAddress objectForKey:@"address_components"] objectAtIndex:1] objectForKey:@"short_name"]];
        strState = [NSString stringWithFormat:@"%@", [[[dictAddress objectForKey:@"address_components"] objectAtIndex:3] objectForKey:@"short_name"]];
    }
    
    [self findTimeZoneFromSelectedAddressWithLat:latitude andLon:longitude andOptional:zipCode];
}

#pragma mark - TimeZone
-(void)findTimeZoneFromSelectedAddressWithLat:(double)latitude andLon:(double)longitude andOptional:(NSString *)zipCode {
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    [geoCoder reverseGeocodeLocation: location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        
//        self->strTimeZone = [NSString stringWithFormat:@"%@", placemark.timeZone];
        //And to get country name simply.
        NSLog(@"PLACEMARK ARRAY:::...%@", placemarks);
        NSLog(@"Country -%@",placemark.country);
        NSLog(@"Timezone -%@\n%@", placemark.timeZone.name, placemark.timeZone.abbreviation);
        NSLog(@"State -%@\n%@\n%@\n%@\n%@\n\n\n\n%@", placemark.subLocality, placemark.postalCode, placemark.subAdministrativeArea, placemark.administrativeArea, placemark.addressDictionary, placemark);
        
        if (self->strCountry.length == 0) {
            self->strCountry = placemark.country;
        }
        if (self->strCity.length == 0) {
            self->strCity = [placemark.addressDictionary objectForKey:@"City"];
        }
        if (self->strState.length == 0) {
            self->strState = placemark.subAdministrativeArea;
        }
        if (self->strZipCode.length == 0) {
            if (placemark.postalCode == nil) {
                self->strZipCode = zipCode;
            } else {
                self->strZipCode = placemark.postalCode;
            }
        }
//
//        self->strTimeZone = placemark.timeZone.name;
        self->strStreet = [NSString stringWithFormat:@"%@", [placemark.addressDictionary objectForKey:@"Street"]];
        if ([self->strStreet isEqualToString:@""]) {
            self->strStreet = [NSString stringWithFormat:@"%@", [placemark.addressDictionary objectForKey:@"Name"]];
        }
        
        self->txtZipCode.text = self->strZipCode;
        self->txtState.text = self->strState;
        self->txtCity.text = self->strCity;
        self->txtStreetAddress.text = self->strStreet;
    }];
}
*/

#pragma mark - UIButton Action
- (IBAction)SaveCompanyDetails:(id)sender {
    
    [txtCompanyName resignFirstResponder];
    [txtEmail resignFirstResponder];
    [txtWebsite resignFirstResponder];
    [txtPhoneNumber resignFirstResponder];
    [txtNumberOfEmp resignFirstResponder];
    [txtBusinessDesc resignFirstResponder];
    [txtStreetAddress resignFirstResponder];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Confirm company profile update" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([self validateTextFields]) {
            [self connectionForSaveCompanyDetails];
        }
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    [alertController.view setTintColor:COLOR_THEME];
}

- (IBAction)TypeOfBusiness:(id)sender {
    [self textFieldDidBeginEditing:txtTypeOfBusiness];
}

#pragma mark - UITextField Validation
-(BOOL)validateTextFields {
    
    BOOL isValid = YES;
    
    NSString *strMsg = @"";
    
    if (txtEmail.text.length > 0) {
        if (![self validateEmail:txtEmail.text]) {
            strMsg = @"Please enter valid email";
            isValid = NO;
            [txtEmail becomeFirstResponder];
        }
    } else {
        strMsg = @"Please enter email";
        isValid = NO;
        [txtEmail becomeFirstResponder];
    }
    
    if (txtWebsite.text.length == 0) {
        strMsg = @"Please enter company website";
        isValid = NO;
        [txtWebsite becomeFirstResponder];
    } else if (txtStreetAddress.text.length == 0) {
        
        strMsg = @"Please provide address.";
        isValid = NO;
        [txtStreetAddress becomeFirstResponder];
    } else if (txtState.text.length == 0 || txtZipCode.text.length == 0 || txtCity.text.length == 0) {
        
        strMsg = @"Please choose proper address.";
        isValid = NO;
        [txtStreetAddress becomeFirstResponder];
    } else if (txtBusinessDesc.text.length > 0) {
        if (arrRestrictedWordsFound.count > 0) {
            
            NSString *strKhisti = [arrRestrictedWordsFound componentsJoinedByString:@","];
            strMsg = [NSString stringWithFormat:@"Invalid term: %@ - Please be respectful of others. Remove and try again", strKhisti];
            [txtBusinessDesc becomeFirstResponder];
            isValid = NO;
        } else if ([[AppManager sharedDataAccess] detectType:txtBusinessDesc.text]) {
            
            strMsg = self.strRestrictionMsg;
            isValid = NO;
        }
    } else if (txtNumberOfEmp.text.length == 0) {
        
        strMsg = @"Enter no. of employees";
        isValid = NO;
        [txtNumberOfEmp becomeFirstResponder];
    } else if (txtCompanyName.text.length == 0) {
        
        strMsg = @"Please enter company name";
        isValid = NO;
        [txtCompanyName becomeFirstResponder];
    }
    
    if (strMsg.length > 0) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strMsg];
    }
    
    return isValid;
}

//-(BOOL) validateEmail:(NSString*) emailString {
//    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
//    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
//    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
//    NSLog(@"%lu", (unsigned long)regExMatches);
//    if (regExMatches == 0) {
//        return NO;
//    }
//    else
//        return YES;
//}
@end
