//
//  CompanyProfileVC.h
//  ProringerPro
//
//  Created by Soma Halder on 23/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CompanyProfileVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITextView *txtBusinessDesc;
@property (weak, nonatomic) IBOutlet UITextField *txtCompanyName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtWebsite;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtNumberOfEmp;
@property (weak, nonatomic) IBOutlet UITextField *txtTypeOfBusiness;
@property (weak, nonatomic) IBOutlet UITextField *txtStreetAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtZipCode;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtState;

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceHolder;

@property (strong, nonatomic) IBOutlet UIView *viewAccessory;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerBusinessTypes;

- (IBAction)SaveCompanyDetails:(id)sender;
- (IBAction)TypeOfBusiness:(id)sender;

@end

NS_ASSUME_NONNULL_END
