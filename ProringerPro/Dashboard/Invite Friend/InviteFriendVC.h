//
//  InviteFriendVC.h
//  ProringerPro
//
//  Created by Soma Halder on 29/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InviteFriendVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtConfEmail;

@property (strong, nonatomic) IBOutlet UIView *viewAccessory;

- (IBAction)InviteFriend:(id)sender;

@end

NS_ASSUME_NONNULL_END
