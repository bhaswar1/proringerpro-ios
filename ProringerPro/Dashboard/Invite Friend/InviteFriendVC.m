//
//  InviteFriendVC.m
//  ProringerPro
//
//  Created by Soma Halder on 29/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "InviteFriendVC.h"

@interface InviteFriendVC () {
    
    BOOL isShouldShowAD, isInvited;
}

@end

@implementation InviteFriendVC
@synthesize txtFirstName, txtLastName, txtConfEmail, txtEmail, viewAccessory;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"INVITE FRIEND";
    
    [self connectionForGetDashboardData];
    
//    self.tabBarController.tabBar.hidden = NO;
    
    isInvited = NO;
    
    txtEmail.delegate = self;
    txtLastName.delegate = self;
    txtFirstName.delegate = self;
    txtConfEmail.delegate = self;
    
    txtEmail.inputAccessoryView = txtLastName.inputAccessoryView = txtConfEmail.inputAccessoryView = txtFirstName.inputAccessoryView = viewAccessory;
    
    [self paddingForTextField:txtFirstName];
    [self paddingForTextField:txtLastName];
    [self paddingForTextField:txtEmail];
    [self paddingForTextField:txtConfEmail];
    
    [self shadowForTextField:txtFirstName];
    [self shadowForTextField:txtLastName];
    [self shadowForTextField:txtEmail];
    [self shadowForTextField:txtConfEmail];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callBannerService:) name:@"showBanner" object:nil];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
//    if (isInvited) {
//        
//        isInvited=false;
//        
//                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[[NSUserDefaults standardUserDefaults]valueForKey:@"alertMsg"] preferredStyle:UIAlertControllerStyleAlert];
//        
//                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//        
//                        if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
//                            if (self.arrInterstitialAd.count > 0) {
//                                for (int i = 0; i < self.arrInterstitialAd.count; i++) {
//                                    NSDictionary *dict = [self.arrInterstitialAd objectAtIndex:i];
//                                    if ([[dict objectForKey:@"title"] isEqualToString:@"Invite a friend"]) {
//                                        if ([[dict objectForKey:@"ios_interstitial_ads"] intValue] == 1) {
//                                            if (self.interstitial.isReady) {
//                                                [self.interstitial presentFromRootViewController:self];
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//        
//        
//                        self->txtLastName.text = self->txtEmail.text = self->txtFirstName.text = self->txtConfEmail.text = @"";
//        //                [[NSNotificationCenter defaultCenter] postNotificationName:@"rateAppNotification" object:self];
//                    }];
//        
//                    [alertController addAction:okAction];
//                    [alertController.view setTintColor:COLOR_THEME];
//                    [self presentViewController:alertController animated:YES completion:nil];
//        
//    }
}

- (void) callBannerService:(NSNotification *) notification {
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[[NSUserDefaults standardUserDefaults]valueForKey:@"alertMsg"] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if (self.iPremiumStatus == 0) {
            if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
                if (self.arrInterstitialAd.count > 0) {
                    for (int i = 0; i < self.arrInterstitialAd.count; i++) {
                        NSDictionary *dict = [self.arrInterstitialAd objectAtIndex:i];
                        if ([[dict objectForKey:@"title"] isEqualToString:@"Invite a friend"]) {
                            if ([[dict objectForKey:@"ios_interstitial_ads"] intValue] == 1) {
                                if (self.interstitial.isReady) {
                                    [self.interstitial presentFromRootViewController:self];
                                }
                            }
                        }
                    }
                }
            }
        }
        
        self->txtLastName.text = self->txtEmail.text = self->txtFirstName.text = self->txtConfEmail.text = @"";
        //                [[NSNotificationCenter defaultCenter] postNotificationName:@"rateAppNotification" object:self];
    }];
    
    [alertController addAction:okAction];
    [alertController.view setTintColor:COLOR_THEME];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
    
//    [self connectionForGetDashboardData];
//
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.b
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSDate *datePrevious = [userDefault objectForKey:@"date"];
    NSDate *today = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    [userDefault synchronize];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSLog(@"Rate Flag:::...%d", self.iRateFlag);
    NSLog(@"Rate status:::...%d", self.iRateStatus);
    
    if (isInvited) {
        if (self.iRateFlag == 1) {
            if (self.iRateStatus == 0) {
                [[AppManager sharedDataAccess] showAlertWithTitle];
            } else if (self.iRateStatus == 2) {
                if (datePrevious == nil) {
                    [[AppManager sharedDataAccess] showAlertWithTitle];
                } else {
                    NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:datePrevious toDate:today options:0];
                    NSLog(@"Remaining days:::...%ld", [components day]);
                    if ([components day] > 5) {
                        [[AppManager sharedDataAccess] showAlertWithTitle];
                    } else {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"showBanner" object:self];
                    }
                }
            } else /* if (self.iRateStatus == 3 || self.iRateStatus == 1) */ {
//                [[AppManager sharedDataAccess] showAlertWithTitle];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showBanner" object:self];
            }
        }
    }
    
    if (self.iPremiumStatus == 0) {
        [self setupADBanner];
    }
}

- (void) rateAppNotification:(NSNotification *) notification {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSDate *datePrevious = [userDefault objectForKey:@"date"];
    NSDate *today = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    [userDefault synchronize];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSLog(@"Rate status:::...%d", self.iRateStatus);
    NSLog(@"Rate Flag:::...%d", self.iRateFlag);
    
    if (isInvited) {
        if (self.iRateFlag == 1) {
            if (self.iRateStatus == 0) {
                [[AppManager sharedDataAccess] showAlertWithTitle];
            } else if (self.iRateStatus == 2) {
                if (datePrevious == nil) {
                    [[AppManager sharedDataAccess] showAlertWithTitle];
                } else {
                    NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:datePrevious toDate:today options:0];
                    NSLog(@"Remaining days:::...%ld", [components day]);
                    if ([components day] > 5) {
                        [[AppManager sharedDataAccess] showAlertWithTitle];
                    }
                }
            } else if (self.iRateStatus == 3) {
                [[AppManager sharedDataAccess] showAlertWithTitle];
            }
        }
    }
}

-(void)setupADBanner {
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Refer Friend"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                    isShouldShowAD = YES;
                } else {
                    isShouldShowAD = NO;
                    txtEmail.inputAccessoryView = txtLastName.inputAccessoryView = txtConfEmail.inputAccessoryView = txtFirstName.inputAccessoryView = nil;
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        isShouldShowAD = NO;
        txtEmail.inputAccessoryView = txtLastName.inputAccessoryView = txtConfEmail.inputAccessoryView = txtFirstName.inputAccessoryView = nil;
        [self.bannerView removeFromSuperview];
    }
}

#pragma mark - AD Banner Delegate
- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.bottomLayoutGuide
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:0],
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (isShouldShowAD) {
        [viewAccessory addSubview:self.bannerView];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (isShouldShowAD) {
        [self addBannerViewToView:self.bannerView];
    }
}

#pragma mark - Validation
-(BOOL)validateTextfield {
    
    BOOL isValid = YES;
    NSString *strMsg = @"";
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtFirstName.text]) {
        [txtFirstName becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please enter first name";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtLastName.text]) {
        [txtLastName becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please enter last name";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtEmail.text]) {
        [txtEmail becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please enter email";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtConfEmail.text]) {
        [txtConfEmail becomeFirstResponder];
        isValid = NO;
        strMsg = @"Confirm email address";
    } else if (![self validateEmail:txtEmail.text]) {
        
        [txtEmail becomeFirstResponder];
        isValid = NO;
        strMsg = @"Please enter valid email";
    }
    else if (![txtEmail.text isEqualToString:txtConfEmail.text]) {
        [txtConfEmail becomeFirstResponder];
        isValid = NO;
        strMsg = @"Email does not match";
    }
    if (!isValid) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strMsg];
    }
    return isValid;
}

-(BOOL) validateEmail:(NSString*) emailString {
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    }
    else
        return YES;
}

#pragma mark - Web Service
-(void)connectionForInviteFriend {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_INVITE_FRIEND];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"first_name": txtFirstName.text,
                             @"last_name": txtLastName.text,
                             @"email": txtEmail.text,
                             @"conf_emailid": txtConfEmail.text
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        NSLog(@"response from invite frnd:::...%@", response);
       
        
        if ([[response objectForKey:@"response"] boolValue] == YES) {
            
            self->isInvited = YES;
            
            self->txtLastName.text = self->txtEmail.text = self->txtFirstName.text = self->txtConfEmail.text = @"";
            
            [[NSUserDefaults standardUserDefaults] setValue:[response objectForKey:@"message"] forKey:@"alertMsg"];
            
            [self connectionForGetDashboardData];
             [YXSpritesLoadingView dismiss];
            
//            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[response objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
//
//            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//
//                if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
//                    if (self.arrInterstitialAd.count > 0) {
//                        for (int i = 0; i < self.arrInterstitialAd.count; i++) {
//                            NSDictionary *dict = [self.arrInterstitialAd objectAtIndex:i];
//                            if ([[dict objectForKey:@"title"] isEqualToString:@"Invite a friend"]) {
//                                if ([[dict objectForKey:@"ios_interstitial_ads"] intValue] == 1) {
//                                    if (self.interstitial.isReady) {
//                                        [self.interstitial presentFromRootViewController:self];
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//
//
//                self->txtLastName.text = self->txtEmail.text = self->txtFirstName.text = self->txtConfEmail.text = @"";
////                [[NSNotificationCenter defaultCenter] postNotificationName:@"rateAppNotification" object:self];
//            }];
//
//            [alertController addAction:okAction];
//            [alertController.view setTintColor:COLOR_THEME];
//            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              NSLog(@"ERROR:::...%@", task.error);
              [YXSpritesLoadingView dismiss];
          }];
}

#pragma mark - UIButton Action
- (IBAction)InviteFriend:(id)sender {
    
    if ([self validateTextfield]) {
        [self connectionForInviteFriend];
    }
    
}

-(void)connectionForWebService {
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_INVITE_FRIEND];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"first_name": txtFirstName.text,
                             @"last_name": txtLastName.text,
                             @"email": txtEmail.text,
                             @"conf_emailid": txtConfEmail.text
                             };
    
    [[AppManager sharedDataAccess] connectionForWebServiceForPOSTMethodWithParameters:params andUrl:url];
    
    NSLog(@"%@", [[AppManager sharedDataAccess] resault]);
}

@end
