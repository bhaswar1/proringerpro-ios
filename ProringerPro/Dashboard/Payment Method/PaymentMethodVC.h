//
//  PaymentMethodVC.h
//  ProringerPro
//
//  Created by Soma Halder on 30/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PaymentMethodVC : ProRingerProBaseVC <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblUpdateCampaign;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end

NS_ASSUME_NONNULL_END
