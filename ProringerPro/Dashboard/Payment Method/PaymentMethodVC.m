//
//  PaymentMethodVC.m
//  ProringerPro
//
//  Created by Soma Halder on 30/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "PaymentMethodVC.h"

@interface PaymentMethodVC ()
{
    NSMutableArray *arrUpdateCampaignData;
    UIToolbar *toolBar;
    
    NSString *strMonth, *strYear, *strCardNumber, *strSecurityCode, *strFirstName, *strLastName, *strAddress, *strOptional, *strCity, *strState, *strCountry, *strZip;
}

@end

@implementation PaymentMethodVC
@synthesize tblUpdateCampaign, datePicker;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon"]];
    
    strMonth = strYear = strZip = strCountry = strState = strCity = strOptional = strAddress = strLastName = strFirstName = strSecurityCode = strCardNumber = @"";
    
    arrUpdateCampaignData = [[NSMutableArray alloc ]initWithObjects:
                             @{@"placeHolder": @"Credit Card #", @"text": strCardNumber},
                             @{@"placeHolderMonth": @"Expiry Month", @"textMonth": strMonth, @"placeHolderYear": @"Expiry Year", @"textYear": strYear},
                             @{@"placeHolder": @"Cvv / Security Code", @"text": strSecurityCode},
                             @{@"text": @"Billing Information"},
                             @{@"placeHolder": @"First Name", @"text": strFirstName},
                             @{@"placeHolder": @"Last Name", @"text": strLastName},
                             @{@"placeHolder": @"Address", @"text": strAddress},
                             @{@"placeHolder": @"Apt #, Suite, Floor(Optional)", @"text": strOptional},
                             @{@"placeHolder": @"City", @"text": strCity},
                             @{@"placeHolder": @"State", @"text": strState},
                             @{@"placeHolder": @"Country", @"text": strCountry},
                             @{@"placeHolder": @"Zip / Postal Code", @"text": strZip},
                             nil];
    
    tblUpdateCampaign.dataSource = self;
    tblUpdateCampaign.delegate = self;
    
    datePicker.hidden = YES;
    datePicker.backgroundColor = [UIColor whiteColor];
    [datePicker setValue:COLOR_THEME forKeyPath:@"textColor"];
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height -200, self.view.frame.size.width, 40)];
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    toolBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor redColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    toolBar.items = [NSArray arrayWithObjects:cancelButton, flexibleSpace, doneButton, nil];
    
    [self.view addSubview:toolBar];
    toolBar.hidden = YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrUpdateCampaignData.count;
    //    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CampaignUpdateTableViewCell *cell = (CampaignUpdateTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellUpdateCampaign"];
    DateSelectionTableViewCell *cellDate = (DateSelectionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDateSelection"];
    UITableViewCell *cellHeader = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellHeader"];
    
    cellHeader.textLabel.text = [[arrUpdateCampaignData objectAtIndex:indexPath.row] objectForKey:@"text"];
    cellHeader.textLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightSemibold];
    
    if (indexPath.row == 0) {
        cell.imgCardView.hidden = NO;
    } else {
        cell.imgCardView.hidden = YES;
    }
    
    [cell.txtUpdateData addTarget:self action:@selector(textFieldDidBeginEditing:) forControlEvents:UIControlEventTouchUpInside];
    [cell.txtUpdateData addTarget:self action:@selector(textFieldDidEndEditing:) forControlEvents:UIControlEventTouchUpInside];
    cell.txtUpdateData.tag = indexPath.row;
    
    [cellDate.txtMonth addTarget:self action:@selector(textFieldDidBeginEditing:) forControlEvents:UIControlEventTouchUpInside];
    [cellDate.txtMonth addTarget:self action:@selector(textFieldDidEndEditing:) forControlEvents:UIControlEventTouchUpInside];
    cellDate.txtMonth.tag = indexPath.row;
    
    [cellDate.txtYear addTarget:self action:@selector(textFieldDidBeginEditing:) forControlEvents:UIControlEventTouchUpInside];
    [cellDate.txtYear addTarget:self action:@selector(textFieldDidEndEditing:) forControlEvents:UIControlEventTouchUpInside];
    cellDate.txtYear.tag = indexPath.row;
    
    cell.txtUpdateData.delegate = self;
    cellDate.txtMonth.delegate = self;
    cellDate.txtYear.delegate = self;
    
    if (indexPath.row == 1) {
        
        if ([strMonth isEqualToString:@""]) {
            cellDate.txtMonth.placeholder = [[arrUpdateCampaignData objectAtIndex:indexPath.row] objectForKey:@"placeHolderMonth"];
        } else {
            cellDate.txtMonth.text = [[arrUpdateCampaignData objectAtIndex:indexPath.row] objectForKey:@"textMonth"];
        }
        
        if ([strYear isEqualToString:@""]) {
            cellDate.txtYear.placeholder = [[arrUpdateCampaignData objectAtIndex:indexPath.row] objectForKey:@"placeHolderYear"];
        } else {
            cellDate.txtYear.text = [[arrUpdateCampaignData objectAtIndex:indexPath.row] objectForKey:@"textYear"];
        }
        return cellDate;
    } else if (indexPath.row == 3) {
        return cellHeader;
    } else {
        
        if ([[[arrUpdateCampaignData objectAtIndex:indexPath.row] objectForKey:@"text"] isEqualToString:@""]) {
            cell.txtUpdateData.placeholder = [[arrUpdateCampaignData objectAtIndex:indexPath.row] objectForKey:@"placeHolder"];
        } else {
            cell.txtUpdateData.text = [[arrUpdateCampaignData objectAtIndex:indexPath.row] objectForKey:@"text"];
        }
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 3) {
        return 30;
    } else {
        return 50;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    NSLog(@"TAG NUMBER:::...%ld", textField.tag);
    
    if (textField.tag == 1) {
        datePicker.hidden = toolBar.hidden = NO;
        [textField resignFirstResponder];
    } else if (textField.tag == 6) {
        
        [self performSegueWithIdentifier:@"zipCode" sender:nil];
    } else if (textField.tag == 8 || textField.tag == 9 || textField.tag == 10 || textField.tag == 11) {
        [textField resignFirstResponder];
        textField.userInteractionEnabled = NO;
    } else {
        [textField becomeFirstResponder];
        textField.userInteractionEnabled = YES;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    //    datePicker.hidden = toolBar.hidden = YES;
    [textField resignFirstResponder];
    
    if (textField.tag == 0) {
        strCardNumber = textField.text;
    } else if (textField.tag == 2){
        strSecurityCode = textField.text;
    } else if (textField.tag == 4) {
        strFirstName = textField.text;
    } else if (textField.tag == 5) {
        strLastName = textField.text;
    } else if (textField.tag == 7) {
        strOptional = textField.text;
    }
}

-(void)cancelButtonPressed {
    
    toolBar.hidden = datePicker.hidden = YES;
    
    strMonth = strYear = @"";
    [tblUpdateCampaign reloadData];
}

-(void)doneButtonPressed {
    
    toolBar.hidden = datePicker.hidden = YES;
    
    datePicker.minimumDate = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy"];
    strYear = [dateFormatter stringFromDate:[datePicker date]];
    [dateFormatter setDateFormat:@"MMMM"];
    strMonth = [dateFormatter stringFromDate:[datePicker date]];
    
    //    NSDictionary *dict = @{@"placeHolderMonth": @"Expiry Month", @"textMonth": strMonth, @"placeHolderYear": @"Expiry Year", @"textYear": strYear};
    //
    //    [arrUpdateCampaignData replaceObjectAtIndex:1 withObject:dict];
    
    [self reloadArray];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"zipCode"]) {
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        SearchZipVC *szvc = segue.destinationViewController;
        self.providesPresentationContextTransitionStyle = YES;
        self.definesPresentationContext = YES;
        szvc.deleagte = self;
        szvc.strNavigationTitle = @"Address";
        szvc.strAddressTitle = @"City/Zip code";
        szvc.strPlaceHolder = @"City / zip";
        szvc.strApi = @"(regions)";
        szvc.strSearchText = @"postal_code";
        szvc.strSearchComponent = @"short_name";
        [szvc setModalPresentationStyle:UIModalPresentationCurrentContext];
    }
}

#pragma mark - Protocol Method
-(void)sendAddressToUserInfo:(NSDictionary *)dictAddress {
    
    NSArray *arrAddressComponent = [dictAddress objectForKey:@"address_components"];
    strAddress = [dictAddress objectForKey:@"formatted_address"];
    NSLog(@"Address::...%@", dictAddress);
    
    strZip = strCountry = strState = strCity = strOptional = @"";
    NSString *strStreetNumber;
    
    for (int i = 0; i < arrAddressComponent.count; i++) {
        NSDictionary *dictComponent = [arrAddressComponent objectAtIndex:i];
        NSArray *arrTypes = [dictComponent objectForKey:@"types"];
        for (int iTypes = 0; iTypes < arrTypes.count; iTypes++) {
            if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"postal_code"]) {
                strZip = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"country"]) {
                strCountry = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"administrative_area_level_1"]) {
                strState = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"locality"] || [[arrTypes objectAtIndex:iTypes] isEqualToString:@"sublocality"]) {
                strCity = [dictComponent objectForKey:@"long_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"route"]) {
                strOptional = [dictComponent objectForKey:@"long_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"street_number"]) {
                strStreetNumber = [dictComponent objectForKey:@"long_name"];
            } else if (strOptional.length == 0) {
                if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"administrative_area_level_2"]) {
                    strOptional = [dictComponent objectForKey:@"long_name"];
                }
            }
        }
    }
    NSLog(@"\npostal_code:::...%@\ncountry:::...%@\nstate:::...%@\ncity:::...%@\nstreet:::...%@", strZip, strCountry, strState, strCity, strOptional);
    
    if (strZip.length == 0) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Enter a valid street address" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            ;
        }];
        
        [alertController addAction:okAction];
        [alertController.view setTintColor:COLOR_THEME];
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        
        if (strStreetNumber.length > 0) {
            strOptional = [NSString stringWithFormat:@"%@ %@", strStreetNumber, strOptional];
        }
        strOptional = [NSString stringWithFormat:@"%@, %@", strCity, strState];
        
        double latitude = [[[[dictAddress objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"] doubleValue];
        double longitude = [[[[dictAddress objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"] doubleValue];
        
        [self findTimeZoneFromSelectedAddressWithLat:latitude andLon:longitude];
    }
}

-(void)findTimeZoneFromSelectedAddressWithLat:(double)latitude andLon:(double)longitude {
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> *placemarks, NSError *error) {
//        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        [self reloadArray];
//        self->strTimeZone = placemark.timeZone.name;
    }];
}

-(void)reloadArray {
    
    NSArray *arrUpdatedField = [[NSArray alloc] initWithObjects:
                                @{@"placeHolder": @"Credit Card #", @"text": strCardNumber},
                                @{@"placeHolderMonth": @"Expiry Month", @"textMonth": strMonth, @"placeHolderYear": @"Expiry Year", @"textYear": strYear},
                                @{@"placeHolder": @"Cvv / Security Code", @"text": strSecurityCode},
                                @{@"": @""},
                                @{@"placeHolder": @"First Name", @"text": strFirstName},
                                @{@"placeHolder": @"Last Name", @"text": strLastName},
                                @{@"placeHolder": @"Address", @"text": strAddress},
                                @{@"placeHolder": @"Apt #, Suite, Floor(Optional)", @"text": strOptional},
                                @{@"placeHolder": @"City", @"text": strCity},
                                @{@"placeHolder": @"State", @"text": strState},
                                @{@"placeHolder": @"Country", @"text": strCountry},
                                @{@"placeHolder": @"Zip / Postal Code", @"text": strZip},
                                nil];
    
    [arrUpdateCampaignData removeAllObjects];
    [arrUpdateCampaignData addObjectsFromArray:arrUpdatedField];
    
    [tblUpdateCampaign reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
