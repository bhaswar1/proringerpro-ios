//
//  LoginSettingsVC.m
//  ProringerPro
//
//  Created by Soma Halder on 25/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "LoginSettingsVC.h"

@interface LoginSettingsVC ()
{
    NSString *strUpdateSection;
    NSUserDefaults *userDefault;
}

@end

@implementation LoginSettingsVC
@synthesize txtNewPw, txtConfPw, txtNewEmail, txtConfEmail, txtCurrentPw, txtCurrentEmail, btnNewPw, btnConfPw, btnCurrentPw;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    self.navigationItem.title = @"LOGIN SETTINGS";
    
//    self.tabBarController.tabBar.hidden = NO;
    
    [self paddingForTextField:txtCurrentEmail];
    [self paddingForTextField:txtCurrentPw];
    [self paddingForTextField:txtConfEmail];
    [self paddingForTextField:txtNewEmail];
    [self paddingForTextField:txtConfPw];
    [self paddingForTextField:txtNewPw];

    [self shadowForTextField:txtCurrentPw];
    [self shadowForTextField:txtConfEmail];
    [self shadowForTextField:txtNewEmail];
    [self shadowForTextField:txtConfPw];
    [self shadowForTextField:txtNewPw];

    txtCurrentPw.secureTextEntry = txtConfPw.secureTextEntry = txtNewPw.secureTextEntry = YES;

    btnCurrentPw.hidden = btnConfPw.hidden = btnNewPw.hidden = YES;

    txtCurrentPw.delegate = txtConfEmail.delegate = txtNewEmail.delegate = txtConfPw.delegate = txtNewPw.delegate = self;

    userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];

//    txtCurrentEmail.text = [userDefault objectForKey:@"email"];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    txtCurrentEmail.text = [userDefault objectForKey:@"email"];
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Login Settings"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Login Settings"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
}

//#pragma mark - UITextField Design
//-(void) paddingForTextField:(UITextField *)textField {
//    
//    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(30, 0, 10, 20)];
//    textField.leftView = paddingView;
//    textField.leftViewMode = UITextFieldViewModeAlways;
//    
//    textField.layer.masksToBounds = NO;
//    textField.layer.shadowOffset = CGSizeMake(0.0, 0.0);
//    textField.layer.shadowRadius = 1;
//    textField.layer.shadowOpacity = 0.2;
//    textField.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
//    
//    textField.layer.borderWidth = 1.0;
//    textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
//}
//
//-(void)shadowForTextField:(UITextField *)textfield {
//    
//    UIColor *color = [UIColor blackColor];
//    textfield.layer.shadowColor = [color CGColor];
//    textfield.layer.shadowRadius = 1.5f;
//    textfield.layer.shadowOpacity = 0.2;
//    textfield.layer.shadowOffset = CGSizeZero;
//    textfield.layer.masksToBounds = NO;
//}

#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    textField.layer.borderColor = COLOR_THEME.CGColor;
    
    if (textField == txtCurrentPw) {
        btnCurrentPw.hidden = NO;
    } else if (textField == txtNewPw) {
        btnNewPw.hidden = NO;
    } else if (textField == txtConfPw) {
        btnConfPw.hidden = NO;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
//    textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    [self paddingForTextField:textField];
    [self shadowForTextField:textField];
    
    txtCurrentPw.secureTextEntry = txtConfPw.secureTextEntry = txtNewPw.secureTextEntry = YES;
    
    btnCurrentPw.hidden = btnConfPw.hidden = btnNewPw.hidden = YES;
}

-(BOOL)validateTextField {
    
    BOOL isValid = YES;
    
    NSString *strMsg = @"";
    
    if ([strUpdateSection isEqualToString:@"E"]) {
        if ([txtNewEmail.text isEqualToString:@""]){
            
            isValid = NO;
            strMsg = @"Please enter valid email";
            [txtNewEmail becomeFirstResponder];
        }
        else if (![self validateEmail:txtNewEmail.text]) {
            
            isValid = NO;
            strMsg = @"Please enter valid email";
            [txtNewEmail becomeFirstResponder];
        }
        else if ([txtConfEmail.text isEqualToString:@""]) {
            
            isValid = NO;
            strMsg = @"Please enter email";
            [txtConfEmail becomeFirstResponder];
        } else if (![txtConfEmail.text isEqualToString:txtNewEmail.text]) {
            
            isValid = NO;
            strMsg = @"Confirm Email is not same with given Email";
            [txtCurrentEmail becomeFirstResponder];
        }
    } else if ([strUpdateSection isEqualToString:@"P"]) {
        
        if ([txtCurrentPw.text isEqualToString:@""]) {
            strMsg = @"Enter current password";
            isValid = NO;
            [txtCurrentPw becomeFirstResponder];
        } else if ([txtNewPw.text isEqualToString:@""]) {
            
            isValid = NO;
            strMsg = @"Enter new password";
            [txtNewPw becomeFirstResponder];
        } else if ([txtConfPw.text isEqualToString:@""]) {
            
            strMsg = @"Please enter confirm password";
            isValid = NO;
            [txtConfPw becomeFirstResponder];
        } else if (![txtNewPw.text isEqualToString:txtConfPw.text]) {
            
            strMsg = @"Confirm password is not same with given password";
            isValid = NO;
            [txtConfPw becomeFirstResponder];
        }
    }
    
    if (strMsg.length > 0) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strMsg];
    }
    return isValid;
}

//-(BOOL) validateEmail:(NSString*) emailString {
//    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
//    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
//    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
//    NSLog(@"%lu", (unsigned long)regExMatches);
//    if (regExMatches == 0) {
//        return NO;
//    }
//    else
//        return YES;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIButton Action
- (IBAction)CurrentPasswordShow:(id)sender {
//    ;
    if (txtCurrentPw.secureTextEntry) {
        txtCurrentPw.secureTextEntry = NO;
    } else {
        txtCurrentPw.secureTextEntry = YES;
    }
}

- (IBAction)NewPasswordShow:(id)sender {
//    ;
    if (txtNewPw.secureTextEntry) {
        txtNewPw.secureTextEntry = NO;
    } else {
        txtNewPw.secureTextEntry = YES;
    }
}

- (IBAction)ConfPasswordShow:(id)sender {
//    ;
    if (txtConfPw.secureTextEntry) {
        txtConfPw.secureTextEntry = NO;
    } else {
        txtConfPw.secureTextEntry = YES;
    }
}

- (IBAction)UpdateEmail:(id)sender {
//    ;
    strUpdateSection = @"E";
    
    if ([self validateTextField]) {
        [self connectionForUpdateEmail];
    }
}

- (IBAction)UpdatePassword:(id)sender {
//    ;
    strUpdateSection = @"P";
    
    if ([self validateTextField]) {
        [self connectionForUpdatePassword];
    }
}

#pragma mark - Web Service
-(void)connectionForUpdateEmail {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_UPDATE_EMAIL];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"emailid": txtNewEmail.text,
                             @"conf_emailid": txtConfEmail.text,
                             @"user_type": @"C"
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];

    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        NSLog(@"response from update email:::...%@", response);
        [YXSpritesLoadingView dismiss];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[response objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if ([[response objectForKey:@"response"] intValue] == 1) {
//            self->txtCurrentEmail.text = self->txtConfEmail.text;
            self->txtNewEmail.text = self->txtConfEmail.text = @"";
            }
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        [alertController.view setTintColor:COLOR_THEME];
    }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              [YXSpritesLoadingView dismiss];
              NSLog(@"ERROR:::...%@", task.error);
          }];
}

-(void)connectionForUpdatePassword {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_UPDATE_PASSWORD];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"curr_pass": txtCurrentPw.text,
                             @"new_pass": txtNewPw.text,
                             @"conf_pass": txtConfPw.text,
                             @"email": txtCurrentEmail.text,
                             @"user_type": @"C"
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        NSLog(@"response from update password:::...%@", response);
        
        [YXSpritesLoadingView dismiss];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[response objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if ([[response objectForKey:@"response"] intValue] == 1) {
                self->txtNewPw.text = self->txtConfPw.text = self->txtCurrentPw.text = @"";
            }
        }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        [alertController.view setTintColor:COLOR_THEME];
        
        
    }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              [YXSpritesLoadingView dismiss];
              NSLog(@"ERROR:::...%@", task.error);
          }];
}

@end
