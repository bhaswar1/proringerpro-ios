//
//  LoginSettingsVC.h
//  ProringerPro
//
//  Created by Soma Halder on 25/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginSettingsVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITextField *txtCurrentEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtNewEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtConfEmail;

@property (weak, nonatomic) IBOutlet UITextField *txtCurrentPw;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPw;
@property (weak, nonatomic) IBOutlet UITextField *txtConfPw;

@property (weak, nonatomic) IBOutlet UIButton *btnCurrentPw;
@property (weak, nonatomic) IBOutlet UIButton *btnNewPw;
@property (weak, nonatomic) IBOutlet UIButton *btnConfPw;

- (IBAction)CurrentPasswordShow:(id)sender;
- (IBAction)NewPasswordShow:(id)sender;
- (IBAction)ConfPasswordShow:(id)sender;

- (IBAction)UpdateEmail:(id)sender;
- (IBAction)UpdatePassword:(id)sender;

@end

NS_ASSUME_NONNULL_END
