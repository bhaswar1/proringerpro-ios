//
//  DashboardVC.m
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "DashboardVC.h"

#define kRemoveAdsProductIdentifier @"put your product id (the one that we just made in App Store Connect) in here"

@interface DashboardVC ()
{
    NSMutableArray *arrDashboardsItems;
    NSData *imageData;
    NSString *strPlanId;
    BOOL isPremium, isVarifiedPhone;
    NSDictionary *dictInfo;
    int iVerifyPremium;
    UIImage *chosenImage;
}

@end

@implementation DashboardVC
@synthesize tblDashboard, viewRating, lblCompanyName, lblCompanyAddress, lblNumberOfReviews, lblNewMessageNumber, lblProjectWatchListNumber, lblRating, imgCompany, btnPremium, viewNewMessage, viewProfileInfo, viewProjectWishlist, constGoButtonHeight, constGoButtonTop, viewHeader, footerView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
//        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
//    self.navigationController.interactivePopGestureRecognizer.delegate = self;
//        [self.navigationController.view removeGestureRecognizer:self.navigationController.interactivePopGestureRecognizer];
//    }
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon"]];
    self.tabBarController.tabBar.hidden = NO;
    
    imgCompany.image = [UIImage imageNamed:@""];
    
    self.iPremiumStatus = 0;
    
    self.viewFooterTab.lblNewProject.backgroundColor = self.viewFooterTab.lblNewMessage.backgroundColor = COLOR_THEME;
    self.viewFooterTab.lblNewMessage.layer.borderColor = self.viewFooterTab.lblNewProject.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewFooterTab.lblNewProject.layer.borderWidth = self.viewFooterTab.lblNewMessage.layer.borderWidth = 0.5;
    
    [self.view addSubview:self.viewFooterTab];
    
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
//    self.view.backgroundColor = COLOR_TABLE_HEADER;
    
    arrDashboardsItems = [[NSMutableArray alloc] initWithObjects:
                          @{@"title": @"User Information", @"subTitle": @"Manage Account holders info", @"image": @"UserInfo" },
                          @{@"title": @"Company", @"subTitle": @"Manage company info", @"image": @"Company" },
                          @{@"title": @"Services", @"subTitle": @"Manage notification", @"image": @"Services" },
                          @{@"title": @"Licenses", @"subTitle": @"Manage license and certifications", @"image": @"License" },
                          @{@"title": @"Portfolio", @"subTitle": @"Manage your work portfolio", @"image": @"Portfolio" },
                          @{@"title": @"Campaigns", @"subTitle": @"Manage your campaigns", @"image": @"Campaigns" },
                          @{@"title": @"Login Settings", @"subTitle": @"Manage your account login", @"image": @"LoginSetting" },
                          @{@"title": @"Notifications", @"subTitle": @"Manage email and device notification", @"image": @"Notification" },
                          @{@"title": @"Quick Reply", @"subTitle": @"Manage quick reply response", @"image": @"QuickReply" },
                          @{@"title": @"Availability", @"subTitle": @"Manage unavailable periods", @"image": @"Availability" },
                          @{@"title": @"Social Media", @"subTitle": @"Manage social media links", @"image": @"Social" },
                          @{@"title": @"Payment Methods", @"subTitle": @"Manage payment info", @"image": @"PaymentMethod" },
                          @{@"title": @"Transaction History", @"subTitle": @"See all payment transactions", @"image": @"History" },
                          @{@"title": @"Service Area", @"subTitle": @"Manage the cities you service", @"image": @"ServiceArea" },
                          @{@"title": @"Business Hours", @"subTitle": @"Manage business hours", @"image": @"BusinessHour" },
                          @{@"title": @"Request Reviews", @"subTitle": @"Ask for a 5 star review", @"image": @"RequestReview" },
                          @{@"title": @"Invite Friends", @"subTitle": @"Invite someone to ProRinger", @"image": @"InviteFriend" },
                          @{@"title": @"Analytics", @"subTitle": @"Track how your profile is performing", @"image": @"Analytics" },
                          @{@"title": @"Share Profile", @"subTitle": @"Share your business profile", @"image": @"ShareProfile" },
                          @{@"title": @"My Profile", @"subTitle": @"View your profile", @"image": @"ViewProfile" }, nil];
    
    tblDashboard.dataSource = self;
    tblDashboard.delegate = self;
    
    viewRating.maxRating = 5.0;
    viewRating.delegate = self;
    viewRating.horizontalMargin = 0;
    viewRating.editable = NO;
    viewRating.displayMode = EDStarRatingDisplayAccurate;
    viewRating.rating = 0.0;
    viewRating.starImage = [UIImage imageNamed:@"StarEmpty"];
    viewRating.starHighlightedImage = [UIImage imageNamed:@"StarFull"];
    
    viewNewMessage.layer.borderWidth = viewProfileInfo.layer.borderWidth = viewProjectWishlist.layer.borderWidth = imgCompany.layer.borderWidth = 1.0;
    viewNewMessage.layer.borderColor = viewProfileInfo.layer.borderColor = viewProjectWishlist.layer.borderColor = imgCompany.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
    
    [self shadowForView:viewProfileInfo];
    [self shadowForView:viewNewMessage];
    [self shadowForView:viewProjectWishlist];
    
    [self setFooterView:footerView];
    
    constGoButtonTop.constant = self->constGoButtonHeight.constant = 0.0;
    CGRect frame = self->viewHeader.frame;
    frame.size.height = 197.0;
    viewHeader.frame = frame;
    btnPremium.hidden = YES;
    
    self.interstitial.delegate = self;
    self.interstitial = [self createAndLoadInterstitial];
    self.bannerView.backgroundColor = [UIColor lightGrayColor];
    self.bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
    self.bannerView.adUnitID = API_GOOGLE_AD_BANNER;
    self.bannerView.rootViewController = self;
    self.bannerView.delegate = self;
    [self.bannerView loadRequest:[GADRequest request]];
    [self addBannerViewToView:self.bannerView];
    
    GADMultipleAdsAdLoaderOptions *multipleAdsOptions =
    [[GADMultipleAdsAdLoaderOptions alloc] init];
    multipleAdsOptions.numberOfAds = 5;
    
    self.adLoader = [[GADAdLoader alloc] initWithAdUnitID:API_GOOGLE_AD_BANNER
                                       rootViewController:self
                                                  adTypes:@[kGADAdLoaderAdTypeUnifiedNative]
                                                  options:@[multipleAdsOptions]];
    self.adLoader.delegate = self;
    [self.adLoader loadRequest:[GADRequest request]];
    self.nativeAd.delegate = self;
    
    [self.bannerView removeFromSuperview];
    
//    [self connectionForGetDashboardData];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    imgCompany.image = [UIImage imageNamed:@""];
    
    constGoButtonTop.constant = self->constGoButtonHeight.constant = 0.0;
    CGRect frame = self->viewHeader.frame;
    frame.size.height = 197.0;
    viewHeader.frame = frame;
    btnPremium.hidden = YES;
    
    NSInteger iTag = [[[NSUserDefaults standardUserDefaults] objectForKey:@"footer"] integerValue];
    [self footerButtonDesignWithTag:iTag];
    [self footerButtonTap:1];
    
    self.tabBarController.tabBar.hidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    [self connectionForGetDashboardData];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    imgCompany.image = [UIImage imageNamed:@""];
    
    constGoButtonTop.constant = self->constGoButtonHeight.constant = 0.0;
    CGRect frame = self->viewHeader.frame;
    frame.size.height = 197.0;
    viewHeader.frame = frame;
    btnPremium.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(counterUpdate) name:@"reload_data" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)counterUpdate {
    
    [self connectionForGetDashboardData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if (self.iTotalNewMsg > 0) {
        self.viewFooterTab.lblNewMessage.hidden = NO;
    } else {
        self.viewFooterTab.lblNewMessage.hidden = YES;
    }
    
    if (self.iNewLeads > 0) {
        self.viewFooterTab.lblNewProject.hidden = NO;
    } else {
        self.viewFooterTab.lblNewProject.hidden = YES;
    }
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
    NSLog(@"AD BANNER ARRAY:::...%@", self.arrAdvViewStatus);
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro dashboard"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
}

#pragma mark - Ad Banner
- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    bannerView.backgroundColor = COLOR_LIGHT_BACK;
    [self.view addSubview:bannerView];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.bottomLayoutGuide
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:-60],
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
    //    bannerView.layer.zPosition = -1;
    //    table_view.layer.zPosition = -2;
}

- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    adView.alpha = 0;
    [UIView animateWithDuration:1.0 animations:^{
        adView.alpha = 1;
//        [self addBannerViewToView:self.bannerView];
    }];
}

- (void)adLoader:(GADAdLoader *)adLoader
didReceiveNativeAd:(GADUnifiedNativeAd *)nativeAd {
    
    [self.adLoader loadRequest:[GADRequest request]];
}

- (void)adLoader:(GADAdLoader *)adLoader
didReceiveUnifiedNativeAd:(GADUnifiedNativeAd *)nativeAd {
    // A unified native ad has loaded, and can be displayed.
}

- (void)adLoaderDidFinishLoading:(GADAdLoader *) adLoader {
    // The adLoader has finished loading ads, and a new request can be sent.
}

- (void)nativeAdDidRecordImpression:(GADUnifiedNativeAd *)nativeAd {
    // The native ad was shown.
}

- (void)nativeAdDidRecordClick:(GADUnifiedNativeAd *)nativeAd {
    // The native ad was clicked on.
}

- (void)nativeAdWillPresentScreen:(GADUnifiedNativeAd *)nativeAd {
    // The native ad will present a full screen view.
}

- (void)nativeAdWillDismissScreen:(GADUnifiedNativeAd *)nativeAd {
    // The native ad will dismiss a full screen view.
}

- (void)nativeAdDidDismissScreen:(GADUnifiedNativeAd *)nativeAd {
    // The native ad did dismiss a full screen view.
}

- (void)nativeAdWillLeaveApplication:(GADUnifiedNativeAd *)nativeAd {
    // The native ad will cause the application to become inactive and
    // open a new application.
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

/// Tells the delegate that a full-screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
    NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full-screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
    NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full-screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
    NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
    NSLog(@"adViewWillLeaveApplication");
}

#pragma mark - view positioning
- (void)positionBannerViewFullWidthAtBottomOfSafeArea:(UIView *_Nonnull)bannerView NS_AVAILABLE_IOS(11.0) {
    // Position the banner. Stick it to the bottom of the Safe Area.
    // Make it constrained to the edges of the safe area.
    UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
    
    [NSLayoutConstraint activateConstraints:@[
                                              [guide.leftAnchor constraintEqualToAnchor:bannerView.leftAnchor],
                                              [guide.rightAnchor constraintEqualToAnchor:bannerView.rightAnchor],
                                              [guide.bottomAnchor constraintEqualToAnchor:bannerView.bottomAnchor]
                                              ]];
}

- (void)positionBannerViewFullWidthAtBottomOfView:(UIView *_Nonnull)bannerView {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.bottomLayoutGuide
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
}

- (GADInterstitial *)createAndLoadInterstitial {
    GADInterstitial *interstitial =
    [[GADInterstitial alloc] initWithAdUnitID:API_GOOGLE_AD_INTERSTITIAL];
    interstitial.delegate = self;
    [interstitial loadRequest:[GADRequest request]];
    return interstitial;
}

/// Tells the delegate an ad request succeeded.
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
}

/// Tells the delegate an ad request failed.
- (void)interstitial:(GADInterstitial *)ad
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"interstitial:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

/// Tells the delegate that an interstitial will be presented.
- (void)interstitialWillPresentScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillPresentScreen");
}

/// Tells the delegate the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
}

/// Tells the delegate the interstitial had been animated off the screen.
- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
    NSLog(@"interstitialDidDismissScreen");
}

/// Tells the delegate that a user click will open another app
/// (such as the App Store), backgrounding the current app.
- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    
    if ([gestureRecognizer isEqual:self.navigationController.interactivePopGestureRecognizer]) {
        return NO;
    } else {
        return YES;
    }
}

-(void)shadowForView:(UIView *)shadowView {
    
    shadowView.layer.shadowColor = COLOR_TABLE_HEADER.CGColor;
    shadowView.layer.shadowOffset = CGSizeMake(0, 2);
    shadowView.layer.shadowOpacity = 1;
    shadowView.layer.shadowRadius = 0.5;
    shadowView.layer.masksToBounds = NO;
}

#pragma mark - NavigationBar Button
-(void)rightBarButtonPressed:(id)sender {
    
    NSLog(@"Right button pressed from Dashboard class");
    [self tapToDismissView];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchProjectVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SearchProjectVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrDashboardsItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellDashboard";
    
    NSDictionary *dict = [arrDashboardsItems objectAtIndex:indexPath.row];
    
    DashboardTableViewCell *cell = (DashboardTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.lblTitle.text = [dict objectForKey:@"title"];
    cell.lblDescription.text = [dict objectForKey:@"subTitle"];
    
    cell.imgIcon.image = [UIImage imageNamed:[dict objectForKey:@"image"]];
    
    cell.viewShadow.layer.shadowColor = COLOR_TABLE_HEADER.CGColor;
    cell.viewShadow.layer.shadowOffset = CGSizeMake(0, 3);
    cell.viewShadow.layer.shadowOpacity = 1;
    cell.viewShadow.layer.shadowRadius = 0.8;
    cell.viewShadow.layer.masksToBounds = NO;
    
    return cell;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 90;
    
    /*
    if (indexPath.row == 8) {
        if (self.iPremiumStatus > 0) {
            return 90;
        } else {
            return 0;
        }
    } else if (indexPath.row == 5 || indexPath.row == 11) {
        if (self.iPaymentOption > 0) {
            return 90;
        } else {
            return 0;
        }
    } else {
        return 90;
    }
     */
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSLog(@"SELECTED ROW:::...%ld", indexPath.row);
    
    [self ADSideMenuDrawerSelection:1 withRow:indexPath.row andTitle:[[arrDashboardsItems objectAtIndex:indexPath.row] objectForKey:@"title"]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Web Service
-(void)connectionForGetDashboardData {
    
    [YXSpritesLoadingView show];
    
    self->btnPremium.hidden = YES;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_DASHBOARD];
    
    NSDictionary *params = @{@"user_id": [userDefault objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            self->dictInfo = [[responseObject objectForKey:@"info_array"] objectAtIndex:0];
            self.strShareUrl = [NSString stringWithFormat:@"%@", [self->dictInfo objectForKey:@"url"]];
            self->lblCompanyName.text = [self->dictInfo objectForKey:@"company_name"];
            self->lblCompanyAddress.text = [NSString stringWithFormat:@"%@, %@ %@", [self->dictInfo objectForKey:@"city"], [self->dictInfo objectForKey:@"state"], [self->dictInfo objectForKey:@"zipcode"]];
            self->lblNumberOfReviews.text = [NSString stringWithFormat:@"%@", [self->dictInfo objectForKey:@"total_review"]];
            self->lblNewMessageNumber.text = [NSString stringWithFormat:@"%@", [self->dictInfo objectForKey:@"total_msg"]];
            self->lblProjectWatchListNumber.text = [NSString stringWithFormat:@"%@", [self->dictInfo objectForKey:@"project_watchlist"]];
            self.strUserId = [self->dictInfo objectForKey:@"pro_id"];
            self->strPlanId = [NSString stringWithFormat:@"%@", [self->dictInfo objectForKey:@"plan_id"]];
            [userDefault setObject:self->strPlanId forKey:@"plan_id"];
            [userDefault setObject:[self->dictInfo objectForKey:@"pro_id"] forKey:@"user_id"];
            
            self->viewRating.rating = [[self->dictInfo objectForKey:@"avg_rating"] floatValue];
            self->lblRating.text = [self->dictInfo objectForKey:@"avg_rating"];
            
            [self->imgCompany sd_setImageWithURL:[self->dictInfo objectForKey:@"profile_img"]];
            
            self.iPremiumStatus = [[self->dictInfo objectForKey:@"pro_premium_status"] intValue];
            self.iPaymentOption = [[self->dictInfo objectForKey:@"payment_option"] intValue];
            
            CGRect frame = self->viewHeader.frame;
//            if (self.iPaymentOption == 1) {
//
//                if ([[self->dictInfo objectForKey:@"Pro_verified"] isEqualToString:@"A"]) {
//                    self->constGoButtonTop.constant = self->constGoButtonHeight.constant = 0.0;
//                    frame.size.height = 197.0;
//                    self->btnPremium.hidden = YES;
//                } else {
//                    self->constGoButtonHeight.constant = 45.0;
//                    self->constGoButtonTop.constant = 8.0;
//                    frame.size.height = 250.0;
//                    self->btnPremium.hidden = NO;
//                }
//
//                self.iPremiumStatus = [[self->dictInfo objectForKey:@"pro_premium_status"] intValue];
//
//                if (self.iPremiumStatus > 0) {
//                    [self->btnPremium setTitle:@"GET VERIFIED" forState:UIControlStateNormal];
//                    self->isPremium = YES;
//                } else {
//                    [self->btnPremium setTitle:@"GO PREMIUM" forState:UIControlStateNormal];
//                    self->isPremium = NO;
//                }
//            } else {
//                self->constGoButtonTop.constant = self->constGoButtonHeight.constant = 0.0;
//                frame.size.height = 197.0;
//                self->btnPremium.hidden = YES;
//                [self->btnPremium setTitle:@"" forState:UIControlStateNormal];
//            }
            
            
            if (self.iPremiumStatus == 2) {
                if ([[self->dictInfo objectForKey:@"Pro_verified"] isEqualToString:@"N"]) {
                    [self->btnPremium setTitle:@"GET VERIFIED" forState:UIControlStateNormal];
                    self->btnPremium.hidden = NO;
                    self->iVerifyPremium = 1;
                    self->constGoButtonHeight.constant = 45.0;
                    self->constGoButtonTop.constant = 8.0;
                    frame.size.height = 250.0;
                } else if ([[self->dictInfo objectForKey:@"Pro_verified"] isEqualToString:@"Y"]) {
                    [self->btnPremium setTitle:@"VERIFIED" forState:UIControlStateNormal];
                    self->btnPremium.hidden = NO;
                    self->iVerifyPremium = 2;
                    self->constGoButtonHeight.constant = 45.0;
                    self->constGoButtonTop.constant = 8.0;
                    frame.size.height = 250.0;
                } else if ([[self->dictInfo objectForKey:@"Pro_verified"] isEqualToString:@"A"]) {
                    self->constGoButtonTop.constant = self->constGoButtonHeight.constant = 0.0;
                    frame.size.height = 197.0;
                    self->btnPremium.hidden = YES;
                    [self->btnPremium setTitle:@"" forState:UIControlStateNormal];
                }
            } else {
                [self->btnPremium setTitle:@"GO PREMIUM" forState:UIControlStateNormal];
                self->iVerifyPremium = 3;
                self->btnPremium.hidden = NO;
                self->constGoButtonHeight.constant = 45.0;
                self->constGoButtonTop.constant = 8.0;
                frame.size.height = 250.0;
            }
            
            self->viewHeader.frame = frame;
            
            self.iNewLeads = [[self->dictInfo objectForKey:@"newLead_count"] intValue];
            self.iTotalNewMsg = [[self->dictInfo objectForKey:@"total_msg"] intValue];
            
            if (self.iTotalNewMsg > 0) {
                self.viewFooterTab.lblNewMessage.hidden = NO;
            } else {
                self.viewFooterTab.lblNewMessage.hidden = YES;
            }

            if (self.iNewLeads > 0) {
                self.viewFooterTab.lblNewProject.hidden = NO;
            } else {
                self.viewFooterTab.lblNewProject.hidden = YES;
            }
            
            int iTag = [[userDefault objectForKey:@"footer"] intValue];
            
            [self footerButtonDesignWithTag:iTag];
            
//            if (self.iTotalNewMsg > 0) {
//                self.viewFooterTab.imgMessage.image = [UIImage imageNamed:@"MessagesNew"];
//            } else {
//                self.viewFooterTab.imgMessage.image = [UIImage imageNamed:@"Messages"];
//            }
//
//            if (self.iNewLeads > 0) {
//                self.viewFooterTab.imgMyProject.image = [UIImage imageNamed:@"MyProjectNew"];
//            } else {
//                self.viewFooterTab.imgMyProject.image = [UIImage imageNamed:@"MyProject"];
//            }
            
            [userDefault setObject:[NSNumber numberWithInt:self.iPremiumStatus] forKey:@"pro_premium_status"];
            [userDefault setObject:[NSNumber numberWithInt:self.iPaymentOption] forKey:@"payment_option"];
            
            NSLog(@"INFO DICT FROM MAIN CLASS:::...%d %d", [[userDefault objectForKey:@"payment_option"] intValue], [[userDefault objectForKey:@"pro_premium_status"] intValue]);
            
            [self.arrAdvViewStatus removeAllObjects];
            [self.arrAdvViewStatus addObjectsFromArray:[responseObject objectForKey:@"adv_array"]];
            
            if (self.arrAdvViewStatus.count > 0) {
                for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
                    NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
                    if ([[dict objectForKey:@"title"] isEqualToString:@"Pro dashboard"]) {
                        if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                            [self addBannerViewToView:self.bannerView];
                        } else {
                            [self.bannerView removeFromSuperview];
                        }
                    }
                }
            } else {
                [self.bannerView removeFromSuperview];
            }
            
            [self configureArray];
        }
        
        self.appDelegate.MessageNotificationgesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MessageNotificationTap)];
        [self.appDelegate.notificationView addGestureRecognizer:self.appDelegate.MessageNotificationgesture];
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)imageLoadFromURL:(NSString*)url withContainer:(UIImageView*)imgView {
    NSURL *imageURL = [NSURL URLWithString:url];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            imgView.image = [UIImage imageWithData:imageData];
        });
    });
}

-(void)MessageNotificationTap {
    
    [self.appDelegate MessageNotificationTap];
}

-(void)connectionForUploadImage {
    [YXSpritesLoadingView show];
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_IMAGE_UPLOAD];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSDictionary *params = @{@"user_id": [userDefault objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentType = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentType addObject:@"text/html"];
    [contentType addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentType;
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:self->imageData name:@"profile_image" fileName:@"Image.png" mimeType:@"image/png"];
        
    } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [YXSpritesLoadingView dismiss];
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                UIImage *imageProfile = [UIImage imageWithData:self->imageData];
                self->imgCompany.image = imageProfile;
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", task.error);
        [YXSpritesLoadingView dismiss];
    }];
}

-(void)configureArray {
    
    if (self.iPaymentOption > 0) {
        if (self.iPremiumStatus > 0) {
            
        } else {
            NSArray *arrRemove = [[NSArray alloc] initWithObjects:@{@"title": @"Quick Reply", @"subTitle": @"Manage quick reply response", @"image": @"QuickReply" }, nil];
            [arrDashboardsItems removeObjectsInArray:arrRemove];
        }
    } else {
        NSArray *arrRemove = [[NSArray alloc] initWithObjects:@{@"title": @"Campaigns", @"subTitle": @"Manage your campaigns", @"image": @"Campaigns" }, @{@"title": @"Quick Reply", @"subTitle": @"Manage quick reply response", @"image": @"QuickReply" }, @{@"title": @"Payment Methods", @"subTitle": @"Manage payment info", @"image": @"PaymentMethod" }, nil];
        [arrDashboardsItems removeObjectsInArray:arrRemove];
    }
    
    NSLog(@"ARRAY COUNT:::...%lu", (unsigned long)arrDashboardsItems.count);
    
    [tblDashboard reloadData];
}

#pragma mark - UIButton Action
- (IBAction)ViewAllMessage:(id)sender {
    
//    if ([[self.dictInfo objectForKey:@"pro_premium_status"] intValue] == 0) {
//        if (self.interstitial.isReady) {
//            [self.interstitial presentFromRootViewController:self];
//        }
//    }
    
    [[NSUserDefaults standardUserDefaults] setObject:@"3" forKey:@"footer"];
    [self footerButtonDesignWithTag:3];
    [self footerButtonTap:3];
    /*
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
    MessageVC *mvc = [storyboard instantiateViewControllerWithIdentifier:@"MessageVC"];
    [self.navigationController pushViewController:mvc animated:YES];
     */
}

- (IBAction)ViewAllProjects:(id)sender {
    
//    if ([[self.dictInfo objectForKey:@"pro_premium_status"] intValue] == 0) {
//        if ([[GADRewardBasedVideoAd sharedInstance] isReady]) {
//            [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:self];
//        }
//    }
    
    [[NSUserDefaults standardUserDefaults] setObject:@"4" forKey:@"footer"];
    [self footerButtonDesignWithTag:4];
    [self footerButtonTap:4];
    /*
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Watchlist" bundle:nil];
    WatchListVC *wlvc = [storyboard instantiateViewControllerWithIdentifier:@"WatchListVC"];
    [self.navigationController pushViewController:wlvc animated:YES];
     */
}

- (IBAction)CameraButtonPressed:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"upload profile picture" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Take Photo using Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:nil];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Your device has no camera or camera is not supported" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }];
    UIAlertAction *photo = [UIAlertAction actionWithTitle:@"Choose from Photos" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
    }];
    
    [alertController addAction:camera];
    [alertController addAction:photo];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
    [alertController.view setTintColor:COLOR_THEME];
}

- (IBAction)GoPremium:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"progressWidth"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    UIViewController *vc;
    if (iVerifyPremium == 1) {
        [self progressBarWidth];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"VerifyInfoVC"];
        [self.navigationController pushViewController:vc animated:YES];
    } else if (iVerifyPremium == 3) {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"PremiumVC"];
        [self.navigationController pushViewController:vc animated:YES];
        /*
        if ([SKPaymentQueue canMakePayments]) {
            SKProductsRequest *productRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:kRemoveAdsProductIdentifier]];
            productRequest.delegate = self;
            [productRequest start];
        } else {
            NSLog(@"User cannot make payments due to parental controls");
        }
        */
        /*
        if([PKPaymentAuthorizationViewController canMakePayments]) {
            
            PKPaymentRequest *request = [[PKPaymentRequest alloc] init];
            request.countryCode = @"US";
            request.currencyCode = @"EUR";
            request.supportedNetworks = @[PKPaymentNetworkAmex, PKPaymentNetworkMasterCard, PKPaymentNetworkVisa, PKPaymentNetworkDiscover];
            request.merchantCapabilities = PKMerchantCapability3DS;
            request.merchantIdentifier = @"merchant.com.sensussoft.ApplePayDemo";
            request.requiredShippingAddressFields = PKAddressFieldAll;
            
            PKPaymentSummaryItem *widget1 = [PKPaymentSummaryItem summaryItemWithLabel:@"Widget 1" amount:[NSDecimalNumber decimalNumberWithString:@"0.99"]];
            PKPaymentSummaryItem *widget2 = [PKPaymentSummaryItem summaryItemWithLabel:@"Widget 2" amount:[NSDecimalNumber decimalNumberWithString:@"1.00"]];
            PKPaymentSummaryItem *total = [PKPaymentSummaryItem summaryItemWithLabel:@"Grand Total" amount:[NSDecimalNumber decimalNumberWithString:@"1.99"]];
            request.paymentSummaryItems = @[widget1, widget2, total];
            
            PKPaymentAuthorizationViewController *paymentPane = [[PKPaymentAuthorizationViewController alloc] initWithPaymentRequest:request];
            paymentPane.delegate = self;
            [self presentViewController:paymentPane animated:TRUE completion:nil];
        }
         */
    } else if (iVerifyPremium == 2) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:@"Your account have been verified"];
    }
}

- (IBAction)ShowReviewList:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
//    ReviewListVC *rlvc = [storyboard instantiateViewControllerWithIdentifier:@"ReviewListVC"];
//    [self.navigationController pushViewController:rlvc animated:YES];
    
//    if (self.interstitial.isReady) {
//        [self.interstitial presentFromRootViewController:self];
//    } else {
//        NSLog(@"Ad wasn't ready");
//    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
    ViewAllReviewVC *varvc = [storyboard instantiateViewControllerWithIdentifier:@"ViewAllReviewVC"];
    varvc.strTitle = [dictInfo objectForKey:@"company_name"];
    varvc.strProfileImage = [dictInfo objectForKey:@"profile_img"];
    varvc.strNumberOfReview = [dictInfo objectForKey:@"total_review"];
    varvc.strNumberOfRate = [dictInfo objectForKey:@"avg_rating"];
    varvc.fltRating = [[dictInfo objectForKey:@"avg_rating"] floatValue];
    varvc.isProfileDetails = YES;
    [self.navigationController pushViewController:varvc animated:YES];
    
}

#pragma mark - Payment Delegate
-(void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller didAuthorizePayment:(PKPayment *)payment completion:(void (^)(PKPaymentAuthorizationStatus))completion
{
    NSLog(@"%@", payment.token.description);
    NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:payment.token.paymentData options:kNilOptions error:nil];
    NSLog(@"%@", dict);
    completion(PKPaymentAuthorizationStatusSuccess);
}

-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    SKProduct *validProduct = nil;
    int count = [response.products count];
    if (count > 0) {
        validProduct = [response.products objectAtIndex:0];
        NSLog(@"Product Available");
        [self purchase:validProduct];
    }
}

- (void)purchase:(SKProduct *)product {
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    NSLog(@"received restored transactions: %i", queue.transactions.count);
    for(SKPaymentTransaction *transaction in queue.transactions){
        if(transaction.transactionState == SKPaymentTransactionStateRestored){
            //called when the user successfully restores a purchase
            NSLog(@"Transaction state -> Restored");
            
            //if you have more than one in-app purchase product,
            //you restore the correct product for the identifier.
            //For example, you could use
            //if(productID == kRemoveAdsProductIdentifier)
            //to get the product identifier for the
            //restored purchases, you can use
            //
            //NSString *productID = transaction.payment.productIdentifier;
            [self doRemoveAds];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            break;
        }
    }
}

-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray<SKPaymentTransaction *> *)transactions {
    
    for (SKPaymentTransaction *transaction in transactions) {
        SKPayment *payment = [transaction payment];
        
//        if ([payment.productIdentifier isEqualToString:strPlanId]) {
//            <#statements#>
//        }
    }
}

/*
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    for(SKPaymentTransaction *transaction in transactions){
        //if you have multiple in app purchases in your app,
        //you can get the product identifier of this transaction
        //by using transaction.payment.productIdentifier
        //
        //then, check the identifier against the product IDs
        //that you have defined to check which product the user
        //just purchased
        
        switch(transaction.transactionState){
            case SKPaymentTransactionStatePurchasing: NSLog(@"Transaction state -> Purchasing");
                //called when the user is in the process of purchasing, do not add any of your own code here.
                break;
            case SKPaymentTransactionStatePurchased:
                //this is called when the user has successfully purchased the package (Cha-Ching!)
                [self doRemoveAds]; //you can add your code for what you want to happen when the user buys the purchase here, for this tutorial we use removing ads
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                NSLog(@"Transaction state -> Purchased");
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Transaction state -> Restored");
                //add the same code as you did from SKPaymentTransactionStatePurchased here
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                //called when the transaction does not finish
                if(transaction.error.code == SKErrorPaymentCancelled){
                    NSLog(@"Transaction state -> Cancelled");
                    //the user cancelled the payment ;(
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
        }
    }
}
*/

- (void)doRemoveAds{
//    ADBannerView *banner;
//    [banner setAlpha:0];
//    areAdsRemoved = YES;
//    removeAdsButton.hidden = YES;
//    removeAdsButton.enabled = NO;
//    [[NSUserDefaults standardUserDefaults] setBool:areAdsRemoved forKey:@"areAdsRemoved"];
    //use NSUserDefaults so that you can load whether or not they bought it
    //it would be better to use KeyChain access, or something more secure
    //to store the user data, because NSUserDefaults can be changed.
    //You're average downloader won't be able to change it very easily, but
    //it's still best to use something more secure than NSUserDefaults.
    //For the purpose of this tutorial, though, we're going to use NSUserDefaults
//    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - UIImagePicker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    // output image
//    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
//    imgCompany.image = chosenImage;
//    [picker dismissViewControllerAnimated:YES completion:nil];
//
//    imageData = UIImagePNGRepresentation(chosenImage);
//
//    imageData = [NSData dataWithData:imageData];
//
//    [self connectionForUploadImage];

    
    
    chosenImage = info[UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        [self openEditor];
    }];
}

-(void)openEditor {
    self.cropController.image = chosenImage;
    self.cropController.keepingCropAspectRatio = YES;
    self.cropController.cropAspectRatio = 1.0;
    
    CGFloat ratio = 1.0f / 1.0f;
    CGRect cropRect = self.cropController.cropView.cropRect;
    CGFloat widthCrop = CGRectGetWidth(cropRect);
    cropRect.size = CGSizeMake(widthCrop, widthCrop * ratio);
    self.cropController.cropView.cropRect = cropRect;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.cropController];
    
    [self presentViewController:navigationController animated:YES completion:^{
        [self.cropController clickedButtonAtIndex:1];
    }];
}

- (void)cancelButtonDidPush:(id)sender {
    
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - PECropViewControllerDelegate methods
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
    chosenImage = croppedImage;
    
    [self resizeImage:chosenImage];
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
    chosenImage = croppedImage;
    
//    [self resizeAndConvertImageTodata];
    [self resizeImage:chosenImage];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:^{
//        self->imgLicence.image = self->chosenImage;
    }];
}

- (void)resizeImage:(UIImage*)image {
    
    NSData *finalData = nil;
    NSData *unscaledData = UIImagePNGRepresentation(image);
    
    if (unscaledData.length > 5000.0f ) {
        
//if image size is greater than 5KB dividing its height and width maintaining proportions
        
        UIImage *scaledImage = [self imageWithImage:image andWidth:image.size.width/2 andHeight:image.size.height/2];
        finalData = UIImagePNGRepresentation(scaledImage);
        
        if (finalData.length > 500000.0f ) {
            
            [self resizeImage:scaledImage];
        } else {
//scaled image will be your final image
            imageData = UIImagePNGRepresentation(scaledImage);
            chosenImage = scaledImage;
//            imgLicence.image = chosenImage;
        }
    }
//    imgLicence.image = chosenImage;
    [self connectionForUploadImage];
}

- (UIImage*)imageWithImage:(UIImage*)image andWidth:(CGFloat)width andHeight:(CGFloat)height {
    
    UIGraphicsBeginImageContext( CGSizeMake(width, height));
    [image drawInRect:CGRectMake(0,0,width,height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext() ;
    return newImage;
}

@end
