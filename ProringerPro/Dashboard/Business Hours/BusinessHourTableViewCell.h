//
//  BusinessHourTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 29/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol sendTimeToMainClass <NSObject>

-(void)sendTime:(NSDate *)time;

@end

NS_ASSUME_NONNULL_BEGIN

@interface BusinessHourTableViewCell : UITableViewCell <UITextFieldDelegate>

@property(assign, nonatomic) id delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblWeekDay;

@property (weak, nonatomic) IBOutlet UIButton *btnSelect;

@property (weak, nonatomic) IBOutlet UITextField *txtStartTime;
@property (weak, nonatomic) IBOutlet UITextField *txtEndTime;

@property (strong, nonatomic) UIToolbar *toolBar;
@property (strong, nonatomic) UIBarButtonItem *doneButton;
@property (strong, nonatomic) UIBarButtonItem *cancelButton;
@property (strong, nonatomic) UIBarButtonItem *flexibleSpace;
@property (strong, nonatomic) UIDatePicker *datePicker;
@property (strong, nonatomic) NSDate *date;

@property (nonatomic) int iTag;

-(void)configureCellWith:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
