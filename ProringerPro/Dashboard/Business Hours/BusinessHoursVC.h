//
//  BusinessHoursVC.h
//  ProringerPro
//
//  Created by Soma Halder on 29/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BusinessHoursVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet DLRadioButton *btnSelectedHours;
@property (weak, nonatomic) IBOutlet DLRadioButton *btnAlwaysSelected;

- (IBAction)OnSelected:(id)sender;
- (IBAction)OnAlaways:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tblBusinessHours;

- (IBAction)SaveBusinessHours:(id)sender;

@end

NS_ASSUME_NONNULL_END
