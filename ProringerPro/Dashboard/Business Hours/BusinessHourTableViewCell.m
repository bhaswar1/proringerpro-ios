//
//  BusinessHourTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 29/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "BusinessHourTableViewCell.h"

@implementation BusinessHourTableViewCell
@synthesize btnSelect, lblWeekDay, txtStartTime, txtEndTime, toolBar, doneButton, cancelButton, flexibleSpace, datePicker, delegate, iTag, date;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, 50)];
    
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    toolBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    flexibleSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor redColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    toolBar.items = [NSArray arrayWithObjects:cancelButton, flexibleSpace, doneButton, nil];
    
    datePicker = [[UIDatePicker alloc]init];
//    [datePicker setDate:date];
//    datePicker.minimumDate = [NSDate date];
    datePicker.datePickerMode = UIDatePickerModeTime;
    datePicker.minuteInterval = 15;
    [datePicker setValue:COLOR_THEME forKeyPath:@"textColor"];
    
    txtStartTime.inputView = txtEndTime.inputView = datePicker;
    txtStartTime.inputAccessoryView = txtEndTime.inputAccessoryView = toolBar;
    
    [self paddingForTextField:txtStartTime];
    [self paddingForTextField:txtEndTime];
    
    [self shadowForTextField:txtStartTime];
    [self shadowForTextField:txtEndTime];
    
    txtStartTime.delegate = self;
    txtEndTime.delegate = self;
}

-(void)configureCellWith:(NSDictionary *)dict {
    
    [datePicker setDate:date];
    
    lblWeekDay.text = [dict objectForKey:@"day_name"];
    txtStartTime.text = [dict objectForKey:@"start_time"];
    txtEndTime.text = [dict objectForKey:@"end_time"];
    
    if ([[dict objectForKey:@"day_status"] isEqualToString:@"Y"]) {
        [btnSelect setBackgroundImage:[UIImage imageNamed:@"CheckBoxFull"] forState:UIControlStateNormal];
        txtStartTime.userInteractionEnabled = YES;
        txtEndTime.userInteractionEnabled = YES;
    } else {
        [btnSelect setBackgroundImage:[UIImage imageNamed:@"CheckBoxEmpty"] forState:UIControlStateNormal];
        txtStartTime.userInteractionEnabled = NO;
        txtEndTime.userInteractionEnabled = NO;
    }
}

#pragma mark - UITextField Design
-(void) paddingForTextField:(UITextField *)textField {
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(30, 0, 10, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    textField.layer.masksToBounds = NO;
    textField.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    textField.layer.shadowRadius = 0.5;
    textField.layer.shadowOpacity = 0.2;
    textField.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    
    textField.layer.borderWidth = 1.0;
    textField.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
}

-(void)shadowForTextField:(UITextField *)textfield {
    
    UIColor *color = [UIColor blackColor];
    textfield.layer.shadowColor = [color CGColor];
    textfield.layer.shadowRadius = 1.5f;
    textfield.layer.shadowOpacity = 0.2;
    textfield.layer.shadowOffset = CGSizeZero;
    textfield.layer.masksToBounds = NO;
}

-(void)doneButtonPressed {
//    NSString *strTime;
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:@"hh:mm a"];
//    strTime = [dateFormatter stringFromDate:[datePicker date]];
    
    [self.delegate sendTime:[datePicker date]];
    
    [txtStartTime resignFirstResponder];
    [txtEndTime resignFirstResponder];
}

-(void)cancelButtonPressed {
    
    [txtStartTime resignFirstResponder];
    [txtEndTime resignFirstResponder];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
