//
//  BusinessHoursVC.m
//  ProringerPro
//
//  Created by Soma Halder on 29/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "BusinessHoursVC.h"

@interface BusinessHoursVC () <sendTimeToMainClass>
{
    NSMutableArray *arrBusinessHours;
    
    UIToolbar *toolBar;
    int iIndex, iTag, iBusinessHourType;
    BOOL start;
}

@end

@implementation BusinessHoursVC
@synthesize tblBusinessHours, btnSelectedHours, btnAlwaysSelected;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    self.navigationItem.title = @"BUSINESS HOURS";
    
//    self.tabBarController.tabBar.hidden = NO;
    
    iBusinessHourType = iTag = -1;
    
    arrBusinessHours = [[NSMutableArray alloc] init]; /*WithObjects:
                        @{@"isSelected": @"YES", @"day": @"Sunday", @"startTime": @"", @"endTime": @""},
                        @{@"isSelected": @"YES", @"day": @"Monday", @"startTime": @"", @"endTime": @""},
                        @{@"isSelected": @"NO", @"day": @"Tuesday", @"startTime": @"", @"endTime": @""},
                        @{@"isSelected": @"NO", @"day": @"Wednesday", @"startTime": @"", @"endTime": @""},
                        @{@"isSelected": @"NO", @"day": @"Thursday", @"startTime": @"", @"endTime": @""},
                        @{@"isSelected": @"YES", @"day": @"Friday", @"startTime": @"", @"endTime": @""},
                        @{@"isSelected": @"YES", @"day": @"Saturday", @"startTime": @"", @"endTime": @""}, nil];    */
    
    tblBusinessHours.dataSource = self;
    tblBusinessHours.delegate = self;
    
    [self connectionForGetBusinessHours];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Business Hours"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Business Hours"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrBusinessHours.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellBusinessHours";
    
    BusinessHourTableViewCell *cell = (BusinessHourTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.btnSelect.tag = indexPath.row;
    [cell.btnSelect addTarget:self action:@selector(daySelectionButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.txtEndTime.delegate = self;
    cell.txtStartTime.delegate = self;
    
    cell.delegate = self;
    
    cell.txtStartTime.restorationIdentifier = cell.txtEndTime.restorationIdentifier = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
    cell.txtStartTime.tag = 0;
    cell.txtEndTime.tag = 1;
    
    NSString *strStartDate = [[arrBusinessHours objectAtIndex:iIndex] objectForKey:@"start_time"];
    NSString *strEndDate = [[arrBusinessHours objectAtIndex:iIndex] objectForKey:@"end_time"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    
    if (strStartDate.length > 0) {
        if (cell.txtStartTime.tag == 0) {
            cell.date = [dateFormatter dateFromString:strStartDate];
            cell.datePicker.date = cell.date;
        }
    } else {
        if (cell.txtStartTime.tag == 0) {
            cell.date = [dateFormatter dateFromString:@"08:00 am"];
            cell.datePicker.date = cell.date;
        }
    }
    
    if (strEndDate.length > 0) {
        if (cell.txtEndTime.tag == 1) {
            NSDate *date = [dateFormatter dateFromString:strEndDate];
            cell.datePicker.date = date;
        }
    } else {
        if (cell.txtEndTime.tag == 1) {
            NSDate *date = [dateFormatter dateFromString:@"05:00 pm"];
            cell.datePicker.date = date;
        }
    }
    
    [cell.txtStartTime addTarget:self action:@selector(textFieldDidBeginEditing:) forControlEvents:UIControlEventTouchUpInside];
    [cell.txtEndTime addTarget:self action:@selector(textFieldDidBeginEditing:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell configureCellWith:[arrBusinessHours objectAtIndex:indexPath.row]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70;
}

#pragma mark - UITableView Button
-(void)daySelectionButtonPressed:(UIButton *)sender {
    
    NSMutableDictionary *dictM = [NSMutableDictionary dictionary];
    [dictM addEntriesFromDictionary:[arrBusinessHours objectAtIndex:sender.tag]];
    
    NSLog(@"Showing dictionary%@ and index number %ld", dictM, sender.tag);
    
    if ([[dictM objectForKey:@"day_status"] isEqualToString:@"Y"]) {
        [dictM setObject:@"N" forKey:@"day_status"];// = NO;
        [dictM setObject:@"" forKey:@"start_time"];
        [dictM setObject:@"" forKey:@"end_time"];
    } else {
//        [[dict objectForKey:@"isSelected"] setBool:YES forKey:@"isSelected"];
        [dictM setObject:@"Y" forKey:@"day_status"];
        [dictM setObject:@"08:00 am" forKey:@"start_time"];
        [dictM setObject:@"05:00 pm" forKey:@"end_time"];
    }
    
    [arrBusinessHours replaceObjectAtIndex:sender.tag withObject:dictM];
    [tblBusinessHours reloadData];
}

#pragma mark - UITaxtField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    textField.layer.borderColor = COLOR_THEME.CGColor;
    textField.layer.borderWidth = 1.5f;
    
//    [textField resignFirstResponder];
//    datePicker.hidden = toolBar.hidden = NO;
    iIndex = [textField.restorationIdentifier intValue];
    iTag = (int)textField.tag;
    NSLog(@"name of textfield %d", iIndex);

}
/*
- (void)startTextFieldDidBeginEditing:(UITextField *)textField {
    
    [textField resignFirstResponder];
    datePicker.hidden = toolBar.hidden = NO;
    iIndex = [textField.restorationIdentifier intValue];
    iTag = (int)textField.tag;
    start = YES;
}

- (void)endTextFieldDidBeginEditing:(UITextField *)textField {
    
    [textField resignFirstResponder];
    datePicker.hidden = toolBar.hidden = NO;
    iIndex = [textField.restorationIdentifier intValue];
    iTag = (int)textField.tag;
    start = NO;
}
*/
//- (void)textFieldDidEndEditing:(UITextField *)textField {
//
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    [userDefault synchronize];
//
//    NSMutableDictionary *dictM = [NSMutableDictionary dictionary];
//    [dictM addEntriesFromDictionary:[arrBusinessHours objectAtIndex:textField.tag]];
//
//    if ([[userDefault objectForKey:@"timeTextField"] isEqualToString:@"start"]) {
//        [dictM setObject:textField.text forKey:@"startTime"];
//    } else {
//        [dictM setObject:textField.text forKey:@"endTime"];
//    }
//
//    [arrBusinessHours replaceObjectAtIndex:textField.tag withObject:dictM];
//    [tblBusinessHours reloadData];
//}

#pragma mark - Delegate Methods
-(void)sendTime:(NSDate *)time {
    
    NSMutableDictionary *dictM = [NSMutableDictionary dictionary];
    [dictM addEntriesFromDictionary:[arrBusinessHours objectAtIndex:iIndex]];
    
    NSTimeInterval startTime, endTime;// = [time timeIntervalSince1970];
    
    NSString *strTime;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    strTime = [dateFormatter stringFromDate:time];
    
    if (iTag == 0) {
        
        [dictM setObject:strTime forKey:@"start_time"];
        
        strTime = [dateFormatter stringFromDate:time];
        NSDate *endDate = [[NSDate alloc] init];
        endDate = [dateFormatter dateFromString:[dictM objectForKey:@"end_time"]];
        startTime = [time timeIntervalSinceNow];
        endTime = [endDate timeIntervalSinceNow];
        
        if (startTime < endTime) {
            [arrBusinessHours replaceObjectAtIndex:iIndex withObject:dictM];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"" message:@"End time should be greater than start time." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        
    } else {
        
        [dictM setObject:strTime forKey:@"end_time"];
        
        strTime = [dateFormatter stringFromDate:time];
        NSDate *startDate = [[NSDate alloc] init];
        startDate = [dateFormatter dateFromString:[dictM objectForKey:@"start_time"]];
        endTime = [time timeIntervalSinceNow];
        startTime = [startDate timeIntervalSinceNow];
        
        if (startTime < endTime) {
            [arrBusinessHours replaceObjectAtIndex:iIndex withObject:dictM];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"" message:@"End time should be greater than start time." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }
    
    [tblBusinessHours reloadData];
}

#pragma mark - Web Service
-(void) connectionForGetBusinessHours {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_GET_BUSINESS_HOUR];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [self->arrBusinessHours removeAllObjects];
            [self->arrBusinessHours addObjectsFromArray:[[responseObject objectForKey:@"info_array"] objectForKey:@"business_hr_list"]];
            
            if ([[[responseObject objectForKey:@"info_array"] objectForKey:@"business_hour_value"] intValue] == 0) {
                [self OnSelected:self];
                [self->btnSelectedHours setSelected:YES];
//                [self->SelectedOn setEnabled:YES];
            } else {
                [self->btnSelectedHours setSelected:YES];
                [self OnAlaways:self];
            }
            
            [self->tblBusinessHours reloadData];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void) connectionForSendBusinessHours:(NSString *)dayTime {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_SAVE_BUSINESS_HOUR];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"business_hr_type": [NSString stringWithFormat:@"%d", iBusinessHourType],
                             @"day_time": dayTime
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                ;
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            alertController.view.tintColor = COLOR_THEME;
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIButton Action
- (IBAction)OnSelected:(id)sender {
    
    NSLog(@"Open on selected hours");
    iBusinessHourType = 0;
    tblBusinessHours.hidden = NO;
}

- (IBAction)OnAlaways:(id)sender {
    
    NSLog(@"Alaways open");
    iBusinessHourType = 1;
    tblBusinessHours.hidden = YES;
}

- (IBAction)SaveBusinessHours:(id)sender {
    
    NSMutableArray *arrNewDayTime = [[NSMutableArray alloc] init];
    [arrNewDayTime removeAllObjects];
    if (arrBusinessHours.count > 0) {
        for (int i = 0; i < arrBusinessHours.count; i++) {
            NSDictionary *dictDayTime = @{@"day": [NSString stringWithFormat:@"%d", i +1],
                                          @"start_time": [[arrBusinessHours objectAtIndex:i] objectForKey:@"start_time"],
                                          @"end_time": [[arrBusinessHours objectAtIndex:i] objectForKey:@"end_time"],
                                          @"status": [[arrBusinessHours objectAtIndex:i] objectForKey:@"day_status"]
                                          };
            [arrNewDayTime addObject:dictDayTime];
        }
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrNewDayTime options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [self connectionForSendBusinessHours:jsonString];
}
@end
