//
//  ServiceAreaVC.m
//  ProringerPro
//
//  Created by Soma Halder on 25/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ServiceAreaVC.h"
#import "ServiceCollectionReusableView.h"

@interface ServiceAreaVC ()
{
    NSString *strLatitude, *strLongitude, *strZipCode, *strCountry, *strCity, *strState, *strTimeZone, *strStreet;
    
    NSMutableArray *arrServiceAreaList, *arrNewAddress;
}

@end

@implementation ServiceAreaVC
@synthesize collServiceArea, btnAddAddress;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    self.navigationItem.title = @"SERVICE AREA";
    
//    self.tabBarController.tabBar.hidden = NO;
    
    collServiceArea.dataSource = self;
    collServiceArea.delegate = self;
    
    strState = strCity = @"";
    
    arrServiceAreaList = [[NSMutableArray alloc] init];
    arrNewAddress = [[NSMutableArray alloc] init];
    
    [self connectionForServiceAreaList];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Service Area"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Service Area"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
}

#pragma mark - AD Banner Delegate
- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.bottomLayoutGuide
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:0],
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
    //    bannerView.layer.zPosition = -1;
    //    table_view.layer.zPosition = -2;
}

#pragma mark - UICollectioView DataSource
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        ServiceCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView" forIndexPath:indexPath];
        
        if ([strCity isEqualToString:@""] && [strState isEqualToString:@""]) {
            headerView.txtServiceArea.text = @"";
        } else {
            headerView.txtServiceArea.text = [NSString stringWithFormat:@"%@, %@", strCity, strState];
        }
        headerView.txtServiceArea.tag = indexPath.item;
        headerView.txtServiceArea.delegate = self;
        [headerView.txtServiceArea addTarget:self action:@selector(textFieldDidBeginEditing:) forControlEvents:UIControlEventTouchUpInside];
        
        reusableview = headerView;
    }
    return reusableview;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrServiceAreaList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellArea";
    
    NSDictionary *dictService = [arrServiceAreaList objectAtIndex:indexPath.item];
    
    ServiceAreaCollectionViewCell *cell = (ServiceAreaCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.lblAreaName.text = [dictService objectForKey:@"city_service"];
    
    cell.btnDelete.tag = indexPath.item;
    [cell.btnDelete addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    return CGSizeMake(((collServiceArea.frame.size.width -30) /2) -5, 50);
}

-(void)deleteButtonPressed:(UIButton *)sender {
    
    [arrServiceAreaList removeObjectAtIndex:sender.tag];
    
    [arrNewAddress removeAllObjects];
    
    for (int i = 0; i < arrServiceAreaList.count; i++) {
        NSDictionary *dict = [arrServiceAreaList objectAtIndex:i];
        [arrNewAddress addObject:@{@"citi": [dict objectForKey:@"city_service"]}];
    }
//    [arrNewAddress removeObjectAtIndex:sender.tag];
    
    [self connectionSaveServiceArea:YES];
    
//    [collServiceArea reloadData];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self performSegueWithIdentifier:@"zipCode" sender:nil];
}

#pragma mark - Web Service
-(void)connectionForServiceAreaList {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_GET_SERVICE_AREA];
    
    NSLog(@"USER ID:::...%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]);
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        NSLog(@"response from GET SERVICE ARE:::...%@", response);
        [YXSpritesLoadingView dismiss];
        if ([[response objectForKey:@"response"] boolValue] == YES) {
            
            [self->arrServiceAreaList removeAllObjects];
            [self->arrNewAddress removeAllObjects];
            [self->arrServiceAreaList addObjectsFromArray:[response objectForKey:@"info_array"]];
            
            for (int i = 0; i < self->arrServiceAreaList.count; i++) {
                NSDictionary *dict = [self->arrServiceAreaList objectAtIndex:i];
                [self->arrNewAddress addObject:@{@"citi": [dict objectForKey:@"city_service"]}];
            }
            
            [self->collServiceArea reloadData];
        }
    }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             NSLog(@"ERROR:::...%@", task.error);
             [YXSpritesLoadingView dismiss];
         }];
}

-(void)connectionSaveServiceArea:(BOOL)isDelete {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_SERVICE_AREA_SAVE];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrNewAddress options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                                    @"citi": jsonString
                                    };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        NSLog(@"response from SAVE AREAS:::...%@", response);
        [YXSpritesLoadingView dismiss];
        if ([[response objectForKey:@"response"] boolValue] == YES) {
            
            if (isDelete) {
                [self connectionForServiceAreaList];
            } else {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[response objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                    ;
                    [self connectionForServiceAreaList];
                }];
                
                [alertController addAction:okAction];
                
                [self presentViewController:alertController animated:YES completion:nil];
                [alertController.view setTintColor:COLOR_THEME];
            }
        }
    }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              NSLog(@"ERROR:::...%@", task.error);
              [YXSpritesLoadingView dismiss];
          }];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"zipCode"]) {
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        SearchZipVC *szvc = segue.destinationViewController;
        self.providesPresentationContextTransitionStyle = YES;
        self.definesPresentationContext = YES;
        szvc.deleagte = self;
        szvc.strNavigationTitle = @"Service Area";
        szvc.strAddressTitle = @"City / Zip code";
        szvc.strPlaceHolder = @"City / zip";
        szvc.strApi = @"(regions)";
        szvc.strSearchText = @"locality";
        szvc.strSubSearchText = @"sublocality";
        szvc.strSearchComponent = @"long_name";
        [szvc setModalPresentationStyle:UIModalPresentationCurrentContext];
    }
}

#pragma mark - Protocol Method
-(void)sendAddressToUserInfo:(NSDictionary *)dictAddress {
    
    [YXSpritesLoadingView show];
    
    NSArray *arrAddressComponent = [dictAddress objectForKey:@"address_components"];
    
    NSLog(@"Address::...%@", dictAddress);
    
    strZipCode = strCountry = strState = strCity = strStreet = @"";
    
    for (int i = 0; i < arrAddressComponent.count; i++) {
        NSDictionary *dictComponent = [arrAddressComponent objectAtIndex:i];
        NSArray *arrTypes = [dictComponent objectForKey:@"types"];
        for (int iTypes = 0; iTypes < arrTypes.count; iTypes++) {
            if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"postal_code"]) {
                strZipCode = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"country"]) {
                strCountry = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"administrative_area_level_1"]) {
                strState = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"locality"] || [[arrTypes objectAtIndex:iTypes] isEqualToString:@"sublocality"]) {
                strCity = [dictComponent objectForKey:@"long_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"route"]) {
                strStreet = [dictComponent objectForKey:@"long_name"];
            }
        }
    }
    NSLog(@"\npostal_code:::...%@\ncountry:::...%@\nstate:::...%@\ncity:::...%@\nstreet:::...%@", strZipCode, strCountry, strState, strCity, strStreet);
    
    if (strCity.length > 0 && strState.length > 0) {
        
        btnAddAddress.userInteractionEnabled = YES;
        
        strLatitude = [[[[dictAddress objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"] stringValue];
        strLongitude = [[[[dictAddress objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"] stringValue];
        
        double latitude = [strLatitude doubleValue];
        double longitude = [strLongitude doubleValue];
        
        [self findTimeZoneFromSelectedAddressWithLat:latitude andLon:longitude];
    } else {
        
        btnAddAddress.userInteractionEnabled = NO;
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please enter a valid zip/postal code" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//            txtZipCode.text = @"";
//            ;
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        alertController.view.tintColor = [UIColor colorWithRed:241/255.0 green:89/255.0 blue:42/255.0 alpha:1];
    }
    [YXSpritesLoadingView dismiss];
}

-(void)findTimeZoneFromSelectedAddressWithLat:(double)latitude andLon:(double)longitude {
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        
        self->strTimeZone = placemark.timeZone.name;
        
        [self->collServiceArea reloadData];
    }];
}

#pragma mark - TimeZone
-(void)findTimeZoneFromSelectedAddressWithLat:(double)latitude andLon:(double)longitude andOptional:(NSString *)zipCode {
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    [geoCoder reverseGeocodeLocation: location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        
//        self->strTimeZone = [NSString stringWithFormat:@"%@", placemark.timeZone];
        //And to get country name simply.
//        NSLog(@"PLACEMARK ARRAY:::...%@", placemarks);
//        NSLog(@"Country -%@",placemark.country);
//        NSLog(@"Timezone -%@\n%@", placemark.timeZone.name, placemark.timeZone.abbreviation);
//        NSLog(@"State -%@\n%@\n%@\n%@\n%@\n\n\n\n%@", placemark.subLocality, placemark.postalCode, placemark.subAdministrativeArea, placemark.administrativeArea, placemark.addressDictionary, placemark);
        
//        self->strCountry = placemark.country;
//        self->strCity = [placemark.addressDictionary objectForKey:@"City"];
//        self->strState = placemark.subAdministrativeArea;
        
        if (placemark.postalCode == nil) {
            self->strZipCode = zipCode;
        } else {
            self->strZipCode = placemark.postalCode;
        }

        self->strTimeZone = placemark.timeZone.name;
//         strStreet = [placemark.addressDictionary objectForKey:@"Street"];
    }];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:strZipCode forKey:@"zipCode"];
//    [userDefault setObject:strLatitude forKey:@"latitude"];
//    [userDefault setObject:strLongitude forKey:@"longitude"];
//    [userDefault setObject:strCity forKey:@"city"];
//    [userDefault setObject:strState forKey:@"state"];
//    [userDefault setObject:strCountry forKey:@"country"];
    [userDefault synchronize];
}

#pragma mark - UIButton Action
- (IBAction)AddServiceArea:(id)sender {
//    ;
    if (strCity.length > 0 && strState.length > 0) {
        NSString *strCityService = [NSString stringWithFormat:@"%@, %@", strCity, strState];
        
        NSDictionary *dictAddress = @{@"city_service": strCityService};
        
        BOOL found = false;
        
        for (int i = 0; i < arrServiceAreaList.count; i++) {
            NSDictionary *dict = [arrServiceAreaList objectAtIndex:i];
            
            if ([[dict objectForKey:@"city_service"] isEqualToString:strCityService]) {
                found = true;
            }
        }
        if (!found) {
            [arrServiceAreaList addObject:dictAddress];
            [arrNewAddress addObject:@{@"citi": strCityService}];
            
            if (arrNewAddress.count > 0) {
                [self connectionSaveServiceArea:NO];
            }
            
        } else {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"The city you have selected has already been added to your service area." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                ;
//                txtZipCode.text = @"";
            }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            alertController.view.tintColor = [UIColor colorWithRed:241/255.0 green:89/255.0 blue:42/255.0 alpha:1];
        }
        
        strState = strCity = @"";
        
//        [collServiceArea reloadData];
    }
}

- (IBAction)SaveAllServiceArea:(id)sender {
//    ;
    if (arrNewAddress.count > 0) {
        [self connectionSaveServiceArea:NO];
    }
}

- (IBAction)DropDownButtonPressed:(id)sender {
//    ;
    ServiceCollectionReusableView *headerView = [[ServiceCollectionReusableView alloc] init];
    
    [self textFieldDidBeginEditing:headerView.txtServiceArea];
}
@end
