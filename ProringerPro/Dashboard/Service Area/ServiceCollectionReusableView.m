//
//  ServiceCollectionReusableView.m
//  ProringerPro
//
//  Created by Soma Halder on 25/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ServiceCollectionReusableView.h"

@implementation ServiceCollectionReusableView
@synthesize txtServiceArea;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    txtServiceArea.delegate = self;
    
    [self paddingForTextField:txtServiceArea];
    [self shadowForTextField:txtServiceArea];
}

#pragma mark - UITextField Design
-(void) paddingForTextField:(UITextField *)textField {
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(30, 0, 10, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    textField.layer.masksToBounds = NO;
    textField.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    textField.layer.shadowRadius = 1;
    textField.layer.shadowOpacity = 0.2;
    textField.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    
    textField.layer.borderWidth = 1.0;
    textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)shadowForTextField:(UITextField *)textfield {
    
    UIColor *color = [UIColor blackColor];
    textfield.layer.shadowColor = [color CGColor];
    textfield.layer.shadowRadius = 1.5f;
    textfield.layer.shadowOpacity = 0.2;
    textfield.layer.shadowOffset = CGSizeZero;
    textfield.layer.masksToBounds = NO;
}

#pragma mark - UITextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    textField.layer.borderColor = COLOR_THEME.CGColor;
    textField.layer.borderWidth = 1.5f;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self paddingForTextField:textField];
    [self shadowForTextField:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

@end
