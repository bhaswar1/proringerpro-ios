//
//  ServiceAreaVC.h
//  ProringerPro
//
//  Created by Soma Halder on 25/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServiceAreaVC : ProRingerProBaseVC <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collServiceArea;
@property (weak, nonatomic) IBOutlet UIButton *btnAddAddress;

- (IBAction)AddServiceArea:(id)sender;
- (IBAction)SaveAllServiceArea:(id)sender;
- (IBAction)DropDownButtonPressed:(id)sender;

@end

NS_ASSUME_NONNULL_END
