//
//  ServiceCollectionReusableView.h
//  ProringerPro
//
//  Created by Soma Halder on 25/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServiceCollectionReusableView : UICollectionReusableView <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtServiceArea;

@property (weak, nonatomic) IBOutlet UIButton *btnAddAddress;

@end

NS_ASSUME_NONNULL_END
