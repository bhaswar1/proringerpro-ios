//
//  ServiceAreaCollectionViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 25/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServiceAreaCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblAreaName;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

@end

NS_ASSUME_NONNULL_END
