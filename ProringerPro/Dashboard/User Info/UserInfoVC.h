//
//  UserInfoVC.h
//  ProringerPro
//
//  Created by Soma Halder on 22/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserInfoVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITextField *txtFirseName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtPositionTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNumber;

@property (weak, nonatomic) IBOutlet UIView *viewAccessory;

- (IBAction)Submit:(id)sender;

@end

NS_ASSUME_NONNULL_END
