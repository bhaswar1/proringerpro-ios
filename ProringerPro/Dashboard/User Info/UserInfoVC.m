//
//  UserInfoVC.m
//  ProringerPro
//
//  Created by Soma Halder on 22/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "UserInfoVC.h"

@interface UserInfoVC ()
{
    NSString *strPhoneNumber;
}

@end

@implementation UserInfoVC
@synthesize txtPhoneNumber, txtLastName, txtFirseName, txtPositionTitle, viewAccessory;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"USER INFORMATION";
    
//    self.tabBarController.tabBar.hidden = NO;
    
    self.txtFirseName.delegate = self;
    self.txtLastName.delegate = self;
    self.txtPositionTitle.delegate = self;
    self.txtPhoneNumber.delegate = self;
    
    [self paddingForTextField:txtPositionTitle];
    [self paddingForTextField:txtFirseName];
    [self paddingForTextField:txtLastName];
    [self paddingForTextField:txtPhoneNumber];
    
    [self shadowForTextField:txtPositionTitle];
    [self shadowForTextField:txtFirseName];
    [self shadowForTextField:txtLastName];
    [self shadowForTextField:txtPhoneNumber];
    
    [txtFirseName setInputAccessoryView:viewAccessory];
    [txtLastName setInputAccessoryView:viewAccessory];
    [txtPhoneNumber setInputAccessoryView:viewAccessory];
    [txtPositionTitle setInputAccessoryView:viewAccessory];
    
    [self connectionForGetUserInformation];
    
    [self connectionForGetDashboardData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro User Information"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro User Information"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }

}

//#pragma mark - UITextField Design
//-(void) paddingForTextField:(UITextField *)textField {
//    
//    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(30, 0, 10, 20)];
//    textField.leftView = paddingView;
//    textField.leftViewMode = UITextFieldViewModeAlways;
//    
//    textField.layer.masksToBounds = NO;
//    textField.layer.shadowOffset = CGSizeMake(0.0, 0.0);
//    textField.layer.shadowRadius = 1;
//    textField.layer.shadowOpacity = 0.2;
//    textField.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
//    
//    textField.layer.borderWidth = 1.0;
//    textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
//}
//
//-(void)shadowForTextField:(UITextField *)textfield {
//    
//    UIColor *color = [UIColor blackColor];
//    textfield.layer.shadowColor = [color CGColor];
//    textfield.layer.shadowRadius = 1.5f;
//    textfield.layer.shadowOpacity = 0.2;
//    textfield.layer.shadowOffset = CGSizeZero;
//    textfield.layer.masksToBounds = NO;
//}
//
#pragma mark - UITextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == txtPhoneNumber) {
        NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        BOOL deleting = [newText length] < [textField.text length];
        
        NSString *stripppedNumber = [newText stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [newText length])];
        NSUInteger digits = [stripppedNumber length];
        strPhoneNumber = stripppedNumber;
        if (digits > 10)
            stripppedNumber = [stripppedNumber substringToIndex:10];
        
        UITextRange *selectedRange = [textField selectedTextRange];
        NSInteger oldLength = [textField.text length];
        
        if (digits == 0)
            textField.text = @"";
        else if (digits < 3 || (digits == 3 && deleting))
            textField.text = [NSString stringWithFormat:@"(%@", stripppedNumber];
        else if (digits < 6 || (digits == 6 && deleting))
            textField.text = [NSString stringWithFormat:@"(%@) %@", [stripppedNumber substringToIndex:3], [stripppedNumber substringFromIndex:3]];
        else
            textField.text = [NSString stringWithFormat:@"(%@) %@-%@", [stripppedNumber substringToIndex:3], [stripppedNumber substringWithRange:NSMakeRange(3, 3)], [stripppedNumber substringFromIndex:6]];
        
        UITextPosition *newPosition = [textField positionFromPosition:selectedRange.start offset:[textField.text length] - oldLength];
        UITextRange *newRange = [textField textRangeFromPosition:newPosition toPosition:newPosition];
        [textField setSelectedTextRange:newRange];
        
        return NO;
    }
    return YES;
}

//-(void)textFieldDidBeginEditing:(UITextField *)textField {
//    
//    textField.layer.borderColor = COLOR_THEME.CGColor;
//    textField.layer.borderWidth=1.5f;
//    
//}
//
//- (void)textFieldDidEndEditing:(UITextField *)textField {
//    
//    [self paddingForTextField:textField];
//    [self shadowForTextField:textField];
//}
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    
//    [textField resignFirstResponder];
//    return YES;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Web Service
-(void)connectionForGetUserInformation {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_USER_INFO];
    
    NSDictionary *params = @{@"user_id": [userDefault objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            NSDictionary *dictInfo = [[responseObject objectForKey:@"info_array"] objectAtIndex:0];
            
            self->txtFirseName.text = [dictInfo objectForKey:@"f_name"];
            self->txtLastName.text = [dictInfo objectForKey:@"l_name"];
            self->txtPositionTitle.text = [dictInfo objectForKey:@"title_pos"];
            self->strPhoneNumber = self->txtPhoneNumber.text = [dictInfo objectForKey:@"phone"];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForSaveUserInformation {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_SAVE_INFO];
    
    NSDictionary *params = @{@"user_id": [userDefault objectForKey:@"userId"],
                             @"contact_f_name": txtFirseName.text,
                            @"contact_l_name": txtLastName.text,
                            @"title_pos": txtPositionTitle.text,
                            @"phone": txtPhoneNumber.text
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(BOOL)validateTextField {
    
    NSString *strAlert = @"";
    BOOL isValid = YES;
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtFirseName.text]) {
        [txtFirseName becomeFirstResponder];
        isValid = NO;
        strAlert = @"Please enter First Name";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtLastName.text]) {
        [txtLastName becomeFirstResponder];
        isValid = NO;
        strAlert = @"Please enter Last Name";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtPositionTitle.text]) {
        [txtPositionTitle becomeFirstResponder];
        strAlert = @"Please enter title or position";
        isValid = NO;
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtPhoneNumber.text]) {
        [txtPhoneNumber becomeFirstResponder];
        isValid = NO;
        strAlert = @"Please enter a phone number";
    } else if (txtPhoneNumber.text.length > 0) {
        if (![[AppManager sharedDataAccess] isValidPhoneNumber:strPhoneNumber]) {
            [txtPhoneNumber becomeFirstResponder];
            isValid = NO;
            strAlert = @"Please enter correct phone number";
        }
    }
    
    if (strAlert.length > 0) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strAlert];
    }
    return isValid;
}

- (IBAction)Submit:(id)sender {
    if ([self validateTextField]) {
        [self connectionForSaveUserInformation];
    }
}
@end
