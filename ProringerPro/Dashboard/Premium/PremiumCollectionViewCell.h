//
//  PremiumCollectionViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 30/05/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PremiumCollectionViewCell : UICollectionViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imgPremiumOffer;

@property (weak, nonatomic) IBOutlet UILabel *lblOffer;
@property (weak, nonatomic) IBOutlet UILabel *lblPerMonth;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPlanRange;
@property (weak, nonatomic) IBOutlet UILabel *lblBillType;

@property (weak, nonatomic) IBOutlet UICollectionView *collLeads;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewHeight;

@property (strong, nonatomic) NSMutableArray *arrLeads;
@property (weak, nonatomic) IBOutlet UIView *viewHeader;

-(void)configureCellForCollectionViewWith:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
