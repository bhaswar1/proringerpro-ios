//
//  PremiumVC.m
//  ProringerPro
//
//  Created by Soma Halder on 30/05/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "PremiumVC.h"

@interface PremiumVC ()
{
    NSMutableArray *arrPremium;
    NSString *strPlanId;
}

@end

@implementation PremiumVC
@synthesize btnMonthly, btnAnnually, constViewUnderLine, constLeadingMonthly, collPremium;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
//    self.navigationItem.title = @"PREMIUM";
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon"]];
    
    constViewUnderLine.constant = 0.0;
    
    [self->btnAnnually setBackgroundColor:COLOR_THEME];
    [self->btnMonthly setBackgroundColor:COLOR_TABLE_HEADER];
    
    [self->btnAnnually setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self->btnMonthly setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    arrPremium = [[NSMutableArray alloc] init];
    
    collPremium.dataSource = self;
    collPremium.delegate = self;
    collPremium.pagingEnabled = YES;
    
    [self connectionForGetPremiumDetails];
}

#pragma mark - UICollectionView DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrPremium.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellPremium";
    
    NSDictionary *dictPremium = [arrPremium objectAtIndex:indexPath.item];
    
    PremiumCollectionViewCell *cell = (PremiumCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
//    cell.lblOffer.text = [NSString stringWithFormat:@"SAVE %@ ", [dictPremium objectForKey:@"save"]];
//    cell.lblFirstTitle.text = [dictPremium objectForKey:@"first_title"];
//    cell.lblPerMonth.text = [dictPremium objectForKey:@"plan_cost"];
//    if ([[dictPremium objectForKey:@"plan_type"] isEqualToString:@"M"]) {
//        cell.lbltimeCycle.text = @"/month";
//        cell.imgPremiumOffer.image = [UIImage imageNamed:@"Pret"];
//        cell.lblFirstTitle.textColor = [UIColor cyanColor];
//    } else {
//        cell.lbltimeCycle.text = @"/year";
//        cell.imgPremiumOffer.image = [UIImage imageNamed:@"Premium"];
//        cell.lblFirstTitle.textColor = COLOR_THEME;
//    }
//    cell.arrLeads = [[NSMutableArray alloc] init];
//    [cell.arrLeads removeAllObjects];
//    [cell.arrLeads addObjectsFromArray:[dictPremium objectForKey:@"description"]];
//    cell.lblPlanRange.text = [dictPremium objectForKey:@"plan_range"];
//    cell.lblBillType.text = [dictPremium objectForKey:@"bill_type"];
//    [[cell.arrLeads arrayByAddingObjectsFromArray:[dictPremium objectForKey:@"description"]] mutableCopy];
    
    [cell configureCellForCollectionViewWith:dictPremium];
    
    return cell;
}

#pragma mark - UICollectionView Delegate
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.item == 0) {
        [self PremiumAnnually:nil];
    } else if (indexPath.item == 1) {
        [self PremiumMonthly:nil];
    }
    strPlanId = [[arrPremium objectAtIndex:indexPath.item] objectForKey:@"plan_id"];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(collPremium.frame.size.width, collPremium.frame.size.height);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIButton Action
- (IBAction)PremiumAnnually:(id)sender {
    
    [self->btnAnnually setBackgroundColor:COLOR_THEME];
    [self->btnMonthly setBackgroundColor:COLOR_TABLE_HEADER];
    
    NSIndexPath *indexPath;
    indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
        // scrolling here does work with animation
    [collPremium scrollToItemAtIndexPath:indexPath
                        atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                animated:YES];
    
    [self->btnAnnually setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self->btnMonthly setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.5 animations:^{
        
        self->constViewUnderLine.constant = 0.0;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (IBAction)PremiumMonthly:(id)sender {
    
    [self->btnAnnually setBackgroundColor:COLOR_TABLE_HEADER];
    [self->btnMonthly setBackgroundColor:COLOR_THEME];
    
    NSIndexPath *indexPath;
    indexPath = [NSIndexPath indexPathForItem:1 inSection:0];
    
    // scrolling here does work with animation
    [collPremium scrollToItemAtIndexPath:indexPath
                        atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                animated:YES];
    
    [self->btnAnnually setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self->btnMonthly setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.5 animations:^{

        self->constViewUnderLine.constant = self->btnAnnually.frame.size.width;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (IBAction)GoPremium:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    CampaignUpgradeVC *cuvc = (CampaignUpgradeVC *)[storyboard instantiateViewControllerWithIdentifier:@"CampaignUpgradeVC"];
    cuvc.viewHeader.hidden = NO;
    cuvc.viewPaymentMethod.hidden = YES;
    cuvc.strPlanId = strPlanId;
    [self.navigationController pushViewController:cuvc animated:NO];
}

#pragma mark - Web Service
-(void)connectionForGetPremiumDetails {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_PREMIUM_PLANS];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [self->arrPremium removeAllObjects];
            [self->arrPremium addObjectsFromArray:[[responseObject objectForKey:@"info_array"] objectForKey:@"info"]];
            self->arrPremium = [[[self->arrPremium reverseObjectEnumerator] allObjects] mutableCopy];
            
            [self->collPremium reloadData];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

@end
