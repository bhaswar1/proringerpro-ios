//
//  PremiumCollectionViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 30/05/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "PremiumCollectionViewCell.h"

@implementation PremiumCollectionViewCell
@synthesize constViewHeight, arrLeads, collLeads, lblOffer, lblFirstTitle, lblBillType, lblPerMonth, lblPlanRange, imgPremiumOffer, viewHeader;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    arrLeads = [[NSMutableArray alloc] init];
    
    lblOffer.transform = CGAffineTransformMakeRotation(-M_PI/4);
}

-(void)manageHeaderHeight {
    
    if (arrLeads == nil || arrLeads.count == 0) {
        constViewHeight.constant = 0;
        constViewHeight.constant = constViewHeight.constant + 385;
    } else {
        if ((arrLeads.count % 2) > 0) {
            constViewHeight.constant = ((arrLeads.count /2) *30) +25;
            constViewHeight.constant = constViewHeight.constant + 385;
        }
        else {
            constViewHeight.constant = ((arrLeads.count /2) *30) +10;
            constViewHeight.constant = constViewHeight.constant + 385;
        }
    }
    NSLog(@"CONSTANT HEIGHT :::... %f", constViewHeight.constant);
    
    viewHeader.frame = CGRectMake(0, 0, self.contentView.frame.size.width, constViewHeight.constant);
    
    [collLeads reloadData];
}

-(void)configureCellForCollectionViewWith:(NSDictionary *)dict {
    
    [arrLeads removeAllObjects];
    [arrLeads addObjectsFromArray:[dict objectForKey:@"description"]];
    [arrLeads removeLastObject];
    
    if ([[dict objectForKey:@"save"] intValue] == 0) {
        lblOffer.text = @"";
    } else {
        lblOffer.text = [NSString stringWithFormat:@"SAVE %@%@", [dict objectForKey:@"save"], @"%"];
    }
    
    
    lblFirstTitle.text = [dict objectForKey:@"first_title"];
    lblPerMonth.text = [dict objectForKey:@"plan_cost"];
    if ([[dict objectForKey:@"plan_type"] isEqualToString:@"M"]) {
        imgPremiumOffer.image = [UIImage imageNamed:@"Pret"];
        lblFirstTitle.textColor = COLOR_BLUE;
    } else {
        imgPremiumOffer.image = [UIImage imageNamed:@"Premium"];
        lblFirstTitle.textColor = COLOR_THEME;
    }
    lblPlanRange.text = [dict objectForKey:@"plan_range"];
    lblBillType.text = [dict objectForKey:@"bill_type"];
    
    [self manageHeaderHeight];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrLeads.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellLeads";
    
    LeadsCollectionViewCell *cell = (LeadsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.lblLeadsOffers.text = [arrLeads objectAtIndex:indexPath.row];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake((collLeads.frame.size.width /2) -3, 30);
}

@end
