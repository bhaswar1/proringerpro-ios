//
//  PremiumVC.h
//  ProringerPro
//
//  Created by Soma Halder on 30/05/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PremiumVC : ProRingerProBaseVC <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>


@property (weak, nonatomic) IBOutlet UICollectionView *collPremium;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewUnderLine;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constLeadingMonthly;

@property (weak, nonatomic) IBOutlet UIButton *btnAnnually;
@property (weak, nonatomic) IBOutlet UIButton *btnMonthly;

- (IBAction)PremiumAnnually:(id)sender;
- (IBAction)PremiumMonthly:(id)sender;
- (IBAction)GoPremium:(id)sender;

@end

NS_ASSUME_NONNULL_END
