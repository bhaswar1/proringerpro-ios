//
//  PortfolioEditTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 24/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "PortfolioEditTableViewCell.h"

@implementation PortfolioEditTableViewCell
@synthesize txtPortfolio, btnDropDown;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self paddingForTextField:txtPortfolio];
    [self shadowForTextField:txtPortfolio];
}

#pragma mark - UITextField Design
-(void) paddingForTextField:(UITextField *)textField {
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(30, 0, 10, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    textField.layer.masksToBounds = NO;
    textField.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    textField.layer.shadowRadius = 1;
    textField.layer.shadowOpacity = 0.2;
    textField.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    
    textField.layer.borderWidth = 1.0;
    textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)shadowForTextField:(UITextField *)textfield {
    
    UIColor *color = [UIColor blackColor];
    textfield.layer.shadowColor = [color CGColor];
    textfield.layer.shadowRadius = 1.5f;
    textfield.layer.shadowOpacity = 0.2;
    textfield.layer.shadowOffset = CGSizeZero;
    textfield.layer.masksToBounds = NO;
}

-(void)configureCellWith:(NSDictionary *)dict {
    
    if ([[dict objectForKey:@"text"] isEqualToString:@""]) {
        txtPortfolio.placeholder = [dict objectForKey:@"placeHolder"];
    } else {
        txtPortfolio.text = [dict objectForKey:@"text"];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
