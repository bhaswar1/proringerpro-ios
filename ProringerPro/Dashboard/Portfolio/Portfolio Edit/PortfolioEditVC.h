//
//  PortfolioEditVC.h
//  ProringerPro
//
//  Created by Soma Halder on 24/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PortfolioEditVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITableView *tblEditPortfolio;
@property (weak, nonatomic) IBOutlet UICollectionView *collPhotos;

@property (weak, nonatomic) IBOutlet UIPickerView *pickerCategory;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (strong, nonatomic) NSString *strTitle;

@property (weak, nonatomic) IBOutlet UITextField *txtServiceList;
@property (weak, nonatomic) IBOutlet UITextField *txtMonth;
@property (weak, nonatomic) IBOutlet UITextField *txtYear;

@property (assign) BOOL isEdited;

@property (strong, nonatomic) NSMutableDictionary *dictPortfolioDetails;

- (IBAction)AddPhotos:(id)sender;
- (IBAction)SavePortfolio:(id)sender;
- (IBAction)ServiceList:(id)sender;
- (IBAction)Month:(id)sender;
- (IBAction)Year:(id)sender;

@end

NS_ASSUME_NONNULL_END
