//
//  PhotosCollectionViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 24/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PhotosCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgPortfolio;

@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

@end

NS_ASSUME_NONNULL_END
