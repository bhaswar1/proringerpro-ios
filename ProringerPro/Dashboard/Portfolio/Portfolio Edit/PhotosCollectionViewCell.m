//
//  PhotosCollectionViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 24/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "PhotosCollectionViewCell.h"

@implementation PhotosCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _btnDelete.layer.borderWidth = 1.0;
    _btnDelete.layer.borderColor = COLOR_THEME.CGColor;
}

@end
