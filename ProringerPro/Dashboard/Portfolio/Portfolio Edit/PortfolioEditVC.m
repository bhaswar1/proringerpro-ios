//
//  PortfolioEditVC.m
//  ProringerPro
//
//  Created by Soma Halder on 24/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "PortfolioEditVC.h"

@interface PortfolioEditVC () <UITextFieldDelegate>
{
    NSMutableArray *arrEditPortfolio, *arrYear, *arrPhotos, *arrCategoryList, *arrNewImages, *arrTimeStramp;
    NSString *strCategory, *strMonth, *strYear, *strTimeStramp;
    UIToolbar *toolBar;
    NSMutableDictionary *dictCategory;
    UIImage *chosenImage;
    int iTag;
}

@end

@implementation PortfolioEditVC
@synthesize tblEditPortfolio, pickerCategory, datePicker, collPhotos, strTitle, dictPortfolioDetails, isEdited, txtServiceList, txtMonth, txtYear;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    self.navigationItem.title = strTitle;
    
    self.navigationItem.rightBarButtonItem = nil;
    
//    tblEditPortfolio.dataSource = self;
//    tblEditPortfolio.delegate = self;
    
    pickerCategory.dataSource = self;
    pickerCategory.delegate = self;
    
    collPhotos.dataSource = self;
    collPhotos.delegate = self;
    
    txtServiceList.delegate = self;
    txtMonth.delegate = self;
    txtYear.delegate = self;
    
    [self paddingForTextField:txtServiceList];
    [self paddingForTextField:txtMonth];
    [self paddingForTextField:txtYear];
    
    [self shadowForTextField:txtServiceList];
    [self shadowForTextField:txtMonth];
    [self shadowForTextField:txtYear];
    
    strYear = strCategory = strMonth = @"";
    
    dictCategory = [[NSMutableDictionary alloc] init];
    
    arrCategoryList = [[NSMutableArray alloc] init];
    arrPhotos = [[NSMutableArray alloc] init];
    arrYear = [[NSMutableArray alloc] init];
    arrNewImages = [[NSMutableArray alloc] init];
    arrEditPortfolio = [[NSMutableArray alloc] init];
    arrTimeStramp = [[NSMutableArray alloc] init];
    
    if (isEdited) {
        [arrPhotos addObjectsFromArray:[dictPortfolioDetails objectForKey:@"multiple_gallery_image"]];
        txtYear.text = strYear = [dictPortfolioDetails objectForKey:@"project_year"];
        txtMonth.text = strMonth = [dictPortfolioDetails objectForKey:@"project_month"];
        [dictCategory setObject:[[dictPortfolioDetails objectForKey:@"category"] objectForKey:@"category_name"] forKey:@"category_name"];
        [dictCategory setObject:[[dictPortfolioDetails objectForKey:@"category"] objectForKey:@"category_id"] forKey:@"id"];
        txtServiceList.text = strCategory = [dictCategory objectForKey:@"category_name"];
        
        self.navigationItem.rightBarButtonItem = self.navBarRightButton;
    }
    
    [arrEditPortfolio addObject:@{@"text": strCategory, @"placeHolder": @"Select Category"}];
    [arrEditPortfolio addObject:@{@"text": strMonth, @"placeHolder": @"Month"}];
    [arrEditPortfolio addObject:@{@"text": strYear, @"placeHolder": @"Year"}];
    
    [datePicker setMaximumDate:[NSDate date]];
    pickerCategory.backgroundColor = datePicker.backgroundColor = COLOR_TABLE_HEADER;
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    toolBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor redColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    toolBar.items = [NSArray arrayWithObjects:cancelButton, flexibleSpace, doneButton, nil];
    
//    [self.view addSubview:toolBar];
//    toolBar.hidden = pickerCategory.hidden = datePicker.hidden = YES;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    int iYear = [[formatter stringFromDate:[NSDate date]] intValue];
    
    for (int i = 1960; i <= iYear; i++) {
        [arrYear addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    txtServiceList.inputView = pickerCategory;
    txtYear.inputView = txtMonth.inputView = datePicker;
    
    txtYear.inputAccessoryView = txtMonth.inputAccessoryView = txtServiceList.inputAccessoryView = toolBar;
    
    [self connectionForGetCategryTypeList];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro portfolio add_edit"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro portfolio add_edit"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }

}

-(void)rightBarButtonPressed:(id)sender {
    
    NSLog(@"Right button pressed from sub class");
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to delete this portfolio?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"YES, DELETE IT" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
//        ;
        [self connectionForDeletePortfolio];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//        ;
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    [alertController.view setTintColor:COLOR_THEME];
}

#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    iTag = (int)textField.tag;
    
////    [textField resignFirstResponder];
////    toolBar.hidden = NO;
//    if (textField.tag == 0) {
//        pickerCategory.hidden = NO;
//        datePicker.hidden = YES;
//    } else {
//        datePicker.hidden = NO;
//        pickerCategory.hidden = YES;
////        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
////        if (textField.tag == 1) {
////
////            [formatter setDateFormat:@"MMMM"];
////            strMonth = [formatter stringFromDate:[datePicker date]];
////        } else {
////            [formatter setDateFormat:@"yyyy"];
////            strYear = [formatter stringFromDate:[datePicker date]];
////        }
//    }
}

#pragma mark - UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrEditPortfolio.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellEditPortfolio";
        
    PortfolioEditTableViewCell *cell = (PortfolioEditTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell configureCellWith:[arrEditPortfolio objectAtIndex:indexPath.row]];
    
    cell.txtPortfolio.delegate = self;
    cell.btnDropDown.tag = cell.txtPortfolio.tag = indexPath.row;
    [cell.txtPortfolio addTarget:self action:@selector(textFieldDidBeginEditing:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDropDown addTarget:self action:@selector(dropDownButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}

#pragma mark - UIPickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return arrCategoryList.count;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 50)];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRed:241.0f/255 green:89.0f/255 blue:42.0f/255 alpha:1];
    label.font = [UIFont fontWithName:@"OpenSans" size:18];
    label.numberOfLines = 2;
    
    label.text = [[arrCategoryList objectAtIndex:row] objectForKey:@"category_name"];
    
    return label;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [[arrCategoryList objectAtIndex:row] objectForKey:@"category_name"];
}

#pragma mark - UIPickerView Delegate
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    
    return 45;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    dictCategory = [arrCategoryList objectAtIndex:row];
}

#pragma mark - UICollectionView DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrPhotos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellPhoto";
    
    NSDictionary *dict = [arrPhotos objectAtIndex:indexPath.item];
    
    PhotosCollectionViewCell *cell = (PhotosCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSData *data = [dict objectForKey:@"image_name"];
    if ([[dict objectForKey:@"id"] isEqualToString:@""]) {
        cell.imgPortfolio.image = [UIImage imageWithData:data];
    } else {
        [self imageLoadFromURL:[dict objectForKey:@"image_name"] withContainer:cell.imgPortfolio];
    }
    
    cell.btnDelete.tag = indexPath.row;
    [cell.btnDelete addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)imageLoadFromURL:(NSString*)url withContainer:(UIImageView*)imgView {
    NSURL *imageURL = [NSURL URLWithString:url];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            imgView.image = [UIImage imageWithData:imageData];
        });
    });
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake((collPhotos.frame.size.width /5) -4, (collPhotos.frame.size.width /5));
}

#pragma mark - Button Action
-(void)dropDownButtonPressed:(UIButton *)sender {
    
    toolBar.hidden = NO;
    
    iTag = (int)sender.tag;
    
    if (sender.tag == 0) {
        pickerCategory.hidden = NO;
        datePicker.hidden = YES;
    } else {
        datePicker.hidden = NO;
        pickerCategory.hidden = YES;
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        if (sender.tag == 1) {
//            [formatter setDateFormat:@"MMMM"];
//            strMonth = [formatter stringFromDate:[datePicker date]];
        } else {
//            [formatter setDateFormat:@"yyyy"];
//            strYear = [formatter stringFromDate:[datePicker date]];
        }
    }
}

#pragma mark - UITextField Delegate
//- (void)textFieldDidBeginEditing:(UITextField *)textField {
//
//    iTag = (int)textField.tag;
//
//    [textField resignFirstResponder];
//    toolBar.hidden = NO;
//    if (textField.tag == 0) {
//        pickerCategory.hidden = NO;
//        datePicker.hidden = YES;
//    } else {
//        datePicker.hidden = NO;
//        pickerCategory.hidden = YES;
////        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
////        if (textField.tag == 1) {
////
////            [formatter setDateFormat:@"MMMM"];
////            strMonth = [formatter stringFromDate:[datePicker date]];
////        } else {
////            [formatter setDateFormat:@"yyyy"];
////            strYear = [formatter stringFromDate:[datePicker date]];
////        }
//    }
//}

#pragma mark - UIToolBar Button
-(void)doneButtonPressed {
//    ;
//    toolBar.hidden = pickerCategory.hidden = datePicker.hidden = YES;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    if (iTag == 0) {
        if (dictCategory == nil) {
            dictCategory = [arrCategoryList objectAtIndex:0];
        }
        txtServiceList.text = [dictCategory objectForKey:@"category_name"];
    } else if (iTag == 1) {
        [formatter setDateFormat:@"MMMM"];
        strMonth = [formatter stringFromDate:[datePicker date]];
        [formatter setDateFormat:@"yyyy"];
        strYear = [formatter stringFromDate:[datePicker date]];
//        txtMonth.text = strMonth;
    } else if (iTag == 2) {
        [formatter setDateFormat:@"MMMM"];
        strMonth = [formatter stringFromDate:[datePicker date]];
        [formatter setDateFormat:@"yyyy"];
        strYear = [formatter stringFromDate:[datePicker date]];
//        txtYear.text = strYear;
    }
    txtMonth.text = strMonth;
    txtYear.text = strYear;
    
    arrEditPortfolio = [[NSMutableArray alloc] initWithObjects:
                        @{@"text": txtServiceList.text, @"placeHolder": @"Select Category"},
                        @{@"text": strMonth, @"placeHolder": @"Select Month"},
                        @{@"text": strYear, @"placeHolder": @"Select Year"}, nil];
    
//    [tblEditPortfolio reloadData];
    [self cancelButtonPressed];
}

-(void)cancelButtonPressed {
//    ;
    [txtYear resignFirstResponder];
    [txtMonth resignFirstResponder];
    [txtServiceList resignFirstResponder];
    
//    toolBar.hidden = pickerCategory.hidden = datePicker.hidden = YES;
}

#pragma mark - UITableView Button
-(void)deleteButtonPressed:(UIButton *)sender {
    
    NSDictionary *dict = [arrPhotos objectAtIndex:sender.tag];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to delete this image?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
//        ;
        [self connectionForDeletePortfolioImage:[dict objectForKey:@"id"]];
        [self->arrPhotos removeObjectAtIndex:sender.tag];
        [self->collPhotos reloadData];
    }];
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//        ;
    }];
    
    [alertController addAction:noAction];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:yesAction completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIButton Action
- (IBAction)AddPhotos:(id)sender {
    
//    ;
    
    if (arrPhotos.count < 10) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        
        UIAlertController* alertController = [UIAlertController
                                    alertControllerWithTitle:nil
                                    message:nil
                                    preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *camera = [UIAlertAction actionWithTitle:@"CAMERA" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //        ;
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:picker animated:YES completion:nil];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Your device has no camera or camera is not supported" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            }
        }];
        UIAlertAction *photo = [UIAlertAction actionWithTitle:@"PHOTOS" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //        ;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:picker animated:YES completion:nil];
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            //        ;
        }];
        
        [alertController addAction:camera];
        [alertController addAction:photo];
        [alertController addAction:cancel];
        
        [self presentViewController:alertController animated:YES completion:nil];
        alertController.view.tintColor = COLOR_THEME;
    } else {
        
        [[AppManager sharedDataAccess] showAlertWithTitle:@"Sorry" andMessage:@"You can't upload more than 10 Images"];
    }
}

#pragma mark - UITextField Validation
-(BOOL)validateTextfield {
    
    BOOL isValid = YES;
    NSString *strMsg = @"";
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtServiceList.text]) {
        isValid = NO;
        strMsg = @"Please select service";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtMonth.text]) {
        isValid = NO;
        strMsg = @"Please select month";
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtYear.text]) {
        isValid = NO;
        strMsg = @"Please select Year";
    }
    
    if (!isValid) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strMsg];
    }
    
    return isValid;
}

- (IBAction)SavePortfolio:(id)sender {
    
    if ([self validateTextfield]) {
        if (isEdited) {
            [self connectionForEditPortfolio];
        } else {
            [self connectionForAddPortfolio];
        }
    }
}

- (IBAction)ServiceList:(id)sender {
    [txtServiceList becomeFirstResponder];
}

- (IBAction)Month:(id)sender {
    [txtMonth becomeFirstResponder];
}

- (IBAction)Year:(id)sender {
    [txtYear becomeFirstResponder];
}

//#pragma mark - UIImagePicker Delegate
//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
//
//    UIImage *chosenImage = [info objectForKey:UIImagePickerControllerOriginalImage];
//    NSData *imageData = UIImagePNGRepresentation(chosenImage);
//    imageData = [NSData dataWithData:imageData];
//
//    strTimeStramp = [NSString stringWithFormat:@"%f.png", [[NSDate date] timeIntervalSince1970] *1000];
//    [arrTimeStramp addObject:strTimeStramp];
//
//    [arrNewImages addObject:UIImagePNGRepresentation([self compressImage:chosenImage])];
//
//    NSMutableDictionary *dictPhotos = [[NSMutableDictionary alloc] init];
//    [dictPhotos setObject:imageData forKey:@"image_name"];
//    [dictPhotos setObject:@"" forKey:@"id"];
//
//    [arrPhotos insertObject:dictPhotos atIndex:arrPhotos.count];
//
//    [collPhotos reloadData];
//
//    [picker dismissViewControllerAnimated:YES completion:NULL];
//}

#pragma mark - UIImagePicker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    // output image
    chosenImage = info[UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [self openEditor];
    }];
}

-(UIImage *)compressImage:(UIImage *)image {
    
    NSData *imgData = UIImageJPEGRepresentation(image, 1); //1 it represents the quality of the image.
    NSLog(@"Size of Image(bytes):%ld",(unsigned long)[imgData length]);
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    NSLog(@"Size of Image(bytes):%ld",(unsigned long)[imageData length]);
    
    return [UIImage imageWithData:imageData];
}

-(void)openEditor {
    self.cropController.image = chosenImage;
    self.cropController.keepingCropAspectRatio = YES;
    self.cropController.cropAspectRatio = 1.0;
    CGFloat ratio = 1.0f / 1.0f;
    CGRect cropRect = self.cropController.cropView.cropRect;
    CGFloat widthCrop = CGRectGetWidth(cropRect);
    cropRect.size = CGSizeMake(widthCrop, widthCrop * ratio);
    self.cropController.cropView.cropRect = cropRect;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.cropController];
    
    [self presentViewController:navigationController animated:YES completion:^{
        [self.cropController clickedButtonAtIndex:1];
    }];
}

- (void)cancelButtonDidPush:(id)sender {
    
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - PECropViewControllerDelegate methods
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
    chosenImage = croppedImage;
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
    chosenImage = croppedImage;
    NSData *imageData;// = UIImagePNGRepresentation(chosenImage);
    imageData = UIImageJPEGRepresentation(chosenImage, 1.0);
    imageData = [NSData dataWithData:imageData];
    
    if (imageData.length > 3000000) {
        imageData = UIImageJPEGRepresentation(chosenImage, 0.6);
        imageData = [NSData dataWithData:imageData];
        NSLog(@"Size of Image after resize(bytes):%lu",(unsigned long)[imageData length]);
    }
    
    strTimeStramp = [NSString stringWithFormat:@"%f.png", [[NSDate date] timeIntervalSince1970] *1000];
    [arrTimeStramp addObject:strTimeStramp];
    
    [arrNewImages addObject:UIImagePNGRepresentation([self compressImage:chosenImage])];
    
    NSMutableDictionary *dictPhotos = [[NSMutableDictionary alloc] init];
    [dictPhotos setObject:imageData forKey:@"image_name"];
    [dictPhotos setObject:@"" forKey:@"id"];
    
    [arrPhotos insertObject:dictPhotos atIndex:arrPhotos.count];
    
    [collPhotos reloadData];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:^{
//        self->imgLicence.image = self->chosenImage;
    }];
}

#pragma mark - Web Service
-(void)connectionForGetCategryTypeList {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_CATEGORY_LIST];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
//        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [self->arrCategoryList removeAllObjects];
            [self->arrCategoryList addObjectsFromArray:[responseObject objectForKey:@"info_array"]];
            [self->pickerCategory reloadAllComponents];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForAddPortfolio {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_PORTFOLIO_ADD];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"cat_id": [dictCategory objectForKey:@"id"],
                             @"month": strMonth,
                             @"year": strYear,
                             @"gallery_image[]": @""
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        for (int i = 0; i < self->arrNewImages.count; i++) {
        
            [formData appendPartWithFileData:[self->arrNewImages objectAtIndex:i]
                                        name:[NSString stringWithFormat:@"gallery_image[%d]", i]
                                    fileName:[self->arrTimeStramp objectAtIndex:i] mimeType:@"image/jpg"];
        }
    }
         progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
             NSLog(@"Response: %@", responseObject);
             
             if ([[responseObject objectForKey:@"response"] boolValue] == true) {
                 
                 [YXSpritesLoadingView dismiss];
                 
                 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                     [self->arrNewImages removeAllObjects];
                     [self.navigationController popViewControllerAnimated:YES];
                 }];
                 [alertController addAction:okAction];
                 [self presentViewController:alertController animated:YES completion:nil];
             }
             
         } failure:^(NSURLSessionDataTask *task, NSError *error) {
             [YXSpritesLoadingView dismiss];
             NSLog(@"Error: %@", error);
         }];
}

-(void)connectionForEditPortfolio {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_PORTFOLIO_ADD];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"cat_id": [dictCategory objectForKey:@"id"],
                             @"month": strMonth,
                             @"year": strYear,
                             @"port_id": [dictPortfolioDetails objectForKey:@"id"],
                             @"gallery_image[]": @""
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        for (int i = 0; i < self->arrNewImages.count; i++) {
            
            [formData appendPartWithFileData:[self->arrNewImages objectAtIndex:i]
                                        name:[NSString stringWithFormat:@"gallery_image[%d]", i]
                                    fileName:[self->arrTimeStramp objectAtIndex:i] mimeType:@"image/jpg"];
        }
    }
         progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
             NSLog(@"Response: %@", responseObject);
             
             if ([[responseObject objectForKey:@"response"] boolValue] == true) {
                 
                 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                     [self->arrNewImages removeAllObjects];
                     [self.navigationController popViewControllerAnimated:YES];
                 }];
                 [alertController addAction:okAction];
                 [self presentViewController:alertController animated:YES completion:nil];
                 alertController.view.tintColor = COLOR_THEME;
             }
             [YXSpritesLoadingView dismiss];
         } failure:^(NSURLSessionDataTask *task, NSError *error) {
             [YXSpritesLoadingView dismiss];
             NSLog(@"Error: %@", error);
             [[AppManager sharedDataAccess] showAlertWithTitle:@"Error" andMessage:error];
         }];
}

-(void)connectionForDeletePortfolio {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_PORTFOLIO_DELETE];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"portfolio_id": [dictPortfolioDetails objectForKey:@"id"],
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"DELETE ALL RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForDeletePortfolioImage:(NSString *)strImageId {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_PORTFOLIO_IMAGE_DELETE];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"image_id": strImageId,
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"DELETE IMAGE RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                ;
            }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

@end
