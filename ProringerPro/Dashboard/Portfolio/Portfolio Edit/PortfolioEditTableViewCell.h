//
//  PortfolioEditTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 24/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PortfolioEditTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *txtPortfolio;

@property (weak, nonatomic) IBOutlet UIButton *btnDropDown;

-(void)configureCellWith:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
