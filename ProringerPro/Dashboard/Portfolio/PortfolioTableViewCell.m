//
//  PortfolioTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 24/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "PortfolioTableViewCell.h"

@implementation PortfolioTableViewCell
@synthesize lblProjectDate, lblCategoryName, lblNumberOfImages, imgGalleryImage;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)configureCellWith:(NSDictionary *)dict {
    
    lblCategoryName.text = [[dict objectForKey:@"category"] objectForKey:@"category_name"];
    lblProjectDate.text = [NSString stringWithFormat:@"%@ %@", [dict objectForKey:@"project_month"], [dict objectForKey:@"project_year"]];
    lblNumberOfImages.text = [dict objectForKey:@"count_images"];
    
    [self imageLoadFromURL:[dict objectForKey:@"gallery_images"] withContainer:imgGalleryImage];
}

-(void)imageLoadFromURL:(NSString*)url withContainer:(UIImageView*)imgView {
    NSURL *imageURL = [NSURL URLWithString:url];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            imgView.image = [UIImage imageWithData:imageData];
        });
    });
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
