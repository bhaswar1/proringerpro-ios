//
//  PortfolioVC.m
//  ProringerPro
//
//  Created by Soma Halder on 24/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "PortfolioVC.h"

@interface PortfolioVC ()
{
    NSMutableArray *arrPortfolioList;
}

@end

@implementation PortfolioVC
@synthesize tblPortfolio, imgIndicator, lblSuggestion;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    self.navigationItem.title = @"PORTFOLIO";
    
//    self.tabBarController.tabBar.hidden = NO;
    
//    self.rightButton = YES;
    
    lblSuggestion.hidden = imgIndicator.hidden = YES;
    
    arrPortfolioList = [[NSMutableArray alloc] init];
    
    tblPortfolio.dataSource = self;
    tblPortfolio.delegate = self;
    
//    [self connectionForGetPortfolioList];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro portfolio"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro portfolio"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
}

#pragma mark - AD Banner Delegate
- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.bottomLayoutGuide
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:0],
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
    //    bannerView.layer.zPosition = -1;
    //    table_view.layer.zPosition = -2;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    lblSuggestion.hidden = imgIndicator.hidden = YES;
    
    [self connectionForGetPortfolioList];
}

-(void)rightBarButtonPressed:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    
    PortfolioEditVC *pevc = [storyboard instantiateViewControllerWithIdentifier:@"PortfolioEditVC"];
    pevc.isEdited = NO;
    pevc.strTitle = @"ADD PORTFOLIO";
    [self.navigationController pushViewController:pevc animated:YES];
}

//-(void)rightBarButtonPressed {
//    
//    NSLog(@"Right button pressed");
//}

#pragma mark - UITableView Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrPortfolioList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellPortfolio";
    
    NSDictionary *dictPortfolio = [arrPortfolioList objectAtIndex:indexPath.row];
    
    PortfolioTableViewCell *cell = (PortfolioTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell configureCellWith:dictPortfolio];
    
    return cell;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 85;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    
    PortfolioEditVC *pevc = [storyboard instantiateViewControllerWithIdentifier:@"PortfolioEditVC"];
    pevc.strTitle = @"PORTFOLIO";
    pevc.dictPortfolioDetails = [[NSMutableDictionary alloc] init];
    pevc.dictPortfolioDetails = [arrPortfolioList objectAtIndex:indexPath.row];
    pevc.isEdited = YES;
    [self.navigationController pushViewController:pevc animated:YES];
}

#pragma mark - Web Service
-(void)connectionForGetPortfolioList {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_PORTFOLIO_LIST];
    
    NSDictionary *param = @{@"user_id": [userDefault objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [self->arrPortfolioList removeAllObjects];
            [self->arrPortfolioList addObjectsFromArray:[responseObject objectForKey:@"info_array"]];
            
            if (self->arrPortfolioList.count > 4) {
                self.rightButton = NO;
                self.navigationItem.rightBarButtonItem = nil;
            } else {
                self.navigationItem.rightBarButtonItem = self.navBarRightButton;
            }
            
            if (self->arrPortfolioList.count == 0) {
                self->lblSuggestion.hidden = self->imgIndicator.hidden = NO;
            } else {
                self->lblSuggestion.hidden = self->imgIndicator.hidden = YES;
            }
            
            [self->tblPortfolio reloadData];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
