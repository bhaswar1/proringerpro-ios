//
//  LicenceEditVC.m
//  ProringerPro
//
//  Created by Soma Halder on 23/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "LicenceEditVC.h"

@interface LicenceEditVC ()
{
    NSMutableArray *arrCategoryList;
    NSDictionary *dictCategory, *dictTemp;
//    UIToolbar *toolBar;
    BOOL isSelectedDate;
    NSData *dataLicenseImage;
    UIImage *chosenImage;
}

@end

@implementation LicenceEditVC
@synthesize txtIssuer, txtCategory, txtExpireDate, txtLicenceNumber, strTitle, strImage, imgLicence, isEdited, lblAddImage, dictLicenseDetails, pickerCategoryList, datePicker, btnUploadImage, toolBar, flexibleSpace, doneButton, cancelButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    chosenImage = [UIImage imageNamed:@"DotBlank"];
    self.title = strTitle;
//    self.title = @"LICENSE";
    
    txtIssuer.delegate = self;
    txtExpireDate.delegate = self;
    txtLicenceNumber.delegate = self;
    txtCategory.delegate = self;
    
    pickerCategoryList.dataSource = self;
    pickerCategoryList.delegate = self;
    
    arrCategoryList = [[NSMutableArray alloc] init];
    
    [self paddingForTextField:txtIssuer];
    [self paddingForTextField:txtLicenceNumber];
    [self paddingForTextField:txtExpireDate];
    [self paddingForTextField:txtCategory];

    [self shadowForTextField:txtLicenceNumber];
    [self shadowForTextField:txtExpireDate];
    [self shadowForTextField:txtCategory];
    [self shadowForTextField:txtIssuer];
    
    self.navigationItem.rightBarButtonItem = nil;
    
    [btnUploadImage setBackgroundColor:COLOR_THEME];
    [btnUploadImage setTitle:@"UPLOAD LICENSE" forState:UIControlStateNormal];
    
    if (isEdited) {
        [self imageLoadFromURL:[dictLicenseDetails objectForKey:@"image_info"] withContainer:imgLicence];
        
        txtIssuer.text = [dictLicenseDetails objectForKey:@"license_issuer"];
        txtLicenceNumber.text = [dictLicenseDetails objectForKey:@"licenses_no"];
        txtExpireDate.text = [dictLicenseDetails objectForKey:@"date_expire"];
        strImage = [dictLicenseDetails objectForKey:@"image_info"];
        dictCategory = [dictLicenseDetails objectForKey:@"category"];
        txtCategory.text = [dictCategory objectForKey:@"name"];
        self.navigationItem.rightBarButtonItem = self.navBarRightButton;
        [btnUploadImage setTitle:@"EDIT FILE" forState:UIControlStateNormal];
    }
    
//    toolBar.hidden = datePicker.hidden = pickerCategoryList.hidden = YES;
    datePicker.backgroundColor = pickerCategoryList.backgroundColor = [UIColor whiteColor];
    datePicker.minimumDate = [NSDate date];
    
    [datePicker setValue:[UIColor colorWithRed:241.0f/255 green:89.0f/255 blue:42.0f/255 alpha:1] forKeyPath:@"textColor"];
    
    [txtCategory setInputAccessoryView:toolBar];
    [txtExpireDate setInputAccessoryView:toolBar];
    
    [self connectionForGetCategryTypeList];
    
    if ([strImage isEqualToString:@""] || strImage == nil || [strImage isEqual:[NSNull null]]) {
        self->lblAddImage.text = @"Add image";
        [btnUploadImage setTitle:@"UPLOAD LICENSE" forState:UIControlStateNormal];
    } else {
        self->lblAddImage.text = @"";
        [btnUploadImage setTitle:@"EDIT FILE" forState:UIControlStateNormal];
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro licence add_edit"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro licence add_edit"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }

}

-(void)rightBarButtonPressed:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warnning" message:@"Are you sure you want to Delete this licence?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"YES, DELETE IT" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [self connectionForDeleteLicence];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    [alertController.view setTintColor:COLOR_THEME];
}

-(void)imageLoadFromURL:(NSString*)url withContainer:(UIImageView*)imgView {
    
    NSURL *imageURL = [NSURL URLWithString:url];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            imgView.image = [UIImage imageWithData:imageData];
            self->dataLicenseImage = imageData;
            imgView.alpha = 1.0;
        });
    });
}

#pragma mark - Validation
-(BOOL)validationForAllTextField {
    
    BOOL isValid = YES;
    
    NSString *strMsg = @"";
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtCategory.text]) {
        strMsg = @"Please select Category";
        isValid = NO;
        [txtCategory becomeFirstResponder];
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtIssuer.text]) {
        strMsg = @"Please enter issuer name";
        isValid = NO;
        [txtIssuer becomeFirstResponder];
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtLicenceNumber.text]) {
        strMsg = @"Please enter your license number";
        isValid = NO;
        [txtLicenceNumber becomeFirstResponder];
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtExpireDate.text]) {
        strMsg = @"Please select expiry date";
        isValid = NO;
        [txtExpireDate becomeFirstResponder];
    }
    
    if (strMsg.length > 0) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strMsg];
    }
    
    return isValid;
}

#pragma mark - UIToolBar Button
-(void) doneButtonPressed {
    if (isSelectedDate) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"MM-dd-yyyy"];
        txtExpireDate.text = [dateFormatter stringFromDate:[datePicker date]];
        [txtExpireDate resignFirstResponder];
    } else {
        if (dictCategory == nil) {
            dictCategory = [arrCategoryList objectAtIndex:0];
        }
        txtCategory.text = [dictCategory objectForKey:@"category_name"];
        [txtCategory resignFirstResponder];
    }
    
    pickerCategoryList.hidden = datePicker.hidden = toolBar.hidden = YES;
}

-(void) cancelButtonPressed {
    pickerCategoryList.hidden = datePicker.hidden = toolBar.hidden = YES;
    if (dictTemp != nil) {
        dictCategory = dictTemp;
    }
    [txtExpireDate resignFirstResponder];
    [txtCategory resignFirstResponder];
}

#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    textField.layer.borderColor = COLOR_THEME.CGColor;
    textField.layer.borderWidth = 1.5f;
    if (dictCategory != nil) {
        dictTemp = dictCategory;
        dictCategory = nil;
    }
    
    if (textField == txtCategory) {
//        [textField resignFirstResponder];
        [textField setInputView:pickerCategoryList];
        pickerCategoryList.hidden = toolBar.hidden = NO;
        datePicker.hidden = YES;
        isSelectedDate = NO;
    } else if (textField == txtExpireDate) {
//        [textField resignFirstResponder];
        [textField setInputView:datePicker];
        datePicker.hidden = toolBar.hidden = NO;
        pickerCategoryList.hidden = YES;
        
//        [datePicker setMaximumDate:[NSDate date]];
        
        isSelectedDate = YES;
    } else {
        isSelectedDate = NO;
        [self cancelButtonPressed];
    }
}

#pragma mark - UIPickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return arrCategoryList.count;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 50)];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRed:241.0f/255 green:89.0f/255 blue:42.0f/255 alpha:1];
    label.font = [UIFont fontWithName:@"OpenSans" size:18];
    label.numberOfLines = 2;
    
    label.text = [[arrCategoryList objectAtIndex:row] objectForKey:@"category_name"];
    
    return label;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [[arrCategoryList objectAtIndex:row] objectForKey:@"category_name"];
}

#pragma mark - UIPickerView Delegate
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 50;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
//    strCategoryName = [[arrCategoryList objectAtIndex:row] objectForKey:@"category_name"];
    dictCategory = [arrCategoryList objectAtIndex:row];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Web Service
-(void)connectionForSaveLicense {
    
    [YXSpritesLoadingView show];
    
    if (dataLicenseImage.length > 0) {
        NSLog(@"IMAGE FOUND");
    }
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_ADD_LICENSE];
    
    NSString *strCatId;
    
    if (dictCategory == nil) {
        strCatId = [dictTemp objectForKey:@"id"];
    } else {
        strCatId = [dictCategory objectForKey:@"id"];
    }
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"cat_id": strCatId,
                             @"license_issuer": txtIssuer.text,
                             @"licenses_no": txtLicenceNumber.text,
                             @"date_expire": txtExpireDate.text,
//                             @"image_info":@""
                                 };
    
    NSLog(@"SHOW PARAMETER:::...%@", params);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentType = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentType addObject:@"text/html"];
    [contentType addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentType;
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:self->dataLicenseImage name:@"image_info" fileName:@"LicenceImage.png" mimeType:@"image/png"];
        
    } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [YXSpritesLoadingView dismiss];
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"ERROR:::...%@", task.error);
        [YXSpritesLoadingView dismiss];
    }];
}

-(void)connectionForGetCategryTypeList {
    
    [YXSpritesLoadingView show];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_CATEGORY_LIST];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [self->arrCategoryList removeAllObjects];
            [self->arrCategoryList addObjectsFromArray:[responseObject objectForKey:@"info_array"]];
            [self->pickerCategoryList reloadAllComponents];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForEditLicense {
    
    [YXSpritesLoadingView show];
    
    if (dataLicenseImage.length > 0) {
        NSLog(@"IMAGE FOUND");
    }
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_EDIT_LICENSE];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"license_id": [dictLicenseDetails objectForKey:@"id"],
                             @"cat_id": [dictCategory objectForKey:@"id"],
                             @"license_issuer": txtIssuer.text,
                             @"licenses_no": txtLicenceNumber.text,
                             @"date_expire": txtExpireDate.text,
                             @"image_info":@""
                             };
    
    NSLog(@"SHOW PARAMETER:::...%@", params);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentType = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentType addObject:@"text/html"];
    [contentType addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentType;
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:self->dataLicenseImage
                                    name:@"image_info"
                                fileName:@"LicenceImage.png"
                                mimeType:@"image/jpg"];
        
    } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [YXSpritesLoadingView dismiss];
        
        NSLog(@"RESPONSE FROM LICENSE:::...%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"ERROR:::...%@", task.error);
        [[AppManager sharedDataAccess] showAlertWithTitle:@"Error" andMessage:task.error];
        [YXSpritesLoadingView dismiss];
    }];
}

-(void)connectionForDeleteLicence {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_DELETE_LICENSE];
    
    NSDictionary *param = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                            @"license_id": [dictLicenseDetails objectForKey:@"id"]
                            };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"DELETE ALL RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(UIImage *)centerCropImage:(UIImage *)image {
    // Use smallest side length as crop square length
    CGFloat squareLength = MIN(image.size.width, image.size.height);
    // Center the crop area
    CGRect clippedRect = CGRectMake((image.size.width - squareLength) / 2, (image.size.height - squareLength) / 2, squareLength, squareLength);
    // Crop logic
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], clippedRect);
    UIImage * croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return croppedImage;
}

#pragma mark - UIButton Action
- (IBAction)UploadLicenseImage:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"License image" message:@"Please choose image source type" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"CAMERA" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:nil];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Your device has no camera or camera is not supported" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }];
    UIAlertAction *photo = [UIAlertAction actionWithTitle:@"PHOTOS" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
    }];
    
    [alertController addAction:camera];
    [alertController addAction:photo];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
    alertController.view.tintColor = COLOR_THEME;
}

- (IBAction)SaveLicense:(id)sender {
    
    if ([self validationForAllTextField]) {
        if (dataLicenseImage == nil) {
            [[[UIAlertView alloc] initWithTitle:@"" message:@"Please add image" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        } else {
            if (isEdited) {
                [self connectionForEditLicense];
            } else {
                [self connectionForSaveLicense];
            }
        }
    }
}

- (IBAction)DropDownButtonPressed:(id)sender {
    
    [txtCategory becomeFirstResponder];
    
//    pickerCategoryList.hidden = toolBar.hidden = NO;
//    datePicker.hidden = YES;
//    isSelectedDate = NO;
}

- (IBAction)DoneButtonPressed:(id)sender {
    
    [self doneButtonPressed];
}

- (IBAction)CancelButtonPressed:(id)sender {
    
    [self cancelButtonPressed];
}

#pragma mark - UIImagePicker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    // output image
    chosenImage = info[UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [self openEditor];
    }];
}

-(void)openEditor {
    self.cropController.image = chosenImage;
    self.cropController.keepingCropAspectRatio = YES;
    self.cropController.cropAspectRatio = 1.0;
    
    CGFloat ratio = 1.0f / 1.0f;
    CGRect cropRect = self.cropController.cropView.cropRect;
    CGFloat widthCrop = CGRectGetWidth(cropRect);
    cropRect.size = CGSizeMake(widthCrop, widthCrop * ratio);
    self.cropController.cropView.cropRect = cropRect;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.cropController];
    
    [self presentViewController:navigationController animated:YES completion:^{
        [self.cropController clickedButtonAtIndex:1];
    }];
}

- (void)cancelButtonDidPush:(id)sender {
    
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - PECropViewControllerDelegate methods
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
    chosenImage = croppedImage;
    
//    [self resizeAndConvertImageTodata];
    [self resizeImage:chosenImage];
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
    chosenImage = croppedImage;
    
//    [self resizeAndConvertImageTodata];
    [self resizeImage:chosenImage];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:^{
        self->imgLicence.image = self->chosenImage;
    }];
}

-(void)resizeAndConvertImageTodata {
    
    dataLicenseImage = UIImagePNGRepresentation(chosenImage);
//    dataLicenseImage = UIImageJPEGRepresentation(chosenImage, 1.0);
//    dataLicenseImage = [NSData dataWithData:dataLicenseImage];
    NSLog(@"Size of Image(bytes):%lu",(unsigned long)[dataLicenseImage length]);
    
    NSUInteger iDataSize = [dataLicenseImage length];
  /*
    if (iDataSize > 4000000) {
//        [self centerCropImage:chosenImage];
//        dataLicenseImage = UIImagePNGRepresentation(chosenImage);
        dataLicenseImage = UIImageJPEGRepresentation(chosenImage, 0.6);
        dataLicenseImage = [NSData dataWithData:dataLicenseImage];
        NSLog(@"Size of Image after resize(bytes):%lu",(unsigned long)[dataLicenseImage length]);
    }
   */
    imgLicence.image = chosenImage;
    if (self->imgLicence.image == nil) {
        self->lblAddImage.text = @"Add image";
        [btnUploadImage setTitle:@"UPLOAD LICENSE" forState:UIControlStateNormal];
    } else {
        self->lblAddImage.text = @"";
        [btnUploadImage setTitle:@"EDIT FILE" forState:UIControlStateNormal];
    }
}

- (void)resizeImage:(UIImage*)image {
    
    NSData *finalData = nil;
    NSData *unscaledData = UIImagePNGRepresentation(image);
    
    if (unscaledData.length > 5000.0f ) {
        
        //if image size is greater than 5KB dividing its height and width maintaining proportions
        
        UIImage *scaledImage = [self imageWithImage:image andWidth:image.size.width/2 andHeight:image.size.height/2];
        finalData = UIImagePNGRepresentation(scaledImage);
        
        if (finalData.length > 500000.0f ) {
            
            [self resizeImage:scaledImage];
        } else {
            //scaled image will be your final image
            dataLicenseImage = UIImagePNGRepresentation(scaledImage);
            chosenImage = scaledImage;
            imgLicence.image = chosenImage;
        }
    }
    imgLicence.image = chosenImage;
}

- (UIImage*)imageWithImage:(UIImage*)image andWidth:(CGFloat)width andHeight:(CGFloat)height {
    
    UIGraphicsBeginImageContext( CGSizeMake(width, height));
    [image drawInRect:CGRectMake(0,0,width,height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext() ;
    return newImage;
}

@end
