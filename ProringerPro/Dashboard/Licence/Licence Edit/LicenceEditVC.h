//
//  LicenceEditVC.h
//  ProringerPro
//
//  Created by Soma Halder on 23/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LicenceEditVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITextField *txtCategory;
@property (weak, nonatomic) IBOutlet UITextField *txtIssuer;
@property (weak, nonatomic) IBOutlet UITextField *txtLicenceNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtExpireDate;

@property (weak, nonatomic) IBOutlet UIImageView *imgLicence;

@property (weak, nonatomic) IBOutlet UILabel *lblAddImage;

@property (weak, nonatomic) IBOutlet UIPickerView *pickerCategoryList;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (strong, nonatomic) NSString *strTitle, *strImage;
@property (strong, nonatomic) NSMutableDictionary *dictLicenseDetails;

@property (assign) BOOL isEdited;
@property (weak, nonatomic) IBOutlet UIButton *btnUploadImage;

@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *flexibleSpace;

- (IBAction)UploadLicenseImage:(id)sender;
- (IBAction)SaveLicense:(id)sender;
- (IBAction)DropDownButtonPressed:(id)sender;
- (IBAction)DoneButtonPressed:(id)sender;
- (IBAction)CancelButtonPressed:(id)sender;

@end

NS_ASSUME_NONNULL_END
