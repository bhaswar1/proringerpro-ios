//
//  LicenceVC.h
//  ProringerPro
//
//  Created by Soma Halder on 23/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LicenceVC : ProRingerProBaseVC <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblLicence;

@property (weak, nonatomic) IBOutlet UIImageView *imgIndicator;

@property (weak, nonatomic) IBOutlet UILabel *lblSuggestion;

@end

NS_ASSUME_NONNULL_END
