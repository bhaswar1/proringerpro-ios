//
//  LicenceVC.m
//  ProringerPro
//
//  Created by Soma Halder on 23/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "LicenceVC.h"

@interface LicenceVC ()
{
    NSMutableArray *arrLicenseList;
}

@end

@implementation LicenceVC
@synthesize tblLicence, lblSuggestion, imgIndicator;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    self.title = @"LICENSE";
    
//    self.tabBarController.tabBar.hidden = NO;
    
    self.imgRightButton = [UIImage imageNamed:@"Plus"];
    
    arrLicenseList = [[NSMutableArray alloc] init];
    
    tblLicence.dataSource = self;
    tblLicence.delegate = self;
    
    self->lblSuggestion.hidden = self->imgIndicator.hidden = YES;
    
//    [self connectionForGetLicenseList];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro licence"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro licence"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
}

- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.bottomLayoutGuide
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:-0],
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
    //    bannerView.layer.zPosition = -1;
    //    table_view.layer.zPosition = -2;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [arrLicenseList removeAllObjects];
    [tblLicence reloadData];
    
    [self connectionForGetLicenseList];
}

-(void)rightBarButtonPressed:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    LicenceEditVC *levc = [storyboard instantiateViewControllerWithIdentifier:@"LicenceEditVC"];
    levc.strTitle = @"ADD LICENSE";
    levc.isEdited = NO;
    [self.navigationController pushViewController:levc animated:YES];
}

#pragma mark - UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrLicenseList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellLicence";
    
    NSDictionary *dict = [arrLicenseList objectAtIndex:indexPath.row];
    
    LicenceTableViewCell *cell = (LicenceTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell configureCellWith:dict];
    
    return cell;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 95;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSDictionary *dict = [arrLicenseList objectAtIndex:indexPath.row];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    LicenceEditVC *levc = [storyboard instantiateViewControllerWithIdentifier:@"LicenceEditVC"];
    levc.strTitle = @"LICENSE";
    levc.dictLicenseDetails = [[NSMutableDictionary alloc] init];
    levc.dictLicenseDetails = [arrLicenseList objectAtIndex:indexPath.row];
//    levc.txtIssuer.text = [dict objectForKey:@"license_issuer"];
//    levc.txtLicenceNumber.text = [dict objectForKey:@"licenses_no"];
//    levc.txtCategory.text = [[dict objectForKey:@"category"] objectForKey:@"name"];
//    levc.txtExpireDate.text = [dict objectForKey:@"date_expire"];
//    levc.strImage = [dict objectForKey:@"image_info"];
    levc.isEdited = YES;
    [self.navigationController pushViewController:levc animated:YES];
}

#pragma mark - Web Service
-(void)connectionForGetLicenseList {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_GET_LICENSE];
    
    NSDictionary *param = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager GET:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id response) {

        [YXSpritesLoadingView dismiss];
        
        NSLog(@"response from update email:::...%@", response);
        
        if ([[response objectForKey:@"response"] boolValue] == YES) {
            
            [self->arrLicenseList removeAllObjects];
            [self->arrLicenseList addObjectsFromArray:[response objectForKey:@"info_array"]];
            
            if (self->arrLicenseList.count == 2) {
                self.navigationItem.rightBarButtonItem = nil;
            } else if (self->arrLicenseList.count < 2) {
                self.imgRightButton = [UIImage imageNamed:@"Plus"];
                self.navigationItem.rightBarButtonItem = self.navBarRightButton;
            }
            
            if (self->arrLicenseList.count == 0) {
                self->lblSuggestion.hidden = self->imgIndicator.hidden = NO;
            } else {
               self->lblSuggestion.hidden = self->imgIndicator.hidden = YES;
            }
            
            [self->tblLicence reloadData];
        }
    }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             [YXSpritesLoadingView dismiss];
             NSLog(@"ERROR:::...%@", task.error);
         }];
    }

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
