//
//  LicenceTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 23/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "LicenceTableViewCell.h"

@implementation LicenceTableViewCell

@synthesize lblExpireDate, lblCategoryName, lblLicenseNumber, lblLicenseHolderName, imgLicense;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

/*
 {
 "id": "359",
 "pros_id": "82",
 "image_info": "https://www.proringer.com/assets/upload/licence_image/zpBjfMywnh.jpg",
 "category": {
 "id": "121",
 "name": "Energy Audits"
 },
 "license_issuer": "test",
 "licenses_no": "11111",
 "date_expire": "03-27-2019",
 "date_upload": "03-13-2019"
 }
 */

-(void)configureCellWith:(NSDictionary *)dict {
    
    lblLicenseHolderName.text = [dict objectForKey:@"license_issuer"];
    lblLicenseNumber.text = [dict objectForKey:@"licenses_no"];
    lblCategoryName.text = [[dict objectForKey:@"category"] objectForKey:@"name"];
    lblExpireDate.text = [dict objectForKey:@"date_expire"];
    
//    [self imageLoadFromURL:[dict objectForKey:@"image_info"] withContainer:imgLicense];
    
//    [self->imgHeader sd_setImageWithURL:[NSURL URLWithString:[[[responseObject objectForKey:@"info_array"] objectForKey:@"info"] objectForKey:@"header_image"]]]
    
    [imgLicense sd_setImageWithURL:[dict objectForKey:@"image_info"]];
}

-(void)imageLoadFromURL:(NSString*)url withContainer:(UIImageView*)imgView {
    
    NSURL *imageURL = [NSURL URLWithString:url];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            imgView.image = [UIImage imageWithData:imageData];
        });
    });
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
