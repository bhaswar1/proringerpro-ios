//
//  LicenceTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 23/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LicenceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblLicenseHolderName;
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryName;
@property (weak, nonatomic) IBOutlet UILabel *lblLicenseNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblExpireDate;

@property (weak, nonatomic) IBOutlet UIImageView *imgLicense;

-(void)configureCellWith:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
