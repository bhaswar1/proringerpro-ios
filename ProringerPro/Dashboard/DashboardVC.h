//
//  DashboardVC.h
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PassKit/PassKit.h>
#import <StoreKit/StoreKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DashboardVC : ProRingerProBaseVC <UITableViewDataSource, UITableViewDelegate, EDStarRatingProtocol, UIImagePickerControllerDelegate, UINavigationControllerDelegate, PKPaymentAuthorizationViewControllerDelegate, SKProductsRequestDelegate, SKPaymentTransactionObserver>

@property (weak, nonatomic) IBOutlet UITableView *tblDashboard;
@property (weak, nonatomic) IBOutlet UIView *footerView;

@property (nonatomic, strong) GADBannerView *bannerView;
@property (nonatomic, strong) GADInterstitial *interstitial;
@property (nonatomic, strong) GADAdLoader *adLoader;
@property (strong, nonatomic) GADUnifiedNativeAd *nativeAd;

@property (weak, nonatomic) IBOutlet EDStarRating *viewRating;

@property (weak, nonatomic) IBOutlet UIImageView *imgCompany;

@property (weak, nonatomic) IBOutlet UILabel *lblCompanyName;
@property (weak, nonatomic) IBOutlet UILabel *lblCompanyAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfReviews;
@property (weak, nonatomic) IBOutlet UILabel *lblProjectWatchListNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblNewMessageNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblRating;

@property (weak, nonatomic) IBOutlet UIButton *btnPremium;
@property (weak, nonatomic) IBOutlet UIView *viewProfileInfo;
@property (weak, nonatomic) IBOutlet UIView *viewNewMessage;
@property (weak, nonatomic) IBOutlet UIView *viewProjectWishlist;
@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UIView *viewAdBanner;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constGoButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constGoButtonTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constBannerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constBannerBottom;

- (IBAction)ViewAllMessage:(id)sender;
- (IBAction)ViewAllProjects:(id)sender;
- (IBAction)CameraButtonPressed:(id)sender;
- (IBAction)GoPremium:(id)sender;
- (IBAction)ShowReviewList:(id)sender;

@end

NS_ASSUME_NONNULL_END
