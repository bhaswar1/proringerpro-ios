//
//  CampaignSummaryVC.m
//  ProringerPro
//
//  Created by Soma Halder on 26/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "CampaignSummaryVC.h"

@interface CampaignSummaryVC ()
{
    NSMutableArray *arrCampaign;
    
    NSString *strCurrentPlanId, *strCurrntPlanType;
}

@end

@implementation CampaignSummaryVC
@synthesize tblCampaign, viewCampaignHeader, viewHeader, lblCurrentCampaign, lblPlanName, lblRemainingDays, imgCampaign, btnUpgradeToPremium, daysRemain, constCampaignImageWidth, imgInfinity, lblLeadsBrif;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"CAMPAIGN SUMMARY";
    
//    self.tabBarController.tabBar.hidden = NO;
    
    arrCampaign = [[NSMutableArray alloc] init];
    
    lblCurrentCampaign.hidden = lblRemainingDays.hidden = daysRemain.hidden = YES;
    
    [self connectionForCampaignSummary];
    
    [self shadow:viewCampaignHeader];
    
    [tblCampaign.layer setShadowColor: [UIColor grayColor].CGColor];
    [tblCampaign.layer setShadowOpacity:0.8];
    [tblCampaign.layer setShadowRadius:3.0];
    [tblCampaign.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    tblCampaign.layer.masksToBounds = NO;
    tblCampaign.clipsToBounds = NO;
}

#pragma mark - NavigationBar Button
-(void)rightBarButtonPressed:(id)sender {
    
    NSLog(@"Right button pressed from Dashboard class");
    [self tapToDismissView];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchProjectVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SearchProjectVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

-(void)shadow:(UIView *)subview {
    [subview.layer setShadowColor: [UIColor grayColor].CGColor];
    [subview.layer setShadowOpacity:0.8];
    [subview.layer setShadowRadius:3.0];
    [subview.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

#pragma mark - UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrCampaign.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellCampaign";
    
    NSDictionary *dictCampaign = [arrCampaign objectAtIndex:indexPath.row];
    
    CampaignTableViewCell *cell = (CampaignTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.btnUpgradeNow.tag = indexPath.row;
    [cell.btnUpgradeNow addTarget:self action:@selector(upgradeNowButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell configureDataWith:dictCampaign andPlanType:strCurrntPlanType];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return viewHeader;
}

#pragma mark - UITableView Button Action
-(void)upgradeNowButtonPressed:(UIButton *)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    CampaignUpgradeVC *cuvc = (CampaignUpgradeVC *)[storyboard instantiateViewControllerWithIdentifier:@"CampaignUpgradeVC"];
    cuvc.viewHeader.hidden = NO;
    cuvc.viewPaymentMethod.hidden = YES;
    cuvc.strPlanId = [[arrCampaign objectAtIndex:sender.tag] objectForKey:@"plan_id"];
    [self.navigationController pushViewController:cuvc animated:YES];
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (![strCurrentPlanId isEqualToString:@"2"]) {
        CampaignTableViewCell *cell = (CampaignTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        cell.viewContent.layer.borderWidth = 1.0;
        cell.viewContent.layer.borderColor = COLOR_THEME.CGColor;
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
        CampaignUpgradeVC *cuvc = (CampaignUpgradeVC *)[storyboard instantiateViewControllerWithIdentifier:@"CampaignUpgradeVC"];
        cuvc.viewHeader.hidden = NO;
        cuvc.viewPaymentMethod.hidden = YES;
        cuvc.strPlanId = [[arrCampaign objectAtIndex:indexPath.row] objectForKey:@"plan_id"];
        if (indexPath.row != 0) {
            cell.imgPremium.image = [UIImage imageNamed:@"PremiumAnnualy"];
            [self.navigationController pushViewController:cuvc animated:YES];
        }
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CampaignTableViewCell *cell = (CampaignTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row != 0) {
        cell.viewContent.layer.borderColor = [UIColor whiteColor].CGColor;
        [cell.imgPremium sd_setImageWithURL:[[arrCampaign objectAtIndex:indexPath.row] objectForKey:@"image"]];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Web Service
-(void)connectionForCampaignSummary {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, @"app_pro_campaign"];
    NSDictionary *param = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager GET:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        [YXSpritesLoadingView dismiss];
        
        NSLog(@"response from update email:::...%@", response);
        
        if ([[response objectForKey:@"response"] boolValue] == YES) {
            
            
            if ([[[response objectForKey:@"info_array"] objectForKey:@"no_of_leads"] isEqualToString:@"Free"]) {
                self->lblCurrentCampaign.text = [[response objectForKey:@"info_array"] objectForKey:@"no_of_leads"];
            } else {
                self->lblCurrentCampaign.text = [NSString stringWithFormat:@"%@", @"\u221E"];
            }
            
            self->lblRemainingDays.text = [[[response objectForKey:@"info_array"] objectForKey:@"remain_days"] stringValue];
            
            self->strCurrentPlanId = [[response objectForKey:@"info_array"] objectForKey:@"current_plan_id"];
            self->strCurrntPlanType = [[response objectForKey:@"info_array"] objectForKey:@"current_plantype"];
            
            if ([self->strCurrentPlanId isEqualToString:@"0"]) {
                self->lblPlanName.text = @"Free Account";
                self->lblLeadsBrif.text = @"Ad support - Limited free leads";
                self->lblRemainingDays.hidden = YES;
                self->daysRemain.hidden = YES;
                self->imgCampaign.image = [UIImage imageNamed:@"PremiumFree"];
                self->imgInfinity.hidden = YES;
                self->lblCurrentCampaign.hidden = NO;
                self->lblCurrentCampaign.text = [[response objectForKey:@"info_array"] objectForKey:@"no_of_leads"];
//                self->constCampaignImageWidth.constant = 0.0;
//                self->lblPlanName.textColor = [UIColor lightGrayColor];
            } else {
//                self->constCampaignImageWidth.constant = 76.0;
                self->lblPlanName.text = [[response objectForKey:@"info_array"] objectForKey:@"plan_name"];
                [self->imgCampaign sd_setImageWithURL:[[response objectForKey:@"info_array"] objectForKey:@"current_plan_image"]];
                self->lblLeadsBrif.text = @"No Ads - Unlimited leads";
                self->imgInfinity.hidden = NO;
                self->lblCurrentCampaign.hidden = YES;
                self->daysRemain.hidden = self->lblRemainingDays.hidden = NO;
            }
            
            if ([self->strCurrentPlanId isEqualToString:@"2"]) {
                [self->btnUpgradeToPremium setTitle:@"Cancel Membership" forState:UIControlStateNormal];
            } else {
                [self->btnUpgradeToPremium setTitle:@"Upgrade to Premium" forState:UIControlStateNormal];
            }
            
            [self->arrCampaign removeAllObjects];
            [self->arrCampaign addObjectsFromArray:[[response objectForKey:@"info_array"] objectForKey:@"info"]];
            
            if (self->arrCampaign.count > 0) {
                self->tblCampaign.dataSource = self;
                self->tblCampaign.delegate = self;
                [self->tblCampaign reloadData];
            }
        }
    }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             [YXSpritesLoadingView dismiss];
             NSLog(@"ERROR:::...%@", task.error);
         }];
}

-(void)connectionForCancelCampaign {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_CAMPAIGN_CANCEL];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"confirm_type": @"C"};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE VERIFY ADDRESS::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [self connectionForCampaignSummary];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

- (IBAction)EditPaymentDetails:(id)sender {
    
    //payment method
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    CampaignUpgradeVC *cuvc = (CampaignUpgradeVC *)[storyboard instantiateViewControllerWithIdentifier:@"CampaignUpgradeVC"];
//    cuvc.viewHeader.hidden = YES;
//    cuvc.viewPaymentMethod.hidden = YES;
    cuvc.isEditPayment = YES;
    cuvc.strPlanId = strCurrentPlanId;
    [self.navigationController pushViewController:cuvc animated:YES];
}

- (IBAction)UpgradeToPremium:(id)sender {
    
    // go premium button pressed
    
    if ([strCurrentPlanId isEqualToString:@"2"]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Cancel Campaign" message:@"Are you sure you want to cancel your current subscription?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"CONFIRM" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            [self connectionForCancelCampaign];
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:okAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
        alert.view.tintColor = COLOR_THEME;
        
    } else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
        PremiumVC *pvc = [storyboard instantiateViewControllerWithIdentifier:@"PremiumVC"];
        [self.navigationController pushViewController:pvc animated:YES];
    }
}
@end
