//
//  CampaignTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 26/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "CampaignTableViewCell.h"

@implementation CampaignTableViewCell
@synthesize imgPremium, lblFirstTitle, lblThirdTitle, lblSecondTitle, viewContent, btnUpgradeNow;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self shadowForView:viewContent];
}

-(void)configureDataWith:(NSDictionary *)dict andPlanType:(nonnull NSString *)planType {
    
    lblFirstTitle.text = [dict objectForKey:@"first_title"];
    lblSecondTitle.text = [dict objectForKey:@"seceond_title"];
    lblThirdTitle.text = [dict objectForKey:@"content"];
    
    if ([[dict objectForKey:@"plan_type"] isEqualToString:planType]) {
        viewContent.layer.borderWidth = 1.0;
        viewContent.layer.borderColor = [COLOR_THEME CGColor];
        viewContent.backgroundColor = COLOR_SELECTED_BACK;
        [btnUpgradeNow setTitle:@"Active" forState:UIControlStateNormal];
        [btnUpgradeNow setTitleColor:COLOR_GREEN forState:UIControlStateNormal];
        btnUpgradeNow.userInteractionEnabled = NO;
    } else {
        [btnUpgradeNow setTitle:@"Upgrade Now" forState:UIControlStateNormal];
        [btnUpgradeNow setTitleColor:COLOR_THEME forState:UIControlStateNormal];
        btnUpgradeNow.userInteractionEnabled = YES;
    }
    
    [imgPremium sd_setImageWithURL:[dict objectForKey:@"image"]];
}

-(void)shadowForView:(UIView *)subview {
    
    [subview.layer setShadowColor: [UIColor grayColor].CGColor];
    [subview.layer setShadowOpacity:0.8];
    [subview.layer setShadowRadius:2.0];
    [subview.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
