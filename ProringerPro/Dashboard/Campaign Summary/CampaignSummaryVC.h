//
//  CampaignSummaryVC.h
//  ProringerPro
//
//  Created by Soma Halder on 26/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CampaignSummaryVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITableView *tblCampaign;
@property (weak, nonatomic) IBOutlet UIImageView *imgCampaign;
@property (weak, nonatomic) IBOutlet UIImageView *imgInfinity;

@property (weak, nonatomic) IBOutlet UIView *viewCampaignHeader;
@property (weak, nonatomic) IBOutlet UIView *viewHeader;

@property (weak, nonatomic) IBOutlet UILabel *lblCurrentCampaign;
@property (weak, nonatomic) IBOutlet UILabel *lblPlanName;
@property (weak, nonatomic) IBOutlet UILabel *lblRemainingDays;
@property (weak, nonatomic) IBOutlet UILabel *daysRemain;
@property (weak, nonatomic) IBOutlet UILabel *lblLeadsBrif;

@property (weak, nonatomic) IBOutlet UIButton *btnUpgradeToPremium;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constCampaignImageWidth;

- (IBAction)EditPaymentDetails:(id)sender;
- (IBAction)UpgradeToPremium:(id)sender;

@end

NS_ASSUME_NONNULL_END
