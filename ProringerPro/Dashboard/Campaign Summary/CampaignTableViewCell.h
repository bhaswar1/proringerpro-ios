//
//  CampaignTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 26/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CampaignTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnUpgradeNow;

@property (weak, nonatomic) IBOutlet UIView *viewContent;

@property (weak, nonatomic) IBOutlet UIImageView *imgPremium;

@property (weak, nonatomic) IBOutlet UILabel *lblFirstTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSecondTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblThirdTitle;

-(void)configureDataWith:(NSDictionary *)dict andPlanType:(NSString *)planType;

@end

NS_ASSUME_NONNULL_END
