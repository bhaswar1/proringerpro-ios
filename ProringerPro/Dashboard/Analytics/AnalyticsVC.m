//
//  AnalyticsVC.m
//  ProringerPro
//
//  Created by Soma Halder on 30/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "AnalyticsVC.h"
#import "HACBarChart.h"
#import "MonthCollectionViewCell.h"

@interface AnalyticsVC ()
{
    NSDictionary *dictAnalytics;
    NSMutableArray *arrProfileGraph, *arrLeadGraph, *arrMonth;
}

@end

@implementation AnalyticsVC
@synthesize lblLast12Months, lblYearOverYear, lblPrevious12Months, lblTotalLeads, lblProfileLeads, lblProjectResponse, lblLifeTimeViews, lblViewsPewMonths, lblLeadsReceivedPerMonths, lblNumberOfFavourite, lblLifetimeNumberOfCompletedJobs, lblChangesInLast30Days, chartTotalLeads, chartProfileViews, dictCartInfo, collProfile, collLeads, lblViewConversation, lblConversionRatio;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    self.navigationItem.title = @"ACCOUNT ANALYTICS";
    
    //    self.tabBarController.tabBar.hidden = NO;
    
    arrProfileGraph = [[NSMutableArray alloc] init];
    arrMonth = [[NSMutableArray alloc] init];
    arrLeadGraph = [[NSMutableArray alloc] init];
    
    //    [self connectionForGetAnalytics];
    
    chartProfileViews.backgroundColor = chartTotalLeads.backgroundColor = [UIColor whiteColor];
    //    chartProfileViews.barViewDisplayStyle = BarStyleFlat;
    
    arrProfileGraph = [dictCartInfo objectForKey:@"profile_views_in_12_months"];
    arrLeadGraph = [dictCartInfo objectForKey:@"leads_received_in_12_months"];
    arrMonth = [dictCartInfo objectForKey:@"month_graph"];
    
    [collProfile registerNib:[UINib nibWithNibName:@"MonthCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cellMonth"];
    [collLeads registerNib:[UINib nibWithNibName:@"MonthCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cellMonth"];
    
    collProfile.dataSource = collLeads.dataSource = self;
    collProfile.delegate = collLeads.delegate = self;
    
    [self configureProfileChart];
    [self configureLeadsChart];
    [self configureAnalysisData];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Account Analytics"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Account Analytics"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
}

- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.bottomLayoutGuide
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:0],
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
    //    bannerView.layer.zPosition = -1;
    //    table_view.layer.zPosition = -2;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [chartProfileViews draw];
    [chartTotalLeads draw];
}

#pragma mark - NavigationBar Button
-(void)rightBarButtonPressed:(id)sender {
    
    NSLog(@"Right button pressed from Dashboard class");
    [self tapToDismissView];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchProjectVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SearchProjectVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

-(void)configureProfileChart {
    
    int iProfileMaxValue = [[arrProfileGraph valueForKeyPath:@"@max.self"] intValue];
    int iProfileMinValue = [[arrProfileGraph valueForKeyPath:@"@min.self"] intValue];
    
    if (iProfileMinValue == 0) {
        NSMutableArray *arr = [NSMutableArray new];
        [arr removeAllObjects];
        for (int i = 0; i < arrProfileGraph.count; i++) {
            int iCurrentValue = [[arrProfileGraph objectAtIndex:i] intValue];
            if (iCurrentValue > 0) {
                [arr addObject:[NSNumber numberWithInt:iCurrentValue]];
            }
        }
        iProfileMinValue = [[arr valueForKeyPath:@"@min.self"] intValue];
        arr = nil;
    }
    NSMutableArray *arrProfile = [NSMutableArray new];
    
    for (int i = 0; i < arrProfileGraph.count; i++) {
        int iValue = [[arrProfileGraph objectAtIndex:i] intValue];
        
        if (iValue == iProfileMaxValue) {
            NSDictionary *dict = @{@"percentage":[arrProfileGraph objectAtIndex:i],
                                   @"color"  : [UIColor colorWithRed:109.0/255.0f green:181.0/255.0f blue:236.0/255.0f alpha:1.0f],
                                   @"customText" : [[arrProfileGraph objectAtIndex:i] stringValue]};
            [arrProfile addObject:dict];
        } else if (iValue == iProfileMinValue) {
            NSDictionary *dict = @{@"percentage":[arrProfileGraph objectAtIndex:i],
                                   @"color"  : [UIColor blackColor],
                                   @"customText" : [[arrProfileGraph objectAtIndex:i] stringValue]};
            [arrProfile addObject:dict];
        } else {
            NSDictionary *dict = @{@"percentage":[arrProfileGraph objectAtIndex:i],
                                   @"color"  : [UIColor colorWithRed:241.0/255.0f green:89.0/255.0f blue:42.0/255.0f alpha:1.0f],
                                   @"customText" : [[arrProfileGraph objectAtIndex:i] stringValue]};
            [arrProfile addObject:dict];
        }
    }
    
    chartProfileViews.showAxis                 = NO;   // Show axis line
    chartProfileViews.showProgressLabel        = YES;   // Show text for bar
    chartProfileViews.vertical                 = YES;   // Orientation chart
    chartProfileViews.reverse                  = NO;   // Orientation chart
    chartProfileViews.showDataValue            = YES;   // Show value contains _data, or real percent value
    chartProfileViews.showCustomText           = YES;   // Show custom text, in _data with key kHACCustomText
    chartProfileViews.barsMargin               = 1;     // Margin between bars
    chartProfileViews.sizeLabelProgress        = 30;    // Width of label progress text
    chartProfileViews.numberDividersAxisY      = 8;
    chartProfileViews.animationDuration        = 2;
    chartProfileViews.axisMaxValue             = 100;    // If no define maxValue, get maxium of _data
    chartProfileViews.progressTextColor        = [UIColor blackColor];
    chartProfileViews.axisYTextColor           = [UIColor colorWithRed:0.80 green:0.80 blue:0.80 alpha:1.0];
    chartProfileViews.progressTextFont         = [UIFont fontWithName:@"Open Sans" size:8.0];
    chartProfileViews.typeBar                  = HACBarType1;
    chartProfileViews.dashedLineColor          = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:.3];
    chartProfileViews.axisXColor               = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:1.0];
    chartProfileViews.axisYColor               = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:1.0];
    chartProfileViews.data                     = arrProfile;//CHART SET DATA
    chartProfileViews.axisFormat               = HACAxisFormatInt;
    chartProfileViews.typeBar = HACBarType2;
}

-(void)configureLeadsChart {
    
    int iLeadMaxValue = [[arrLeadGraph valueForKeyPath:@"@max.self"] intValue];
    int iLeadMinValue = [[arrLeadGraph valueForKeyPath:@"@min.self"] intValue];
    
    if (iLeadMinValue == 0) {
        NSMutableArray *arr = [NSMutableArray new];
        [arr removeAllObjects];
        for (int i = 0; i < arrLeadGraph.count; i++) {
            int iCurrentValue = [[arrLeadGraph objectAtIndex:i] intValue];
            if (iCurrentValue > 0) {
                [arr addObject:[NSNumber numberWithInt:iCurrentValue]];
            }
        }
        iLeadMinValue = [[arr valueForKeyPath:@"@min.self"] intValue];
        arr = nil;
    }
    NSMutableArray *arrLead = [NSMutableArray new];
    
    for (int i = 0; i < arrLeadGraph.count; i++) {
        int iValue = [[arrLeadGraph objectAtIndex:i] intValue];
        
        if (iValue == iLeadMaxValue) {
            NSDictionary *dict = @{@"percentage": [arrLeadGraph objectAtIndex:i],
                                   @"color": [UIColor colorWithRed:109.0/255.0f green:181.0/255.0f blue:236.0/255.0f alpha:1.0f],
                                   @"customText": [[arrLeadGraph objectAtIndex:i] stringValue]};
            [arrLead addObject:dict];
        } else if (iValue == iLeadMinValue) {
            NSDictionary *dict = @{@"percentage": [arrLeadGraph objectAtIndex:i],
                                   @"color": [UIColor blackColor],
                                   @"customText": [[arrLeadGraph objectAtIndex:i] stringValue]};
            [arrLead addObject:dict];
        } else {
            NSDictionary *dict = @{@"percentage": [arrLeadGraph objectAtIndex:i],
                                   @"color": [UIColor colorWithRed:241.0/255.0f green:89.0/255.0f blue:42.0/255.0f alpha:1.0f],
                                   @"customText": [[arrLeadGraph objectAtIndex:i] stringValue]};
            [arrLead addObject:dict];
        }
    }
    
    chartTotalLeads.showAxis                 = NO;   // Show axis line
    chartTotalLeads.showProgressLabel        = YES;   // Show text for bar
    chartTotalLeads.vertical                 = YES;   // Orientation chart
    chartTotalLeads.reverse                  = NO;   // Orientation chart
    chartTotalLeads.showDataValue            = YES;   // Show value contains _data, or real percent value
    chartTotalLeads.showCustomText           = YES;   // Show custom text, in _data with key kHACCustomText
    chartTotalLeads.barsMargin               = 1;     // Margin between bars
    chartTotalLeads.sizeLabelProgress        = 30;    // Width of label progress text
    chartTotalLeads.numberDividersAxisY      = 8;
    chartTotalLeads.animationDuration        = 2;
    chartTotalLeads.axisMaxValue             = 100;    // If no define maxValue, get maxium of _data
    chartTotalLeads.progressTextColor        = [UIColor blackColor];
    chartTotalLeads.axisYTextColor           = [UIColor colorWithRed:0.80 green:0.80 blue:0.80 alpha:1.0];
    chartTotalLeads.progressTextFont         = [UIFont fontWithName:@"Open Sans" size:8.0];
    chartTotalLeads.typeBar                  = HACBarType1;
    chartTotalLeads.dashedLineColor          = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:.3];
    chartTotalLeads.axisXColor               = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:1.0];
    chartTotalLeads.axisYColor               = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:1.0];
    chartTotalLeads.data                     = arrLead;//CHART SET DATA
    chartTotalLeads.axisFormat               = HACAxisFormatInt;
    chartTotalLeads.typeBar = HACBarType2;
}

-(void)configureAnalysisData {
    
    lblLast12Months.text = [NSString stringWithFormat:@"%@", [dictCartInfo objectForKey:@"last_12_months"]];
    lblPrevious12Months.text = [NSString stringWithFormat:@"%@", [dictCartInfo objectForKey:@"previous_12_months"]];
    lblYearOverYear.text = [NSString stringWithFormat:@"%@", [dictCartInfo objectForKey:@"year_over_year"]];
    
    lblTotalLeads.text = [NSString stringWithFormat:@"%@", [dictCartInfo objectForKey:@"total_leads"]];
    lblProfileLeads.text = [NSString stringWithFormat:@"%@", [dictCartInfo objectForKey:@"profile_leads"]];
    lblProjectResponse.text = [NSString stringWithFormat:@"%@", [dictCartInfo objectForKey:@"project_responses"]];
    
    lblLifeTimeViews.text = [NSString stringWithFormat:@"%@", [dictCartInfo objectForKey:@"lifetime_views"]];
    
    lblViewsPewMonths.text = [NSString stringWithFormat:@"%@", [dictCartInfo objectForKey:@"profile_views_per_month"]];
    lblLeadsReceivedPerMonths.text = [NSString stringWithFormat:@"%@", [dictCartInfo objectForKey:@"leads_received_per_month"]];
    
    lblLifetimeNumberOfCompletedJobs.text = [NSString stringWithFormat:@"%@", [dictCartInfo objectForKey:@"lifetime_number_of_completed_jobs"]];
    lblNumberOfFavourite.text = [NSString stringWithFormat:@"%@", [dictCartInfo objectForKey:@"favorite"]];
    lblChangesInLast30Days.text = [NSString stringWithFormat:@"%@", [dictCartInfo objectForKey:@"change_in_last_30_days"]];
    lblViewConversation.text = [dictCartInfo objectForKey:@"conversion_view"];
    lblConversionRatio.text = [dictCartInfo objectForKey:@"total_30days_conversion"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrMonth.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellMonth";
    
    MonthCollectionViewCell *cell = (MonthCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.lblMonthName.text = [arrMonth objectAtIndex:indexPath.item];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake((collectionView.frame.size.width /12), collectionView.frame.size.height -2);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
