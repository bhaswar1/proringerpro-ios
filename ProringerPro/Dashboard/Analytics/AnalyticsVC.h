//
//  AnalyticsVC.h
//  ProringerPro
//
//  Created by Soma Halder on 30/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HACBarLayer.h"

NS_ASSUME_NONNULL_BEGIN

@interface AnalyticsVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UILabel *lblLast12Months;
@property (weak, nonatomic) IBOutlet UILabel *lblPrevious12Months;
@property (weak, nonatomic) IBOutlet UILabel *lblYearOverYear;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalLeads;
@property (weak, nonatomic) IBOutlet UILabel *lblProjectResponse;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileLeads;

@property (weak, nonatomic) IBOutlet UILabel *lblLifeTimeViews;
@property (weak, nonatomic) IBOutlet UILabel *lblViewConversation;

@property (weak, nonatomic) IBOutlet UILabel *lblViewsPewMonths;
@property (weak, nonatomic) IBOutlet UILabel *lblLeadsReceivedPerMonths;
@property (weak, nonatomic) IBOutlet UILabel *lblLifetimeNumberOfCompletedJobs;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfFavourite;

@property (weak, nonatomic) IBOutlet UILabel *lblChangesInLast30Days;
@property (weak, nonatomic) IBOutlet UILabel *lblConversionRatio;

@property (strong, nonatomic) NSMutableDictionary *dictCartInfo;

@property (weak, nonatomic) IBOutlet HACBarChart *chartTotalLeads;
@property (weak, nonatomic) IBOutlet HACBarChart *chartProfileViews;

@property (weak, nonatomic) IBOutlet UICollectionView *collProfile;
@property (weak, nonatomic) IBOutlet UICollectionView *collLeads;

@end

NS_ASSUME_NONNULL_END
