//
//  AnalyticsViewController.m
//  ProringerPro
//
//  Created by Soumitra on 01/11/18.
//  Copyright © 2018 Mrinmoyesolz. All rights reserved.
//

#import "AnalyticsViewController.h"
#import "HACBarChart.h"

@interface AnalyticsViewController ()
{
    NSMutableDictionary *graph_dict,*lead_grph_dict;
    NSArray *data;
    NSArray *data3,*data4;
    NSMutableArray *profile_visit_in_12mnth, *lead_rcv_in_12mnth, *profile_mnth_graph, *lead_mnth_graph, *chart_arr, *chrt_lead_grph, *min_prf_arr, *min_lead_arr;
    int j,k,y,z;
    NSDictionary *info_dict;
}
@end

@implementation AnalyticsViewController
@synthesize analytics_scroll,lbl_favourite,lbl_tot_leads,lbl_prv_12_mnth,lbl_last_12_mnth,lbl_profile_leads,lbl_completed_jobs,lbl_year_over_year,lbl_life_time_views,lbl_project_responses,lbl_changein_last_30days,lbl_profile_views_per_month,lbl_leads_recv_per_mnth;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    profile_visit_in_12mnth = [[NSMutableArray alloc]init];
    lead_rcv_in_12mnth = [[NSMutableArray alloc]init];
    profile_mnth_graph = [[NSMutableArray alloc]init];
    lead_mnth_graph = [[NSMutableArray alloc]init];
  
    analytics_scroll.contentInset = UIEdgeInsetsMake(-7, 0, 0, 0);
    if ([[UIScreen mainScreen]bounds].size.height == 568) {
        analytics_scroll.contentSize = CGSizeMake(0, analytics_scroll.frame.origin.y + analytics_scroll.frame.size.height +425);
    } else {
        analytics_scroll.contentSize = CGSizeMake(0, analytics_scroll.frame.origin.y +analytics_scroll.frame.size.height +370);
    }
   [self connectionForGetAnalytics];
}


#pragma mark-BarChart

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    [self configureBarCharts];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

-(void) configureBarCharts {
    
    //Mark For CHART-3
    //================
    
    _lbl_0.text = [profile_mnth_graph objectAtIndex:0];
    _lbl_1.text = [profile_mnth_graph objectAtIndex:1];
    _lbl_2.text = [profile_mnth_graph objectAtIndex:2];
    _lbl_3.text = [profile_mnth_graph objectAtIndex:3];
    _lbl_4.text = [profile_mnth_graph objectAtIndex:4];
    _lbl_5.text = [profile_mnth_graph objectAtIndex:5];
    _lbl_6.text = [profile_mnth_graph objectAtIndex:6];
    _lbl_7.text = [profile_mnth_graph objectAtIndex:7];
    _lbl_8.text = [profile_mnth_graph objectAtIndex:8];
    _lbl_9.text = [profile_mnth_graph objectAtIndex:9];
    _lbl_10.text = [profile_mnth_graph objectAtIndex:10];
    _lbl_11.text = [profile_mnth_graph objectAtIndex:11];
    
    chart_arr = [[NSMutableArray alloc]init];
    NSLog(@"profile views in 12 month%@",profile_visit_in_12mnth);
    
    min_prf_arr = [[NSMutableArray alloc]init];
    
    [min_prf_arr removeAllObjects];
    
    for (int r=0; r<profile_visit_in_12mnth.count; r++) {
        NSString *temp_val = [profile_visit_in_12mnth objectAtIndex:r];
        if ([temp_val intValue]>0) {
            [min_prf_arr addObject:temp_val];
        } else {
            
        }
    }
    
    NSNumber *maxNumber = [profile_visit_in_12mnth valueForKeyPath:@"@max.self"];
    NSNumber *minNumber = [min_prf_arr valueForKeyPath:@"@min.self"];
    NSLog(@"MAX=%@,MIN=%@", maxNumber,minNumber);
    
    for (int i=0; i<profile_visit_in_12mnth.count; i++) {
        if (maxNumber == [profile_visit_in_12mnth objectAtIndex:i] && [maxNumber intValue]> 0) {
            j = i;
            NSLog(@"The Max Val=%ld",(long)j);
        } else if (minNumber == [profile_visit_in_12mnth objectAtIndex:i] && [minNumber intValue] > 0) {
            k = i;
            NSLog(@"The Min Val=%ld",(long)k);
        }
    }
    
    [chart_arr removeAllObjects];
    
    for (int p=0; p<profile_visit_in_12mnth.count; p++) {
        if (maxNumber == [profile_visit_in_12mnth objectAtIndex:p] && [maxNumber intValue] > 0) {
            graph_dict = [[NSMutableDictionary alloc]init];
            [graph_dict setValue:[profile_visit_in_12mnth objectAtIndex:j] forKey:@"percentage"];
            [graph_dict setValue:[UIColor colorWithRed:109.0/255.0f green:181.0/255.0f blue:236.0/255.0f alpha:1.0f] forKey:@"color"];
            [graph_dict setValue:[NSString stringWithFormat:@"%@",[profile_visit_in_12mnth objectAtIndex:j]] forKey:@"customText"];
            [chart_arr addObject:graph_dict];
        } else if (minNumber == [profile_visit_in_12mnth objectAtIndex:p] && [minNumber intValue] > 0) {
            graph_dict = [[NSMutableDictionary alloc]init];
            [graph_dict setValue:[profile_visit_in_12mnth objectAtIndex:k] forKey:@"percentage"];
            [graph_dict setValue:[UIColor blackColor] forKey:@"color"];
            [graph_dict setValue:[NSString stringWithFormat:@"%@",[profile_visit_in_12mnth objectAtIndex:k]] forKey:@"customText"];
            [chart_arr addObject:graph_dict];
        } else {
            graph_dict = [[NSMutableDictionary alloc]init];
            [graph_dict setValue:[profile_visit_in_12mnth objectAtIndex:p] forKey:@"percentage"];
            [graph_dict setValue:COLOR_THEME forKey:@"color"];
            [graph_dict setValue:[NSString stringWithFormat:@"%@",[profile_visit_in_12mnth objectAtIndex:p]] forKey:@"customText"];
            [chart_arr addObject:graph_dict];
        }
    }
    NSLog(@"Chart Arr now:%@",chart_arr);
    
    
    _chart3.showAxis                 = NO;   // Show axis line
    _chart3.showProgressLabel        = YES;   // Show text for bar
    _chart3.vertical                 = YES;   // Orientation chart
    _chart3.reverse                  = NO;   // Orientation chart
    _chart3.showDataValue            = YES;   // Show value contains _data, or real percent value
    _chart3.showCustomText           = YES;   // Show custom text, in _data with key kHACCustomText
    _chart3.barsMargin               = 1;     // Margin between bars
    _chart3.sizeLabelProgress        = 30;    // Width of label progress text
    _chart3.numberDividersAxisY      = 8;
    _chart3.animationDuration        = 2;
    _chart3.axisMaxValue             = 1000;    // If no define maxValue, get maxium of _data
    _chart3.progressTextColor        = [UIColor blackColor];
    _chart3.axisYTextColor           = [UIColor colorWithRed:0.80 green:0.80 blue:0.80 alpha:1.0];
    _chart3.progressTextFont         = [UIFont fontWithName:@"Open Sans" size:8.0];
    _chart3.typeBar                  = HACBarType1;
    _chart3.dashedLineColor          = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:.3];
    _chart3.axisXColor               = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:1.0];
    _chart3.axisYColor               = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:1.0];
    _chart3.data                     = chart_arr;//CHART SET DATA
    _chart3.axisFormat               = HACAxisFormatInt;
    _chart3.typeBar = HACBarType2;
//    [_chart3 draw];
    
    //Mark For CHART-4
    //================
    
    NSLog(@"leads_mnth_graph==%@",lead_mnth_graph);
    _Lbl_lead_0.text = [lead_mnth_graph objectAtIndex:0];
    _Lbl_lead_1.text = [lead_mnth_graph objectAtIndex:1];
    _Lbl_lead_2.text = [lead_mnth_graph objectAtIndex:2];
    _Lbl_lead_3.text = [lead_mnth_graph objectAtIndex:3];
    _Lbl_lead_4.text = [lead_mnth_graph objectAtIndex:4];
    _Lbl_lead_5.text = [lead_mnth_graph objectAtIndex:5];
    _Lbl_lead_6.text = [lead_mnth_graph objectAtIndex:6];
    _Lbl_lead_7.text = [lead_mnth_graph objectAtIndex:7];
    _Lbl_lead_8.text = [lead_mnth_graph objectAtIndex:8];
    _Lbl_lead_9.text = [lead_mnth_graph objectAtIndex:9];
    _Lbl_lead_10.text = [lead_mnth_graph objectAtIndex:10];
    _Lbl_lead_11.text = [lead_mnth_graph objectAtIndex:11];
    
    chrt_lead_grph = [[NSMutableArray alloc]init];
    min_lead_arr = [[NSMutableArray alloc]init];
    
    [min_lead_arr removeAllObjects];
    
    for (int s = 0; s<lead_rcv_in_12mnth.count; s++) {
        NSString *temp = [lead_rcv_in_12mnth objectAtIndex:s];
        if ([temp intValue] > 0) {
            [min_lead_arr addObject:temp];
        }
        else{
            NSLog(@"this is zero");
        }
    }
    NSNumber *max_Number = [lead_rcv_in_12mnth valueForKeyPath:@"@max.self"];
    NSNumber *min_Number = [min_lead_arr valueForKeyPath:@"@min.self"];
    NSLog(@"MAX=%@,MIN=%@", max_Number,min_Number);
    
    for (int x=0; x<lead_rcv_in_12mnth.count; x++)
    {
        if (max_Number == [lead_rcv_in_12mnth objectAtIndex:x] && [max_Number intValue]> 0) {
            y = x;
            NSLog(@"The Max Val=%ld",(long)y);
        }
        else if (min_Number == [lead_rcv_in_12mnth objectAtIndex:x] && [min_Number intValue]> 0)
        {
            z = x;
            NSLog(@"The Min Val=%ld",(long)z);
        }
    }
    
    [chrt_lead_grph removeAllObjects];
    
    for (int q=0; q<lead_rcv_in_12mnth.count; q++)
    {
        if (max_Number == [lead_rcv_in_12mnth objectAtIndex:q] && [max_Number intValue]> 0)
        {
            lead_grph_dict = [[NSMutableDictionary alloc]init];
            [lead_grph_dict setValue:[lead_rcv_in_12mnth objectAtIndex:y] forKey:@"percentage"];
            [lead_grph_dict setValue:[UIColor colorWithRed:109.0/255.0f green:181.0/255.0f blue:236.0/255.0f alpha:1.0f] forKey:@"color"];
            [lead_grph_dict setValue:[NSString stringWithFormat:@"%@",[lead_rcv_in_12mnth objectAtIndex:y]] forKey:@"customText"];
            [chrt_lead_grph addObject:lead_grph_dict];
        }
        else if (min_Number == [lead_rcv_in_12mnth objectAtIndex:q] && [min_Number intValue]> 0)
        {
            lead_grph_dict = [[NSMutableDictionary alloc]init];
            [lead_grph_dict setValue:[lead_rcv_in_12mnth objectAtIndex:z] forKey:@"percentage"];
            [lead_grph_dict setValue:[UIColor blackColor] forKey:@"color"];
            [lead_grph_dict setValue:[NSString stringWithFormat:@"%@",[lead_rcv_in_12mnth objectAtIndex:z]] forKey:@"customText"];
            [chrt_lead_grph addObject:lead_grph_dict];
        }
        else
        {
            lead_grph_dict = [[NSMutableDictionary alloc]init];
            [lead_grph_dict setValue:[lead_rcv_in_12mnth objectAtIndex:q] forKey:@"percentage"];
            [lead_grph_dict setValue:[UIColor colorWithRed:241.0/255.0f green:89.0/255.0f blue:42.0/255.0f alpha:1.0f] forKey:@"color"];
            [lead_grph_dict setValue:[NSString stringWithFormat:@"%@",[lead_rcv_in_12mnth objectAtIndex:q]] forKey:@"customText"];
            [chrt_lead_grph addObject:lead_grph_dict];
            
        }
        
    }
    NSLog(@"Chart Arr now:%@",chrt_lead_grph);
    
    _chart4.showAxis                 = NO;   // Show axis line
    _chart4.showProgressLabel        = YES;   // Show text for bar
    _chart4.vertical                 = YES;   // Orientation chart
    _chart4.reverse                  = NO;   // Orientation chart
    _chart4.showDataValue            = YES;   // Show value contains _data, or real percent value
    _chart4.showCustomText           = YES;   // Show custom text, in _data with key kHACCustomText
    _chart4.barsMargin               = 1;     // Margin between bars
    _chart4.sizeLabelProgress        = 30;    // Width of label progress text
    _chart4.numberDividersAxisY      = 8;
    _chart4.animationDuration        = 2;
    _chart4.axisMaxValue             = 100;    // If no define maxValue, get maxium of _data
    _chart4.progressTextColor        = [UIColor blackColor];
    _chart4.axisYTextColor           = [UIColor colorWithRed:0.80 green:0.80 blue:0.80 alpha:1.0];
    _chart4.progressTextFont         = [UIFont fontWithName:@"Open Sans" size:8.0];
    _chart4.typeBar                  = HACBarType1;
    _chart4.dashedLineColor          = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:.3];
    _chart4.axisXColor               = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:1.0];
    _chart4.axisYColor               = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:1.0];
    _chart4.data                     = chrt_lead_grph;//CHART SET DATA
    _chart4.axisFormat               = HACAxisFormatInt;
    _chart4.typeBar = HACBarType2;
//    [_chart4 draw];
}

#pragma mark - JSON API Fire

//-(void)urlData
//{
//
////http://esolz.co.in/lab6/proringer_latest/app_pro_account_analytics
//
//    NSError *error;
//    NSString *url_string = [NSString stringWithFormat:@"%@app_pro_account_analytics?user_id=%@",ROOT_URL,[[NSUserDefaults standardUserDefaults]valueForKey:@"userId"]];
//    NSLog(@"url=%@",url_string);
//    NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
//    result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
//    NSLog(@"result=%@",result);
//
//    NSNumber* num = [result valueForKey:@"response"];
//    int result_val = [num intValue];
//
//   if (result_val == 1)
//    {
//        info_dict = [result valueForKey:@"info_array"];
//        NSLog(@"info_dict==%@",info_dict);
//
//        lbl_life_time_views.text = [NSString stringWithFormat:@"%d",[[info_dict valueForKey:@"lifetime_views"]intValue]];
//        lbl_profile_leads.text = [NSString stringWithFormat:@"%d",[[info_dict valueForKey:@"profile_leads"]intValue]];
//        lbl_project_responses.text = [NSString stringWithFormat:@"%d",[[info_dict valueForKey:@"project_responses"]intValue]];
//        lbl_tot_leads.text = [NSString stringWithFormat:@"%d",[[info_dict valueForKey:@"total_leads"]intValue]];
//        lbl_last_12_mnth.text = [NSString stringWithFormat:@"%d",[[info_dict valueForKey:@"last_12_months"]intValue]];
//        lbl_prv_12_mnth.text = [NSString stringWithFormat:@"%d",[[info_dict valueForKey:@"previous_12_months"]intValue]];
//        lbl_year_over_year.text = [NSString stringWithFormat:@"%d",[[info_dict valueForKey:@"year_over_year"]intValue]];
//        lbl_completed_jobs.text = [NSString stringWithFormat:@"%d",[[info_dict valueForKey:@"completed_jobs"]intValue]];
//        lbl_favourite.text = [NSString stringWithFormat:@"%d",[[info_dict valueForKey:@"favorite"]intValue]];
//        lbl_changein_last_30days.text = [NSString stringWithFormat:@"%d",[[info_dict valueForKey:@"change_in_last_30_days"]intValue]];
//        lbl_profile_views_per_month.text = [NSString stringWithFormat:@"%d",[[info_dict valueForKey:@"profile_views_per_month"]intValue]];
//        lbl_leads_recv_per_mnth.text = [NSString stringWithFormat:@"%d",[[info_dict valueForKey:@"leads_received_per_month"]intValue]];
//        profile_visit_in_12mnth = [info_dict valueForKey:@"profile_views_in_12_months"];
//        lead_rcv_in_12mnth = [info_dict valueForKey:@"leads_received_in_12_months"];
//        profile_mnth_graph = [info_dict valueForKey:@"month_graph"];
//        lead_mnth_graph = [info_dict valueForKey:@"leads_graph_month"];
//    }
//    else
//    {
//        NSLog(@"");
//    }
//
//}

#pragma mark - Web Service
-(void)connectionForGetAnalytics {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_ANALYTICS];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSDictionary *params = @{@"user_id": [userDefault objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            self->info_dict = [responseObject valueForKey:@"info_array"];
            NSLog(@"info_dict==%@",self->info_dict);
            
            self->lbl_life_time_views.text = [NSString stringWithFormat:@"%d",[[self->info_dict valueForKey:@"lifetime_views"]intValue]];
            self->lbl_profile_leads.text = [NSString stringWithFormat:@"%d",[[self->info_dict valueForKey:@"profile_leads"]intValue]];
            self->lbl_project_responses.text = [NSString stringWithFormat:@"%d",[[self->info_dict valueForKey:@"project_responses"]intValue]];
            self->lbl_tot_leads.text = [NSString stringWithFormat:@"%d",[[self->info_dict valueForKey:@"total_leads"]intValue]];
            self->lbl_last_12_mnth.text = [NSString stringWithFormat:@"%d",[[self->info_dict valueForKey:@"last_12_months"]intValue]];
            self->lbl_prv_12_mnth.text = [NSString stringWithFormat:@"%d",[[self->info_dict valueForKey:@"previous_12_months"]intValue]];
            self->lbl_year_over_year.text = [NSString stringWithFormat:@"%d",[[self->info_dict valueForKey:@"year_over_year"]intValue]];
            self->lbl_completed_jobs.text = [NSString stringWithFormat:@"%d",[[self->info_dict valueForKey:@"completed_jobs"]intValue]];
            self->lbl_favourite.text = [NSString stringWithFormat:@"%d",[[self->info_dict valueForKey:@"favorite"]intValue]];
            self->lbl_changein_last_30days.text = [NSString stringWithFormat:@"%d",[[self->info_dict valueForKey:@"change_in_last_30_days"]intValue]];
            self->lbl_profile_views_per_month.text = [NSString stringWithFormat:@"%d",[[self->info_dict valueForKey:@"profile_views_per_month"]intValue]];
            self->lbl_leads_recv_per_mnth.text = [NSString stringWithFormat:@"%d",[[self->info_dict valueForKey:@"leads_received_per_month"]intValue]];
            
            [self->profile_visit_in_12mnth removeAllObjects];
            [self->profile_visit_in_12mnth addObjectsFromArray:[self->info_dict objectForKey:@"profile_views_in_12_months"]];
            
            [self->lead_rcv_in_12mnth removeAllObjects];
            [self->lead_rcv_in_12mnth addObjectsFromArray:[self->info_dict objectForKey:@"leads_received_in_12_months"]];
            
            [self->profile_mnth_graph removeAllObjects];
            [self->profile_mnth_graph addObjectsFromArray:[self->info_dict objectForKey:@"month_graph"]];
            
            [self->lead_mnth_graph removeAllObjects];
            [self->lead_mnth_graph addObjectsFromArray:[self->info_dict objectForKey:@"leads_graph_month"]];
            
            [self configureBarCharts];
        }
        [YXSpritesLoadingView dismiss];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
