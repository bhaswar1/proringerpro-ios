//
//  MonthCollectionViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 31/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MonthCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblMonthName;

@end

NS_ASSUME_NONNULL_END
