//
//  AnalyticsViewController.h
//  ProringerPro
//
//  Created by Soumitra on 01/11/18.
//  Copyright © 2018 Mrinmoyesolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HACBarChart.h"

@interface AnalyticsViewController : ProRingerProBaseVC

@property (strong, nonatomic) IBOutlet UIScrollView *analytics_scroll;

@property (strong, nonatomic) IBOutlet UILabel *lbl_life_time_views;
@property (strong, nonatomic) IBOutlet UILabel *lbl_profile_leads;
@property (strong, nonatomic) IBOutlet UILabel *lbl_project_responses;
@property (strong, nonatomic) IBOutlet UILabel *lbl_tot_leads;
@property (strong, nonatomic) IBOutlet UILabel *lbl_profile_views_per_month;
@property (strong, nonatomic) IBOutlet UILabel *lbl_leads_recv_per_mnth;

@property (strong, nonatomic) IBOutlet UILabel *lbl_last_12_mnth;
@property (strong, nonatomic) IBOutlet UILabel *lbl_prv_12_mnth;
@property (strong, nonatomic) IBOutlet UILabel *lbl_year_over_year;
@property (strong, nonatomic) IBOutlet UILabel *lbl_completed_jobs;
@property (strong, nonatomic) IBOutlet UILabel *lbl_favourite;
@property (strong, nonatomic) IBOutlet UILabel *lbl_changein_last_30days;

@property (strong, nonatomic) IBOutlet HACBarChart *chart3;

@property (strong, nonatomic) IBOutlet UILabel *lbl_0;
@property (strong, nonatomic) IBOutlet UILabel *lbl_1;
@property (strong, nonatomic) IBOutlet UILabel *lbl_2;
@property (strong, nonatomic) IBOutlet UILabel *lbl_3;
@property (strong, nonatomic) IBOutlet UILabel *lbl_4;
@property (strong, nonatomic) IBOutlet UILabel *lbl_5;
@property (strong, nonatomic) IBOutlet UILabel *lbl_6;
@property (strong, nonatomic) IBOutlet UILabel *lbl_7;
@property (strong, nonatomic) IBOutlet UILabel *lbl_8;
@property (strong, nonatomic) IBOutlet UILabel *lbl_9;
@property (strong, nonatomic) IBOutlet UILabel *lbl_10;
@property (strong, nonatomic) IBOutlet UILabel *lbl_11;

@property (strong, nonatomic) IBOutlet HACBarChart *chart4;

@property (strong, nonatomic) IBOutlet UILabel *Lbl_lead_0;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_lead_1;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_lead_2;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_lead_3;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_lead_4;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_lead_5;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_lead_6;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_lead_7;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_lead_8;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_lead_9;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_lead_10;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_lead_11;

@end
