//
//  CampaignUpgradeVC.m
//  ProringerPro
//
//  Created by Soma Halder on 26/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "CampaignUpgradeVC.h"

@interface CampaignUpgradeVC () <sendDateToMainClass>
{
    NSMutableArray *arrOfferLeadsList, *arrYear;
    NSArray *arrMonth;
    NSMutableDictionary *dictDate;
    NSString *strMonth, *strYear, *strCardNumber, *strSecurityCode, *strFirstName, *strLastName, *strAddress, *strOptional, *strCity, *strState, *strCountry, *strZip;
}

@end

@implementation CampaignUpgradeVC
@synthesize collUpgradesLeads, constViewHeight, viewHeader, tblUpdateCampaign, datePicker, lblOffer, viewPaymentMethod, viewTableHeader, strPlanId, imgPremiumOffer, lblBillType, lblPlanRange, lblPerMonth, lblFirstTitle, txtCardNumber, txtCity, txtState, txtAddress, txtCountry, txtZipCode, txtLastName, txtFirstName, txtExpiryYear, txtExpiryMonth, txtSecurityCode, txtOptionalAddress, imgCardView, isEditPayment, isPremium, datePickerView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon"]];
    
//    self.tabBarController.tabBar.hidden = NO;
    
    strMonth = strYear = strZip = strCountry = strState = strCity = strOptional = strAddress = strLastName = strFirstName = strSecurityCode = strCardNumber = @"";
    dictDate = [NSMutableDictionary new];
    datePickerView.dataSource = self;
    datePickerView.delegate = self;
    
    arrOfferLeadsList = [[NSMutableArray alloc] init];
    arrMonth = [[NSArray alloc] initWithObjects:@"January", @"February", @"March", @"April", @"May", @"June", @"July", @"August", @"September", @"October", @"November", @"December", nil];
    arrYear = [[NSMutableArray alloc] init];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY"];
    NSString *strYear = [formatter stringFromDate:[NSDate date]];
    int iStartYear = [strYear intValue];
//    int iEndYear = iStartYear +20;
    
    [arrYear removeAllObjects];
    
    for (int i = 0; i < 21; i++) {
        int iValue = iStartYear +i;
        [arrYear addObject:[NSString stringWithFormat:@"%d", iValue]];
    }
    
    lblOffer.transform = CGAffineTransformMakeRotation(-M_PI/4);
    
    collUpgradesLeads.dataSource = self;
    collUpgradesLeads.delegate = self;
    
    tblUpdateCampaign.hidden = YES;
    
    txtCardNumber.delegate = self;
    txtExpiryMonth.delegate = self;
    txtExpiryYear.delegate = self;
    txtSecurityCode.delegate = self;
    txtFirstName.delegate = self;
    txtLastName.delegate = self;
    txtAddress.delegate = self;
    txtOptionalAddress.delegate = self;
    txtCity.delegate = self;
    txtState.delegate = self;
    txtCountry.delegate = self;
    txtZipCode.delegate = self;
    
    [self paddingForTextField:txtCardNumber];
    [self paddingForTextField:txtExpiryMonth];
    [self paddingForTextField:txtExpiryYear];
    [self paddingForTextField:txtSecurityCode];
    [self paddingForTextField:txtFirstName];
    [self paddingForTextField:txtZipCode];
    [self paddingForTextField:txtLastName];
    [self paddingForTextField:txtState];
    [self paddingForTextField:txtAddress];
    [self paddingForTextField:txtCity];
    [self paddingForTextField:txtOptionalAddress];
    [self paddingForTextField:txtState];
    [self paddingForTextField:txtCountry];
    
    [self shadowForTextField:txtCardNumber];
    [self shadowForTextField:txtExpiryMonth];
    [self shadowForTextField:txtExpiryYear];
    [self shadowForTextField:txtSecurityCode];
    [self shadowForTextField:txtFirstName];
    [self shadowForTextField:txtZipCode];
    [self shadowForTextField:txtLastName];
    [self shadowForTextField:txtState];
    [self shadowForTextField:txtAddress];
    [self shadowForTextField:txtCity];
    [self shadowForTextField:txtOptionalAddress];
    [self shadowForTextField:txtState];
    [self shadowForTextField:txtCountry];
    
//    [datePicker setDate:[NSDate date]];
    datePicker.date = datePicker.minimumDate = [NSDate date];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setValue:COLOR_THEME forKeyPath:@"textColor"];
    
//    txtExpiryMonth.inputView = txtExpiryYear.inputView = datePicker;
    txtExpiryMonth.inputView = txtExpiryYear.inputView = datePickerView;
    txtExpiryMonth.inputAccessoryView = txtExpiryYear.inputAccessoryView = self.toolBar;
    
    [self manageHeaderHeight];
    
    [self connectionForGetPremiumDetails];
}

-(void)manageHeaderHeight {
    
    CGRect frame = viewTableHeader.frame;
    
    if (arrOfferLeadsList == nil || arrOfferLeadsList.count == 0) {
        constViewHeight.constant = 0;
        viewHeader.hidden = YES;
        viewPaymentMethod.hidden = NO;
        frame.size.height = 140;
    } else {
        
        viewPaymentMethod.hidden = YES;
        viewHeader.hidden = NO;
        
        if ((arrOfferLeadsList.count % 2) > 0) {
            constViewHeight.constant = ((arrOfferLeadsList.count /2) *30) +25;
            viewHeader.frame = CGRectMake(0, 0, self.view.frame.size.width, 320 + constViewHeight.constant);
        }
        else {
            constViewHeight.constant = ((arrOfferLeadsList.count /2) *30) +5;
            viewHeader.frame = CGRectMake(0, 0, self.view.frame.size.width, 320 + constViewHeight.constant);
        }
        frame.size.height = viewHeader.frame.size.height;
    }
    viewTableHeader.frame = frame;
}

#pragma mark - UICollectionView DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrOfferLeadsList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellLeads";
    
    LeadsCollectionViewCell *cell = (LeadsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.lblLeadsOffers.text = [arrOfferLeadsList objectAtIndex:indexPath.row];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake((collUpgradesLeads.frame.size.width /2) -3, 30);
}

#pragma mark - Custom Delegate
-(void)sendDateWith:(NSString *)month andYear:(NSString *)year {
    
    strMonth = month;
    strYear = year;
}

#pragma mark - UIPickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    // return the number of components required
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return arrMonth.count;
    }
    else {
        return arrYear.count;
    }
}

// Populate the rows of the Picker
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    // Component 0 should load the array1 values, Component 1 will have the array2 values
    if (component == 0) {
        return [arrMonth objectAtIndex:row];
    }
    else if (component == 1) {
        return [arrYear objectAtIndex:row];
    }
    return nil;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 45)];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRed:241.0f/255 green:89.0f/255 blue:42.0f/255 alpha:1];
    label.font = [UIFont fontWithName:@"OpenSans" size:20];
    label.numberOfLines = 1;
    
    if (component == 0) {
        label.text = [arrMonth objectAtIndex:row];
    }
    else if (component == 1) {
        label.text = [arrYear objectAtIndex:row];
    }
    
    return label;
}

#pragma mark - UIPickerView Delegate
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 50;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (component == 0) {
        [dictDate setObject:[arrMonth objectAtIndex:row] forKey:@"month"];
    } else if (component == 1) {
        [dictDate setObject:[arrYear objectAtIndex:row] forKey:@"year"];
    }
}

#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    textField.layer.borderColor = COLOR_THEME.CGColor;
    textField.layer.borderWidth = 1.5f;
    
    if (textField == txtAddress) {
        [self performSegueWithIdentifier:@"zipCode" sender:nil];
    } else if (textField == txtOptionalAddress || textField == txtZipCode || textField == txtCountry || textField == txtCity || textField == txtState) {
        [textField resignFirstResponder];
        textField.userInteractionEnabled = NO;
    } else {
        [textField becomeFirstResponder];
        textField.userInteractionEnabled = YES;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == txtCardNumber) {
        // All digits entered
        if (range.location == 19) {
            return NO;
        }
        
        // Reject appending non-digit characters
        if (range.length == 0 &&
            ![[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[string characterAtIndex:0]]) {
            return NO;
        }
        
        // Auto-add hyphen before appending 4rd or 7th digit
        if (range.length == 0 &&
            (range.location == 4 || range.location == 9 || range.location == 14)) {
            textField.text = [NSString stringWithFormat:@"%@-%@", textField.text, string];
            return NO;
        }
        
        // Delete hyphen when deleting its trailing digit
        if (range.length == 1 &&
            (range.location == 5 || range.location == 10 || range.location == 15))  {
            range.location--;
            range.length = 2;
            textField.text = [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
        
        if (range.location == 0) {
            
            if ([string isEqualToString:@"3"]) {
                imgCardView.image = [UIImage imageNamed:@"AmaricanExepress"];
            } else if ([string isEqualToString:@"4"]) {
                imgCardView.image = [UIImage imageNamed:@"Visa"];
            } else if ([string isEqualToString:@"5"]) {
                imgCardView.image = [UIImage imageNamed:@"MasterCard"];
            } else if ([string isEqualToString:@"6"]) {
                imgCardView.image = [UIImage imageNamed:@"Discover"];
            } else {
                imgCardView.image = [UIImage imageNamed:@"EmptyCard"];
            }
        }
    } else if (textField == txtSecurityCode) {
        if (range.location == 3) {
            return NO;
        } else {
            return YES;
        }
    }
    
    return YES;
}

#pragma mark - UIToolBar Button
-(void)doneButtonPressed {

//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:@"yyyy"];
//    txtExpiryYear.text = [dateFormatter stringFromDate:[datePicker date]];
//    [dateFormatter setDateFormat:@"MMMM"];
//    txtExpiryMonth.text = [dateFormatter stringFromDate:[datePicker date]];
    
    txtExpiryMonth.text = [dictDate objectForKey:@"month"];
    txtExpiryYear.text = [dictDate objectForKey:@"year"];
    
    [self cancelButtonPressed];
}

-(void)cancelButtonPressed {
    
    [txtExpiryMonth resignFirstResponder];
    [txtExpiryYear resignFirstResponder];
}


#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"zipCode"]) {
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        SearchZipVC *szvc = segue.destinationViewController;
        self.providesPresentationContextTransitionStyle = YES;
        self.definesPresentationContext = YES;
        szvc.deleagte = self;
//        szvc.strNavigationTitle = @"Address";
//        szvc.strAddressTitle = @"City/Zip code";
//        szvc.strPlaceHolder = @"City / zip";
//        szvc.strApi = @"(regions)";
//        szvc.strSearchText = @"postal_code";
//        szvc.strSearchComponent = @"short_name";
        
        szvc.strNavigationTitle = @"Business Address";
        szvc.strAddressTitle = @"Business Street Address";
        szvc.strPlaceHolder = @"Street address";
        szvc.strApi = @"geocode";
        szvc.strSearchText = @"postal_code";
        szvc.strRoute = @"route";
        szvc.strSearchComponent = @"short_name";
        
        [szvc setModalPresentationStyle:UIModalPresentationCurrentContext];
    }
}

#pragma mark - Protocol Method
-(void)sendAddressToUserInfo:(NSDictionary *)dictAddress {
    
    NSArray *arrAddressComponent = [dictAddress objectForKey:@"address_components"];
    txtAddress.text = [dictAddress objectForKey:@"formatted_address"];
    NSLog(@"Address::...%@", dictAddress);
    
    strZip = strCountry = strState = strCity = strOptional = @"";
    NSString *strStreetNumber;
    
    for (int i = 0; i < arrAddressComponent.count; i++) {
        NSDictionary *dictComponent = [arrAddressComponent objectAtIndex:i];
        NSArray *arrTypes = [dictComponent objectForKey:@"types"];
        for (int iTypes = 0; iTypes < arrTypes.count; iTypes++) {
            if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"postal_code"]) {
                strZip = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"country"]) {
                txtCountry.text = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"administrative_area_level_1"]) {
                txtState.text = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"locality"] || [[arrTypes objectAtIndex:iTypes] isEqualToString:@"sublocality"]) {
                txtCity.text = [dictComponent objectForKey:@"long_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"route"]) {
                strOptional = [dictComponent objectForKey:@"long_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"street_number"]) {
                strStreetNumber = [dictComponent objectForKey:@"long_name"];
            } else if (strOptional.length == 0) {
                if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"administrative_area_level_2"]) {
                    strOptional = [dictComponent objectForKey:@"long_name"];
                }
            }
        }
    }
    NSLog(@"\npostal_code:::...%@\ncountry:::...%@\nstate:::...%@\ncity:::...%@\nstreet:::...%@", strZip, strCountry, strState, strCity, strOptional);
    
    if (strZip.length == 0) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Enter a valid street address" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            ;
        }];
        
        [alertController addAction:okAction];
        [alertController.view setTintColor:COLOR_THEME];
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        
        txtZipCode.text = strZip;
        
        if (strStreetNumber.length > 0) {
            txtOptionalAddress.text = [NSString stringWithFormat:@"%@ %@", strStreetNumber, strOptional];
        } else {
            txtOptionalAddress.text = strOptional;
        }
        strOptional = [NSString stringWithFormat:@"%@, %@", strCity, strState];
            
        double latitude = [[[[dictAddress objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"] doubleValue];
        double longitude = [[[[dictAddress objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"] doubleValue];
            
        [self findTimeZoneFromSelectedAddressWithLat:latitude andLon:longitude];
    }
}

-(void)findTimeZoneFromSelectedAddressWithLat:(double)latitude andLon:(double)longitude {
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> *placemarks, NSError *error) {
//        CLPlacemark *placemark = [placemarks objectAtIndex:0];
//        self->strTimeZone = placemark.timeZone.name;
    }];
}

#pragma mark - Web Service
-(void)connectionForGetPremiumDetails {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_PREMIUM_PLANS];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                           /*  @"plan_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"plan_id"] */
                             @"plan_id": strPlanId
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            NSDictionary *dict = [[[responseObject objectForKey:@"info_array"] objectForKey:@"info"] objectAtIndex:0];
            
            [self->arrOfferLeadsList removeAllObjects];
            [self->arrOfferLeadsList addObjectsFromArray:[dict objectForKey:@"description"]];
            [self->arrOfferLeadsList removeLastObject];
            
            self->lblFirstTitle.text = [dict objectForKey:@"first_title"];
            self->lblPlanRange.text = [dict objectForKey:@"plan_range"];
            self->lblBillType.text = [dict objectForKey:@"bill_type"];
            self->lblPerMonth.text = [dict objectForKey:@"plan_cost"];
            if ([[dict objectForKey:@"save"] intValue] == 0) {
                self->lblOffer.text = @"";
            } else {
                self->lblOffer.text = [NSString stringWithFormat:@"SAVE %@%@", [dict objectForKey:@"save"], @"%"];
            }
            if ([[dict objectForKey:@"plan_type"] isEqualToString:@"M"]) {
//                self->lbltimeCycle.text = @"/month";
                self->imgPremiumOffer.image = [UIImage imageNamed:@"Pret"];
                self->lblFirstTitle.textColor = COLOR_BLUE;
            } else {
//                self->lbltimeCycle.text = @"/year";
                self->imgPremiumOffer.image = [UIImage imageNamed:@"Premium"];
                self->lblFirstTitle.textColor = COLOR_THEME;
            }
            
            if (self->isEditPayment) {
                [self->arrOfferLeadsList removeAllObjects];
            }
            self->tblUpdateCampaign.hidden = NO;
            [self->collUpgradesLeads reloadData];
            [self manageHeaderHeight];
            [self->tblUpdateCampaign reloadData];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForPayment {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_PAYMENT];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"plan_id": strPlanId,
                             @"cardHolderName": [NSString stringWithFormat:@"%@ %@", txtFirstName.text, txtLastName.text],
                             @"cardNo": strCardNumber,
                             @"cvvNo": txtSecurityCode.text,
                             @"month": txtExpiryMonth.text,
                             @"year": txtExpiryYear.text,
                             @"czip": txtZipCode.text,
                             @"bill_fname": txtFirstName.text,
                             @"bill_lname": txtLastName.text,
                             @"bill_addr": txtAddress.text,
                             @"bill_optaddr": txtOptionalAddress.text,
                             @"bill_city": txtCity.text,
                             @"bill_state": txtState.text,
                             @"bill_country": txtCountry.text
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            self->txtSecurityCode.text = @"";
            
            if ([[responseObject objectForKey:@"response"] intValue] == 1) {
                
                self->txtExpiryMonth.text = self->txtExpiryYear.text = self->txtCity.text = self->txtState.text = self->txtAddress.text = self->txtCountry.text = self->txtZipCode.text = self->txtLastName.text = self->txtFirstName.text = self->txtCardNumber.text = self->txtOptionalAddress.text = @"";
                
                UIViewController *controller;
                BOOL isFound = NO;
                
                for (controller in self.navigationController.viewControllers) {
                    
                    if ([controller isKindOfClass:[DashboardVC class]]) {
                        [self.navigationController popToViewController:controller animated:YES];
                        isFound = YES;
                        return;
                    } else {
                        isFound = NO;
                    }
                }
                
                if (!isFound) {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
                    DashboardVC *hvc = [storyboard instantiateViewControllerWithIdentifier:@"DashboardVC"];
                    
                    CATransition* transition = [CATransition animation];
                    transition.duration = 0.65;
                    transition.type = kCATransitionPush;
                    transition.subtype = kCATransitionFromLeft;
                    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
                    [self.navigationController pushViewController:hvc animated:NO];
                }
            }
        }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

#pragma mark - UIButton Action
- (IBAction)ExpiryMonth:(id)sender {
    
    [txtExpiryMonth becomeFirstResponder];
}

- (IBAction)ExpiryYear:(id)sender {
    
    [txtExpiryYear becomeFirstResponder];
}

- (IBAction)PayButtonPressed:(id)sender {
    
    strCardNumber = txtCardNumber.text;
    
    NSMutableArray *characters = [[NSMutableArray alloc] initWithCapacity:[strCardNumber length]];
    for (int i=0; i < [strCardNumber length]; i++) {
        NSString *ichar  = [NSString stringWithFormat:@"%c", [strCardNumber characterAtIndex:i]];
        [characters addObject:ichar];
    }
    
    for (int i = 0; i < characters.count; i++) {
        if ([[characters objectAtIndex:i] isEqualToString:@"-"]) {
            [characters removeObjectAtIndex:i];
        }
    }
    
    strCardNumber = [characters componentsJoinedByString:@""];
    
    if ([self validationTextField]) {
        [self connectionForPayment];
    }
}

#pragma mark - Validation
-(BOOL)validationTextField {
    
    BOOL isValid;
    NSString *strMsg = @"";
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtCardNumber.text]) {
        strMsg = @"Please enter card number";
        isValid = NO;
        [txtCardNumber becomeFirstResponder];
    } else if (txtExpiryMonth.text.length == 0 || txtExpiryYear.text.length == 0) {
        strMsg = @"Please enter expiry date";
        isValid = NO;
        [txtExpiryMonth becomeFirstResponder];
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtSecurityCode.text]) {
        strMsg = @"Please enter cvv / security code";
        isValid = NO;
        [txtSecurityCode becomeFirstResponder];
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtFirstName.text]) {
        strMsg = @"Please enter first name";
        isValid = NO;
        [txtFirstName becomeFirstResponder];
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtLastName.text]) {
        strMsg = @"Please enter last name";
        isValid = NO;
        [txtLastName becomeFirstResponder];
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtAddress.text]) {
        strMsg = @"Please enter address";
        isValid = NO;
        [txtAddress becomeFirstResponder];
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtCity.text]) {
        strMsg = @"Please enter city";
        isValid = NO;
        [txtCity becomeFirstResponder];
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtState.text]) {
        strMsg = @"Please enter state";
        isValid = NO;
        [txtState becomeFirstResponder];
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtCountry.text]) {
        strMsg = @"Please enter country";
        isValid = NO;
        [txtCountry becomeFirstResponder];
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtZipCode.text]) {
        strMsg = @"Please enter zip code";
        isValid = NO;
        [txtZipCode becomeFirstResponder];
    } else if (txtCardNumber.text.length > 0 && txtCardNumber.text.length < 19) {
        strMsg = @"Please enter valid card number";
        isValid = NO;
        [txtCardNumber becomeFirstResponder];
    } else {
        isValid = YES;
    }
    
    if (!isValid) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strMsg];
    }
    
    return isValid;
}
@end
