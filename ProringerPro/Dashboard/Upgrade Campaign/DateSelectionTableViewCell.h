//
//  DateSelectionTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 26/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol sendDateToMainClass <NSObject>

-(void)sendDateWith:(NSString *)month andYear:(NSString *)year;

@end

NS_ASSUME_NONNULL_BEGIN

@interface DateSelectionTableViewCell : UITableViewCell <UITextFieldDelegate>

@property(assign, nonatomic) id delegate;

@property (weak, nonatomic) IBOutlet UITextField *txtMonth;
@property (weak, nonatomic) IBOutlet UITextField *txtYear;

@property (weak, nonatomic) IBOutlet UIButton *btnMonth;
@property (weak, nonatomic) IBOutlet UIButton *btnYear;

@property (strong, nonatomic) UIToolbar *toolBar;
@property (strong, nonatomic) UIBarButtonItem *doneButton;
@property (strong, nonatomic) UIBarButtonItem *cancelButton;
@property (strong, nonatomic) UIBarButtonItem *flexibleSpace;
@property (strong, nonatomic) UIDatePicker *datePicker;

-(void)doneButtonPressed;
-(void)cancelButtonPressed;

@end

NS_ASSUME_NONNULL_END
