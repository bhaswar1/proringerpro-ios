//
//  DateSelectionTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 26/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "DateSelectionTableViewCell.h"

@implementation DateSelectionTableViewCell 
@synthesize txtYear, txtMonth, btnYear, btnMonth, toolBar, datePicker, doneButton, cancelButton, flexibleSpace;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, 50)];
    
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    toolBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    flexibleSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor redColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    toolBar.items = [NSArray arrayWithObjects:cancelButton, flexibleSpace, doneButton, nil];
    
    datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    datePicker.minimumDate = [NSDate date];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setValue:COLOR_THEME forKeyPath:@"textColor"];
    
    txtMonth.inputView = txtYear.inputView = datePicker;
    txtMonth.inputAccessoryView = txtYear.inputAccessoryView = toolBar;
    
    txtMonth.delegate = self;
    txtYear.delegate = self;
    
    [self paddingForTextField:txtMonth];
    [self paddingForTextField:txtYear];
    
    [self shadowForTextField:txtMonth];
    [self shadowForTextField:txtYear];
}

#pragma mark - UITextField Design
-(void) paddingForTextField:(UITextField *)textField {
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(30, 0, 10, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    textField.layer.masksToBounds = NO;
    textField.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    textField.layer.shadowRadius = 1;
    textField.layer.shadowOpacity = 0.2;
    textField.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    
    textField.layer.borderWidth = 1.0;
    textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)shadowForTextField:(UITextField *)textfield {
    
    UIColor *color = [UIColor blackColor];
    textfield.layer.shadowColor = [color CGColor];
    textfield.layer.shadowRadius = 1.5f;
    textfield.layer.shadowOpacity = 0.2;
    textfield.layer.shadowOffset = CGSizeZero;
    textfield.layer.masksToBounds = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)doneButtonPressed {
    NSString *strYear, *strMonth;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy"];
    strYear = [dateFormatter stringFromDate:[datePicker date]];
    [dateFormatter setDateFormat:@"MMMM"];
    strMonth = [dateFormatter stringFromDate:[datePicker date]];
    
    NSLog(@"YEAR:::...%@\nMONTHS:::...%@", strYear, strMonth);
    
    [self.delegate sendDateWith:strMonth andYear:strYear];
    
    [txtMonth resignFirstResponder];
    [txtYear resignFirstResponder];
}

-(void)cancelButtonPressed {
    
    [txtMonth resignFirstResponder];
    [txtYear resignFirstResponder];
}

@end
