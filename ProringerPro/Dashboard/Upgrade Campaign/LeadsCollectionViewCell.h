//
//  LeadsCollectionViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 26/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LeadsCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblLeadsOffers;

@end

NS_ASSUME_NONNULL_END
