//
//  CampaignUpgradeVC.h
//  ProringerPro
//
//  Created by Soma Halder on 26/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CampaignUpgradeVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UICollectionView *collUpgradesLeads;
@property (weak, nonatomic) IBOutlet UITableView *tblUpdateCampaign;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UIPickerView *datePickerView;

@property (weak, nonatomic) IBOutlet UILabel *lblOffer;
@property (weak, nonatomic) IBOutlet UIImageView *imgPremiumOffer;
@property (weak, nonatomic) IBOutlet UIImageView *imgCardView;

@property (weak, nonatomic) IBOutlet UILabel *lblPerMonth;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPlanRange;
@property (weak, nonatomic) IBOutlet UILabel *lblBillType;

@property (weak, nonatomic) IBOutlet UIView *viewTableHeader;
@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UIView *viewPaymentMethod;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewHeight;

@property (assign, nonatomic) BOOL isEditPayment, isPremium;

@property (strong, nonatomic) NSString *strPlanId;

@property (weak, nonatomic) IBOutlet UITextField *txtCardNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtExpiryMonth;
@property (weak, nonatomic) IBOutlet UITextField *txtExpiryYear;
@property (weak, nonatomic) IBOutlet UITextField *txtSecurityCode;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtOptionalAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtCountry;
@property (weak, nonatomic) IBOutlet UITextField *txtState;
@property (weak, nonatomic) IBOutlet UITextField *txtZipCode;

- (IBAction)ExpiryMonth:(id)sender;
- (IBAction)ExpiryYear:(id)sender;

- (IBAction)PayButtonPressed:(id)sender;

@end

NS_ASSUME_NONNULL_END
