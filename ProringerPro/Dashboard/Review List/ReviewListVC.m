//
//  ReviewListVC.m
//  ProringerPro
//
//  Created by Soma Halder on 02/07/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ReviewListVC.h"

@interface ReviewListVC ()
{
    NSMutableArray *arrReviewList;
}

@end

@implementation ReviewListVC
@synthesize lblRating, lblNumberOfReviews, imgProUser, viewHeader, starRating, barChart;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    arrReviewList = [[NSMutableArray alloc] init];
    
    arrReviewList = @[
             @{kHACPercentage:@1000, kHACColor  : [UIColor colorWithRed:0.000f green:0.620f blue:0.890f alpha:1.0f], kHACCustomText : @"January"},
             @{kHACPercentage:@875,  kHACColor  : [UIColor colorWithRed:0.431f green:0.000f blue:0.533f alpha:1.0f], kHACCustomText : @"February"},
             @{kHACPercentage:@875,  kHACColor  : [UIColor colorWithRed:0.922f green:0.000f blue:0.000f alpha:1.0f], kHACCustomText : @"March"},
             @{kHACPercentage:@875,  kHACColor  : [UIColor colorWithRed:0.000f green:0.671f blue:0.180f alpha:1.0f], kHACCustomText : @"April"},
             @{kHACPercentage:@750,  kHACColor  : [UIColor colorWithRed:1.000f green:0.000f blue:0.851f alpha:1.0f], kHACCustomText : @"May"},
             @{kHACPercentage:@750,  kHACColor  : [UIColor colorWithRed:1.000f green:0.808f blue:0.000f alpha:1.0f], kHACCustomText : @"June"},
             @{kHACPercentage:@625,  kHACColor  : [UIColor colorWithRed:0.294f green:0.843f blue:0.251f alpha:1.0f], kHACCustomText : @"July"},
             @{kHACPercentage:@500,  kHACColor  : [UIColor colorWithRed:1.000f green:0.404f blue:0.000f alpha:1.0f], kHACCustomText : @"August"},
             @{kHACPercentage:@500,  kHACColor  : [UIColor colorWithRed:0.282f green:0.631f blue:0.620f alpha:1.0f], kHACCustomText : @"September"},
             @{kHACPercentage:@375,  kHACColor  : [UIColor colorWithRed:0.776f green:0.000f blue:0.702f alpha:1.0f], kHACCustomText : @"October"},
             @{kHACPercentage:@250,  kHACColor  : [UIColor colorWithRed:0.282f green:0.631f blue:0.620f alpha:1.0f], kHACCustomText : @"November"},
             @{kHACPercentage:@125,  kHACColor  : [UIColor colorWithRed:0.776f green:0.000f blue:0.702f alpha:1.0f], kHACCustomText : @"December"}
             ];
    
    barChart.showAxis                 = NO;   // Show axis line
    barChart.showProgressLabel        = YES;   // Show text for bar
    barChart.vertical                 = YES;   // Orientation chart
    barChart.reverse                  = NO;   // Orientation chart
    barChart.showDataValue            = YES;   // Show value contains _data, or real percent value
    barChart.showCustomText           = YES;   // Show custom text, in _data with key kHACCustomText
    barChart.barsMargin               = 1;     // Margin between bars
    barChart.sizeLabelProgress        = 30;    // Width of label progress text
    barChart.numberDividersAxisY      = 8;
    barChart.animationDuration        = 2;
    barChart.axisMaxValue             = 1000;    // If no define maxValue, get maxium of _data
    barChart.progressTextColor        = [UIColor blackColor];
    barChart.axisYTextColor           = [UIColor colorWithRed:0.80 green:0.80 blue:0.80 alpha:1.0];
    barChart.progressTextFont         = [UIFont fontWithName:@"Open Sans" size:8.0];
    barChart.typeBar                  = HACBarType1;
    barChart.dashedLineColor          = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:.3];
    barChart.axisXColor               = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:1.0];
    barChart.axisYColor               = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:1.0];
    barChart.data                     = arrReviewList;//CHART SET DATA
    barChart.axisFormat               = HACAxisFormatInt;
    barChart.typeBar = HACBarType2;
    
    // Assign data
    barChart.data = arrReviewList;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [barChart draw];
}

#pragma mark - UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellRreview";
    
    ReviewListTableViewCell *cell = (ReviewListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    return cell;
}

#pragma mark - UITableView Delegate
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return viewHeader;
}

#pragma mark - Web Service
-(void)connectionForGetReviewDetails {
    
    [YXSpritesLoadingView show];
    
//    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL]
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
