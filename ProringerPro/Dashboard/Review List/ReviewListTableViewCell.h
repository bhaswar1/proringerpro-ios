//
//  ReviewTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 02/07/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReviewListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblPostedDate;
@property (weak, nonatomic) IBOutlet UILabel *lblReviewDetails;

@property (weak, nonatomic) IBOutlet UIButton *btnReport;
@property (weak, nonatomic) IBOutlet UIButton *btnReply;

@end

NS_ASSUME_NONNULL_END
