//
//  ReviewTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 02/07/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ReviewListTableViewCell.h"

@implementation ReviewListTableViewCell
@synthesize lblUserName, lblPostedDate, lblReviewDetails, btnReply, btnReport, imgUser;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
