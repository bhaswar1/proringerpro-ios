//
//  LeadResponseVC.h
//  ProringerPro
//
//  Created by Soma Halder on 26/12/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ADCustomNavigation <NSObject>

-(void)navigateToMessage:(int)indexPath;

@end

NS_ASSUME_NONNULL_BEGIN

@interface LeadResponseVC : UIViewController <UIGestureRecognizerDelegate, UITextViewDelegate, GADInterstitialDelegate>

@property (weak, nonatomic) id <ADCustomNavigation> customNavigation;

@property (weak, nonatomic) IBOutlet UILabel *lblLeadRemaining;
@property (weak, nonatomic) IBOutlet UIView *viewPopUp;
@property (weak, nonatomic) IBOutlet UIView *viewResponse;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceHolder;
@property (weak, nonatomic) IBOutlet UITextView *txtResponse;

@property (nonatomic, strong) GADInterstitial *interstitial;
@property (nonatomic, strong) GADAdLoader *adLoader;
@property (strong, nonatomic) GADUnifiedNativeAd *nativeAd;

@property (assign, nonatomic) BOOL isMessage;
@property (nonatomic, assign) int iIndexNumber;

@property (strong, nonatomic) NSDictionary *dictInfo;

@property (strong, nonatomic) NSString *strLeadsRemaining, *strProjectId, *strProjectOwnerId;

@property (weak, nonatomic) IBOutlet UIButton *btnResponseNow;

- (IBAction)RespondNow:(id)sender;
- (IBAction)GoPremium:(id)sender;
- (IBAction)CloseButtonPressed:(id)sender;
- (IBAction)SendResponse:(id)sender;

-(void)tapToDismissView;

@end

NS_ASSUME_NONNULL_END
