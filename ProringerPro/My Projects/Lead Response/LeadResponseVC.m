//
//  LeadResponseVC.m
//  ProringerPro
//
//  Created by Soma Halder on 26/12/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "LeadResponseVC.h"

@interface LeadResponseVC ()
{
    UITapGestureRecognizer *tapGesture;
    NSMutableArray *arrRestrictedWordsFound;
    BOOL isAlreadyShown;
}

@end

@implementation LeadResponseVC

@synthesize strLeadsRemaining, lblLeadRemaining, strProjectId, strProjectOwnerId, viewPopUp, viewResponse, txtResponse, lblPlaceHolder, isMessage, customNavigation, iIndexNumber, dictInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    self.interstitial.delegate = self;
    self.interstitial = [self createAndLoadInterstitial];
    
    tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapToDismissView)];
    [self.view addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;
    
    arrRestrictedWordsFound = [[NSMutableArray alloc] init];
    
    txtResponse.delegate = self;
    
    [self paddingForTextView:txtResponse];
    [self shadowForTextView:txtResponse];
    
    viewResponse.hidden = YES;
    
    lblLeadRemaining.text = strLeadsRemaining;
    
    isAlreadyShown = NO;
    
//    [self performSelector:@selector(myMethod) withObject:nil afterDelay:5.0];
    
    
}

-(void)myMethod {
    
    if (!isAlreadyShown) {
        if (isMessage) {
            if (self.interstitial.isReady) {
                [self.interstitial presentFromRootViewController:self];
                isAlreadyShown = YES;
            }
        }
    }
}

#pragma mark - view positioning
- (void)positionBannerViewFullWidthAtBottomOfSafeArea:(UIView *_Nonnull)bannerView NS_AVAILABLE_IOS(11.0) {
    // Position the banner. Stick it to the bottom of the Safe Area.
    // Make it constrained to the edges of the safe area.
    UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
    
    [NSLayoutConstraint activateConstraints:@[
                                              [guide.leftAnchor constraintEqualToAnchor:bannerView.leftAnchor],
                                              [guide.rightAnchor constraintEqualToAnchor:bannerView.rightAnchor],
                                              [guide.bottomAnchor constraintEqualToAnchor:bannerView.bottomAnchor]
                                              ]];
}

- (void)positionBannerViewFullWidthAtBottomOfView:(UIView *_Nonnull)bannerView {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.bottomLayoutGuide
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
}

- (GADInterstitial *)createAndLoadInterstitial {
    GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID:API_GOOGLE_AD_INTERSTITIAL];
    interstitial.delegate = self;
    [interstitial loadRequest:[GADRequest request]];
    return interstitial;
}

/// Tells the delegate an ad request succeeded.
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
    
}

/// Tells the delegate an ad request failed.
- (void)interstitial:(GADInterstitial *)ad
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"interstitial:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

/// Tells the delegate that an interstitial will be presented.
- (void)interstitialWillPresentScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillPresentScreen");
}

/// Tells the delegate the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
}

/// Tells the delegate the interstitial had been animated off the screen.
- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
    NSLog(@"interstitialDidDismissScreen");
}

/// Tells the delegate that a user click will open another app
/// (such as the App Store), backgrounding the current app.
- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if (viewPopUp.hidden) {
        if (CGRectContainsPoint(viewPopUp.bounds, [touch locationInView:viewPopUp])) {
            return YES;
        }
    } else {
        if (CGRectContainsPoint(viewPopUp.bounds, [touch locationInView:viewPopUp])) {
            return NO;
        } else
            return YES;
    }
    return YES;
}

#pragma mark - UITextView Design
-(void) paddingForTextView:(UITextView *)textView {
    
    textView.contentInset = UIEdgeInsetsMake(3, 5, 10, 5);
    
    textView.layer.masksToBounds = NO;
    textView.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    textView.layer.shadowRadius = 1.5;
    textView.layer.shadowOpacity = 0.2;
    textView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
}

-(void)shadowForTextView:(UITextView *)textView {
    
    textView.layer.borderWidth = 1.5;
    textView.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
}

#pragma mark - UITextView Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    textView.layer.borderColor = COLOR_THEME.CGColor;
    textView.layer.borderWidth = 1.5f;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    [self paddingForTextView:textView];
    [self shadowForTextView:textView];
    
    [arrRestrictedWordsFound removeAllObjects];
    NSArray *arrNewWords = [textView.text componentsSeparatedByString:@" "];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@""]) {
        
        NSLog(@"%@",textView.text);
        
        if (textView.text.length == 1) {
            lblPlaceHolder.hidden = NO;
        }
    } else if ([NSString stringWithFormat:@"%@%@",textView.text,text].length > 0) {
        
        lblPlaceHolder.hidden = YES;
    }
    
    return YES;
}

-(void)tapToDismissView {
    
//    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)RespondNow:(id)sender {
    
//    [self tapToDismissView];
//    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
//    
//    if ([[dictInfo objectForKey:@"pro_premium_status"] intValue] == 0) {
//        if (self.interstitial.isReady) {
//            [self.interstitial presentFromRootViewController:self];
//        }
//    }
    
    if ([strLeadsRemaining intValue] > 0) {
        if (isMessage) {
            if (self.interstitial.isReady) {
                [self.interstitial presentFromRootViewController:self];
                _interstitial.delegate = nil;
            }
        }
        [customNavigation navigateToMessage:iIndexNumber];
    } else {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:@"You don't have enough leads to view this message"];
    }
    
//    [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [self dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    /*
    if (isMessage) {
        UIStoryboard *sbMessage = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
        MessageDetailsVC *mdvc = [sbMessage instantiateViewControllerWithIdentifier:@"MessageDetailsVC"];
        mdvc.strProjectId = strProjectId;
        [self.navigationController pushViewController:mdvc animated:YES];
        [self presentViewController:[[MessageDetailsVC alloc] init] animated:YES completion:nil];
    } else {
        viewResponse.hidden = NO;
        viewPopUp.hidden = YES;
    }
    */
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Watchlist" bundle:nil];
//    ProjectDetailsVC *pdvc = [storyboard instantiateViewControllerWithIdentifier:@"ProjectDetailsVC"];
//    pdvc.strProjectId = strProjectId;
//    pdvc.strProjectOwnerId = strProjectOwnerId;
//    [self.navigationController pushViewController:pdvc animated:YES];
}

- (IBAction)GoPremium:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    PremiumVC *pvc = [storyboard instantiateViewControllerWithIdentifier:@"PremiumVC"];
    [self.navigationController pushViewController:pvc animated:YES];
}

- (IBAction)CloseButtonPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)SendResponse:(id)sender {
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtResponse.text]) {
        [[[UIAlertView alloc]initWithTitle:@"" message:@"Please type your message or select image" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    } else {
        
        [arrRestrictedWordsFound removeAllObjects];
        NSArray *arrNewWords = [txtResponse.text componentsSeparatedByString:@" "];
        NSLog(@"WORDS ARRAY FROM TEXTVIEW:::...%@", arrNewWords);
        
        if (arrRestrictedWordsFound.count > 0) {
            NSString *strKhisti = [arrRestrictedWordsFound componentsJoinedByString:@", "];
            [[[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"Invalid term: %@ - Please be respectful of others. Remove and try again", strKhisti] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            [txtResponse becomeFirstResponder];
        } else if ([[AppManager sharedDataAccess] detectType:txtResponse.text]) {
            [[[UIAlertView alloc]initWithTitle:@"" message:@"Email addresses, phone numbers and links are not permitted. You will be able to share contact information later if you choose." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            [txtResponse becomeFirstResponder];
        } else {
            [self connectionForRespond];
        }
    }
}

-(void)connectionForRespond {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_RESPOND];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"project_owner_id": strProjectOwnerId,
                             @"project_id": strProjectId,
                             @"job_response": txtResponse.text};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE FROM MY PROJECT:::...%@", responseObject);
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //            ;
//            [self connectionForGetProjectDetails];
            [self tapToDismissView];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        [alertController.view setTintColor:COLOR_THEME];
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

@end
