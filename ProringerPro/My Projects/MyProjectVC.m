//
//  MyProjectVC.m
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "MyProjectVC.h"

@interface MyProjectVC () <ADCustomNavigation>
{
    int iSelectedButtonIndex, iSenderTag, iStartFrom, iNextData;
    BOOL isFromMessage;
    NSMutableArray *arrMyProjectList;
    
    UISwipeGestureRecognizer *rightGesture, *leftGesture;
}

@end

@implementation MyProjectVC
@synthesize tblProjectTypes, btnSearchLocalProject, viewHeader, btnLeads, btnExpired, btnPending, btnAccepted, lblNoLeads, lblNoPending, lblNoExpired, lblNoAccepted, constLeadsHeight, constExpiredHeight, constPendingHeight, constAcceptedHeight, constViewUnderLine, viewPopUp, viewBackground, lblRemainingLeads, strRemainingLeads, strProjectId, strProjectOwnerId, iNumberOfLeads, strType;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    [self connectionForGetDashboardData];
    
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.viewFooterTab.lblNewProject.backgroundColor = self.viewFooterTab.lblNewMessage.backgroundColor = COLOR_THEME;
    self.viewFooterTab.lblNewMessage.layer.borderColor = self.viewFooterTab.lblNewProject.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewFooterTab.lblNewProject.layer.borderWidth = self.viewFooterTab.lblNewMessage.layer.borderWidth = 0.5;
    
    [self.view addSubview:self.viewFooterTab];
    
//    [self.view addGestureRecognizer:self.panGesture];
    
//    [[[[self.tabBarController tabBar] items] objectAtIndex:1] setFinishedSelectedImage:[UIImage imageNamed:@"MyProject_Selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"MyProject"]];
    
    iSelectedButtonIndex = 1;
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon"]];
    
    arrMyProjectList = [[NSMutableArray alloc] init];
    
//    [btnSearchLocalProject setBackgroundColor:COLOR_GREEN];
    [btnSearchLocalProject setTintColor:[UIColor whiteColor]];
    
    rightGesture.delegate = self;
    leftGesture.delegate = self;
    
    rightGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeToRightDirection)];
    leftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeToLeftDirection)];
    
    rightGesture.direction = UISwipeGestureRecognizerDirectionRight;
    leftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    
    [tblProjectTypes addGestureRecognizer:rightGesture];
    [tblProjectTypes addGestureRecognizer:leftGesture];
    
    tblProjectTypes.dataSource = self;
    tblProjectTypes.delegate = self;
    
    [tblProjectTypes registerNib:[UINib nibWithNibName:@"AcceptedTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellAccepted"];
    [tblProjectTypes registerNib:[UINib nibWithNibName:@"ExpiredTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellExpired"];
    [tblProjectTypes registerNib:[UINib nibWithNibName:@"LeadsTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellLeads"];
    [tblProjectTypes registerNib:[UINib nibWithNibName:@"PendingTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellPending"];
    
    [self Leads:nil];
    
    NSInteger iTag = [[[NSUserDefaults standardUserDefaults] objectForKey:@"footer"] integerValue];
    [self footerButtonDesignWithTag:iTag];
    [self footerButtonTap:2];
    
    viewPopUp.hidden = viewBackground.hidden = YES;
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self connectionForGetDashboardData];
    
    iNextData = iStartFrom = 0;
    
    self.tabBarController.tabBar.hidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    
    viewPopUp.hidden = viewBackground.hidden = YES;
    
    arrMyProjectList = [[NSMutableArray alloc] init];
    [arrMyProjectList removeAllObjects];
    
    [tblProjectTypes reloadData];
    
    [self callWebService:iSelectedButtonIndex];
    
    NSInteger iTag = [[[NSUserDefaults standardUserDefaults] objectForKey:@"footer"] integerValue];
    [self footerButtonDesignWithTag:iTag];
    [self footerButtonTap:2];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(counterUpdate) name:@"reload_data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    viewPopUp.hidden = viewBackground.hidden = YES;
}

#pragma mark - Ad Banner
- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    bannerView.backgroundColor = COLOR_LIGHT_BACK;
    [self.view addSubview:bannerView];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.bottomLayoutGuide
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:-60],
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
    //    bannerView.layer.zPosition = -1;
    //    table_view.layer.zPosition = -2;
}

- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    adView.alpha = 0;
    [UIView animateWithDuration:1.0 animations:^{
        adView.alpha = 1;
        //        [self addBannerViewToView:self.bannerView];
    }];
}

- (void)adLoader:(GADAdLoader *)adLoader
didReceiveNativeAd:(GADUnifiedNativeAd *)nativeAd {
    
    [self.adLoader loadRequest:[GADRequest request]];
}

- (void)adLoader:(GADAdLoader *)adLoader
didReceiveUnifiedNativeAd:(GADUnifiedNativeAd *)nativeAd {
    // A unified native ad has loaded, and can be displayed.
}

- (void)adLoaderDidFinishLoading:(GADAdLoader *) adLoader {
    // The adLoader has finished loading ads, and a new request can be sent.
}

- (void)nativeAdDidRecordImpression:(GADUnifiedNativeAd *)nativeAd {
    // The native ad was shown.
}

- (void)nativeAdDidRecordClick:(GADUnifiedNativeAd *)nativeAd {
    // The native ad was clicked on.
}

- (void)nativeAdWillPresentScreen:(GADUnifiedNativeAd *)nativeAd {
    // The native ad will present a full screen view.
}

- (void)nativeAdWillDismissScreen:(GADUnifiedNativeAd *)nativeAd {
    // The native ad will dismiss a full screen view.
}

- (void)nativeAdDidDismissScreen:(GADUnifiedNativeAd *)nativeAd {
    // The native ad did dismiss a full screen view.
}

- (void)nativeAdWillLeaveApplication:(GADUnifiedNativeAd *)nativeAd {
    // The native ad will cause the application to become inactive and
    // open a new application.
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

/// Tells the delegate that a full-screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
    NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full-screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
    NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full-screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
    NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
    NSLog(@"adViewWillLeaveApplication");
}

#pragma mark - view positioning
- (void)positionBannerViewFullWidthAtBottomOfSafeArea:(UIView *_Nonnull)bannerView NS_AVAILABLE_IOS(11.0) {
    // Position the banner. Stick it to the bottom of the Safe Area.
    // Make it constrained to the edges of the safe area.
    UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
    
    [NSLayoutConstraint activateConstraints:@[
                                              [guide.leftAnchor constraintEqualToAnchor:bannerView.leftAnchor],
                                              [guide.rightAnchor constraintEqualToAnchor:bannerView.rightAnchor],
                                              [guide.bottomAnchor constraintEqualToAnchor:bannerView.bottomAnchor]
                                              ]];
}

- (void)positionBannerViewFullWidthAtBottomOfView:(UIView *_Nonnull)bannerView {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.bottomLayoutGuide
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
}

-(void) counterUpdate {
    
    iNextData = iStartFrom = 0;
    
//    viewPopUp.hidden = viewBackground.hidden = NO;
    
    arrMyProjectList = [[NSMutableArray alloc] init];
    [arrMyProjectList removeAllObjects];
    
    [tblProjectTypes reloadData];
    
//    lblRemainingLeads.text = [self.dictInfoArray objectForKey:@"no_of_leads"];
    
    [self connectionForGetDashboardData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
    [self callWebService:iSelectedButtonIndex];
}

-(void)MessageNotificationTap {
    
    [self.appDelegate MessageNotificationTap];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    lblRemainingLeads.text = strRemainingLeads = [self.dictInfoArray objectForKey:@"no_of_leads"];
    
    if (self.iTotalNewMsg > 0) {
        self.viewFooterTab.lblNewMessage.hidden = NO;
    } else {
        self.viewFooterTab.lblNewMessage.hidden = YES;
    }
    
    if (self.iNewLeads > 0) {
        self.viewFooterTab.lblNewProject.hidden = NO;
    } else {
        self.viewFooterTab.lblNewProject.hidden = YES;
    }
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
    NSLog(@"AD BANNER ARRAY:::...%@", self.arrAdvViewStatus);
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pros My Project"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
}

-(void)swipeToRightDirection {
    
    NSLog(@"RIGHT");
    [tblProjectTypes addGestureRecognizer:leftGesture];
//    int iTag = 0;
    
    if ([strType isEqualToString:@"E"]) {
        [self Accepted:nil];
    } else if ([strType isEqualToString:@"A"]) {
//        iTag = 2;
        [self Pending:nil];
    } else if ([strType isEqualToString:@"P"]) {
//        iTag = 1;
        [self Leads:nil];
    } else {
        [tblProjectTypes removeGestureRecognizer:rightGesture];
    }
    
}

-(void)swipeToLeftDirection {
    
    NSLog(@"LEFT");
    [tblProjectTypes addGestureRecognizer:rightGesture];
//    int iTag = 0;
    
    if ([strType isEqualToString:@"L"]) {
//        iTag = 2;
        [self Pending:nil];
    } else if ( [strType isEqualToString:@"P"]) {
//        iTag = 3;
        [self Accepted:nil];
    } else if ([strType isEqualToString:@"A"]) {
//        iTag = 4;
        [self Expired:nil];
    } else {
        [tblProjectTypes removeGestureRecognizer:leftGesture];
    }
    
}

-(void)callWebService:(int)tag {
    
//    NSString *strType;
    
    lblNoAccepted.hidden = lblNoExpired.hidden = YES;
    lblNoPending.text = lblNoLeads.text = @"";
    
    NSString *strLinks = @"";
    NSDictionary *attributesLinks = @{NSForegroundColorAttributeName: [UIColor darkGrayColor],NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]};
    
    if (tag == 1) {
        strType = @"L";
        constLeadsHeight.constant = 60;
        constPendingHeight.constant = 0;
        constAcceptedHeight.constant = 0;
        constExpiredHeight.constant = 0;
        
        strLinks = @"No lead projects - Find new project now";
        lblNoLeads.attributedText = [[NSAttributedString alloc]initWithString:strLinks attributes:attributesLinks];
        
    } else if (tag == 2) {
        strType = @"P";
        constLeadsHeight.constant = 0;
        constPendingHeight.constant = 60;
        constAcceptedHeight.constant = 0;
        constExpiredHeight.constant = 0;
        
        strLinks = @"No pending projects - Find new project now";
        lblNoPending.attributedText = [[NSAttributedString alloc]initWithString:strLinks attributes:attributesLinks];
        
    } else if (tag == 3) {
        strType = @"A";
        constLeadsHeight.constant = 0;
        constPendingHeight.constant = 0;
        constAcceptedHeight.constant = 60;
        constExpiredHeight.constant = 0;
        
        lblNoAccepted.hidden = NO;
        
    } else if (tag == 4) {
        strType = @"E";
        constLeadsHeight.constant = 0;
        constPendingHeight.constant = 0;
        constAcceptedHeight.constant = 0;
        constExpiredHeight.constant = 60;
        
        lblNoExpired.hidden = NO;
    }
    
    void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
        if ([substring isEqualToString:@"Find new project now"]) {
            [self rightBarButtonPressed:nil];
        }
    };
    [lblNoLeads setLinksForSubstrings:@[@"Find new project now"] withLinkHandler:handler];
    [lblNoPending setLinksForSubstrings:@[@"Find new project now"] withLinkHandler:handler];
    
    iSelectedButtonIndex = tag;
    [self connectionForGetProjectList:strType];
}

-(void)rightBarButtonPressed:(id)sender {
    
    NSLog(@"Right button pressed in base class");
//    ;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchProjectVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SearchProjectVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

#pragma mark - UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrMyProjectList.count;
//    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dictProject = [arrMyProjectList objectAtIndex:indexPath.row];
    
    if (iSelectedButtonIndex == 1) {
        LeadsTableViewCell *cell = (LeadsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellLeads"];
        
        [cell configureLeadsCellWith:dictProject];
        
        cell.btnProject.tag = indexPath.row;
        cell.btnDeleteProject.tag = indexPath.row;
        cell.btnMessage.tag = indexPath.row;
        
        [cell.btnProject addTarget:self action:@selector(leadsProjectButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnMessage addTarget:self action:@selector(leadsMessageButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnDeleteProject addTarget:self action:@selector(deleteLeadsButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
        
    } else if (iSelectedButtonIndex == 2) {
        PendingTableViewCell *cell = (PendingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellPending"];
        
        cell.btnDeleteProject.tag = cell.btnProjectStatus.tag = indexPath.row;
        
        [cell.btnDeleteProject addTarget:self action:@selector(deleteButtonPressedFromPending:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnProjectStatus addTarget:self action:@selector(projectStatusButtonPressedFromPending:) forControlEvents:UIControlEventTouchUpInside];
        
//        if ([[dictProject objectForKey:@"msg_received"] intValue] == 0) {
//            [cell.btnProjectStatus addTarget:self action:@selector(awaitingResponceButtonPressedFromPending:) forControlEvents:UIControlEventTouchUpInside];
//        } else {
//            [cell.btnProjectStatus addTarget:self action:@selector(chatButtonPressedFromPending:) forControlEvents:UIControlEventTouchUpInside];
//        }
        
        [cell configureCellWith:dictProject];
        
        return cell;
        
    } else if (iSelectedButtonIndex == 3) {
        AcceptedTableViewCell *cell = (AcceptedTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellAccepted"];
        
        cell.btnDelete.tag = cell.btnReview.tag = cell.btnCloseOrChat.tag = indexPath.row;
        
        [cell.btnDelete addTarget:self action:@selector(deleteButtonPressedFromAccept:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([[dictProject objectForKey:@"chat_btn"] isEqualToString:@"C"]) {
            
            [cell.btnCloseOrChat addTarget:self action:@selector(chatButtonPressedFromAccept:) forControlEvents:UIControlEventTouchUpInside];
        } else {
            [cell.btnCloseOrChat addTarget:self action:@selector(closedButtonPressedFromAccept:) forControlEvents:UIControlEventTouchUpInside];
        }
        
//        if ([[dictProject objectForKey:@"request_review_status"] isEqualToString:@"N"] || [[dictProject objectForKey:@"request_review_status"] isEqualToString:@"RAW"]) {
//            [cell.btnReview addTarget:self action:@selector(requestReviewButtonPressedFromAccept:) forControlEvents:UIControlEventTouchUpInside];
//        } else if ([[dictProject objectForKey:@"request_review_status"] isEqualToString:@"A"]) {
//            [cell.btnReview addTarget:self action:@selector(viewReviewButtonPressedFromAccept:) forControlEvents:UIControlEventTouchUpInside];
//        } else if ([[dictProject objectForKey:@"request_review_status"] isEqualToString:@"AW"]) {
//            [cell.btnReview addTarget:self action:@selector(awaitingReviewButtonPressedFromAccept:) forControlEvents:UIControlEventTouchUpInside];
//        }
        
        [cell.btnReview addTarget:self action:@selector(viewReviewButtonPressedFromAccept:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell configureAcceptedCellWith:dictProject];
        
        return cell;
        
    } else if (iSelectedButtonIndex == 4) {
        
        ExpiredTableViewCell *cell = (ExpiredTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellExpired"];
        
        cell.btnDeleteProject.tag = indexPath.row;
        
        [cell.btnDeleteProject addTarget:self action:@selector(deleteButtonPressedFromExpire:) forControlEvents:UIControlEventTouchUpInside];
        [cell configureExpiredTabCellWith:dictProject];
        
        return cell;
    }
    return nil;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dictProjectDetails = [arrMyProjectList objectAtIndex:indexPath.row];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if ([[dictProjectDetails objectForKey:@"leadView"] intValue] == 0) {
        int iBadge = [[userDefault objectForKey:@"badge"] intValue];
        if (iBadge > 0) {
            iBadge = iBadge -1;
        }
        [userDefault setObject:[NSString stringWithFormat:@"%d", iBadge] forKey:@"badge"];
        [userDefault synchronize];
        
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:iBadge];
    }
    
    if (iSelectedButtonIndex != 4 && iSelectedButtonIndex != 3) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Watchlist" bundle:nil];
        ProjectDetailsVC *pdvc = [storyboard instantiateViewControllerWithIdentifier:@"ProjectDetailsVC"];
        pdvc.strProjectId = [dictProjectDetails objectForKey:@"project_id"];
        pdvc.strProjectOwnerId = [dictProjectDetails objectForKey:@"homeowner_id"];
        if ([[dictProjectDetails objectForKey:@"chat_btn"] isEqualToString:@"C"] || [[dictProjectDetails objectForKey:@"msg_received"] intValue] == 1) {
            pdvc.strResponseButtonTitle = @"CHAT";
        } else if ([[dictProjectDetails objectForKey:@"msg_received"] intValue] == 0) {
            pdvc.strResponseButtonTitle = @"AWAITING RESPONSE";
        } else if ([[dictProjectDetails objectForKey:@"chat_btn"] isEqualToString:@""] || [[dictProjectDetails objectForKey:@"msg_received"] intValue] == 0) {
            pdvc.strResponseButtonTitle = @"RESPOND NOW";
        } else {
            pdvc.strResponseButtonTitle = [dictProjectDetails objectForKey:@"pending_btn"];
        }
        [self.navigationController pushViewController:pdvc animated:YES];
    }
}

#pragma mark - UIScrollView Delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//
//    CGFloat height = scrollView.frame.size.height;
//    CGFloat contentYoffset = scrollView.contentOffset.y;
//    int distanceFromBottom = scrollView.contentSize.height - contentYoffset;
//    
//    if (distanceFromBottom == height) {
//        if (iNextData == 1) {
//            [self callWebService:iSelectedButtonIndex];
//        }
//    }
    NSLog(@"scrollViewDidEndDecelerating");
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
//
//    CGFloat height = scrollView.frame.size.height;
//    CGFloat contentYoffset = scrollView.contentOffset.y;
//    int distanceFromBottom = scrollView.contentSize.height - contentYoffset;
//
//    if (distanceFromBottom == height) {
//        if (iNextData == 1) {
//            [self callWebService:iSelectedButtonIndex];
//        }
//    }
    NSLog(@"scrollViewDidEndScrollingAnimation");
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat height = scrollView.frame.size.height;
    CGFloat contentYoffset = scrollView.contentOffset.y;
    int distanceFromBottom = scrollView.contentSize.height - contentYoffset;

    if (distanceFromBottom == height) {
        if (iNextData == 1) {
            [self callWebService:iSelectedButtonIndex];
            NSLog(@"CALL WEB SERVICE");
        }
    }
    NSLog(@"scrollViewDidScroll");
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
//    if (!decelerate) {
        CGFloat height = scrollView.frame.size.height;
        CGFloat contentYoffset = scrollView.contentOffset.y;
        int distanceFromBottom = scrollView.contentSize.height - contentYoffset;
        
        if (distanceFromBottom == height) {
            if (iNextData == 1) {
//                [self callWebService:iSelectedButtonIndex];
            }
        }
//    }
    NSLog(@"scrollViewDidEndDragging");
}

#pragma mark - UITableViewCell Button Action
-(void)leadsProjectButtonPressed:(UIButton *)sender {
    
    NSDictionary *dictProjectDetails = [arrMyProjectList objectAtIndex:sender.tag];
    
//    if ([[dictProjectDetails objectForKey:@"new_lead_status"] isEqualToString:@"D"]) {
//        if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
//            if (self.arrInterstitialAd.count > 0) {
//                for (int i = 0; i < self.arrInterstitialAd.count; i++) {
//                    NSDictionary *dict = [self.arrInterstitialAd objectAtIndex:i];
//                    if ([[dict objectForKey:@"title"] isEqualToString:@"Direct Lead see message"]) {
//                        if ([[dict objectForKey:@"ios_interstitial_ads"] intValue] == 1) {
//                            if (self.interstitial.isReady) {
//                                [self.interstitial presentFromRootViewController:self];
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Watchlist" bundle:nil];
    ProjectDetailsVC *pdvc = [storyboard instantiateViewControllerWithIdentifier:@"ProjectDetailsVC"];
    pdvc.strProjectId = [dictProjectDetails objectForKey:@"project_id"];
    pdvc.strNewLeadStatus = [dictProjectDetails objectForKey:@"new_lead_status"];
    pdvc.strProjectOwnerId = [dictProjectDetails objectForKey:@"homeowner_id"];
    [self.navigationController pushViewController:pdvc animated:YES];
}

-(void)leadsMessageButtonPressed:(UIButton *)sender {
    
    NSDictionary *dictDelete = [arrMyProjectList objectAtIndex:sender.tag];
    iSenderTag = sender.tag;
    if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
        if ([[dictDelete objectForKey:@"new_lead_status"] isEqualToString:@"D"]) {
            
            if ([[dictDelete objectForKey:@"leadView"] intValue] == 0) {
                
                viewPopUp.hidden = viewBackground.hidden = NO;
                
                strRemainingLeads = [dictDelete objectForKey:@"no_of_leads"];
                lblRemainingLeads.text = strRemainingLeads;
                iNumberOfLeads = [[dictDelete objectForKey:@"no_of_leads"] intValue];
                strProjectOwnerId = [dictDelete objectForKey:@"homeowner_id"];
                strProjectId = [dictDelete objectForKey:@"project_id"];
            } else {
                UIStoryboard *sbMessage = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
                MessageDetailsVC *mdvc = [sbMessage instantiateViewControllerWithIdentifier:@"MessageDetailsVC"];
                mdvc.strProjectId = [dictDelete objectForKey:@"project_id"];
                mdvc.isFromProject = YES;
                [self.navigationController pushViewController:mdvc animated:YES];
            }
        } else {
            UIStoryboard *sbMessage = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
            MessageDetailsVC *mdvc = [sbMessage instantiateViewControllerWithIdentifier:@"MessageDetailsVC"];
            mdvc.strProjectId = [dictDelete objectForKey:@"project_id"];
            mdvc.isFromProject = YES;
            [self.navigationController pushViewController:mdvc animated:YES];
        }
    } else {
        UIStoryboard *sbMessage = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
        MessageDetailsVC *mdvc = [sbMessage instantiateViewControllerWithIdentifier:@"MessageDetailsVC"];
        mdvc.strProjectId = [dictDelete objectForKey:@"project_id"];
        mdvc.isFromProject = YES;
        [self.navigationController pushViewController:mdvc animated:YES];
    }
}

-(void)deleteLeadsButtonPressed:(UIButton *)sender {
    
    NSDictionary *dictDelete = [arrMyProjectList objectAtIndex:sender.tag];
    
    [self ShowAlertForDeleteWithType:@"L" andProjectId:[dictDelete objectForKey:@"project_id"] andAppliedId:[dictDelete objectForKey:@"applied_jobid"] atIndex:sender.tag];
}

-(void)deleteButtonPressedFromPending:(UIButton *)sender {
    
    NSDictionary *dictDelete = [arrMyProjectList objectAtIndex:sender.tag];
    
    [self ShowAlertForDeleteWithType:@"P" andProjectId:[dictDelete objectForKey:@"project_id"] andAppliedId:[dictDelete objectForKey:@"applied_jobid"] atIndex:sender.tag];
}

-(void)deleteButtonPressedFromAccept:(UIButton *)sender {
    
    NSDictionary *dictDelete = [arrMyProjectList objectAtIndex:sender.tag];
    
    [self ShowAlertForDeleteWithType:@"A" andProjectId:[dictDelete objectForKey:@"project_id"] andAppliedId:[dictDelete objectForKey:@"applied_jobid"] atIndex:sender.tag];
}

//-(void)awaitingResponceButtonPressedFromPending:(UIButton *)sender {
//
//    NSDictionary *dictDelete = [arrMyProjectList objectAtIndex:sender.tag];
//
//    if ([[dictDelete objectForKey:@"msg_received"] intValue] == 0) {
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Watchlist" bundle:nil];
//        ProjectDetailsVC *pdvc = [storyboard instantiateViewControllerWithIdentifier:@"ProjectDetailsVC"];
//        pdvc.strProjectId = [dictDelete objectForKey:@"project_id"];
//        pdvc.strProjectOwnerId = [dictDelete objectForKey:@"homeowner_id"];
//        pdvc.strResponseButtonTitle = @"AWAITING RESPONSE";
//        [self.navigationController pushViewController:pdvc animated:YES];
//    }
//}

-(void)projectStatusButtonPressedFromPending:(UIButton *)sender {
    
    NSDictionary *dictDelete = [arrMyProjectList objectAtIndex:sender.tag];
    
    if ([[dictDelete objectForKey:@"msg_received"] intValue] == 0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Watchlist" bundle:nil];
        ProjectDetailsVC *pdvc = [storyboard instantiateViewControllerWithIdentifier:@"ProjectDetailsVC"];
        pdvc.strProjectId = [dictDelete objectForKey:@"project_id"];
        pdvc.strProjectOwnerId = [dictDelete objectForKey:@"homeowner_id"];
        pdvc.strResponseButtonTitle = @"AWAITING RESPONSE";
        [self.navigationController pushViewController:pdvc animated:YES];
    } else {
        UIStoryboard *sbMessage = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
        MessageDetailsVC *mdvc = [sbMessage instantiateViewControllerWithIdentifier:@"MessageDetailsVC"];
        mdvc.strProjectId = [dictDelete objectForKey:@"project_id"];
        mdvc.isFromProject = YES;
        [self.navigationController pushViewController:mdvc animated:YES];
    }
}
/*
-(void)requestReviewButtonPressedFromAccept:(UIButton *)sender {
    
    NSDictionary *dictDelete = [arrMyProjectList objectAtIndex:sender.tag];
    
    NSLog(@"DICTIONARY FROM REQUEST BUTTON:::...%@", dictDelete);
    
    if ([[dictDelete objectForKey:@"request_review_status"] isEqualToString:@"N"] || [[dictDelete objectForKey:@"request_review_status"] isEqualToString:@"RAW"]) {
        
        UIStoryboard *sbProject = [UIStoryboard storyboardWithName:@"Projects" bundle:nil];
        ProjectRequestReviewVC *prrvc = [sbProject instantiateViewControllerWithIdentifier:@"ProjectRequestReviewVC"];
        prrvc.strProjectId = [dictDelete objectForKey:@"project_id"];
        prrvc.strHomeownerId = [dictDelete objectForKey:@"homeowner_id"];
        prrvc.strProjectImage = [dictDelete objectForKey:@"project_image"];
        prrvc.strProjectName = [dictDelete objectForKey:@"project_name"];
        [self.navigationController pushViewController:prrvc animated:YES];
    } else if ([[dictDelete objectForKey:@"request_review_status"] isEqualToString:@"A"]) {
        
        UIStoryboard *sbProject = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
        ViewAllReviewVC *varvc = [sbProject instantiateViewControllerWithIdentifier:@"ViewAllReviewVC"];
        varvc.isProfileDetails = NO;
        [self.navigationController pushViewController:varvc animated:YES];
    }
}
*/
-(void)viewReviewButtonPressedFromAccept:(UIButton *)sender {
    
    NSDictionary *dictProject = [arrMyProjectList objectAtIndex:sender.tag];

    if ([[dictProject objectForKey:@"request_review_status"] isEqualToString:@"A"]) {

        UIStoryboard *sbProject = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
        ViewAllReviewVC *varvc = [sbProject instantiateViewControllerWithIdentifier:@"ViewAllReviewVC"];
        varvc.isProfileDetails = NO;
        [self.navigationController pushViewController:varvc animated:YES];
    } else if ([[dictProject objectForKey:@"request_review_status"] isEqualToString:@"N"] || [[dictProject objectForKey:@"request_review_status"] isEqualToString:@"RAW"]) {

        UIStoryboard *sbProject = [UIStoryboard storyboardWithName:@"Projects" bundle:nil];
        ProjectRequestReviewVC *prrvc = [sbProject instantiateViewControllerWithIdentifier:@"ProjectRequestReviewVC"];
        prrvc.strProjectId = [dictProject objectForKey:@"project_id"];
        prrvc.strHomeownerId = [dictProject objectForKey:@"homeowner_id"];
        prrvc.strProjectImage = [dictProject objectForKey:@"project_image"];
        prrvc.strProjectName = [dictProject objectForKey:@"project_name"];
        [self.navigationController pushViewController:prrvc animated:YES];
    }
}

-(void)awaitingReviewButtonPressedFromAccept:(UIButton *)sender {
    
    
}

-(void)chatButtonPressedFromAccept:(UIButton *)sender {
    
    NSDictionary *dictDelete = [arrMyProjectList objectAtIndex:sender.tag];
    
    UIStoryboard *sbMessage = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
    MessageDetailsVC *mdvc = [sbMessage instantiateViewControllerWithIdentifier:@"MessageDetailsVC"];
    mdvc.strProjectId = [dictDelete objectForKey:@"project_id"];
    mdvc.isFromProject = YES;
    [self.navigationController pushViewController:mdvc animated:YES];
}

-(void)closedButtonPressedFromAccept:(UIButton *)sender {
    
    
}

-(void)deleteButtonPressedFromExpire:(UIButton *)sender {
    
    NSDictionary *dictDelete = [arrMyProjectList objectAtIndex:sender.tag];
    
    [self ShowAlertForDeleteWithType:@"E" andProjectId:[dictDelete objectForKey:@"project_id"] andAppliedId:[dictDelete objectForKey:@"applied_jobid"]  atIndex:sender.tag];
}

-(void)ShowAlertForDeleteWithType:(NSString *)type andProjectId:(NSString *)projectId andAppliedId:(NSString *)appliedId atIndex:(NSUInteger)iIndex {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure want to delete this project?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        [self connectionForDeleteProjectWithProjectId:projectId andJobId:appliedId andType:type atIndex:iIndex];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
//        ;
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [alertController.view setTintColor:COLOR_THEME];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Web Service
-(void)connectionForGetProjectList:(NSString *)type {
    
    CGRect frame = viewHeader.frame;
    frame.size.height = 0.0;
    viewHeader.hidden = YES;
    viewHeader.frame = frame;
    iNumberOfLeads = 0;
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_MY_PROJECT];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"type": type,
                             @"start_from": [NSString stringWithFormat:@"%d", iStartFrom],
                             @"per_page": @"10"
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE FROM MY PROJECT:::...%@", responseObject);
        
        CGRect frame = self->viewHeader.frame;
        
        frame.size.height = 0.0;
        self->viewHeader.hidden = YES;
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            if (self->iStartFrom == 0) {
                [self->arrMyProjectList removeAllObjects];
            }
            
            NSArray *arrTemp = [NSArray arrayWithArray:[responseObject objectForKey:@"info_array"]];
            [self->arrMyProjectList addObjectsFromArray:arrTemp];
        }
        
        self->iNextData = [[responseObject objectForKey:@"next_data"] intValue];
        if (self->iNextData == 1) {
            self->iStartFrom = self->iStartFrom + 10;
        }
        
        if (self->arrMyProjectList.count > 0) {
            frame.size.height = 0.0;
            self->viewHeader.hidden = YES;
        } else {
            frame.size.height = 80.0;
            self->viewHeader.hidden = NO;
        }
        
        self->viewHeader.frame = frame;
        [self->tblProjectTypes reloadData];
        
        self.appDelegate.MessageNotificationgesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MessageNotificationTap)];
        [self.appDelegate.notificationView addGestureRecognizer:self.appDelegate.MessageNotificationgesture];
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForDeleteProjectWithProjectId:(NSString *)projectId andJobId:(NSString *)appliedJobId andType:(NSString *)projectType atIndex:(NSUInteger)indexNumber {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_DELETE_PROJECT];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"project_appliedid": appliedJobId,
                             @"project_ID": projectId,
                             @"project_type": projectType};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE FROM DELETE PROJECT:::...%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [self->arrMyProjectList removeObjectAtIndex:indexNumber];
            [self->tblProjectTypes reloadData];
            [self connectionForGetDashboardData];
            [self connectionForGetProjectList:self->strType];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
        }
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    LeadResponseVC *szvc = segue.destinationViewController;
    self.providesPresentationContextTransitionStyle = YES;
    self.definesPresentationContext = YES;
    [szvc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    szvc.strLeadsRemaining = [NSString stringWithFormat:@"%d", iNumberOfLeads];
    szvc.strProjectOwnerId = strProjectOwnerId;
    szvc.strProjectId = strProjectId;
    szvc.isMessage = isFromMessage;
    szvc.dictInfo = self.dictInfoArray;
    szvc.customNavigation = self;
//    [self presentModalViewController:szvc animated:NO];
//    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//    self.providesPresentationContextTransitionStyle = YES;
//    self.definesPresentationContext = YES;
}

#pragma mark - Custom Navigation
-(void)navigateToMessage:(int)indexPath {
    
    LeadResponseVC *lrvc = [[LeadResponseVC alloc] init];
    [lrvc tapToDismissView];
    
    iSenderTag = indexPath;
    
    NSDictionary *dict = [arrMyProjectList objectAtIndex:indexPath];
    
    UIStoryboard *sbMessage = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
    MessageDetailsVC *mdvc = [sbMessage instantiateViewControllerWithIdentifier:@"MessageDetailsVC"];
    mdvc.strProjectId = [dict objectForKey:@"project_id"];
    mdvc.isFromProject = YES;
    [self.navigationController pushViewController:mdvc animated:YES];
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
    
//    NSDictionary *dict = [arrMyProjectList objectAtIndex:iSenderTag];
//
//    UIStoryboard *sbMessage = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
//    MessageDetailsVC *mdvc = [sbMessage instantiateViewControllerWithIdentifier:@"MessageDetailsVC"];
//    mdvc.strProjectId = [dict objectForKey:@"project_id"];
//    [self.navigationController pushViewController:mdvc animated:YES];
}

#pragma mark - UIButton Action
- (IBAction)Leads:(id)sender {
    
    iNextData = iStartFrom = 0;
    
    [arrMyProjectList removeAllObjects];
    [tblProjectTypes reloadData];
    
    [self.view layoutIfNeeded];
    [UIView  animateWithDuration:0.5 animations:^{
        self->constViewUnderLine.constant = 0.0;
        [self.view layoutIfNeeded];
    }];
    [UIView commitAnimations];
    
    iSelectedButtonIndex = 1;
    [self callWebService:iSelectedButtonIndex];
    
    [btnLeads setBackgroundColor:COLOR_THEME];
    [btnPending setBackgroundColor:COLOR_TABLE_HEADER];
    [btnAccepted setBackgroundColor:COLOR_TABLE_HEADER];
    [btnExpired setBackgroundColor:COLOR_TABLE_HEADER];
    
    [btnLeads setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnPending setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
    [btnAccepted setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
    [btnExpired setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
}

- (IBAction)Pending:(id)sender {
    
    iNextData = iStartFrom = 0;
    
    [arrMyProjectList removeAllObjects];
    [tblProjectTypes reloadData];

    [self.view layoutIfNeeded];
    [UIView  animateWithDuration:0.5 animations:^{
        self->constViewUnderLine.constant = self->btnPending.frame.origin.x;
        [self.view layoutIfNeeded];
    } completion:nil];
    [UIView commitAnimations];
    
    iSelectedButtonIndex = 2;
    [self callWebService:iSelectedButtonIndex];
    
    [btnPending setBackgroundColor:COLOR_THEME];
    [btnLeads setBackgroundColor:COLOR_TABLE_HEADER];
    [btnAccepted setBackgroundColor:COLOR_TABLE_HEADER];
    [btnExpired setBackgroundColor:COLOR_TABLE_HEADER];
    
    [btnLeads setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
    [btnPending setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnAccepted setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
    [btnExpired setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
}

- (IBAction)Accepted:(id)sender {
    
    iNextData = iStartFrom = 0;
    
    [arrMyProjectList removeAllObjects];
    [tblProjectTypes reloadData];
    
    [self.view layoutIfNeeded];
    [UIView  animateWithDuration:0.5 animations:^{
        self->constViewUnderLine.constant = self->btnAccepted.frame.origin.x;
        [self.view layoutIfNeeded];
    }];
    [UIView commitAnimations];
    
    iSelectedButtonIndex = 3;
    [self callWebService:iSelectedButtonIndex];
    
    [btnPending setBackgroundColor:COLOR_TABLE_HEADER];
    [btnLeads setBackgroundColor:COLOR_TABLE_HEADER];
    [btnAccepted setBackgroundColor:COLOR_THEME];
    [btnExpired setBackgroundColor:COLOR_TABLE_HEADER];
    
    [btnLeads setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
    [btnPending setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
    [btnAccepted setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnExpired setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
}

- (IBAction)Expired:(id)sender {
    
    iNextData = iStartFrom = 0;
    
    [arrMyProjectList removeAllObjects];
    [tblProjectTypes reloadData];
    
    [self.view layoutIfNeeded];
    [UIView  animateWithDuration:0.5 animations:^{
        self->constViewUnderLine.constant = self->btnExpired.frame.origin.x;
        [self.view layoutIfNeeded];
    }];
    [UIView commitAnimations];
    
    iSelectedButtonIndex = 4;
    [self callWebService:iSelectedButtonIndex];
    
    [btnPending setBackgroundColor:COLOR_TABLE_HEADER];
    [btnLeads setBackgroundColor:COLOR_TABLE_HEADER];
    [btnAccepted setBackgroundColor:COLOR_TABLE_HEADER];
    [btnExpired setBackgroundColor:COLOR_THEME];
    
    [btnLeads setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
    [btnPending setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
    [btnAccepted setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
    [btnExpired setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (IBAction)FindNewProject:(id)sender {
    
    [self rightBarButtonPressed:nil];
    /*
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchProjectVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SearchProjectVC"];
    [self.navigationController pushViewController:svc animated:YES];
     */
}
- (IBAction)ClosedButtonPressed:(id)sender {
    
    viewPopUp.hidden = viewBackground.hidden = YES;
}

- (IBAction)ShowMessage:(id)sender {
    
    if (self.arrInterstitialAd.count > 0) {
        for (int i = 0; i < self.arrInterstitialAd.count; i++) {
            NSDictionary *dict = [self.arrInterstitialAd objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Direct Lead see message"]) {
                if ([[dict objectForKey:@"ios_interstitial_ads"] intValue] == 1) {
                    if (self.interstitial.isReady) {
                        [self.interstitial presentFromRootViewController:self];
                    }
                }
            }
        }
    }
    
    NSDictionary *dict = [arrMyProjectList objectAtIndex:iSenderTag];
    
    UIStoryboard *sbMessage = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
    MessageDetailsVC *mdvc = [sbMessage instantiateViewControllerWithIdentifier:@"MessageDetailsVC"];
    mdvc.strProjectId = [dict objectForKey:@"project_id"];
    mdvc.isFromProject = YES;
    [self.navigationController pushViewController:mdvc animated:YES];
}

- (IBAction)GoPremium:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    PremiumVC *pvc = [storyboard instantiateViewControllerWithIdentifier:@"PremiumVC"];
    [self.navigationController pushViewController:pvc animated:YES];
}
@end
