//
//  ProjectReviewVC.m
//  ProringerPro
//
//  Created by Soma Halder on 19/11/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ProjectReviewVC.h"

@interface ProjectReviewVC ()
{
    NSMutableArray *arrRestrictedWordsFound;
}

@end

@implementation ProjectReviewVC

@synthesize txtLastName, txtComment, txtFirstName, strLastName, strFirstName, lblComment, viewAccessory, strProjectId, strEmailId;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"REQUEST REVIEW";
    
    [self connectionForGetDashboardData];
    
    txtFirstName.delegate = self;
    txtComment.delegate = self;
    txtLastName.delegate = self;
    
    [self paddingForTextField:txtFirstName];
    [self paddingForTextField:txtLastName];
    
    [self shadowForTextField:txtFirstName];
    [self shadowForTextField:txtLastName];
    
    [self shadowForTextView:txtComment];
    txtComment.layer.borderWidth = 1.0;
    txtComment.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtComment.backgroundColor = [UIColor clearColor];
    
//    txtComment.inputAccessoryView = txtLastName.inputAccessoryView =  txtFirstName.inputAccessoryView = viewAccessory;
    
    txtFirstName.text = strFirstName;
    txtLastName.text = strLastName;
    
    arrRestrictedWordsFound = [[NSMutableArray alloc] init];

    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
}

#pragma mark - UITextView Delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@""]) {
        
        NSLog(@"%@",textView.text);
        
        if (textView.text.length==1) {
            lblComment.hidden = NO;
        }
    } else if ([NSString stringWithFormat:@"%@%@",textView.text,text].length > 0) {
        
        lblComment.hidden = YES;
    } else {
        lblComment.hidden = NO;
    }
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    [textView becomeFirstResponder];
    lblComment.text = @"";
    textView.layer.borderColor = COLOR_THEME.CGColor;
    textView.layer.borderWidth = 1.5f;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    [self paddingForTextView:textView];
    [self shadowForTextView:textView];
    
    [arrRestrictedWordsFound removeAllObjects];
    
    NSString *strText = [textView.text stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    NSArray *arrNewWords = [strText componentsSeparatedByString:@" "];
    NSLog(@"WORDS ARRAY FROM TEXTVIEW:::...%@", arrNewWords);
    arrRestrictedWordsFound = [self doArraysContainTheSameObjects:arrNewWords];
    
    if (![txtComment hasText]) {
        lblComment.hidden = NO;
    } else {
        lblComment.hidden = YES;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Validation
-(BOOL)validationForAllTextFields {
    
    BOOL isValid = YES;
    
    NSString *strMsg = @"";
    
    if (txtComment.text.length > 0) {
        if (arrRestrictedWordsFound.count > 0) {
            NSString *strKhisti = [arrRestrictedWordsFound componentsJoinedByString:@","];
            strMsg = [NSString stringWithFormat:@"Invalid term: %@ - Please be respectful of others. Remove and try again", strKhisti];
            [txtComment becomeFirstResponder];
            isValid = NO;
        } else if ([[AppManager sharedDataAccess] detectType:txtComment.text]) {
            
            strMsg = self.strRestrictionMsg;
            isValid = NO;
        }
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtFirstName.text]) {
        
        strMsg = @"Please enter first name";
        isValid = NO;
        [txtFirstName becomeFirstResponder];
    } else if ([[AppManager sharedDataAccess] isEmptyString:txtLastName.text]) {
        
        strMsg = @"Please enter last name";
        isValid = NO;
        [txtLastName becomeFirstResponder];
    }
    
    if (strMsg.length > 0) {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:strMsg];
    }
    
    return isValid;
}

#pragma mark - UIButton Action
- (IBAction)RequestReview:(id)sender {
    
    [self textViewDidEndEditing:txtComment];
    
    if ([self validationForAllTextFields]) {
        [self connectionForRequestReview];
    }
}

#pragma mark - Web Service
-(void) connectionForRequestReview {
    
    [YXSpritesLoadingView show];
    
    NSLog(@"INTERSTITIAL AD:::...%@", self.arrInterstitialAd);
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_REQUEST_REVIEW];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"first_name": txtFirstName.text,
                             @"last_name": txtLastName.text,
                             @"comment": txtComment.text,
                             @"email": strEmailId,
                             @"conf_emailid": strEmailId,
                             @"project_id": strProjectId};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        
        NSLog(@"response from update password:::...%@", response);
        [YXSpritesLoadingView dismiss];
        if ([[response objectForKey:@"response"] boolValue] == YES) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[response objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                [self.navigationController popViewControllerAnimated:YES];
//                [self.navigationController popToRootViewControllerAnimated:YES];
                
//                if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
//                    if (self.interstitial.isReady) {
//                        [self.interstitial presentFromRootViewController:self];
//                    }
//                }
                
                if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
                    if (self.arrInterstitialAd.count > 0) {
                        for (int i = 0; i < self.arrInterstitialAd.count; i++) {
                            NSDictionary *dict = [self.arrInterstitialAd objectAtIndex:i];
                            if ([[dict objectForKey:@"title"] isEqualToString:@"Requesting a review from Accepted tab"]) {
                                if ([[dict objectForKey:@"ios_interstitial_ads"] intValue] == 1) {
                                    if (self.interstitial.isReady) {
                                        [self.interstitial presentFromRootViewController:self];
                                    }
                                }
                            }
                        }
                    }
                }
                
                [self turnBackToAnOldViewController];
            }];
            
            [alertController addAction:okAction];
            [alertController.view setTintColor:COLOR_THEME];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              NSLog(@"ERROR:::...%@", task.error);
              [YXSpritesLoadingView dismiss];
          }];
}

- (void)turnBackToAnOldViewController{
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        //Do not forget to import AnOldViewController.h
        if ([controller isKindOfClass:[MyProjectVC class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            return;
        }
    }
}

@end
