//
//  ProjectReviewVC.h
//  ProringerPro
//
//  Created by Soma Halder on 19/11/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ProRingerProBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProjectReviewVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextView *txtComment;

@property (strong, nonatomic) NSString *strFirstName, *strLastName, *strProjectId, *strEmailId;

@property (weak, nonatomic) IBOutlet UILabel *lblComment;
@property (strong, nonatomic) IBOutlet UIView *viewAccessory;

- (IBAction)RequestReview:(id)sender;
@end

NS_ASSUME_NONNULL_END
