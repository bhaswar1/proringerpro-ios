//
//  MyProjectVC.h
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyProjectVC : ProRingerProBaseVC <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnSearchLocalProject;

@property (weak, nonatomic) IBOutlet UITableView *tblProjectTypes;

@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIView *viewPopUp;

@property (strong, nonatomic) NSString *strType, *strProjectId, *strProjectOwnerId, *strRemainingLeads;
@property (assign, nonatomic) int iNumberOfLeads;

- (IBAction)Leads:(id)sender;
- (IBAction)Pending:(id)sender;
- (IBAction)Accepted:(id)sender;
- (IBAction)Expired:(id)sender;
- (IBAction)FindNewProject:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnLeads;
@property (weak, nonatomic) IBOutlet UIButton *btnPending;
@property (weak, nonatomic) IBOutlet UIButton *btnAccepted;
@property (weak, nonatomic) IBOutlet UIButton *btnExpired;

@property (weak, nonatomic) IBOutlet FRHyperLabel *lblNoLeads;
@property (weak, nonatomic) IBOutlet FRHyperLabel *lblNoPending;
@property (weak, nonatomic) IBOutlet UILabel *lblNoAccepted;
@property (weak, nonatomic) IBOutlet UILabel *lblNoExpired;
@property (weak, nonatomic) IBOutlet UILabel *lblRemainingLeads;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constLeadsHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constPendingHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constAcceptedHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constExpiredHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewUnderLine;

- (IBAction)ClosedButtonPressed:(id)sender;
- (IBAction)ShowMessage:(id)sender;
- (IBAction)GoPremium:(id)sender;


@end

NS_ASSUME_NONNULL_END
