//
//  ProjectRequestReviewVC.h
//  ProringerPro
//
//  Created by Soma Halder on 17/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ProRingerProBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProjectRequestReviewVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UIView *viewHeading;
@property (weak, nonatomic) IBOutlet UIView *viewUserInfo;

@property (weak, nonatomic) IBOutlet EDStarRating *ratingView;

@property (weak, nonatomic) IBOutlet UIImageView *imgProject;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserName;

@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblCheckMark;
@property (weak, nonatomic) IBOutlet UILabel *lblProjectName;
@property (weak, nonatomic) IBOutlet UILabel *lblNotRequestReview;
@property (weak, nonatomic) IBOutlet UILabel *lblUserCheckMark;

@property (strong, nonatomic) NSString *strProjectId, *strProjectImage, *strUserImage, *strProjectName, *strHomeownerId;
@property (weak, nonatomic) IBOutlet UIButton *btnRequestReview;
@property (weak, nonatomic) IBOutlet UIButton *btnRequestReviewCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelReviewCheck;

- (IBAction)RequestReview:(id)sender;
- (IBAction)CancelRequest:(id)sender;
- (IBAction)SetRequest:(id)sender;

@end

NS_ASSUME_NONNULL_END
