//
//  ProjectRequestReviewVC.m
//  ProringerPro
//
//  Created by Soma Halder on 17/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ProjectRequestReviewVC.h"

@interface ProjectRequestReviewVC () {
    
    int isCancel;
    
    NSString *strFirstName, *strLastName, *strEmailId;
}

@end

@implementation ProjectRequestReviewVC
@synthesize viewHeading, viewUserInfo, ratingView, strProjectId, strUserImage, strProjectImage, imgProject, imgUserName, lblUserName, strProjectName, lblCheckMark, btnRequestReview, lblProjectName, strHomeownerId, lblUserCheckMark, lblNotRequestReview, btnCancelReviewCheck, btnRequestReviewCheck;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    self.navigationItem.title = @"Request Review";
    
    viewHeading.backgroundColor = COLOR_LIGHT_BACK;
    
    [self shadowForView:viewHeading];
    [self shadowForView:viewUserInfo];
    
    ratingView.rating = ratingView.maxRating = 5.0;
    ratingView.delegate = self;
    ratingView.horizontalMargin = 0;
    ratingView.editable = NO;
    ratingView.displayMode = EDStarRatingDisplayAccurate;
    ratingView.starImage = [UIImage imageNamed:@"StarEmpty"];
    ratingView.starHighlightedImage = [UIImage imageNamed:@"StarFull"];
    
    btnCancelReviewCheck.hidden = btnRequestReviewCheck.hidden = YES;
    lblUserName.textColor = lblNotRequestReview.textColor = [UIColor darkGrayColor];
    
    isCancel = 2;
    
    lblProjectName.text = strProjectName;
    [imgProject sd_setImageWithURL:[NSURL URLWithString:strProjectImage]];
    
    [self connectionForProjectRequestReview];
}

-(void)rightBarButtonPressed:(id)sender {
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        //Do not forget to import AnOldViewController.h
        if ([controller isKindOfClass:[MyProjectVC class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            return;
        }
    }
}

-(void)shadowForView:(UIView *)viewShadow {
    
    UIColor *color = [UIColor blackColor];
    viewShadow.layer.shadowColor = [color CGColor];
    viewShadow.layer.shadowRadius = 1.5f;
    viewShadow.layer.shadowOpacity = 0.1;
    viewShadow.layer.cornerRadius = 2.0;
    viewShadow.layer.shadowOffset = CGSizeZero;
    viewShadow.layer.masksToBounds = NO;
    
    viewShadow.layer.shadowColor = COLOR_TABLE_HEADER.CGColor;
    viewShadow.layer.shadowOffset = CGSizeMake(0, 3);
    viewShadow.layer.shadowOpacity = 0.5;
    viewShadow.layer.shadowRadius = 0.6;
}

-(void)connectionForProjectRequestReview {
    
    [YXSpritesLoadingView show];
    
    NSString *url;
    
    if (isCancel == 1) {
        url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_PROJECT_CANCEL_REVIEW];
    } else {
        url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_PROJECT_REQUEST_REVIEW];
    }
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                            @"project_id": strProjectId};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE FROM REVIEW::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            if (self->isCancel == 1) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                    if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
//                        if (self.interstitial.isReady) {
//                            [self.interstitial presentFromRootViewController:self];
//                        }
//                    }
                    
                    if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
                        if (self.arrInterstitialAd.count > 0) {
                            for (int i = 0; i < self.arrInterstitialAd.count; i++) {
                                NSDictionary *dict = [self.arrInterstitialAd objectAtIndex:i];
                                if ([[dict objectForKey:@"title"] isEqualToString:@"Requesting a review from Accepted tab"]) {
                                    if ([[dict objectForKey:@"ios_interstitial_ads"] intValue] == 1) {
                                        if (self.interstitial.isReady) {
                                            [self.interstitial presentFromRootViewController:self];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
                alertController.view.tintColor = COLOR_THEME;
            } else {
                
                self->lblUserName.text = [[[responseObject objectForKey:@"info_array"] objectAtIndex:0] objectForKey:@"homeowner_name"];
                [self->imgProject sd_setImageWithURL:[NSURL URLWithString:[[[responseObject objectForKey:@"info_array"] objectAtIndex:0] objectForKey:@"project_image"]]];
                [self->imgUserName sd_setImageWithURL:[NSURL URLWithString:[[[responseObject objectForKey:@"info_array"] objectAtIndex:0] objectForKey:@"homeowner_image"]]];
                
                self->strFirstName = [[[responseObject objectForKey:@"info_array"] objectAtIndex:0] objectForKey:@"homeowner_fname"];
                self->strLastName = [[[responseObject objectForKey:@"info_array"] objectAtIndex:0] objectForKey:@"homeowner_lname"];
                self->strEmailId = [[[responseObject objectForKey:@"info_array"] objectAtIndex:0] objectForKey:@"homeowner_email"];
            }
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)RequestReview:(id)sender {
    
    if (isCancel == 1) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to close and permanently remove?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            [self connectionForProjectRequestReview];
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:nil];
        
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        alertController.view.tintColor = COLOR_THEME;
        [self presentViewController:alertController animated:YES completion:nil];
    } else if (isCancel == 0) {
        UIStoryboard *sbProject = [UIStoryboard storyboardWithName:@"Projects" bundle:nil];
        ProjectReviewVC *prrvc = [sbProject instantiateViewControllerWithIdentifier:@"ProjectReviewVC"];
        prrvc.strProjectId = strProjectId;
        prrvc.strFirstName = strFirstName;
        prrvc.strLastName = strLastName;
        prrvc.strEmailId = strEmailId;
        [self.navigationController pushViewController:prrvc animated:YES];
    }
}

- (IBAction)CancelRequest:(id)sender {
    
    btnCancelReviewCheck.hidden = NO;
    btnRequestReviewCheck.hidden = YES;
    isCancel = 1;
    lblNotRequestReview.textColor = [UIColor greenColor];
    lblUserName.textColor = [UIColor darkGrayColor];
    [btnRequestReview setTitle:@"REMOVE PROJECT" forState:UIControlStateNormal];
}

- (IBAction)SetRequest:(id)sender {
    
    btnCancelReviewCheck.hidden = YES;
    btnRequestReviewCheck.hidden = NO;
    lblUserName.textColor = [UIColor greenColor];
    lblNotRequestReview.textColor = [UIColor darkGrayColor];
    isCancel = 0;
    lblNotRequestReview.textColor = [UIColor darkGrayColor];
    [btnRequestReview setTitle:@"REQUEST REVIEW" forState:UIControlStateNormal];
}
@end
