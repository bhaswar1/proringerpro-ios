//
//  MyProjectCollectionViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 08/07/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyProjectCollectionViewCell : UICollectionViewCell

@end

NS_ASSUME_NONNULL_END
