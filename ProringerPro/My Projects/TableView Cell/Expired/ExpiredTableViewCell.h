//
//  ExpiredTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 19/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExpiredTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewContent;

@property (weak, nonatomic) IBOutlet UILabel *lblProjectTitle;

@property (weak, nonatomic) IBOutlet UIButton *btnProjectStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteProject;

-(void)configureExpiredTabCellWith:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
