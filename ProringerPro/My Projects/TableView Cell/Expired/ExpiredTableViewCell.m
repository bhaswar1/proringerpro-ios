//
//  ExpiredTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 19/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ExpiredTableViewCell.h"

@implementation ExpiredTableViewCell
@synthesize viewContent, lblProjectTitle, btnDeleteProject, btnProjectStatus;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    viewContent.layer.cornerRadius = 5.0;
    viewContent.layer.shadowOffset = CGSizeZero;
    viewContent.layer.masksToBounds = NO;
    viewContent.layer.shadowColor = COLOR_BUTTON_BACK.CGColor;
    viewContent.layer.shadowOffset = CGSizeMake(0, 3);
    viewContent.layer.shadowOpacity = 0.5;
    viewContent.layer.shadowRadius = 0.6;
    
    btnDeleteProject.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
    btnDeleteProject.layer.borderWidth = 1.0;
}

-(void)configureExpiredTabCellWith:(NSDictionary *)dict {
    
    lblProjectTitle.text = [dict objectForKey:@"project_name"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
