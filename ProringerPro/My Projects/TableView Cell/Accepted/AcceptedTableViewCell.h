//
//  AcceptedTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 19/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AcceptedTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewShadow;

@property (weak, nonatomic) IBOutlet UILabel *lblProjectName;

@property (weak, nonatomic) IBOutlet UIButton *btnCloseOrChat;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnReview;

-(void)configureAcceptedCellWith:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
