//
//  AcceptedTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 19/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "AcceptedTableViewCell.h"

@implementation AcceptedTableViewCell
@synthesize viewShadow, lblProjectName, btnDelete, btnReview, btnCloseOrChat;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    viewShadow.layer.cornerRadius = 5.0;
    viewShadow.layer.shadowOffset = CGSizeZero;
    viewShadow.layer.masksToBounds = NO;
    viewShadow.layer.shadowColor = COLOR_BUTTON_BACK.CGColor;
    viewShadow.layer.shadowOffset = CGSizeMake(0, 3);
    viewShadow.layer.shadowOpacity = 0.5;
    viewShadow.layer.shadowRadius = 0.6;
}

-(void)configureAcceptedCellWith:(NSDictionary *)dict {
    
    lblProjectName.text = [dict objectForKey:@"project_name"];
    [btnDelete setBackgroundColor:COLOR_BUTTON_BACK];
    [btnCloseOrChat setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnReview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    if ([[dict objectForKey:@"chat_btn"] isEqualToString:@"C"]) {
        [btnCloseOrChat setTitle:@"          CHAT          " forState:UIControlStateNormal];
        [btnCloseOrChat setBackgroundColor:COLOR_THEME];
    } else {
        [btnCloseOrChat setTitle:@"         CLOSED         " forState:UIControlStateNormal];
        [btnCloseOrChat setBackgroundColor:COLOR_BUTTON_BACK];
    }
    
    if ([[dict objectForKey:@"request_review_status"] isEqualToString:@"N"]) {
        [btnReview setTitle:@"   REQUEST REVIEW   " forState:UIControlStateNormal];
        [btnReview setBackgroundColor:COLOR_GREEN];
    } else if ([[dict objectForKey:@"request_review_status"] isEqualToString:@"A"]) {
        [btnReview setTitle:@"   VIEW REVIEW   " forState:UIControlStateNormal];
        [btnReview setBackgroundColor:COLOR_BUTTON_BACK];
    } else if ([[dict objectForKey:@"request_review_status"] isEqualToString:@"AW"]) {
        [btnReview setTitle:@"   AWAITING REVIEW   " forState:UIControlStateNormal];
        [btnReview setBackgroundColor:COLOR_THEME];
    } else if ([[dict objectForKey:@"request_review_status"] isEqualToString:@"RAW"]) {
        [btnReview setTitle:@"   RESEND REQUEST   " forState:UIControlStateNormal];
        [btnReview setBackgroundColor:COLOR_GREEN];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
