//
//  PendingTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 19/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PendingTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewContent;

@property (weak, nonatomic) IBOutlet UILabel *lblProjectTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubmittedDate;
@property (weak, nonatomic) IBOutlet UILabel *lblExpireDate;
@property (weak, nonatomic) IBOutlet UILabel *lblNew;

@property (weak, nonatomic) IBOutlet UIButton *btnProjectStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteProject;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constNewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constLeadingNew;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constNewLabelWidth;


-(void)configureCellWith:(NSDictionary *)dictPending;

@end

NS_ASSUME_NONNULL_END
