//
//  PendingTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 19/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "PendingTableViewCell.h"

@implementation PendingTableViewCell
@synthesize viewContent, btnDeleteProject, btnProjectStatus, lblExpireDate, lblProjectTitle, lblSubmittedDate, constNewWidth, constLeadingNew, lblNew, constNewLabelWidth;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    viewContent.layer.cornerRadius = 5.0;
    viewContent.layer.shadowOffset = CGSizeZero;
    viewContent.layer.masksToBounds = NO;
    viewContent.layer.shadowColor = COLOR_BUTTON_BACK.CGColor;
    viewContent.layer.shadowOffset = CGSizeMake(0, 3);
    viewContent.layer.shadowOpacity = 0.5;
    viewContent.layer.shadowRadius = 0.6;
    
    [btnDeleteProject setBackgroundColor:COLOR_BUTTON_BACK];
    [btnDeleteProject setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
}

-(void)configureCellWith:(NSDictionary *)dictPending {
    
    NSString *strButtonTitle;
    
    if ([[dictPending objectForKey:@"msg_received"] intValue] == 0) {
        strButtonTitle = [NSString stringWithFormat:@"   %@   ", [dictPending objectForKey:@"pending_btn"]];
        [btnProjectStatus setBackgroundColor:COLOR_AWAITING_RESPONSE_YELLOW];
        [btnProjectStatus setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
    } else {
        strButtonTitle = [NSString stringWithFormat:@"          %@          ", @"CHAT"];
        [btnProjectStatus setBackgroundColor:COLOR_THEME];
        [btnProjectStatus setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    [btnProjectStatus setTitle:strButtonTitle forState:UIControlStateNormal];
    
    lblExpireDate.text = [dictPending objectForKey:@"expiry_date"];
    lblSubmittedDate.text = [dictPending objectForKey:@"submitted_date"];
    lblProjectTitle.text = [dictPending objectForKey:@"project_name"];
    
    if ([[dictPending objectForKey:@"read_status"] intValue] == 0) {
        lblNew.text = @"NEW";
        constLeadingNew.constant = 8.0;
        constNewWidth.constant = 3.0;
        constNewLabelWidth.constant = 25.0;
    } else {
        lblNew.text = @"";
        constLeadingNew.constant = constNewWidth.constant = constNewLabelWidth.constant = 0.0;
    }
    
//    if ([[dictPending objectForKey:@"leadView"] intValue] == 0) {
//        lblNew.text = @"NEW";
//        constLeadingNew.constant = 8.0;
//        constNewWidth.constant = 5.0;
//        constNewLabelWidth.constant = 25.0;
//    } else {
//        lblNew.text = @"";
//        constLeadingNew.constant = constNewWidth.constant = constNewLabelWidth.constant = 0.0;
//    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
