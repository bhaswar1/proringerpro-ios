//
//  LeadsTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 19/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LeadsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewContent;

@property (weak, nonatomic) IBOutlet UILabel *lblProjectTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubmittedDate;
@property (weak, nonatomic) IBOutlet UILabel *lblExpireDate;

@property (weak, nonatomic) IBOutlet UIButton *btnProject;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteProject;
@property (weak, nonatomic) IBOutlet UIButton *btnMessage;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constNewLeads;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constNewViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constNewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constMessageWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constDeleteLeads;

@property (weak, nonatomic) IBOutlet UILabel *lblNew;

@property (weak, nonatomic) IBOutlet UIView *viewNewProject;

-(void)configureLeadsCellWith:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
