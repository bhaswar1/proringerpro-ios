//
//  LeadsTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 19/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "LeadsTableViewCell.h"

@implementation LeadsTableViewCell
@synthesize viewContent, btnProject, btnDeleteProject, lblNew, lblExpireDate, lblProjectTitle, lblSubmittedDate, viewNewProject, constNewLeads, constNewViewWidth, constNewWidth, btnMessage, constDeleteLeads, constMessageWidth;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIColor *color = [UIColor blackColor];
    viewContent.layer.shadowColor = [color CGColor];
    viewContent.layer.shadowRadius = 1.5f;
    viewContent.layer.shadowOpacity = 0.1;
    viewContent.layer.cornerRadius = 5.0;
    viewContent.layer.shadowOffset = CGSizeZero;
    viewContent.layer.masksToBounds = NO;
    
    viewContent.layer.shadowColor = COLOR_BUTTON_BACK.CGColor;
    viewContent.layer.shadowOffset = CGSizeMake(0, 3);
    viewContent.layer.shadowOpacity = 0.5;
    viewContent.layer.shadowRadius = 0.6;
    
    viewContent.layer.cornerRadius = 5.0;
    
    [btnProject setBackgroundColor:COLOR_GREEN];
    [btnProject setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [btnMessage setBackgroundColor:COLOR_THEME];
    [btnMessage setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnMessage setTitle:@"" forState:UIControlStateNormal];
    
    [btnDeleteProject setBackgroundColor:COLOR_BUTTON_BACK];
    [btnDeleteProject setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    btnProject.layer.cornerRadius = btnDeleteProject.layer.cornerRadius = 13.0;
}

-(void)configureLeadsCellWith:(NSDictionary *)dict {
    
    if ([[dict objectForKey:@"leadView"] intValue] == 1) {
        lblNew.hidden = YES;
        viewNewProject.hidden = YES;
        lblNew.text = @"";
        constNewViewWidth.constant = constNewLeads.constant = constNewWidth.constant = 0;
    } else if ([[dict objectForKey:@"leadView"] intValue] == 0) {
        lblNew.hidden = NO;
        lblNew.text = @"NEW";
        viewNewProject.hidden = NO;
        constNewLeads.constant = 5;
        constNewViewWidth.constant = 3;
        constNewWidth.constant = 25;
    }

    lblProjectTitle.text = [dict objectForKey:@"project_name"];
    lblSubmittedDate.text = [dict objectForKey:@"submitted_date"];
    lblExpireDate.text = [dict objectForKey:@"expiry_date"];
    
    if ([[dict objectForKey:@"new_lead_status"] isEqualToString:@"D"]) {
        [btnProject setTitle:@"      DIRECT LEAD      " forState:UIControlStateNormal];
        [btnMessage setTitle:@"MESSAGE" forState:UIControlStateNormal];
        constMessageWidth.constant = 80;
        constDeleteLeads.constant = 8.0;
    } else if ([[dict objectForKey:@"new_lead_status"] isEqualToString:@"N"]) {
        [btnProject setTitle:@"      PROJECT      " forState:UIControlStateNormal];
        [btnMessage setTitle:@"" forState:UIControlStateNormal];
        constMessageWidth.constant = constDeleteLeads.constant = 0;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
