//
//  SearchTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 02/05/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "SearchTableViewCell.h"

@implementation SearchTableViewCell
@synthesize lblAddress, lblJobDetails, lblProjectName, lblServiceName, lblServiceType, lblCategoryName, lblServicePostTime, imgService, viewContent;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIColor *color = [UIColor blackColor];
    viewContent.layer.shadowColor = [color CGColor];
    viewContent.layer.shadowRadius = 1.5f;
    viewContent.layer.shadowOpacity = 0.1;
    viewContent.layer.cornerRadius = 2.0;
    viewContent.layer.shadowOffset = CGSizeZero;
    viewContent.layer.masksToBounds = NO;
    
    viewContent.layer.shadowColor = COLOR_TABLE_HEADER.CGColor;
    viewContent.layer.shadowOffset = CGSizeMake(-2, -3);
    viewContent.layer.shadowOpacity = 0.5;
    viewContent.layer.shadowRadius = 0.6;
}

-(void)configureCellWith:(NSDictionary *)dictProject {
    
    lblProjectName.text = [dictProject objectForKey:@"prjct_name"];
    lblCategoryName.text = [dictProject objectForKey:@"category_name"];
    lblAddress.text = [NSString stringWithFormat:@"%@, %@ %@", [dictProject objectForKey:@"city"], [dictProject objectForKey:@"state"], [dictProject objectForKey:@"zip"]];
    lblServiceName.text = [dictProject objectForKey:@"service_name"];
    lblServiceType.text = [dictProject objectForKey:@"service_look_type"];
    lblJobDetails.text = [dictProject objectForKey:@"job_details"];
    lblServicePostTime.text = [dictProject objectForKey:@"post_time"];
    
//    [self imageLoadFromURL:[dictProject objectForKey:@"prjct_img"] withContainer:imgService];
    [imgService sd_setImageWithURL:[dictProject objectForKey:@"prjct_img"]];
}

-(void)imageLoadFromURL:(NSString*)url withContainer:(UIImageView*)imgView {
    NSURL *imageURL = [NSURL URLWithString:url];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            imgView.image = [UIImage imageWithData:imageData];
        });
    });
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
