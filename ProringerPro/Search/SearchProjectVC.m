//
//  SearchProjectVC.m
//  ProringerPro
//
//  Created by Soma Halder on 02/05/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "SearchProjectVC.h"

@interface SearchProjectVC ()
{
    NSString *strTimeZone, *strZipCode, *strSearchText, *strCity, *strState;
    NSMutableArray *arrProjectList;
    BOOL isResponse;
    NSInteger iTag;
}

@end

@implementation SearchProjectVC
@synthesize tblSearchProject, viewHeader, lblTitle, lblMessage, searchBar;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon"]];
//    self.tabBarController.tabBar.hidden = NO;
    
    [self connectionForGetDashboardData];
    
    searchBar.delegate = self;
    
    self.viewFooterTab.lblNewProject.backgroundColor = self.viewFooterTab.lblNewMessage.backgroundColor = COLOR_THEME;
    self.viewFooterTab.lblNewMessage.layer.borderColor = self.viewFooterTab.lblNewProject.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewFooterTab.lblNewProject.layer.borderWidth = self.viewFooterTab.lblNewMessage.layer.borderWidth = 0.5;
    
    [self.view addSubview:self.viewFooterTab];
    
    arrProjectList = [[NSMutableArray alloc] init];
    
    tblSearchProject.dataSource = self;
    tblSearchProject.delegate = self;
    
    strZipCode = [[NSString alloc] init];
    strSearchText = [[NSString alloc] init];
    strCity = @"";
    
    CGRect frame = self->viewHeader.frame;
    frame.size.height = 0.0;
    self->viewHeader.hidden = YES;
    self->viewHeader.frame = frame;
    
    [tblSearchProject registerNib:[UINib nibWithNibName:@"WatchListTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellWatchList"];
    
    [self connectionForGetProjectListWithCategory:strSearchText];
    
    [self footerButtonDesignWithTag:0];
    [self footerButtonTap:0];
    
//    iTag = [[[NSUserDefaults standardUserDefaults] objectForKey:@"footer"] integerValue];
//    [self footerButtonDesignWithTag:iTag];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Search Project"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Search Project"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }  else {
//                [self.bannerView removeFromSuperview];
//            }
//        }
//    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
    
    [self connectionForGetProjectListWithCategory:strSearchText];
    
//    NSInteger iTag = [[[NSUserDefaults standardUserDefaults] objectForKey:@"footer"] integerValue];
//    [self footerButtonDesignWithTag:iTag];
}

-(void)rightBarButtonPressed:(id)sender {
    
    NSLog(@"Right button pressed from Search Projects class");
    [self performSegueWithIdentifier:@"zipCode" sender:nil];
}

#pragma mark - UISearchBar
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    strSearchText = searchBar.text;
    
    [self connectionForGetProjectListWithCategory:strSearchText];
}

//- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
//
//    strSearchText = searchText;
//    [searchBar resignFirstResponder];
//    [self connectionForGetProjectListWithCategory:strSearchText andService:@"1"];
//
//    if (searchText.length == 0) {
//        [searchBar resignFirstResponder];
//    }
//}

#pragma mark - UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrProjectList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellWatchList";
    
    NSDictionary *dictProjectList = [arrProjectList objectAtIndex:indexPath.row];
    
    WatchListTableViewCell *cell = (WatchListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell.btnFavourite addTarget:self action:@selector(favouriteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnFavourite.tag = indexPath.row;
    
    [cell configureCellWith:dictProjectList];
    
    return cell;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    
//    return searchBar;
//}


//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//
//    return 56;
//}

-(void)favouriteButtonPressed:(UIButton *)sender {
    
    NSDictionary *dict = [arrProjectList objectAtIndex:sender.tag];
    
    int iProjectFunction = 0;
    
    if ([[dict objectForKey:@"watchlist_status"] intValue] == 1) {
        iProjectFunction = 0;
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to remove from watchlist" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [self connectionForDeleteOrAddFromWatchlist:[dict objectForKey:@"id"] andProjectStatus:iProjectFunction];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //        ;
        }];
        
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        [alertController.view setTintColor:COLOR_THEME];
    } else {
        iProjectFunction = 1;
        [self connectionForDeleteOrAddFromWatchlist:[dict objectForKey:@"id"] andProjectStatus:iProjectFunction];
    }
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 120;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSDictionary *dictProjectDetails = [arrProjectList objectAtIndex:indexPath.row];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Watchlist" bundle:nil];
    ProjectDetailsVC *pdvc = [storyboard instantiateViewControllerWithIdentifier:@"ProjectDetailsVC"];
    pdvc.strProjectId = [dictProjectDetails objectForKey:@"id"];
    pdvc.strProjectOwnerId = [dictProjectDetails objectForKey:@"homeowner_id"];
    [self.navigationController pushViewController:pdvc animated:YES];
}

#pragma mark - Web Service
-(void)connectionForDeleteOrAddFromWatchlist:(NSString *)strProjectId andProjectStatus:(int)iProjectFunction {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_ADD_DELETE_WATCHLIST];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"project_id": strProjectId,
                             @"project_function": [NSString stringWithFormat:@"%d", iProjectFunction]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE FROM MY PROJECT:::...%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //                ;
                //                [self.navigationController popViewControllerAnimated:YES];
                [self connectionForGetProjectListWithCategory:@""];
            }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        } else {
            //            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            //            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //                [self tapToDismissView];
            //            }];
            //            [alertController addAction:okAction];
            //            [self presentViewController:alertController animated:YES completion:nil];
        }
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForGetProjectListWithCategory:(NSString *)category {
    
    [YXSpritesLoadingView show];
    
    NSString *strZipSearch;
    if (strCity.length > 0 || strState.length > 0) {
        strZipSearch = [NSString stringWithFormat:@"%@, %@", strCity, strState];
    } else
        strZipSearch = @"";
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_PROJECT_SEARCH];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"category_search": category,
                             @"zip_search": strZipSearch,
                             @"allservice": @"1"};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE FROM MY PROJECT DETAILS:::...%@", responseObject);
        
        self->isResponse = [responseObject objectForKey:@"response"];
        
        [self->arrProjectList removeAllObjects];
        CGRect frame = self->viewHeader.frame;
        if ([[responseObject objectForKey:@"response"] boolValue]) {
            
            [self->arrProjectList addObjectsFromArray:[responseObject objectForKey:@"info_array"]];
            if (self->arrProjectList.count > 0) {
                frame.size.height = 0.0;
                self->viewHeader.hidden = YES;
            }
        } else {
            self->lblTitle.text = [responseObject objectForKey:@"message_title"];
//            self->lblMessage.text = [responseObject objectForKey:@"message"];
            self->viewHeader.hidden = NO;
            frame.size.height = 250.0;
            
            
            NSString *strMessage = [responseObject objectForKey:@"message"];
            NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor darkGrayColor],NSFontAttributeName: [UIFont fontWithName:@"OpenSans" size:13.0]};
            self->lblMessage.attributedText = [[NSAttributedString alloc]initWithString:strMessage attributes:attributes];
            
            void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
                if ([substring isEqualToString:@"Hint:"]) {
                    
                } else if ([substring isEqualToString:@"services"]) {
                    [self navigateToServices];
                } else if ([substring isEqualToString:@"service areas"]) {
                    [self navigateToServicesArea];
                } else if ([substring isEqualToString:@"notification"]) {
                    [self navigateToNotification];
                }
            };
            
            //Step 3: Add link substrings
            [self->lblMessage setLinksForSubstrings:@[@"Hint:", @"services", @"service areas", @"notification"] withLinkHandler:handler];
        }
        self->viewHeader.frame = frame;
        [self->tblSearchProject reloadData];
        
        [YXSpritesLoadingView dismiss];
//       [self footerButtonDesignWithTag:iTag];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"zipCode"]) {
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        SearchZipVC *szvc = segue.destinationViewController;
        self.providesPresentationContextTransitionStyle = YES;
        self.definesPresentationContext = YES;
        szvc.deleagte = self;
        szvc.strNavigationTitle = @"LOCATION";
        szvc.strAddressTitle = @"Zip / Postal Code";
        szvc.strPlaceHolder = @"Zip / Postal";
        szvc.strApi = @"(regions)";
        szvc.strSearchText = @"locality";
        szvc.strSubSearchText = @"sublocality";
        szvc.strSearchComponent = @"long_name";
        [szvc setModalPresentationStyle:UIModalPresentationCurrentContext];
    }
}

-(void) navigateToServices {
    
    [self ADSideMenuDrawerSelection:1 withRow:2 andTitle:@"Services"];
}

-(void) navigateToServicesArea {
    
    [self ADSideMenuDrawerSelection:1 withRow:12 andTitle:@"Service Area"];
}

-(void) navigateToNotification {
    
    [self ADSideMenuDrawerSelection:1 withRow:7 andTitle:@"Notifications"];
}

#pragma mark - Protocol Method
-(void)sendAddressToUserInfo:(NSDictionary *)dictAddress {
    
    NSArray *arrAddressComponent = [dictAddress objectForKey:@"address_components"];
    
    NSLog(@"Address::...%@", dictAddress);
    
    strZipCode = strState = strCity = @"";
    
    for (int i = 0; i < arrAddressComponent.count; i++) {
        NSDictionary *dictComponent = [arrAddressComponent objectAtIndex:i];
        NSArray *arrTypes = [dictComponent objectForKey:@"types"];
        for (int iTypes = 0; iTypes < arrTypes.count; iTypes++) {
            if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"postal_code"]) {
                strZipCode = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"administrative_area_level_1"]) {
                strState = [dictComponent objectForKey:@"short_name"];
            } else if ([[arrTypes objectAtIndex:iTypes] isEqualToString:@"locality"] || [[arrTypes objectAtIndex:iTypes] isEqualToString:@"sublocality"]) {
                strCity = [dictComponent objectForKey:@"long_name"];
            }
        }
    }
    NSLog(@"\npostal_code:::..:::...%@\nstate:::...%@\ncity:::...%@\n", strZipCode, strState, strCity);
    
    if (strState.length > 0 && strCity.length > 0) {
    
        [self connectionForGetProjectListWithCategory:strSearchText];
        
    } else {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please enter a valid zip/postal code" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        alertController.view.tintColor = [UIColor colorWithRed:241/255.0 green:89/255.0 blue:42/255.0 alpha:1];
    }
}

@end
