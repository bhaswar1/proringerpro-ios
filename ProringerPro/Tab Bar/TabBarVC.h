//
//  TabBarVC.h
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TabBarVC : UITabBarController <UITabBarControllerDelegate, UITabBarDelegate>

@property (weak, nonatomic) IBOutlet UITabBar *tabBar;


@end

NS_ASSUME_NONNULL_END
