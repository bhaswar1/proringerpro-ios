//
//  WatchListTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 26/06/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "WatchListTableViewCell.h"

@implementation WatchListTableViewCell
@synthesize lblZipCode, lblPostTime, lblJobDetails, lblProjectName, lblServiceType, lblCategoryName, lblCategoryServiceName, viewShawod, btnFavourite, imgProject;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self shadowForView:self.viewShawod];
}

-(void)configureCellWith:(NSDictionary *)dict {
    
    NSLog(@"dictionary:::...%@", dict);
    
    lblProjectName.text = [dict objectForKey:@"prjct_name"];
    lblCategoryName.text = [dict objectForKey:@"category_name"];
    lblServiceType.text = [dict objectForKey:@"service_look_type"];
    
    if ([dict objectForKey:@"city"] || [dict objectForKey:@"state"]) {
        lblZipCode.text = [NSString stringWithFormat:@"%@, %@ %@", [dict objectForKey:@"city"], [dict objectForKey:@"state"], [dict objectForKey:@"zip"]];
    } else {
        lblZipCode.text = [dict objectForKey:@"zip"];
    }
    
    NSString *strServiceName = [dict objectForKey:@"category_service_name"];
    
    if (strServiceName.length == 0) {
        lblCategoryServiceName.text = [dict objectForKey:@"service_name"];
    } else {
        lblCategoryServiceName.text = strServiceName;
    }
    
    if ([dict objectForKey:@"watchlist_status"]) {
        if ([[dict objectForKey:@"watchlist_status"] intValue] == 1) {
            [btnFavourite setTintColor:COLOR_THEME];
        } else if ([[dict objectForKey:@"watchlist_status"] intValue] == 0) {
            [btnFavourite setTintColor:COLOR_TABLE_HEADER];
        }
    } else {
        [btnFavourite setTintColor:COLOR_THEME];
    }
    
    lblJobDetails.text = [dict objectForKey:@"job_details"];
    lblPostTime.text = [dict objectForKey:@"post_time"];
    
    [imgProject sd_setImageWithURL:[dict objectForKey:@"prjct_img"]];
}

- (IBAction)FavoutiteButtonPressed:(id)sender {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)shadowForView:(UIView *)subview {
    
    [subview.layer setShadowColor: COLOR_TABLE_HEADER.CGColor];
    [subview.layer setShadowOpacity:0.8];
    [subview.layer setShadowRadius:2.0];
    [subview.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
}

@end
