//
//  WatchListTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 26/06/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WatchListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblProjectName;
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryName;
@property (weak, nonatomic) IBOutlet UILabel *lblZipCode;
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryServiceName;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceType;
@property (weak, nonatomic) IBOutlet UILabel *lblJobDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblPostTime;
@property (weak, nonatomic) IBOutlet UIView *viewShawod;

@property (weak, nonatomic) IBOutlet UIImageView *imgProject;
@property (weak, nonatomic) IBOutlet UIButton *btnFavourite;

-(void)configureCellWith:(NSDictionary *)dict;

- (IBAction)FavoutiteButtonPressed:(id)sender;
@end

NS_ASSUME_NONNULL_END
