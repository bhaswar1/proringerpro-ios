//
//  ProjectDetailsVC.h
//  ProringerPro
//
//  Created by Soma Halder on 26/06/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProjectDetailsVC : ProRingerProBaseVC <UITextViewDelegate, UIGestureRecognizerDelegate, GMSMapViewDelegate>

@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *viewBlank;

@property (weak, nonatomic) IBOutlet UILabel *lblProjectName;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkType;
@property (weak, nonatomic) IBOutlet UILabel *lblService;
@property (weak, nonatomic) IBOutlet UILabel *lblProperty;
//@property (weak, nonatomic) IBOutlet UILabel *lblProjectDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblPostDate;
@property (weak, nonatomic) IBOutlet UILabel *lblPostedBy;
@property (weak, nonatomic) IBOutlet UITextView *lblProjectDetails;

@property (weak, nonatomic) IBOutlet UIImageView *imgProject;

@property (weak, nonatomic) IBOutlet UITableView *tblProjectDetails;
@property (weak, nonatomic) IBOutlet UIView *viewHeader;

//@property (strong, nonatomic) NSMutableDictionary *dictProjectDetails;
@property (strong, nonatomic) NSString *strProjectId, *strProjectOwnerId, *strResponseButtonTitle, *strNewLeadStatus;

@property (weak, nonatomic) IBOutlet UIButton *btnFavourite;
@property (weak, nonatomic) IBOutlet UIButton *btnRespondNow;
@property (weak, nonatomic) IBOutlet UIButton *btnClickToView;
@property (weak, nonatomic) IBOutlet UIButton *btnResponseMain;

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceHolder;
@property (weak, nonatomic) IBOutlet FRHyperLabel *lblPremiumNow;

@property (weak, nonatomic) IBOutlet UILabel *lblRemainingResponse;

@property (weak, nonatomic) IBOutlet UIView *viewShadow;
@property (weak, nonatomic) IBOutlet UIView *viewRespond;
@property (weak, nonatomic) IBOutlet UIView *viewLeadsRemaining;

@property (weak, nonatomic) IBOutlet UITextView *txtRespondMsg;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constBtnMainRespHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constBottomResponse;

- (IBAction)FavoutiteButtonPressed:(id)sender;
- (IBAction)RespondNow:(id)sender;
- (IBAction)Send:(id)sender;
- (IBAction)ClickToViewPhoneNumber:(id)sender;
- (IBAction)CloseLeadsReamining:(id)sender;
- (IBAction)ResponseNowFromLeadsRemainingView:(id)sender;
- (IBAction)GoPremium:(id)sender;

@end

NS_ASSUME_NONNULL_END
