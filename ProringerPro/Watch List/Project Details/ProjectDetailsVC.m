//
//  ProjectDetailsVC.m
//  ProringerPro
//
//  Created by Soma Halder on 26/06/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ProjectDetailsVC.h"

@interface ProjectDetailsVC ()
{
    BOOL isFavourite, isResponse, isLeadApplied;
    NSString *strZipCode, *strPhoneNumber, *strAppliedJobStatus;
    UITapGestureRecognizer *tapGesture;
    
    double latitude;
    double longitude;
    GMSMarker *marker;
    CLLocationCoordinate2D target;
    NSMutableArray *arrRestrictedWordsFound;
    int iLeadsRemaining, iPhoneStatus, iFreeDealStatus, iPremiumStatus, iResponseExists;
}

@end

@implementation ProjectDetailsVC
@synthesize viewHeader, lblAddress, lblService, lblProperty, lblWorkType, lblProjectName, lblProjectDetails, imgProject, btnFavourite, lblPostDate, strProjectId, lblPostedBy, tblProjectDetails, strProjectOwnerId, txtRespondMsg, viewShadow, viewRespond, lblPlaceHolder, mapView, lblRemainingResponse, viewLeadsRemaining, lblPremiumNow, btnRespondNow, btnClickToView, strResponseButtonTitle, btnResponseMain, webView,constBtnMainRespHeight, viewBlank, strNewLeadStatus, constBottomResponse;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    [self connectionForGetProjectDetails];
    
    isLeadApplied = NO;
    lblProjectName.textColor = COLOR_THEME;
    
    arrRestrictedWordsFound = [[NSMutableArray alloc] init];
    
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon"]];
    constBtnMainRespHeight.constant = 0.0;
    constBottomResponse.constant = 60.0;
    viewRespond.hidden = viewShadow.hidden = viewLeadsRemaining.hidden = YES;
    
    tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapToDismissView)];
    tapGesture.delegate = self;
    [viewShadow addGestureRecognizer:tapGesture];
    
    txtRespondMsg.delegate = self;
    [self paddingForTextView:txtRespondMsg];
    [self shadowForTextView:txtRespondMsg];
    
    mapView.delegate = self;
    mapView.mapType = kGMSTypeNormal;

    viewBlank.hidden = NO;
//    viewBlank.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    viewBlank.frame = CGRectMake(0, 0, 0, 0);
    
    NSString *strLeads = @"Respond to an unlimited number of projects. Get instant notifications when new projects are listed and much more. Upgrade your account to premium now.";
//    NSDictionary *attributesLeads = @{NSForegroundColorAttributeName: [UIColor darkGrayColor],NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote]};
//    lblPremiumNow.attributedText = [[NSAttributedString alloc]initWithString:strLeads attributes:attributesLeads];
//
//    void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
//        if ([substring isEqualToString:@"premium"]) {
//            [self goForPremium];
//        }
//    };
//    [lblPremiumNow setLinksForSubstrings:@[@"premium"] withLinkHandler:handler];
    
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor darkGrayColor],NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]};
    lblPremiumNow.attributedText = [[NSAttributedString alloc]initWithString:strLeads attributes:attributes];
    
    void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
        if ([substring isEqualToString:@"premium"]) {
            [self goForPremium];
        }
    };
    
    //Step 3: Add link substrings
    [lblPremiumNow setLinksForSubstrings:@[@"premium"] withLinkHandler:handler];

    if (strResponseButtonTitle == nil || strResponseButtonTitle.length == 0 || [strResponseButtonTitle isEqual:[NSNull null]]) {
        [btnResponseMain setTitle:@"" forState:UIControlStateNormal];
    } else {
        [btnResponseMain setTitle:strResponseButtonTitle forState:UIControlStateNormal];
//        constBtnMainRespHeight.constant = 40.0;
    }
    
    if ([strResponseButtonTitle isEqualToString:@"AWAITING RESPONSE"]) {
        [btnResponseMain setBackgroundColor:[UIColor lightGrayColor]];
        btnResponseMain.userInteractionEnabled = NO;
    } else {
        [btnResponseMain setBackgroundColor:COLOR_THEME];
        btnResponseMain.userInteractionEnabled = YES;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self tapToDismissView];
}

//-(void)callDashboardWebService:(void (^) (void)) completion {
//
//    [self connectionForGetDashboardData];
//
//    completion();
//}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro dashboard"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }

    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Project Posting Details"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                    constBottomResponse.constant = 60.0;
                } else {
                    [self.bannerView removeFromSuperview];
                    constBottomResponse.constant = 10.0;
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
        constBottomResponse.constant = 10.0;
    }
}

- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.bottomLayoutGuide
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:-0],
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
    //    bannerView.layer.zPosition = -1;
    //    table_view.layer.zPosition = -2;
}

-(void)embedMapWithPlaceId:(NSString *)placeId {
    
//    [YXSpritesLoadingView show];
    
    float fWidth = mapView.frame.size.width;
    float fHeight = mapView.frame.size.height;
    
    // Read create a NSString object with iframe
    NSString *embedHTML = [NSString stringWithFormat:@"<html><head><title>.</title><style>body,html,iframe{margin:0;padding:0;}</style></head><body><iframe width=\"%f\" height=\"%f\" src=\"https://www.google.com/maps/embed/v1/place?q=place_id:%@&key=%@\" frameborder=\"0\" allowfullscreen></iframe></body></html>", fWidth, fHeight, placeId, API_GOOGLE_KEY];
    
    webView.delegate = self;
    // Initialize the html data to webview
    [webView loadHTMLString:embedHTML baseURL:nil];
    
    // Add webview to your main view
    [viewHeader addSubview:webView];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
//    [YXSpritesLoadingView show];
    viewBlank.hidden = NO;
    viewBlank.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    viewBlank.frame = CGRectMake(0, 0, 0, 0);
    [YXSpritesLoadingView dismiss];
    viewBlank.hidden = YES;
}

#pragma mark - UIGesture Recognizer
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {

    if (CGRectContainsPoint(viewRespond.bounds, [touch locationInView:viewRespond])) {
        return NO;
    } else if (CGRectContainsPoint(viewLeadsRemaining.bounds, [touch locationInView:viewLeadsRemaining])) {
        return NO;
    } else
        return YES;
}

-(void)tapToDismissView {

    viewRespond.hidden = viewShadow.hidden = viewLeadsRemaining.hidden = YES;
    [txtRespondMsg resignFirstResponder];
    [viewShadow removeGestureRecognizer:tapGesture];
}

-(void)favouriteButtonImageSettings {
    
    UIImage *imgFavouriteButtonImage;
    
    if (isFavourite) {
        imgFavouriteButtonImage = [UIImage imageNamed:@"HeartLike"];
    } else {
        imgFavouriteButtonImage = [UIImage imageNamed:@"HeartDislike"];
    }
    
    [btnFavourite setImage:imgFavouriteButtonImage forState:UIControlStateNormal];
}

-(void)imageLoadFromURL:(NSString*)url withContainer:(UIImageView*)imgView {
    NSURL *imageURL = [NSURL URLWithString:url];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            
            NSLog(@"LABEL HEIGHT BEFORE TEXT:::...%f", self->lblProjectDetails.frame.size.height);
            
//            CGRect frame = self->viewHeader.frame;
//            frame.size.height = self->lblProjectDetails.frame.origin.y + self->lblProjectDetails.frame.size.height + 20;
//            self->viewHeader.frame = frame;
            
            NSLog(@"LABEL HEIGHT AFTER TEXT:::...%f", self->lblProjectDetails.frame.size.height);
            
            imgView.image = [UIImage imageWithData:imageData];
        });
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - MapView Delegate
-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr {
    
//    [YXSpritesLoadingView show];
    
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *strPlaceId;
    NSString *req = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@&key=%@&language=en&components=country:US|country:CA",esc_addr, API_GOOGLE_KEY];
    
    req = [req stringByAddingPercentEscapesUsingEncoding:NSStringEncodingConversionExternalRepresentation];
    
    NSURL *url = [NSURL URLWithString:req];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"GET"];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (!response) {
        NSLog(@"Connection Error, 1Failed to Connect to the Internet");
    }
    
    id dict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    if (dict != nil) {
        NSLog(@"Dict: %@", dict);
        
        if ([[dict objectForKey:@"status"] isEqualToString:@"OK"]) {
            
//            NSArray *arr = [dict objectForKey:@"results"];
//            NSDictionary *dictResult = [arr objectAtIndex:0];
//            NSDictionary *dictGeometry = [dictResult objectForKey:@"geometry"];
//            NSDictionary *dictLocation = [dictGeometry objectForKey:@"location"];
            
            latitude = [[[[[[dict objectForKey:@"results"] objectAtIndex:0] objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"] doubleValue];
            longitude = [[[[[[dict objectForKey:@"results"] objectAtIndex:0] objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"] doubleValue];
            
            strPlaceId = [[[dict objectForKey:@"results"] objectAtIndex:0] objectForKey:@"place_id"];
            
            NSLog(@"LATITUDE::::::%f\nLONGITUDE:::::%f,\n PLACE ID:::...%@", latitude, longitude, strPlaceId);
            
        } else {
            [[[UIAlertView alloc]initWithTitle:@"Warnning!!!" message:[dict objectForKey:@"status"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    } else {
        NSLog(@"Error: %@", error);
    }
    
    [self embedMapWithPlaceId:strPlaceId];
    
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    NSLog(@"View Controller get Location Logitute : %f",center.latitude);
    NSLog(@"View Controller get Location Latitute : %f",center.longitude);
    
    mapView.layer.borderColor = [[UIColor grayColor] CGColor];
    mapView.layer.borderWidth = 1.0;
    
    marker = [[GMSMarker alloc]init];
    marker.position = CLLocationCoordinate2DMake(latitude,longitude);
    marker.map = mapView;
    //[mapView animateToZoom:0.05];
    marker.title = @"test";
    
    target = CLLocationCoordinate2DMake(latitude, longitude);
    [mapView animateToLocation:target];
    [mapView animateToZoom:5.0];
    
    return center;
}

#pragma mark - Web Service
-(void)connectionForGetProjectDetails {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_PROJECT_DETAILS];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"project_id": strProjectId};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE FROM MY PROJECT DETAILS:::...%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] boolValue]) {
            NSDictionary *dictProjectDetails = [[responseObject objectForKey:@"info_array"] objectAtIndex:0];
            
//            NSLog(@"LABEL HEIGHT BEFORE TEXT:::...%f", self->lblProjectDetails.frame.size.height);
            
            self->lblAddress.text = [dictProjectDetails objectForKey:@"address"];
            self->lblPostDate.text = [dictProjectDetails objectForKey:@"post_date"];
            self->lblPostedBy.text = [dictProjectDetails objectForKey:@"posted_by"];
            self->lblProjectName.text = [dictProjectDetails objectForKey:@"project_name"];
            self->lblProperty.text = [dictProjectDetails objectForKey:@"property"];
            self->lblService.text = [dictProjectDetails objectForKey:@"service"];
            self->lblWorkType.text = [dictProjectDetails objectForKey:@"type_of_work"];
            self->lblProjectDetails.text = [dictProjectDetails objectForKey:@"job_details"];
            self->iLeadsRemaining = [[dictProjectDetails objectForKey:@"no_of_free_leads"] intValue];
            self->iPhoneStatus = [[dictProjectDetails objectForKey:@"phone_status"] intValue];
            self->strPhoneNumber = [dictProjectDetails objectForKey:@"phone"];
            self->iFreeDealStatus = [[dictProjectDetails objectForKey:@"freedeal_status"] intValue];
            self->iPremiumStatus = [[dictProjectDetails objectForKey:@"premium_status"] intValue];
            self->strAppliedJobStatus = [dictProjectDetails objectForKey:@"applied_jobstatus"];
            self->iResponseExists = [[dictProjectDetails objectForKey:@"response_exists"] intValue];
            
            if (self->iPhoneStatus == 0) {
                [self->btnClickToView setTitle:@"Private Number" forState:UIControlStateNormal];
                [self->btnClickToView setUserInteractionEnabled:NO];
                [self->btnClickToView setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
            } else {
                [self->btnClickToView setTitle:@"Click to view" forState:UIControlStateNormal];
                [self->btnClickToView setUserInteractionEnabled:YES];
                [self->btnClickToView setTitleColor:COLOR_THEME forState:UIControlStateNormal];
            }
            
            int iButtonType = [[dictProjectDetails objectForKey:@"button_type"] intValue];
            
            if (iButtonType == 0) {
                [self->btnResponseMain setTitle:@"RESPOND NOW" forState:UIControlStateNormal];
                [self->btnResponseMain setBackgroundColor:COLOR_THEME];
                self->btnResponseMain.userInteractionEnabled = YES;
                self->constBtnMainRespHeight.constant = 40.0;
            } else if (iButtonType == 1) {
                [self->btnResponseMain setTitle:@"AWAITING RESPONSE" forState:UIControlStateNormal];
                [self->btnResponseMain setBackgroundColor:[UIColor lightGrayColor]];
                self->btnResponseMain.userInteractionEnabled = NO;
                self->constBtnMainRespHeight.constant = 40.0;
            } else if (iButtonType == 2) {
                [self->btnResponseMain setTitle:@"CHAT" forState:UIControlStateNormal];
                [self->btnResponseMain setBackgroundColor:COLOR_THEME];
                self->btnResponseMain.userInteractionEnabled = YES;
                self->constBtnMainRespHeight.constant = 40.0;
            } else {
                [self->btnResponseMain setTitle:self->strResponseButtonTitle forState:UIControlStateNormal];
                [self->btnResponseMain setBackgroundColor:COLOR_THEME];
                self->btnResponseMain.userInteractionEnabled = YES;
                self->constBtnMainRespHeight.constant = 40.0;
                
                if ([self->strResponseButtonTitle isEqualToString:@"AWAITING RESPONSE"]) {
                    [self->btnResponseMain setBackgroundColor:[UIColor lightGrayColor]];
                    self->btnResponseMain.userInteractionEnabled = NO;
                } else {
                    [self->btnResponseMain setBackgroundColor:COLOR_THEME];
                    self->btnResponseMain.userInteractionEnabled = YES;
                }
            }
            
//            self->strZipCode = [dictProjectDetails objectForKey:@"zip"];
            
            if ([[dictProjectDetails objectForKey:@"add_favouite"] intValue] == 1) {
                self->isFavourite = YES;
            } else {
                self->isFavourite = NO;
            }
            [self favouriteButtonImageSettings];
            
//            [self imageLoadFromURL:[dictProjectDetails objectForKey:@"project_image"] withContainer:self->imgProject];
            [self->imgProject sd_setImageWithURL:[dictProjectDetails objectForKey:@"project_image"]];
            
            CGRect frame = self->viewHeader.frame;
            frame.size.height = self->lblProjectDetails.frame.origin.y + self->lblProjectDetails.frame.size.height + 20;
            self->viewHeader.frame = frame;
            
            [self->tblProjectDetails reloadData];

            NSLog(@"LABEL HEIGHT AFTER TEXT:::...%f", self->lblProjectDetails.frame.size.height);
            
//            [self connectionForGetLatAndLonUsing:[dictProjectDetails objectForKey:@"zip"]];
            
            [self getLocationFromAddressString:[dictProjectDetails objectForKey:@"zip"]];
        }
        
//        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForRespond {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_RESPOND];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"project_owner_id": strProjectOwnerId,
                             @"project_id": strProjectId,
                             @"job_response": txtRespondMsg.text};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE FROM MY PROJECT:::...%@", responseObject);
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
//            if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
//                if (self.interstitial.isReady) {
//                    [self.interstitial presentFromRootViewController:self];
//                }
//            }
            
            if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
                if (self.arrInterstitialAd.count > 0) {
                    for (int i = 0; i < self.arrInterstitialAd.count; i++) {
                        NSDictionary *dict = [self.arrInterstitialAd objectAtIndex:i];
                        if ([[dict objectForKey:@"title"] isEqualToString:@"First response to project"]) {
                            if ([[dict objectForKey:@"ios_interstitial_ads"] intValue] == 1) {
                                if (self.interstitial.isReady) {
                                    [self.interstitial presentFromRootViewController:self];
                                }
                            }
                        }
                    }
                }
            }
            
            [self connectionForGetProjectDetails];
            [self tapToDismissView];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        [alertController.view setTintColor:COLOR_THEME];
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForDeleteOrAddFromWatchlist {
    
    [YXSpritesLoadingView show];
    
    if (isFavourite) {
        isFavourite = NO;
    } else {
        isFavourite = YES;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_ADD_DELETE_WATCHLIST];
    
    int iProjectFunction = (isFavourite ? 1 : 0);
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                            @"project_id": strProjectId,
                            @"project_function": [NSString stringWithFormat:@"%d", iProjectFunction]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE FROM MY PROJECT:::...%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {

//            if (self->isFavourite) {
//                [self favouriteButtonImageSettings];
//            } else {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self favouriteButtonImageSettings];
                }];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
                [alertController.view setTintColor:COLOR_THEME];
//            }
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                [self tapToDismissView];
            }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForViewPhoneNumber {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_VIEW_PHONE_NUMBER];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"project_id": strProjectId};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE FROM MY PROJECT:::...%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
//
//            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                [self.navigationController popViewControllerAnimated:YES];
//                [self favouriteButtonImageSettings];
            [self tapToDismissView];
            [self->btnClickToView setTitle:self->strPhoneNumber forState:UIControlStateNormal];
            [self->btnClickToView setUserInteractionEnabled:NO];
            [self->btnClickToView setTitleColor:COLOR_TEXT forState:UIControlStateNormal];
//            }];
//            [alertController addAction:okAction];
//            [self presentViewController:alertController animated:YES completion:nil];
//            [alertController.view setTintColor:COLOR_THEME];
        } else {
//            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                [self tapToDismissView];
//            }];
//            [alertController addAction:okAction];
//            [self presentViewController:alertController animated:YES completion:nil];
        }
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForGetLatAndLonUsing:(NSString *)zipcode {
    /*
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@",zipcode]]];
    
    [request setHTTPMethod:@"POST"];
    
    NSError *err;
    NSURLResponse *response;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    NSString *resSrt = [[NSString alloc]initWithData:responseData encoding:NSASCIIStringEncoding];
    
    NSLog(@"got response==%@", resSrt);
    
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    
    NSString *lataddr=[[[[[dict objectForKey:@"results"] objectAtIndex:0]objectForKey:@"geometry"]objectForKey:@"location"]objectForKey:@"lat"];
    
    NSString *longaddr=[[[[[dict objectForKey:@"results"] objectAtIndex:0]objectForKey:@"geometry"]objectForKey:@"location"]objectForKey:@"lng"];
    
    
    NSLog(@"The resof latitude=%@", lataddr);
    
    NSLog(@"The resof longitude=%@", longaddr);
     */
    NSLog(@"ZIPCODE:::...\n%@", zipcode);
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:zipcode completionHandler:^(NSArray *placemarks, NSError *error) {
         if(!error) {
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSLog(@"%f",placemark.location.coordinate.latitude);
             NSLog(@"%f",placemark.location.coordinate.longitude);
             NSLog(@"%@",[NSString stringWithFormat:@"%@",[placemark description]]);
         } else {
             NSLog(@"There was a forward geocoding error\n%@",[error localizedDescription]);
         }
     }];
}

#pragma mark - UITextView Delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@""]) {
        
        NSLog(@"%@",textView.text);
        
        if (textView.text.length==1) {
             lblPlaceHolder.hidden = NO;
        }
    }
    
   else if ([NSString stringWithFormat:@"%@%@",textView.text,text].length>0) {
     
        lblPlaceHolder.hidden = YES;
    }
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    // lblPlaceholder.hidden = YES;
    textView.layer.borderColor = COLOR_THEME.CGColor;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    textView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    if ([textView.text isEqualToString:@""]) {
        lblPlaceHolder.hidden = NO;
    } else {
        lblPlaceHolder.hidden = YES;
    }
}

#pragma mark - UIButton Action
- (IBAction)FavoutiteButtonPressed:(id)sender {
    
    if (isFavourite) {
//        strMsg = @"Are you sure you want to remove from watchlist";
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to remove from watchlist" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self connectionForDeleteOrAddFromWatchlist];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        }];
        
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        [alertController.view setTintColor:COLOR_THEME];
    } else {
//        strMsg = @"Are you sure you want to add to watchlist";
        [self connectionForDeleteOrAddFromWatchlist];
    }
}

- (IBAction)RespondNow:(id)sender {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
//    int iPremiumStatus = [[userDefault objectForKey:@"pro_premium_status"] intValue];
    
    NSString *strBtnTitle = [NSString stringWithFormat:@"%@", [[sender titleLabel] text]];
    
    if ([strBtnTitle isEqualToString:@"CHAT"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
        MessageDetailsVC *mdvc = [storyBoard instantiateViewControllerWithIdentifier:@"MessageDetailsVC"];
        mdvc.strProjectId = strProjectId;
        mdvc.isFromProject = YES;
        [self.navigationController pushViewController:mdvc animated:YES];
    } else if ([strBtnTitle isEqualToString:@"RESPOND NOW"]) {
        if (self.iPremiumStatus == 2) {
            viewRespond.hidden = NO;
            viewShadow.hidden = NO;
            [txtRespondMsg becomeFirstResponder];
            [viewShadow addGestureRecognizer:tapGesture];
        } else {
            if (isLeadApplied) {
                [viewShadow addGestureRecognizer:tapGesture];
                viewRespond.hidden = NO;
                viewShadow.hidden = NO;
                [txtRespondMsg becomeFirstResponder];
                [viewShadow addGestureRecognizer:tapGesture];
            } else {
                if (iFreeDealStatus == 1 || iFreeDealStatus == 0) {
                    if (iLeadsRemaining > 0) {
                        lblRemainingResponse.text = [NSString stringWithFormat:@"%d", iLeadsRemaining];
//                        iLeadsRemaining = iLeadsRemaining -1;
                        viewShadow.hidden = NO;
                        viewLeadsRemaining.hidden = NO;
                        isResponse = YES;
                        [btnRespondNow setTitle:@"CONFIRM NOW" forState:UIControlStateNormal];
                        [viewShadow addGestureRecognizer:tapGesture];
                    } else {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"SORRY!!!" message:@"You don't have any free leads / responses remaining" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                            [self tapToDismissView];
                        }];
                        [alertController addAction:okAction];
                        [self presentViewController:alertController animated:YES completion:nil];
                        [alertController.view setTintColor:COLOR_THEME];
                    }
                } else if (iFreeDealStatus == 2) {
                    
                    viewRespond.hidden = NO;
                    viewShadow.hidden = NO;
                    [txtRespondMsg becomeFirstResponder];
                    [viewShadow addGestureRecognizer:tapGesture];
                }
            }
        }
    }
}

- (IBAction)Send:(id)sender {
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtRespondMsg.text]) {
        [[[UIAlertView alloc]initWithTitle:@"" message:@"Please type your message or select image" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    } else {
        
        [arrRestrictedWordsFound removeAllObjects];
        NSArray *arrNewWords = [txtRespondMsg.text componentsSeparatedByString:@" "];
        NSLog(@"WORDS ARRAY FROM TEXTVIEW:::...%@", arrNewWords);
        
        if (arrRestrictedWordsFound.count > 0) {
            NSString *strKhisti = [arrRestrictedWordsFound componentsJoinedByString:@", "];
            [[[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"Invalid term: %@ - Please be respectful of others. Remove and try again", strKhisti] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            [txtRespondMsg becomeFirstResponder];
        } else if ([[AppManager sharedDataAccess] detectType:txtRespondMsg.text]) {
            [[[UIAlertView alloc]initWithTitle:@"" message:@"Email addresses, phone numbers and links are not permitted. You will be able to share contact information later if you choose." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            [txtRespondMsg becomeFirstResponder];
        } else {
            if ([strNewLeadStatus isEqualToString:@"D"]) {
                if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
                    if (self.arrInterstitialAd.count > 0) {
                        for (int i = 0; i < self.arrInterstitialAd.count; i++) {
                            NSDictionary *dict = [self.arrInterstitialAd objectAtIndex:i];
                            if ([[dict objectForKey:@"title"] isEqualToString:@"First response to project"]) {
                                if ([[dict objectForKey:@"ios_interstitial_ads"] intValue] == 1) {
                                    if (self.interstitial.isReady) {
                                        [self.interstitial presentFromRootViewController:self];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            [self connectionForRespond];
        }
    }
    
    /*
    
//    ;
    if(([[txtRespondMsg.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0)) {
        //string is all whitespace or newline
        
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warnning" message:@"Please enter some message to response" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//            ;
            [self->txtRespondMsg becomeFirstResponder];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        [alertController.view setTintColor:COLOR_THEME];
    }
     */
}

- (IBAction)ClickToViewPhoneNumber:(id)sender {
    
    lblRemainingResponse.text = [NSString stringWithFormat:@"%d", iLeadsRemaining];
    
    if (self.iPremiumStatus == 2) {
        if (iPhoneStatus > 0) {
            [self connectionForViewPhoneNumber];
        }
    } else {
        if (iPhoneStatus > 0) {
            if (isLeadApplied) {
                [self connectionForViewPhoneNumber];
            } else {
                if (iFreeDealStatus == 1 || iFreeDealStatus == 0) {
                    if (iLeadsRemaining > 0) {
//                        iLeadsRemaining = iLeadsRemaining -1;
                        viewShadow.hidden = NO;
                        viewLeadsRemaining.hidden = NO;
                        isResponse = YES;
                        [btnRespondNow setTitle:@"CONFIRM NOW" forState:UIControlStateNormal];
//                        lblRemainingResponse.text = [NSString stringWithFormat:@"%d", iLeadsRemaining];
                        [viewShadow addGestureRecognizer:tapGesture];
                    } else {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"SORRY!!!" message:@"You don't have any free leads / responses remaining" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                            [self tapToDismissView];
                        }];
                        [alertController addAction:okAction];
                        [self presentViewController:alertController animated:YES completion:nil];
                        [alertController.view setTintColor:COLOR_THEME];
                    }
                } else if (iFreeDealStatus == 2) {
                    
                    viewRespond.hidden = NO;
                    viewShadow.hidden = NO;
                    [txtRespondMsg becomeFirstResponder];
                    [viewShadow addGestureRecognizer:tapGesture];
                }
            }
        }
    }
}

- (IBAction)CloseLeadsReamining:(id)sender {
    
    [self tapToDismissView];
}

- (IBAction)ResponseNowFromLeadsRemainingView:(id)sender {
    
    isLeadApplied = YES;
    
    iLeadsRemaining = iLeadsRemaining -1;
    
//    if (self.iPaymentOption > 0) {
        if (isResponse) {
            viewLeadsRemaining.hidden = YES;
//            viewLeadsRemaining.layer.zPosition = -1;
            viewRespond.hidden = NO;
            viewShadow.hidden = NO;
            [txtRespondMsg becomeFirstResponder];
            [viewShadow addGestureRecognizer:tapGesture];
        } else {
            [self connectionForViewPhoneNumber];
        }
//    } else {
//        viewLeadsRemaining.hidden = YES;
//        viewRespond.hidden = NO;
//        viewShadow.hidden = NO;
//        [txtRespondMsg becomeFirstResponder];
//        [viewShadow addGestureRecognizer:tapGesture];
//    }
}

- (IBAction)GoPremium:(id)sender {
    
    [self goForPremium];
}

-(void) goForPremium {

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    PremiumVC *pvc = [storyboard instantiateViewControllerWithIdentifier:@"PremiumVC"];
    [self.navigationController pushViewController:pvc animated:YES];
}
@end
