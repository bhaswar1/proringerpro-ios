//
//  WatchListVC.h
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WatchListVC : ProRingerProBaseVC <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblWatchList;

@property (weak, nonatomic) IBOutlet UIView *viewNoData;
@property (weak, nonatomic) IBOutlet UIView *viewNoWatchList;
@property (weak, nonatomic) IBOutlet UIView *viewNoSearchList;


@property (weak, nonatomic) IBOutlet FRHyperLabel *lblNoWatchList;
@property (weak, nonatomic) IBOutlet FRHyperLabel *lblNoSearchList;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

- (IBAction)FindNewProject:(id)sender;

@end

NS_ASSUME_NONNULL_END
