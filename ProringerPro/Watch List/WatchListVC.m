//
//  WatchListVC.m
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "WatchListVC.h"

@interface WatchListVC ()
{
    NSMutableArray *arrWatchList;
}

@end

@implementation WatchListVC
@synthesize tblWatchList, lblNoWatchList, searchBar, viewNoData, viewNoWatchList, viewNoSearchList, lblNoSearchList;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
//    [self.view addGestureRecognizer:self.panGesture];
    
//    self.view.backgroundColor = COLOR_TABLE_HEADER;
    
    self.viewFooterTab.lblNewProject.backgroundColor = self.viewFooterTab.lblNewMessage.backgroundColor = COLOR_THEME;
    self.viewFooterTab.lblNewMessage.layer.borderColor = self.viewFooterTab.lblNewProject.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewFooterTab.lblNewProject.layer.borderWidth = self.viewFooterTab.lblNewMessage.layer.borderWidth = 0.5;
    
    [self.view addSubview:self.viewFooterTab];
    
    arrWatchList = [[NSMutableArray alloc] init];
    
    tblWatchList.dataSource = self;
    tblWatchList.delegate = self;
    
    searchBar.delegate = self;
    
    viewNoSearchList.hidden = viewNoWatchList.hidden = viewNoData.hidden = lblNoSearchList.hidden = lblNoWatchList.hidden = YES;
    
    [tblWatchList registerNib:[UINib nibWithNibName:@"WatchListTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellWatchList"];
    
    [self connectionForGetWatchList:@""];
    
    NSInteger iTag = [[[NSUserDefaults standardUserDefaults] objectForKey:@"footer"] integerValue];
    [self footerButtonDesignWithTag:iTag];
    [self footerButtonTap:4];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self connectionForGetDashboardData];
    
    self.tabBarController.tabBar.hidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    
    [self connectionForGetWatchList:@""];
    
    [self footerButtonTap:4];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
//    self.tabBarController.tabBar.hidden = YES;
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if (self.iTotalNewMsg > 0) {
        self.viewFooterTab.lblNewMessage.hidden = NO;
    } else {
        self.viewFooterTab.lblNewMessage.hidden = YES;
    }
    
    if (self.iNewLeads > 0) {
        self.viewFooterTab.lblNewProject.hidden = NO;
    } else {
        self.viewFooterTab.lblNewProject.hidden = YES;
    }
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Watchlist"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Watchlist"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
}

-(void)configureNoDataViewWith:(NSString *)strLeads {
    
    NSDictionary *attributesLeads = @{NSForegroundColorAttributeName: [UIColor darkGrayColor],NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]};
    lblNoWatchList.attributedText = [[NSAttributedString alloc]initWithString:strLeads attributes:attributesLeads];
    
    void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
        if ([substring isEqualToString:@"Find new project now"]) {
            [self rightBarButtonPressed:nil];
        }
    };
    [lblNoWatchList setLinksForSubstrings:@[@"Find new project now"] withLinkHandler:handler];
}

-(void)rightBarButtonPressed:(id)sender {
    
    NSLog(@"Right button pressed in base class");
//    ;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchProjectVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SearchProjectVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

#pragma mark - SearchBar Delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [self connectionForGetWatchList:searchBar.text];
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    [self connectionForGetWatchList:searchBar.text];
}

#pragma mark - UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrWatchList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dictWatchlist = [arrWatchList objectAtIndex:indexPath.row];
    
    WatchListTableViewCell *cell = (WatchListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellWatchList"];
    
    [cell.btnFavourite addTarget:self action:@selector(favouriteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnFavourite.tag = indexPath.row;
    
    [cell configureCellWith:dictWatchlist];
    
    return cell;
}

-(void)favouriteButtonPressed:(UIButton *)sender {
    
    NSDictionary *dict = [arrWatchList objectAtIndex:sender.tag];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to remove from watchlist" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self connectionForDeleteOrAddFromWatchlist:[dict objectForKey:@"id"]];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    [alertController.view setTintColor:COLOR_THEME];
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 120.0;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    
//    return searchBar;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    
//    return 56.0;
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dictProjectDetails = [arrWatchList objectAtIndex:indexPath.row];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Watchlist" bundle:nil];
    ProjectDetailsVC *pdvc = [storyboard instantiateViewControllerWithIdentifier:@"ProjectDetailsVC"];
    pdvc.strProjectOwnerId = [dictProjectDetails objectForKey:@"homeowner_id"];
    pdvc.strProjectId = [dictProjectDetails objectForKey:@"id"];
    [self.navigationController pushViewController:pdvc animated:YES];
}

#pragma mark - Web Service
-(void)connectionForGetWatchList:(NSString *)searchText {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_WATCHLIST];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"search_field": searchText};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE FROM MY PROJECT:::...%@", responseObject);
        NSString *strLeads = @"";
        [self->arrWatchList removeAllObjects];
        [self->arrWatchList addObjectsFromArray:[responseObject objectForKey:@"info_array"]];
        
        if (self->arrWatchList.count > 0) {
            self->viewNoSearchList.hidden = self->viewNoWatchList.hidden = self->viewNoData.hidden = self->lblNoSearchList.hidden = self->lblNoWatchList.hidden = YES;
        } else {
            self->viewNoData.hidden = NO;
            if (searchText.length == 0) {
                self->viewNoSearchList.hidden = self->viewNoWatchList.hidden = self->viewNoData.hidden = self->lblNoSearchList.hidden = self->lblNoWatchList.hidden = NO;
//                strLeads = @"No projects in watchlist - Find new project now";
                self->lblNoSearchList.text = @"";
            } else {
//                self->lblNoSearchList.text = [NSString stringWithFormat:@"No projects in watchlist under - %@", searchText];
                self->viewNoSearchList.hidden = self->viewNoWatchList.hidden = self->viewNoData.hidden = self->lblNoSearchList.hidden = self->lblNoWatchList.hidden = NO;
//                strLeads = @"";
            }
            strLeads = @"No projects in watchlist - Find new project now";
        }
        
        [self configureNoDataViewWith:strLeads];
        [self->tblWatchList reloadData];
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForDeleteOrAddFromWatchlist:(NSString *)strProjectId {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_ADD_DELETE_WATCHLIST];
    
    int iProjectFunction = 0;
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"project_id": strProjectId,
                             @"project_function": [NSString stringWithFormat:@"%d", iProjectFunction]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE FROM MY PROJECT:::...%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                ;
//                [self.navigationController popViewControllerAnimated:YES];
                [self connectionForGetWatchList:@""];
            }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        } else {
//            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                [self tapToDismissView];
//            }];
//            [alertController addAction:okAction];
//            [self presentViewController:alertController animated:YES completion:nil];
        }
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)FindNewProject:(id)sender {
}
@end
