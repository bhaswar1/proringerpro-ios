//
//  ADTextField.m
//  ProringerPro
//
//  Created by Soma Halder on 22/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ADTextField.h"

@implementation ADTextField

-(id)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super initWithCoder:aDecoder]) {
        self.delegate = self;
        
        self.clipsToBounds = YES;
    }
    
//    [self paddingForTextField:ADTextField];
//    [self shadowForTextField:ADTextField];
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark - UITextField Design
-(void) paddingForTextField:(UITextField *)textField {

    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(30, 0, 10, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;

    textField.layer.masksToBounds = NO;
    textField.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    textField.layer.shadowRadius = 1;
    textField.layer.shadowOpacity = 0.2;
    textField.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    
    textField.layer.borderWidth = 1.0;
    textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)shadowForTextField:(UITextField *)textfield {
    
    UIColor *color = [UIColor blackColor];
    textfield.layer.shadowColor = [color CGColor];
    textfield.layer.shadowRadius = 1.5f;
    textfield.layer.shadowOpacity = 0.2;
    textfield.layer.shadowOffset = CGSizeZero;
    textfield.layer.masksToBounds = NO;
}

@end
