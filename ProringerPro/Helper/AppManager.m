//
//  AppManager.m
//  TablePouncer
//
//  Created by Kazma Technology on 12/04/18.
//  Copyright © 2018 Kazma Technology. All rights reserved.
//

#import "AppManager.h"
#import "AppDelegate.h"
#import <objc/runtime.h>


//#import "IEURLConnection.h"

@interface AppManager ()
{
    //UIAlertView *alertview;
    int iRateStatus;
    
}
@end

@implementation AppManager

@synthesize isMenuOppenedFromLoginScreen = _isMenuOppenedFromLoginScreen;
@synthesize isSetting = _isSetting;
@synthesize isMenuSelected = _isMenuSelected;
@synthesize selectedEnterprise =_selectedEnterprise;
@synthesize imgLogo = _imgLogo;
@synthesize userInfo = _userInfo;
@synthesize restaurantInfo = _restaurantInfo;
@synthesize arrUserMenuList = _arrUserMenuList;
@synthesize arrInviteeList = _arrInviteeList;
@synthesize strDeviceToken = _strDeviceToken;
@synthesize strUUID = _strUUID;
@synthesize strCurrentUUID = _strCurrentUUID;
@synthesize selectedRestaurantId = _selectedRestaurantId;
@synthesize isFromPushNotification = _isFromPushNotification;
@synthesize isPickUpOptionMe = _isPickUpOptionMe;
@synthesize isUserCreateOrder = _isUserCreateOrder;
//@synthesize viewRightMenu = _viewRightMenu;
@synthesize latitudeLast = _latitudeLast;
@synthesize longitudeLast = _longitudeLast;

@synthesize strPaymentUUID = _strPaymentUUID;
@synthesize menuType = _menuType;

@synthesize iEditMenuIndex = _iEditMenuIndex;
@synthesize arrInvitees = _arrInvitees;
@synthesize senderId = _senderId, senderName = _senderName, pickupVenmoAccount = _pickupVenmoAccount, picupId = _picupId, picupName = _picupName, lastOrderAmount = _lastOrderAmount;

@synthesize isInvitationActive = _isInvitationActive;
@synthesize appNav = _appNav;

@synthesize resault;

static AppManager *sharedInstance_ = nil;


+ (AppManager *) sharedDataAccess{
    @synchronized(self){
		if (sharedInstance_ == nil) {
			sharedInstance_ = [[self alloc] init];
            [sharedInstance_ initalization];
            return sharedInstance_;
		}
        return sharedInstance_;
	}
}

//Web Service
-(void) connectionForWebServiceForGETMethodWithParameters:(NSDictionary *)params andUrl:(NSString *)url {
    
    [YXSpritesLoadingView show];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(id) connectionForWebServiceForPOSTMethodWithParameters:(NSDictionary *)params andUrl:(NSString *)url {
    
    [YXSpritesLoadingView show];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        self->resault = responseObject;
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
    return resault;
}

-(void)initalization{
    
    /*
    [self connectionAuth:^(BOOL finished) {
        if(finished){
            NSLog(@"success");
        }
    }];
    */
    self.pickupVenmoAccount = @"";
    self.lastOrderAmount = 0;
    self.selectedRestaurantId = @"0";
    self.strDeviceToken = @"";
    self.strUUID = @"";
    self.arrUserMenuList = [NSMutableArray array];
    self.arrInviteeList = [NSMutableArray array];
    [AppManager sharedDataAccess].userInfo = [NSMutableDictionary dictionary];
    self.iEditMenuIndex = 0;
    /*
    self.userId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] intValue];
    self.firstName = [[NSUserDefaults standardUserDefaults] objectForKey:@"first_name"];
    NSLog(@"Name : %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"first_name"]);
    self.lastName = [[NSUserDefaults standardUserDefaults] objectForKey:@"last_name"];
    self.email = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
     */
}

-(void)clearInstance{
    /*
    self.userId = 0;
    self.firstName = @"";
    self.lastName = @"";
    self.email = @"";
    */


    // TO DO
    //self.selectedRestaurantId = 0;
    //self.restaurantInfo = nil;
    //[self.arrUserMenuList removeAllObjects];
    
    self.arrInvitees = nil;
    self.lastOrderAmount = 0;
    self.selectedRestaurantId = @"0";
    self.strDeviceToken = @"";
    self.strUUID = @"";
    self.pickupVenmoAccount = @"";
    [self.arrUserMenuList removeAllObjects];
    [self.arrInviteeList removeAllObjects];
    self.isFromPushNotification = NO;
    self.isUserCreateOrder = NO;
    
    
}

-(NSAttributedString*)getStrikeString:(NSString*)str {
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:str];
    [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@1 range:NSMakeRange(0, [attributeString length])];
    
    return attributeString;
}

+ (NSString*)getTodayTomorrowFormat:(NSString*)day{
    
    NSDate *dtBooking = [NSDate date];
    
    if([day isEqualToString:@"Tomorrow"])
        dtBooking = [[NSDate date] dateByAddingTimeInterval:60*60*24*1];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM"];
    day = [formatter stringFromDate:dtBooking];
    
    return day;
}

#pragma mark - File handeling

+ (Boolean)writeFile:(NSString*)filePath Content:(NSString*)fileContent{
    
    @autoreleasepool {
        NSData *data = [fileContent dataUsingEncoding:NSUTF8StringEncoding];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:filePath] == NO){
            [[NSFileManager defaultManager] createFileAtPath:filePath contents:data attributes:nil];
        }
        else{
            if ([[NSFileManager defaultManager] isWritableFileAtPath:filePath]) {
                NSFileHandle *file = [NSFileHandle fileHandleForUpdatingAtPath:filePath];
                [file writeData:data];
                [file closeFile];
            }
        }
    }
    
    return true;
}

+ (Boolean)truncateFile:(NSString*)filePath{
    @autoreleasepool {
        if([[NSFileManager defaultManager] fileExistsAtPath:filePath] == YES){
            if ([[NSFileManager defaultManager] isWritableFileAtPath:filePath]) {
                NSFileHandle *file = [NSFileHandle fileHandleForUpdatingAtPath:filePath];
                [file truncateFileAtOffset:0];
                [file closeFile];
            }
        }
    }
    
    return true;
}

+ (NSString*)readFile:(NSString*)filePath{
    
    NSString *fileContent;
    @autoreleasepool {
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath] ==YES) {
            if ([[NSFileManager defaultManager] isReadableFileAtPath:filePath]) {
                NSData *data = [[NSFileManager defaultManager] contentsAtPath:filePath];
                fileContent = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            }
        }
    }
    return fileContent;
}

+ (NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}

-(id)getDataFromHex:(NSString*)hex
{
    {
        char buf[3];
        buf[2] = '\0';
        NSAssert(0 == [hex length] % 2, @"Hex strings should have an even number of digits (%@)", hex);
        unsigned char *bytes = malloc([hex length]/2);
        unsigned char *bp = bytes;
        for (CFIndex i = 0; i < [hex length]; i += 2) {
            buf[0] = [hex characterAtIndex:i];
            buf[1] = [hex characterAtIndex:i+1];
            char *b2 = NULL;
            *bp++ = strtol(buf, &b2, 16);
            NSAssert(b2 == buf + 2, @"String should be all hex digits: %@ (bad digit around %ld)", hex, i);
        }
        
        return [NSData dataWithBytesNoCopy:bytes length:[hex length]/2 freeWhenDone:YES];
    }
}

//- (Boolean)isValidPhoneNumber:(NSString *)phoneNumber{
//    if ([phoneNumber isEqualToString:@""])
//        return false;
//
//    bool result = false;
//
//    if ([phoneNumber length] >= 10) {
//
//        NSString *mobileNumberPattern = @"[789][0-9]{9}";
//        NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
//
//        result = [mobileNumberPred evaluateWithObject:phoneNumber];
//    }
//
//    return result;
//}

- (Boolean)isValidPhoneNumber:(NSString *)phoneNumber{
    
    if ([phoneNumber length] >= 10)
    {
//        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
//        NSString *filtered = [[phoneNumber componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
//        Boolean result = [phoneNumber isEqualToString:filtered];
        BOOL result = true;
        return result;
    }
    
    return false;
}

- (Boolean)validateEmailWithString:(NSString*)email{
    if ([email isEqualToString:@""])
        return false;
//    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
//    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
//    return [emailTest evaluateWithObject:email];
    
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
    
}
/*
- (Boolean)validateEmailWithString:(NSString*)email{
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    BOOL res =  [emailTest evaluateWithObject:email];
    return res;
}
*/
- (Boolean)isEmptyString:(NSString*) textToCheck{
    BOOL blnReruntValue = false;
    
    textToCheck = [textToCheck stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([textToCheck length] <= 0) {
        return true;
    }
    
    return blnReruntValue;
}

-(BOOL)detectType:(NSString*)stringToUse {
    __block BOOL isFound = false;
    NSError *error;
    NSDataDetector *detectorLink = [NSDataDetector dataDetectorWithTypes:(NSTextCheckingTypeLink | NSTextCheckingTypePhoneNumber) error:&error];
    
    if (!error && detectorLink) {
        [detectorLink enumerateMatchesInString:stringToUse options:0 range:NSMakeRange(0, stringToUse.length) usingBlock:^(NSTextCheckingResult * __nullable result, NSMatchingFlags flags, BOOL *stop){
            
            NSString *type;
            NSString *matched = [stringToUse substringWithRange:result.range];
            
            // Type checking can be eliminated since it is used for logging only.
            if (result.resultType == NSTextCheckingTypeLink) {
                if ([matched rangeOfString:@"@"].length) {
                    type = @"Email";
                    isFound = YES;
                } else {
                    type = @"Link";
                    isFound = YES;
                }
            } else if (result.resultType == NSTextCheckingTypePhoneNumber) {
                type = @"Phone number";
                isFound = YES;
            } else {
                type = @"Unknown";
                isFound = NO;
            }
            NSLog(@"Matched %@: %@",type,matched);
        }];
    } else {
        NSLog(@"%@",error.description);
    }
    return isFound;
}

- (void)loadImage2:(NSString *)strURL withImageView:(UIImageView *)imageView{
    // Start the activity indicator before moving off the main thread
    // Start the activity indicator before moving off the main thread
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activityIndicator.center = imageView.center;//CGRectMake(30, 30, 24, 24);
    [self.activityIndicator startAnimating];
    // Move off the main thread to start our blocking tasks.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        // Create the image URL from a known string.
        NSURL *imageURL = [NSURL URLWithString:strURL];
        
        NSError *downloadError = nil;
        if(imageURL)
        {
            // Create an NSData object from the contents of the given URL.
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL
                                                      options:kNilOptions
                                                        error:&downloadError];
            // ALWAYS utilize the error parameter!
            if (downloadError) {
                // Something went wrong downloading the image. Figure out what went wrong and handle the error.
                // Don't forget to return to the main thread if you plan on doing UI updates here as well.
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.activityIndicator stopAnimating];
                    imageView.image = [UIImage imageNamed:@"profile"];
                    NSLog(@"%@",[downloadError localizedDescription]);
                    [self.activityIndicator stopAnimating];
                });
            }
            else
            {
                
                // Successful or not we need to stop the activity indicator, so switch back the the main thread.
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Now that we're back on the main thread, you can make changes to the UI.
                    // This is where you might display the saved image in some image view, or
                    // stop the activity indicator.
                    
                    // Check if saving the file was successful, once again, utilizing the error parameter.
                    // if (writeWasSuccessful) {
                    // Get the saved image data from the file.
                    // NSData *imageData = [NSData dataWithContentsOfURL:saveLocation];
                    // Set the imageView's image to the image we just saved.
                    if(imageData)
                    {
                        UIImage *image = [UIImage imageWithData:imageData];
                        if(image)
                            imageView.image = image;
                        else{
                            imageView.image = [UIImage imageNamed:@"profile"];
                        }
                    }
                    else
                        imageView.image = [UIImage imageNamed:@"profile"];
                    //                } else {
                    //                    NSLog(@"%@",[saveError localizedDescription]);
                    //                    // Something went wrong saving the file. Figure out what went wrong and handle the error.
                    //                }
                    
                    [self.activityIndicator stopAnimating];
                });
            }
            
            
        }
        else
        {
            [self.activityIndicator stopAnimating];
            imageView.image = [UIImage imageNamed:@"profile"];
        }
    });
}
                   
-(void)loadImage:(NSString*)strURL withImageView:(UIImageView*)imgView
{
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityView.frame = CGRectMake(30, 30, 24, 24);
    [activityView startAnimating];
    [imgView addSubview:activityView];
    //activityView.center = imgView.center;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        NSURL* urlImage = [NSURL URLWithString:strURL];
        if(urlImage)
        {
            NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
            if(imageData)
            {
                UIImage* image = [[UIImage alloc] initWithData:imageData];
                if (image) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        imgView.image = image;
                        [activityView removeFromSuperview];
                    });
                }
            }
            else{
                imgView.image = [UIImage imageNamed:@"profile"];
                [activityView removeFromSuperview];
            }
        }
        else
        {
            imgView.image = [UIImage imageNamed:@"profile"];
            [activityView removeFromSuperview];
        }
    });
}
/*
- (void)setRatingForValue:(NSString*)value forImageViews:(NSArray *)imageViews
{
    float ratingValue = [value floatValue];
    UIImage* image = [UIImage imageNamed:@"starblue"];
    int noOfImages = [imageViews count];
    if(ratingValue >= 4.75)
    {
        for(int i=0;i<noOfImages;i++)
        {
            UIImageView* imageView = [imageViews objectAtIndex:i];
            imageView.image = image;
        }
        
    }
    else if(ratingValue >= 4.25)
    {
        for(int i=0;i<noOfImages-1;i++)
        {
            UIImageView* imageView = [imageViews objectAtIndex:i];
            imageView.image = image;
        }

        UIImageView* imageView5 = [imageViews objectAtIndex:noOfImages -1];
        imageView5.image = [UIImage imageNamed:@"starHalf"];
        
    }
    else if(ratingValue >= 3.75)
    {
        for(int i=0;i<noOfImages-1;i++)
        {
            UIImageView* imageView = [imageViews objectAtIndex:i];
            imageView.image = image;
        }

        
    }
    else if(ratingValue >= 3.25)
    {
        for(int i=0;i<noOfImages-2;i++)
        {
            UIImageView* imageView = [imageViews objectAtIndex:i];
            imageView.image = image;
        }

        UIImageView* imageView4 = [imageViews objectAtIndex:noOfImages-2];
        imageView4.image = [UIImage imageNamed:@"starHalf"];
    }
    else if(ratingValue >= 2.75)
    {
        for(int i=0;i<noOfImages-2;i++)
        {
            UIImageView* imageView = [imageViews objectAtIndex:i];
            imageView.image = image;
        }
    }
    else if(ratingValue >= 2.25)
    {
        for(int i=0;i<noOfImages-3;i++)
        {
            UIImageView* imageView = [imageViews objectAtIndex:i];
            imageView.image = image;
        }
        UIImageView* imageView3 = [imageViews objectAtIndex:noOfImages-3];
        imageView3.image = [UIImage imageNamed:@"starHalf"];;
    }
    else if(ratingValue >= 1.75)
    {
        for(int i=0;i<noOfImages-3;i++)
        {
            UIImageView* imageView = [imageViews objectAtIndex:i];
            imageView.image = image;
        }
    }
    else if(ratingValue >= 1.25)
    {
        UIImageView* imageView1 = [imageViews objectAtIndex:0];
        imageView1.image = image;
        UIImageView* imageView2 = [imageViews objectAtIndex:1];
        imageView2.image = [UIImage imageNamed:@"starHalf"];

    }
    else if(ratingValue >= 0.75)
    {
        UIImageView* imageView1 = [imageViews objectAtIndex:0];
        imageView1.image = image;
    }
    else if(ratingValue >= 0.25)
    {
        UIImageView* imageView1 = [imageViews objectAtIndex:0];
        imageView1.image = [UIImage imageNamed:@"starHalf"];
        
    }
    else //if(ratingValue == 0.0)
    {
        
    }
}
*/

#pragma validation Checking

-(void) checkPasswordValidation:(NSString *) password withConfirmedPassword:(NSString *) ConfirmedPassword{
    if([password  isEqualToString:ConfirmedPassword])
        return ;
    else
    {
        
        [self showAlertWithTitle:@"Alert" andMessage:@"Password Mismatch"];
    }
}

- (NSDate *) getJSONDate:(NSString *)strLongDateTime{
    NSString* header = @"/Date(";
    uint headerLength = (uint)[header length];
    
    NSString*  timestampString;
    //@"/Date(1459989880088+0530)"
    NSScanner* scanner = [[NSScanner alloc] initWithString:strLongDateTime];
    [scanner setScanLocation:headerLength];
    [scanner scanUpToString:@")" intoString:&timestampString];
    
    NSCharacterSet* timezoneDelimiter = [NSCharacterSet characterSetWithCharactersInString:@"+-"];
    NSRange rangeOfTimezoneSymbol = [timestampString rangeOfCharacterFromSet:timezoneDelimiter];
    
    
    if (rangeOfTimezoneSymbol.length!=0) {
        scanner = [[NSScanner alloc] initWithString:timestampString];
        
        NSRange rangeOfFirstNumber;
        rangeOfFirstNumber.location = 0;
        rangeOfFirstNumber.length = rangeOfTimezoneSymbol.location;
        
        NSRange rangeOfSecondNumber;
        rangeOfSecondNumber.location = rangeOfTimezoneSymbol.location + 1;
        rangeOfSecondNumber.length = [timestampString length] - rangeOfSecondNumber.location;
        
        NSString* firstNumberString = [timestampString substringWithRange:rangeOfFirstNumber];
        //NSString* secondNumberString = [timestampString substringWithRange:rangeOfSecondNumber];
        
        unsigned long long firstNumber = [firstNumberString longLongValue];
        //uint secondNumber = [secondNumberString intValue];
        
        NSTimeInterval interval = firstNumber/1000;
        
        return [NSDate dateWithTimeIntervalSince1970:interval];
    }
    
    unsigned long long firstNumber = [timestampString longLongValue];
    NSTimeInterval interval = firstNumber/1000;
    
    return [NSDate dateWithTimeIntervalSince1970:interval];
}

#pragma mark - Alert

// For ---> (Default Alert) TOBE Presented From Any Place

-(void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", comment: "")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    
    [alertController addAction:cancelAction];
    
    //   Finally we can present the alert view controller as with any other view controller:
    
    AppDelegate *appdelegate = (AppDelegate*)[UIApplication sharedApplication].delegate ;
    UIViewController *vc = [ appdelegate.window rootViewController];
    
    [vc presentViewController: alertController animated:YES completion:nil];
    
}

// For ---> (Default Alert) TOBE Presented From Any View Controller

-(void)showAlertWithTitle /*:(NSString *)title andMessage:(NSString *)message fromViewController:(UIViewController *)vc*/ {
    
    NSDate *today = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Rate our app" message:@"If you like our app, please take a moment to rate in app store!" preferredStyle:UIAlertControllerStyleAlert];
/*
## 0 = rate can be given,
## 1 = rate already given,
## 2 = later after 5 days,
## 3 = never
*/
    
    UIAlertAction *nowAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Rate Now", comment: "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action) {
                                       
                                       NSLog(@"Yes action");
                                       
                                       self->iRateStatus = 1;
                                       
                                       [self connectionForRateStatus];
                                       
                                       
                                   }];
    
    UIAlertAction *laterAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Maybe Later", comment: "")
                                   style:UIAlertActionStyleDestructive
                                   handler:^(UIAlertAction *action) {
                                       
                                       [userDefault setObject:today forKey:@"date"];
                                       NSLog(@"Cancel action");
                                       
                                       self->iRateStatus = 2;
                                       
                                       [self connectionForRateStatus];
                                   }];
    
    UIAlertAction *neverAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"No Thanks", comment: "")
                                   style:UIAlertActionStyleDestructive
                                   handler:^(UIAlertAction *action) {
                                       [userDefault setObject:today forKey:@"date"];
                                       NSLog(@"Cancel action");
                                       
                                       self->iRateStatus = 3;
                                       
                                       [self connectionForRateStatus];
                                   }];
    
    UIAlertAction *dismissAction = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"No thanks", comment: "")
                                  style:UIAlertActionStyleDestructive
                                  handler:^(UIAlertAction *action) {
                                      [userDefault setObject:today forKey:@"date"];
                                      NSLog(@"Cancel action");
                                      
                                      self->iRateStatus = 0;
                                      
                                      [self connectionForRateStatus];
                                  }];
    
    [alertController addAction:nowAction];
    [alertController addAction:laterAction];
    [alertController addAction:neverAction];
//    [alertController addAction:dismissAction];
    
    AppDelegate *appdelegate = (AppDelegate*)[UIApplication sharedApplication].delegate ;
    UIViewController *vc = [ appdelegate.window rootViewController];
    
    [vc presentViewController: alertController animated:YES completion:nil];
    
}

#pragma mark - Webservice

-(void)connectionForRateStatus {
    
    [YXSpritesLoadingView show];
    
    
 
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_RATE_STATUS];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"],
                             @"rate_status": [NSString stringWithFormat:@"%d", iRateStatus]
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id response) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"RESPONSE:::...%@", response);
        
        if ([[response objectForKey:@"response"] boolValue]) {
            
            
            [YXSpritesLoadingView dismiss];
            
            
            if (self->iRateStatus == 1) {
                NSURL *url = [NSURL URLWithString:@"https://www.proringer.com/"];
                
                [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {
                    
                      [[NSNotificationCenter defaultCenter] postNotificationName:@"showBanner" object:self];
                    
                    if (success) {
                        NSLog(@"Opened url");
                    } else {
                        NSLog(@"Can't Open url:%@",[url description]);
                    }
                }];
            }
            else
            {
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"showBanner" object:self];
            }
        }
    }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"showBanner" object:self];
              [YXSpritesLoadingView dismiss];
              NSLog(@"ERROR:::...%@", task.error);
          }];
}

-(void) connectionAuth:(tokenCompletion) compblock{
    
    NSString *string = [NSString stringWithFormat:@"%@auth", ROOT_URL];
    //NSDictionary *params1 = @ {@"userid" : @"sworksapi", @"password" : @"KM3nACXKEXbRqn5G"};
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:@"sworksapi", @"userid", @"KM3nACXKEXbRqn5G", @"password", nil];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:string parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //        NSLog(@"JSON: %@", responseObject);
        
        if([[responseObject objectForKey:@"status"] intValue] == 100)
        {
            NSString *authToken = [responseObject objectForKey:@"apitoken"];
            [[NSUserDefaults standardUserDefaults] setObject:authToken forKey:@"apitoken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            if(compblock)
                compblock(YES);
        }
        else
            compblock(NO);
    }
          failure:^(NSURLSessionTask *operation, NSError *error)
     {
         NSLog(@"Error: %@, %@", error.localizedDescription, operation.response);
         compblock(NO);
     }];
    
}


/*
-(void) connectionAuth:(tokenCompletion) compblock{
    
    NSString *string = [NSString stringWithFormat:@"%@auth", ROOT_URL];
    NSDictionary *params = @ {@"userid" : @"sworksapi", @"password" : @"YL3nUUXKEXbRqn7H"};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:string parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"JSON: %@", responseObject);
        
        if([[responseObject objectForKey:@"status"] intValue] == 100)
        {
            NSString *authToken = [responseObject objectForKey:@"apitoken"];
            [[NSUserDefaults standardUserDefaults] setObject:authToken forKey:@"apitoken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            compblock(YES);
        }
        else
            compblock(NO);
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@, %@", error, operation.responseString);
         compblock(NO);
         
     }];
    
}
*/
- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}


@end
