//
//  ADTextField.h
//  ProringerPro
//
//  Created by Soma Halder on 22/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ADTextField : UITextField <UITextFieldDelegate>

-(void) paddingForTextField:(UITextField *)textField;
-(void)shadowForTextField:(UITextField *)textfield;

@end

NS_ASSUME_NONNULL_END
