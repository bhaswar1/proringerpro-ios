//
//  Define.h
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#ifndef Define_h
#define Define_h

#define LeftWidth ([[UIScreen mainScreen] bounds].size.width/2.0+50)

//#ifdef DEBUG // Code for development
//
//#define IMAGE_URL          @"http://111.93.181.155/lab6/proringer_latest/assets/uploads/"
//#define ROOT_URL           @"http://111.93.181.155/lab6/proringer_latest/"
//
//#else // Code for deployment to iTunes

#define IMAGE_URL          @"https://www.proringer.com/assets/uploads/"
#define ROOT_URL           @"https://www.proringer.com/"

//#endif

#define API_GOOGLE_AD_BANNER               @"ca-app-pub-3940256099942544/2934735716"
//#define API_GOOGLE_AD_BANNER               @"ca-app-pub-3940256099942544/3986624511"
//#define API_GOOGLE_AD_BANNER               @"ca-app-pub-4675181796473738/5720396042"
//#define API_GOOGLE_AD_BANNER               @"ca-app-pub-3173649292956926/5779879484"
//#define API_GOOGLE_AD_BANNER               @"ca-app-pub-4675181796473738/1721431544"
//#define API_GOOGLE_AD_INTERSTITIAL         @"ca-app-pub-4675181796473738/1611500387"
#define API_GOOGLE_AD_INTERSTITIAL         @"ca-app-pub-3940256099942544/4411468910"
//#define API_GOOGLE_AD_INTERSTITIAL         @"ca-app-pub-4675181796473738/2718866171"
//#define API_GOOGLE_AD_INTERSTITIAL         @"ca-app-pub-3173649292956926/7323033628"
#define API_GOOGLE_AD_REWARD               @"ca-app-pub-3940256099942544/1712485313"

#define API_KHISTI_LIST             @"app_banned_keywords"

#define API_RATE_STATUS             @"app_rate_status"

#define API_HOME_SCREEN             @"app_pro_homescreen_section"
#define API_LOGIN                   @"app_pro_login"
#define API_SIGN_UP                 @"app_pro_signup"
#define API_FORGOT_PW               @"app_forgot_password"
#define API_CONTACT_US              @"app_contact_us"
#define API_RESEND_CONFIRMATION     @"app_resend_confirm"
#define API_RESET_PASSWORD          @"app_resetpassword"

#define API_CATEGORY_LIST           @"app_categorylist"
#define API_CATEGORY_SERVICE_LIST   @"app_catrgoryservice_list"
#define API_UPDATE_EMAIL            @"app_change_email"
#define API_UPDATE_PASSWORD         @"app_change_password"
#define API_DASHBOARD               @"app_pro_dashboard"
#define API_USER_INFO               @"app_prouserinfo_list"
#define API_SAVE_INFO               @"app_prouserinfo_save"
#define API_IMAGE_UPLOAD            @"app_pro_profile_img"
#define API_HEADER_IMAGE            @"app_pro_headerbg_img"

#define API_COMPANY_INFO            @"app_procompany_info"
#define API_BUSINESS_TYPE           @"app_probusiness_option"
#define API_SAVE_COMP_INFO          @"app_procompany_info_save"

#define API_SERVICE_LIST            @"app_pro_services"
#define API_SAVE_SERVICE            @"app_proservices_save"
#define API_GET_SERVICE_LIST        @"app_pro_services"

#define API_GET_BUSINESS_HOUR       @"app_pro_businesshours"
#define API_SAVE_BUSINESS_HOUR      @"app_businesshr_save_ios"

#define API_GET_SERVICE_AREA        @"app_pro_servicearea"
#define API_SERVICE_AREA_SAVE       @"app_serviceareae_save_ios"

#define API_QUICK_REPLY             @"app_pro_quickreply"
#define API_QUICK_REPLY_SAVE        @"app_pro_quickreply_save"

#define API_REQUEST_REVIEW          @"app_pro_request_review"
#define API_ALL_REVIEW_LIST         @"app_homeowner_allreview"

#define API_TRANSACTION_GET         @"app_pro_transaction_history"

#define API_PREMIUM_PLANS           @"app_pro_premium_plan"

#define API_PAYMENT                 @"app_pro_premium_plan_payment"

#define API_AVAILABILITY_LIST       @"app_pro_availability"
#define API_AVAILABILITY_DELETE     @"app_pro_availability_delete"
#define API_UNAVAILABILITY_REASON   @"app_unavailabilityreason_option"
#define API_SAVE_UNAVAILABILITY     @"app_pro_availability_add"
#define API_UNAVAILABILITY_EDIT_LIST     @"app_pro_availability_edit"

#define API_GET_NOTIFICATION        @"app_pro_notification"
#define API_SAVE_NOTIFICATION       @"app_pronotification_save"

#define API_GET_LICENSE             @"app_prolicense_list"
#define API_ADD_LICENSE             @"app_prolicense_add"
#define API_EDIT_LICENSE            @"app_prolicense_edit"
#define API_DELETE_LICENSE          @"app_prolicense_delete"

#define API_INVITE_FRIEND           @"app_pro_invite_friend"

#define API_PORTFOLIO_LIST          @"app_pro_portfolio"
#define API_PORTFOLIO_ADD           @"app_proportfolio_add"
#define API_PORTFOLIO_DELETE        @"ap_protfolio_delete"
#define API_PORTFOLIO_IMAGE_DELETE  @"app_portfolio_image_delete"

#define API_GET_SOCIAL_MEDIA        @"app_pro_socialmedia"
#define API_SAVE_SOCIAL_MEDIA       @"app_pro_socialmedia_save"

#define API_ANALYTICS               @"app_pro_account_analytics"

#define API_MY_PROJECT              @"app_pro_myproject"
#define API_DELETE_PROJECT          @"app_pro_myproject_delete"

#define API_WATCHLIST               @"app_pro_watchlist"
#define API_ADD_DELETE_WATCHLIST    @"app_pro_watchlist_delete"
#define API_PROJECT_DETAILS         @"app_pro_myproject_details"
#define API_RESPOND                 @"app_pro_respond_now"
#define API_VIEW_PHONE_NUMBER       @"app_pro_click_phoneno"

#define API_PROJECT_SEARCH          @"app_pro_project_search"

#define API_PROFILE_DETAILS         @"app_pro_myprofile"

#define API_CAMPAIGN_SUMMARY        @"app_pro_campaign";
#define API_CAMPAIGN_CANCEL         @"app_pro_campaign_cancel"

#define API_MESSAGE_LIST            @"app_pro_project_message"
#define API_MESSAGE_DELETE          @"app_pro_project_deleteconversation"
#define API_MESSAGE_DETAILS         @"app_pro_project_message"
#define API_MESSAGE_SEND            @"app_pro_project_message_send"

#define API_PHONE_NUMBER            @"app_pro_verified_number"
#define API_VERIFY_PINCODE          @"app_pro_verified_pin"
#define API_VERIFY_ADDRESS          @"app_pro_verified_address"
#define API_VERIFY_DOCUMENT         @"app_pro_verify_business_doc"

#define API_REPORT                  @"app_homeowner_reportreview"
#define API_REPLY                   @"app_homeowner_replyreview"
#define API_PROJECT_REQUEST_REVIEW  @"app_pro_myproject_requestreview"
#define API_PROJECT_CANCEL_REVIEW   @"app_pro_requestreview_cancel"


#define API_GOOGLE_KEY              @"AIzaSyAHLnD9mtaoO9grq1TlVArjy64jTFbIz7M"

#define COLOR_TABLE_HEADER      [UIColor colorWithRed:215/255.0 green:215/255.0 blue:215/255.0 alpha:1]
#define COLOR_THEME             [UIColor colorWithRed:241.0f/255 green:89.0f/255 blue:42.0f/255 alpha:1]
//#define COLOR_BUTTON_TAB        [UIColor colorWithRed:250.0f/255 green:75.0f/255 blue:55.0f/255 alpha:1]
//#define COLOR_BUTTON_TAB        [UIColor colorWithRed:251.0f/255 green:64.0f/255 blue:18.0f/255 alpha:1]
#define COLOR_BUTTON_TAB        [UIColor colorWithRed:255.0f/255 green:44.0f/255 blue:17.0f/255 alpha:1]
#define COLOR_GREEN             [UIColor colorWithRed:46.0f/255 green:204.0f/255 blue:113.0f/255 alpha:1]
#define COLOR_TEXT              [UIColor colorWithRed:49.0f/255 green:49.0f/255 blue:49.0f/255 alpha:1]
#define COLOR_DARK_BACK         [UIColor colorWithRed:108.0f/255 green:108.0f/255 blue:108.0f/255 alpha:1]
#define COLOR_LIGHT_BACK        [UIColor colorWithRed:0.95 green:0.96 blue:0.98 alpha:1.0]
#define COLOR_BLUE              [UIColor colorWithRed:6.0f/255 green:165.0f/255 blue:231.0f/255 alpha:1]
#define COLOR_SELECTED_BACK     [UIColor colorWithRed:251.0f/255 green:207.0f/255 blue:193.0f/255 alpha:1]
#define COLOR_BUTTON_BACK       [UIColor colorWithRed:0.76 green:0.76 blue:0.76 alpha:1.0]
#define COLOR_AWAITING_RESPONSE_YELLOW  [UIColor colorWithRed:238.0f/255 green:233.0f/255 blue:137.0f/255 alpha:1]


#endif /* Define_h */
