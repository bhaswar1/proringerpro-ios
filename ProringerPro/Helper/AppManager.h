//
//  AppManager.h
//  TablePouncer
//
//  Created by Kazma Technology on 12/04/18.
//  Copyright © 2018 Kazma Technology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import  <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

//#import "SBRRightMenu.h"

typedef void(^tokenCompletion)(BOOL);

@class SettingModel;
@interface AppManager : NSObject

@property (nonatomic) BOOL isSetting;
@property (nonatomic) BOOL isMenuSelected;

@property (strong, nonatomic) NSString *strTotalAmount;
@property (strong, nonatomic) NSMutableDictionary *dictDetails;

@property (nonatomic, strong) NSMutableDictionary *userInfo;
@property (nonatomic, strong) NSDictionary *restaurantInfo;
@property (nonatomic, strong) NSMutableDictionary *orderInfo;
@property (nonatomic, strong) NSMutableArray *arrUserMenuList;
@property (nonatomic, strong) NSMutableArray *arrInviteeList;
@property (nonatomic, strong) NSString *strDeviceToken;
@property (nonatomic, strong) NSString *strUUID;
@property (nonatomic, strong) NSString *strCurrentUUID;
@property (nonatomic, strong) NSString *strPaymentUUID;
@property (nonatomic, strong) NSString *selectedRestaurantId;
@property (nonatomic) BOOL isFromPushNotification;
@property (nonatomic) BOOL isPickUpOptionMe;

@property (nonatomic, strong) NSArray *arrInvitees;

@property (nonatomic) int iEditMenuIndex;

@property (nonatomic, strong) NSString *senderId;
@property (nonatomic, strong) NSString *senderName;

@property (nonatomic, strong) NSString *picupId;
@property (nonatomic, strong) NSString *picupName;
@property (nonatomic, strong) NSString *pickupVenmoAccount;
@property (nonatomic) float lastOrderAmount;
@property (nonatomic) BOOL isUserCreateOrder;

@property (nonatomic) BOOL isMenuOppenedFromLoginScreen;

@property (nonatomic) float latitudeLast;
@property (nonatomic) float longitudeLast;

@property (nonatomic) BOOL isInvitationActive;
@property (nonatomic) BOOL isLoggedIn;

@property (nonatomic, strong) SettingModel *selectedEnterprise;

@property (nonatomic, strong) UINavigationController *appNav;
@property (nonatomic, strong) UIImage *imgLogo;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic) int menuType;

@property (nonatomic, assign) id resault;

+ (AppManager *) sharedDataAccess;
-(void)initalization;
-(void)clearInstance;

+ (NSString*)getTodayTomorrowFormat:(NSString*)day;

+ (Boolean)writeFile:(NSString*)filePath Content:(NSString*)fileContent;
+ (Boolean)truncateFile:(NSString*)filePath;
+ (NSString*)readFile:(NSString*)filePath;
+ (NSString *)contentTypeForImageData:(NSData *)data ;

- (NSDate *) getJSONDate:(NSString *)strLongDateTime;
- (Boolean)isValidPhoneNumber:(NSString*)phoneNumber;
- (Boolean)validateEmailWithString:(NSString*)email;
- (Boolean)isEmptyString:(NSString*) textToCheck;
//-(NSString*)getAPIUrl:(NSString*)strMethod;
-(void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message;
-(void)showAlertWithTitle /*:(NSString *)title andMessage:(NSString *)message fromViewController:(UIViewController *)vc */;

-(void)loadImage:(NSString*)strURL withImageView:(UIImageView*)imgView;
-(void)loadImage2:(NSString*)strURL withImageView:(UIImageView*)imgView;
//-(void)setRatingForValue:(NSString*)value forImageViews:(NSArray *)imageViews;

-(NSAttributedString*)getStrikeString:(NSString*)str ;

-(id)getDataFromHex:(NSString*)hex;
-(BOOL)detectType:(NSString*)stringToUse;
-(void) connectionAuth:(tokenCompletion) compblock;

//Web Service
-(void) connectionForWebServiceForGETMethodWithParameters:(NSDictionary *)params andUrl:(NSString *)url;
-(void) connectionForWebServiceForPOSTMethodForImageWithParameters:(NSDictionary *)params andUrl:(NSString *)url andImageData:(NSData *)imageData andImageKey:(NSString *)strImageKey;

-(id) connectionForWebServiceForPOSTMethodWithParameters:(NSDictionary *)params andUrl:(NSString *)url;

//-(SBRRightMenu*)getRightMenu:(CGRect)rc;

//-(void) connectionAuth:(tokenCompletion) compblock;
//- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size;

@end
