//
//  ProRingerProBaseVC.h
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "leftMenu.h"
#import "Footer.h"
@import GoogleMobileAds;

NS_ASSUME_NONNULL_BEGIN

// OG-18-9906-1843-00001654

@interface ProRingerProBaseVC : UIViewController <UIGestureRecognizerDelegate, ADSideMenuDrawerDelegate, UITextFieldDelegate, UITextViewDelegate, AVAudioPlayerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITabBarControllerDelegate, MFMailComposeViewControllerDelegate, CAAnimationDelegate, PECropViewControllerDelegate, EDStarRatingProtocol, GADBannerViewDelegate, GADInterstitialDelegate, ADFooterDelegate, GADUnifiedNativeAdDelegate, GADUnifiedNativeAdLoaderDelegate, GADVideoControllerDelegate, UIWebViewDelegate, GADRewardBasedVideoAdDelegate>
{
//    NSArray *arrAccount, *arrSupport, *arrAbout;
    BOOL account, support, about;
}

@property (nonatomic, strong) GADBannerView *bannerView;
@property (nonatomic, strong) GADInterstitial *interstitial;
@property (nonatomic, strong) GADAdLoader *adLoader;
@property (strong, nonatomic) GADUnifiedNativeAd *nativeAd;

@property (strong, nonatomic) YBHud *hud;

@property (strong, nonatomic) PECropViewController *cropController;

@property (strong, nonatomic) NSDictionary *dictInfoArray;
@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@property (strong, nonatomic) IBOutlet UIImage *imgLogo;
@property (strong, nonatomic) IBOutlet UIImage *imgLeftButton;
@property (strong, nonatomic) IBOutlet UIImage *imgRightButton;

@property (nonatomic) Footer *viewFooterTab;

@property (strong, nonatomic) NSMutableArray *arrAdvViewStatus, *arrInterstitialAd;

@property (strong, nonatomic) IBOutlet UIView *viewSideMenu, *viewProgress;//, *viewFooterMenu;
@property (strong, nonatomic) IBOutlet UILabel *lblProgress;

@property (strong, nonatomic) NSString *strShareUrl, *strUserId, *strLatitude, *strLongitude, *strRestrictionMsg, *strKhistiRestrictionMsg;

@property (assign, nonatomic) BOOL leftButton, rightButton;

@property (assign, nonatomic) int iPremiumStatus, iPaymentOption, iNewLeads, iTotalNewMsg, iPlanId, iRateFlag, iRateStatus;

@property (strong, nonatomic) UIBarButtonItem *navBarRightButton, *navBarLeftButton;

@property (strong, nonatomic) UIToolbar *toolBar;
//@property (strong, nonatomic) UIBarButtonItem *doneButton;
//@property (strong, nonatomic) UIBarButtonItem *cancelButton;
//@property (strong, nonatomic) UIBarButtonItem *flexibleSpace;

@property(nonatomic,weak) UIButton *btnDashboard, *btnMyProject, *btnMessage, *btnWatchlist;

//- (void)ADSideMenuDrawerSelection:(NSInteger)selectedSection withRow:(NSInteger)selectedRow;
-(void)leftBarButtonPressed;
-(void)tapToDismissView;
-(void)rightBarButtonPressed:(id)sender;
-(BOOL)validateEmail:(NSString*) emailString;
-(void)loginProRinger;
-(void)logOutProRinger;
-(void)goToProRingerHome;
-(NSMutableArray *)doArraysContainTheSameObjects:(NSArray *)firstArray;
-(void)doneButtonPressed;
-(void)cancelButtonPressed;
-(void)progressBarWidth;
-(void)addBannerViewToView:(UIView *)bannerView;
-(void)footerButtonDesignWithTag:(int)iTag;
-(void)connectionForGetDashboardData;
-(void)addFooter:(UIView *)mainView;
-(void)paddingForTextField:(UITextField *)textField;
-(void)shadowForTextField:(UITextField *)textfield;
-(void) paddingForTextView:(UITextView *)textView;
-(void)shadowForTextView:(UITextView *)textView;

@end

NS_ASSUME_NONNULL_END
