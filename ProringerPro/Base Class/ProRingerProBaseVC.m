//
//  ProRingerProBaseVC.m
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ProRingerProBaseVC.h"


@interface ProRingerProBaseVC ()
{
    UITapGestureRecognizer *tapGesture;
//    UISwipeGestureRecognizer *swipeLeft, *swipeRight;
    LeftMenu *viewLeftMenu;
    
//    UIView *footerView;
    
//    LeftMenu *lm;
//    int iPremiumStatus, iNewLeads, iTotalNewMsg;
    NSString *filePath, *strClassName;
}

@end

@implementation ProRingerProBaseVC

@synthesize imgLogo, imgLeftButton, imgRightButton, viewSideMenu, rightButton, leftButton, navBarLeftButton, navBarRightButton, audioPlayer, iPremiumStatus, cropController, toolBar, strShareUrl, viewProgress, lblProgress, strUserId, dictInfoArray, strLatitude, strLongitude, iPaymentOption, strRestrictionMsg,/* bannerView, interstitial,*/ arrAdvViewStatus, strKhistiRestrictionMsg,/* viewFooterMenu,*/ viewFooterTab, btnMessage, btnDashboard, btnMyProject, btnWatchlist, iNewLeads, iTotalNewMsg, iPlanId, appDelegate, arrInterstitialAd, hud, iRateFlag, iRateStatus;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBar.tintColor = COLOR_DARK_BACK;
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
//    self.navigationController.navigationBar.translucent = NO;
//    [self.navigationController.navigationBar setTitleTextAttributes:
//     @{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    
    self.navigationController.navigationBar.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.navigationController.navigationBar.layer.shadowRadius = 2.0f;
    self.navigationController.navigationBar.layer.shadowOpacity = 1.0f;
    self.navigationController.navigationBar.layer.masksToBounds = NO;
    
    self.view.backgroundColor = COLOR_LIGHT_BACK;
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{ NSFontAttributeName: [UIFont fontWithName:@"OpenSans" size:18.0f],
                                                                       NSForegroundColorAttributeName: [UIColor darkGrayColor]
                                                                       }];
    
//    [self.navigationController.navigationBar setTitleTextAttributes:@{ NSFontAttributeName: [UIFont systemFontOfSize:16.0 weight:UIFontWeightRegular],
//                                                                       NSForegroundColorAttributeName: [UIColor darkGrayColor]
//                                                                       }];
    
//       NSFontAttributeName:[UIFont fontWithName:@"OpenSans-Regular" size:14.0]}]; NSFontAttributeName: [UIFont fontWithName:@"OpenSans" size:14.0]
    [self.navigationController.navigationBar setBackgroundColor:[UIColor whiteColor]];
    
    hud = [[YBHud alloc] initWithHudType:DGActivityIndicatorAnimationTypeCookieTerminator andText:@""];
    hud.tintColor = COLOR_THEME;
    hud.UserInteractionDisabled = YES;
    hud.dimAmount = 0.3;
    
//    badgeConfiguration = [UITabBarItem getDefaultConfigurationProvider];
    
//    swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
//    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
//    swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeToDismiss:)];
//    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    
    self.tabBarController.delegate = self;
    
    iPaymentOption = iNewLeads = iTotalNewMsg = 0;
    
    cropController = [[PECropViewController alloc] init];
    cropController.delegate = self;
    viewProgress = [[UIView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height + 21, self.view.frame.size.width, 5)];
    lblProgress = [[UILabel alloc] init];
    [self progressBarWidth];
    lblProgress.backgroundColor = COLOR_THEME;
    [viewProgress addSubview:lblProgress];
    [self.view addSubview:viewProgress];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    strClassName = NSStringFromClass([self class]);
    
//    if ([strClassName isEqualToString:@"UserInfoVC"]) {
//        self.tabBarController.tabBar.hidden = NO;
//    }
    
    if ([strClassName isEqualToString:@"DashboardVC"] || [strClassName isEqualToString:@"MyProjectVC"] || [strClassName isEqualToString:@"WatchListVC"] || [strClassName isEqualToString:@"MessageVC"]/* || [strClassName isEqualToString:@"ProjectDetailsVC"]*/) {
        
        self.tabBarController.tabBar.hidden = NO;
        imgRightButton = [UIImage imageNamed:@"Search"];
        imgLeftButton = [UIImage imageNamed:@"Menu"];
        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon"]];
        /*rightButton = */leftButton = YES;
//        [self.view addGestureRecognizer:swipeRight];
//        [self.view removeGestureRecognizer:swipeLeft];
    } else {
        imgLeftButton = [UIImage imageNamed:@"Back"];
        self.tabBarController.tabBar.hidden = YES;
        /*rightButton = */leftButton = NO;
//        [self.view removeGestureRecognizer:swipeRight];
//        [self.view removeGestureRecognizer:swipeLeft];
    }
    
    NSInteger iTag = [[userDefault objectForKey:@"footer"] integerValue];
    
    [self footerButtonDesignWithTag:iTag];
    
    if ([strClassName isEqualToString:@"PortfolioEditVC"] || [strClassName isEqualToString:@"LicenceEditVC"]) {
        
        imgRightButton = [UIImage imageNamed:@"TrashCan"];
    } else if ([strClassName isEqualToString:@"SearchProjectVC"]) {
        
        imgRightButton = [UIImage imageNamed:@"Location"];
    } else if ([strClassName isEqualToString:@"LicenceVC"] || [strClassName isEqualToString:@"PortfolioVC"]) {
        
        imgRightButton = [UIImage imageNamed:@"Plus"];
    } else if ([strClassName isEqualToString:@"ProDetailsVC"]) {
        
        imgRightButton = [UIImage imageNamed:@"Share"];
    } else if ([strClassName isEqualToString:@"ForgotPasswordVC"] || [strClassName isEqualToString:@"ProjectRequestReviewVC"]) {
        
        imgRightButton = [UIImage imageNamed:@"Home"];
    } else if ([strClassName isEqualToString:@"ForgotPasswordVC"] || [strClassName isEqualToString:@"ResendConfVC"]) {
        
        imgRightButton = [UIImage imageNamed:@"Home"];
    } else if ([strClassName isEqualToString:@"SignUpCompleteVC"] || [strClassName isEqualToString:@"GalleryListVC"] || [strClassName isEqualToString:@"GalleryVC"]) {
        
        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon"]];
        imgRightButton = [UIImage imageNamed:@"CrossRound"];
    } else if ([strClassName isEqualToString:@"NotificationVC"] || [strClassName isEqualToString:@"QuickReplyVC"] || [strClassName isEqualToString:@"AvailalabilityVC"] || [strClassName isEqualToString:@"CampaignSummaryVC"] || [strClassName isEqualToString:@"SocialMediaVC"] || [strClassName isEqualToString:@"AnalyticsVC"]) {
        imgRightButton = [UIImage imageNamed:@"Search"];
    }
    
    if ([strClassName isEqualToString:@"VerifyInfoVC"] || [strClassName isEqualToString:@"VerifyMethodVC"] || [strClassName isEqualToString:@"VerifyPhoneVC"] || [strClassName isEqualToString:@"VerifyBusinessInfoVC"] || [strClassName isEqualToString:@"VerifyPinVC"] || [strClassName isEqualToString:@"VerifyBusinessAddressVC"] || [strClassName isEqualToString:@"VerifyPendingVC"]) {
        
        viewProgress.hidden = NO;
        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon"]];
    } else {
        viewProgress.hidden = YES;
    }
    
    navBarRightButton = [[UIBarButtonItem alloc]initWithImage:imgRightButton style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonPressed:)];
    self.navigationItem.rightBarButtonItem = navBarRightButton;
    
    navBarLeftButton = [[UIBarButtonItem alloc]initWithImage:imgLeftButton style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonPressed)];
    if ([strClassName isEqualToString:@"SignUpCompleteVC"] || [strClassName isEqualToString:@"GalleryVC"] || [strClassName isEqualToString:@"VerifyPinVC"]) {
        self.navigationItem.leftBarButtonItem = nil;
    } else {
        self.navigationItem.leftBarButtonItem = navBarLeftButton;
    }
    
    viewSideMenu = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    viewSideMenu.backgroundColor = [UIColor blackColor];
    viewSideMenu.hidden = YES;
    viewSideMenu.alpha = 0.0;
    [self.view addSubview:viewSideMenu];
    
    viewLeftMenu = [[LeftMenu alloc] initWithFrame:CGRectMake((self.view.frame.origin.x - self.view.frame.size.width), self.view.frame.origin.y +54, 250, self.view.frame.size.height -100)];
    viewLeftMenu.hidden = YES;
    [self.view addSubview:viewLeftMenu];
    viewLeftMenu.delegate = self;
    
//    viewFooterMenu = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height -45, self.view.frame.size.width, 45)];
//    viewFooterMenu.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    viewFooterMenu.backgroundColor = [UIColor clearColor];
//    viewFooterMenu.layer.zPosition = 2;
//    viewFooterMenu.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
//    viewFooterMenu.layer.shadowOffset = CGSizeMake(2.0f, 0.0f);
//    viewFooterMenu.layer.shadowRadius = 2.0f;
//    viewFooterMenu.layer.shadowOpacity = 0.8f;
//    viewFooterMenu.layer.masksToBounds = NO;
//    [self.view addSubview:viewFooterMenu];
    
//    viewFooterTab = [[Footer alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, viewFooterMenu.frame.size.height)];
    viewFooterTab = [[[NSBundle mainBundle] loadNibNamed:@"Footer" owner:self options:nil] objectAtIndex:0];
    viewFooterTab.frame = CGRectMake(0, self.view.frame.size.height -60, self.view.frame.size.width, 60);
    viewFooterTab.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    viewFooterTab.backgroundColor = [UIColor clearColor];
//    viewFooterTab.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
//    viewFooterTab.layer.shadowOffset = CGSizeMake(2.0f, 0.0f);
//    viewFooterTab.layer.shadowRadius = 2.0f;
//    viewFooterTab.layer.shadowOpacity = 0.8f;
//    viewFooterTab.layer.masksToBounds = NO;
    viewFooterTab.footerDelegate = self;
//    [viewFooterMenu addSubview:viewFooterTab];
    viewFooterTab.lblNewProject.backgroundColor = viewFooterTab.lblNewMessage.backgroundColor = COLOR_THEME;
    
    tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapToDismissView)];
    
    [viewSideMenu addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;
    
    arrAdvViewStatus = [[NSMutableArray alloc] init];
    arrInterstitialAd = [[NSMutableArray alloc] init];
//    arrKhistiFound = [NSMutableArray new];
//    arrKhisti = [[NSMutableArray alloc] init];
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [arrKhisti addObjectsFromArray:appDelegate.arrKhistiList];
    
//    lm = [[LeftMenu alloc] init];
//    [lm.tblLeftMenu reloadData];
//    [self setUpDrawer];
    
    NSLog(@"SHOW ME THE BOOL VALUE:::...%@", [userDefault objectForKey:@"isLoggedIn"]);
    
//    if ([[userDefault objectForKey:@"isLoggedIn"] boolValue] == YES) {
//        [self connectionForGetDashboardData];
//    }
//    [self badgeFontSizeSettings:@"10"];
//    [self badgeTextColorSettings:@"white"];
//    [self badgeBackgroundColorSettings:@"orange"];
    
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
//        for (UITabBarItem *tbi in self.tabBarController.tabBar.items) {
//            tbi.image = [tbi.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//        }
//    }

    tapGesture.enabled = YES;
    
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    toolBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor redColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    toolBar.items = [NSArray arrayWithObjects:cancelButton, flexibleSpace, doneButton, nil];
    
    strRestrictionMsg = @"Email addresses, phone numbers and links are not permitted. You will be able to share contact information later if you choose.";
    
    self.bannerView.backgroundColor = COLOR_LIGHT_BACK;
    self.bannerView = [[GADBannerView alloc]
                       initWithAdSize:kGADAdSizeSmartBannerPortrait];
    self.bannerView.adUnitID = API_GOOGLE_AD_BANNER;
    self.bannerView.rootViewController = self;
    self.bannerView.delegate = self;
    [self.bannerView loadRequest:[GADRequest request]];
//    [self addBannerViewToView:self.bannerView];
    
    self.interstitial = [[GADInterstitial alloc]
                         initWithAdUnitID:API_GOOGLE_AD_INTERSTITIAL];
    self.interstitial.delegate = self;
    self.interstitial = [self createAndLoadInterstitial];
    [self.interstitial loadRequest:[GADRequest request]];
    
    GADMultipleAdsAdLoaderOptions *multipleAdsOptions =
    [[GADMultipleAdsAdLoaderOptions alloc] init];
    multipleAdsOptions.numberOfAds = 5;
    
    self.adLoader = [[GADAdLoader alloc] initWithAdUnitID:API_GOOGLE_AD_BANNER
                                       rootViewController:self
                                                  adTypes:@[kGADAdLoaderAdTypeUnifiedNative]
                                                  options:@[multipleAdsOptions]];
    self.adLoader.delegate = self;
    [self.adLoader loadRequest:[GADRequest request]];
    self.nativeAd.delegate = self;
    
    [GADRewardBasedVideoAd sharedInstance].delegate = self;
    [[GADRewardBasedVideoAd sharedInstance] loadRequest:[GADRequest request]
                                           withAdUnitID:API_GOOGLE_AD_REWARD];
    
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    [self.bannerView removeFromSuperview];
}

#pragma mark - AD Banner Delegate
- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.bottomLayoutGuide
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:-60],
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
    //    bannerView.layer.zPosition = -1;
    //    table_view.layer.zPosition = -2;
}

- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    adView.alpha = 0;
    [UIView animateWithDuration:1.0 animations:^{
        adView.alpha = 1;
//        [self addBannerViewToView:self.bannerView];
    }];
}

- (void)adLoader:(GADAdLoader *)adLoader
didReceiveNativeAd:(GADUnifiedNativeAd *)nativeAd {
    
    [self.adLoader loadRequest:[GADRequest request]];
}

- (void)adLoader:(GADAdLoader *)adLoader
didReceiveUnifiedNativeAd:(GADUnifiedNativeAd *)nativeAd {
    // A unified native ad has loaded, and can be displayed.
}

- (void)adLoaderDidFinishLoading:(GADAdLoader *) adLoader {
    // The adLoader has finished loading ads, and a new request can be sent.
}

- (void)nativeAdDidRecordImpression:(GADUnifiedNativeAd *)nativeAd {
    // The native ad was shown.
}

- (void)nativeAdDidRecordClick:(GADUnifiedNativeAd *)nativeAd {
    // The native ad was clicked on.
}

- (void)nativeAdWillPresentScreen:(GADUnifiedNativeAd *)nativeAd {
    // The native ad will present a full screen view.
}

- (void)nativeAdWillDismissScreen:(GADUnifiedNativeAd *)nativeAd {
    // The native ad will dismiss a full screen view.
}

- (void)nativeAdDidDismissScreen:(GADUnifiedNativeAd *)nativeAd {
    // The native ad did dismiss a full screen view.
}

- (void)nativeAdWillLeaveApplication:(GADUnifiedNativeAd *)nativeAd {
    // The native ad will cause the application to become inactive and
    // open a new application.
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

/// Tells the delegate that a full-screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
    NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full-screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
    NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full-screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
    NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
    NSLog(@"adViewWillLeaveApplication");
}

#pragma mark - AD view positioning
- (void)positionBannerViewFullWidthAtBottomOfSafeArea:(UIView *_Nonnull)bannerView NS_AVAILABLE_IOS(11.0) {
    // Position the banner. Stick it to the bottom of the Safe Area.
    // Make it constrained to the edges of the safe area.
    UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
    
    [NSLayoutConstraint activateConstraints:@[
                                              [guide.leftAnchor constraintEqualToAnchor:bannerView.leftAnchor],
                                              [guide.rightAnchor constraintEqualToAnchor:bannerView.rightAnchor],
                                              [guide.bottomAnchor constraintEqualToAnchor:bannerView.bottomAnchor]
                                              ]];
}

- (void)positionBannerViewFullWidthAtBottomOfView:(UIView *_Nonnull)bannerView {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.bottomLayoutGuide
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
}

#pragma mark - Interstitial
- (GADInterstitial *)createAndLoadInterstitial {
    GADInterstitial *interstitial =
    [[GADInterstitial alloc] initWithAdUnitID:API_GOOGLE_AD_INTERSTITIAL];
    interstitial.delegate = self;
    [interstitial loadRequest:[GADRequest request]];
    return interstitial;
}

/// Tells the delegate an ad request succeeded.
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
}

/// Tells the delegate an ad request failed.
- (void)interstitial:(GADInterstitial *)ad
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"interstitial:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

/// Tells the delegate that an interstitial will be presented.
- (void)interstitialWillPresentScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillPresentScreen");
}

/// Tells the delegate the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
}

/// Tells the delegate the interstitial had been animated off the screen.
- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
}

/// Tells the delegate that a user click will open another app
/// (such as the App Store), backgrounding the current app.
- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
}
/*
#pragma mark - Reward
- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
   didRewardUserWithReward:(GADAdReward *)reward {
    NSString *rewardMessage = [NSString stringWithFormat:@"Reward received with currency %@ , amount %lf", reward.type, [reward.amount doubleValue]];
    NSLog(@"%@", rewardMessage);
}

- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is received.");
}

- (void)rewardBasedVideoAdDidOpen:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Opened reward based video ad.");
}

- (void)rewardBasedVideoAdDidStartPlaying:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad started playing.");
}

- (void)rewardBasedVideoAdDidCompletePlaying:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad has completed.");
}

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is closed.");
    [[GADRewardBasedVideoAd sharedInstance] loadRequest:[GADRequest request]
                                           withAdUnitID:API_GOOGLE_AD_REWARD];
}

- (void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad will leave application.");
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error {
    NSLog(@"Reward based video ad failed to load.");
}
*/
#pragma mark - Progress Bar
-(void)progressBarWidth {
    
    int iProgressWidth = 0;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"progressWidth"]) {
        iProgressWidth = [[[NSUserDefaults standardUserDefaults] objectForKey:@"progressWidth"] intValue];
    }
    
    lblProgress.frame = CGRectMake(0, 0, (viewProgress.frame.size.width *iProgressWidth) /6, viewProgress.frame.size.height);
}

#pragma mark - UIGesture
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
//
//    if (CGRectContainsPoint(lm.tblLeftMenu.bounds, [touch locationInView:lm.tblLeftMenu])) {
//        return NO;
//    } else
//        return YES;
//}

-(void)handlePan:(UISwipeGestureRecognizer*)swipe {
    
    [self leftBarButtonPressed];
}

-(void)swipeToDismiss:(UISwipeGestureRecognizer*)swipe {
    
    [self leftBarButtonPressed];
}

//- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//
//    UITouch *touch = [[event allTouches] anyObject];
//
//    CGPoint location = [touch locationInView:self.view];
//
//    if (location.x >= self.view.frame.origin.x && location.x <= self.view.frame.origin.x && location.y >= self.view.frame.origin.y && location.y <= self.view.frame.origin.y) {
//
//        NSLog(@"TOUCH HAPENED IN LOCATION");
//    }
//    if (touches.count > 0) {
//        NSLog(@"TOUCH HAPENED");
//    }
//}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//
//}

//- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
//
//    // do your stuff here
//    // return nil if you want to prevent interaction with UI elements
//    return [super hitTest:point withEvent:event];
//}

#pragma mark UINavigationBar Button
-(void)tapToDismissView {

    [UIView animateWithDuration:0.5 animations:^{
        self->viewSideMenu.alpha = 0.0;
        self->viewLeftMenu.frame = CGRectMake((self.view.frame.origin.x - self.view.frame.size.width), self.view.frame.origin.y +54, 250, self.view.frame.size.height -100);
    } completion:^(BOOL finished) {
        self->viewSideMenu.hidden = YES;
        self->viewLeftMenu.hidden = YES;
//        self.view.hidden = YES;
    }];
//    [self.view removeGestureRecognizer:swipeLeft];
}

-(void)rightBarButtonPressed:(id)sender {
}

-(void)leftBarButtonPressed {
    
    if ([strClassName isEqualToString:@"LoginVC"] || [strClassName isEqualToString:@"SignUpVC"]) {
        [self goToProRingerHome];
    } else {
        if (leftButton) {
            if (viewLeftMenu.hidden == YES) {
                viewLeftMenu.hidden = NO;
                viewSideMenu.hidden = NO;
                NSLog(@"Left button pressed");
//                [self.view addGestureRecognizer:swipeLeft];
                [UIView animateWithDuration:0.5 animations:^{
                    
                    self->viewSideMenu.alpha = 0.5;
                    self->viewLeftMenu.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +54, 250, self.view.frame.size.height -100);
                } completion:^(BOOL finished) {
                    
                }];
            } else {
                [self tapToDismissView];
            }
        } else {
            int iProgressBar = [[[NSUserDefaults standardUserDefaults] objectForKey:@"progressWidth"] intValue];
            if (iProgressBar > 1) {
                iProgressBar = iProgressBar -1;
            }
            [[NSUserDefaults standardUserDefaults] setInteger:iProgressBar forKey:@"progressWidth"];
            [self progressBarWidth];
            if ([strClassName isEqualToString:@"CampaignUpgradeVC"]) {
                [self.navigationController popViewControllerAnimated:NO];
            } else {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}

#pragma mark - UITabBar Delegate
-(void) tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    NSLog(@"Selected INDEX OF TAB-BAR ==> %lu", (unsigned long)tabBarController.selectedIndex);
    
    
    NSUInteger iTabIndex = tabBarController.selectedIndex;
    /*
     
     ## It not proper way to place footer menu in this kind of pages because it doesn’t follow proper iOS rules.
     
    UIViewController *selectedVC = [self.tabBarController.viewControllers objectAtIndex:self.tabBarController.selectedIndex];
    
    if (viewController == tabBarController.moreNavigationController)
    {
        tabBarController.moreNavigationController.delegate = self;
    }
    
    NSString *strViewController = NSStringFromClass([viewController class]);
    NSLog(@"Selected ViewController of TabBar ==> %@", strViewController);
    if (tabBarController.selectedIndex == 0) {
        UIStoryboard *sbDashboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
        DashboardVC *dvc = [sbDashboard instantiateViewControllerWithIdentifier:@"DashboardVC"];
        [self.navigationController pushViewController:dvc animated:NO];
    } else if (tabBarController.selectedIndex == 1) {
        UIStoryboard *sbProject = [UIStoryboard storyboardWithName:@"Projects" bundle:nil];
        MyProjectVC *mpvc = [sbProject instantiateViewControllerWithIdentifier:@"MyProjectVC"];
        [self.navigationController pushViewController:mpvc animated:NO];
    } else if (tabBarController.selectedIndex == 2) {
        UIStoryboard *sbMessage = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
        MessageVC *mvc = [sbMessage instantiateViewControllerWithIdentifier:@"MessageVC"];
        [self.navigationController pushViewController:mvc animated:NO];
    } else if (tabBarController.selectedIndex == 3) {
        UIStoryboard *sbWatchlist = [UIStoryboard storyboardWithName:@"Watchlist" bundle:nil];
        WatchListVC *wlvc = [sbWatchlist instantiateViewControllerWithIdentifier:@"WatchListVC"];
        [self.navigationController pushViewController:wlvc animated:NO];
    }
    */
    if (iTabIndex == 0) {
        [self turnBackToAnOldViewController:@"DashboardVC" withNibName:@"Dashboard"];
//        if (![strViewController isEqualToString:@"DashboardVC"]) {
//            UIStoryboard *sbDashboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
//            DashboardVC *dvc = [sbDashboard instantiateViewControllerWithIdentifier:@"DashboardVC"];
//            [self.navigationController pushViewController:dvc animated:NO];
//        }
    }
    else if (iTabIndex == 1) {
        [self turnBackToAnOldViewController:@"MyProjectVC" withNibName:@"Projects"];
//        if (![strViewController isEqualToString:@"MyProjectVC"]) {
//            UIStoryboard *sbProject = [UIStoryboard storyboardWithName:@"Projects" bundle:nil];
//            MyProjectVC *mpvc = [sbProject instantiateViewControllerWithIdentifier:@"MyProjectVC"];
//            [self.navigationController pushViewController:mpvc animated:NO];
//        }
    } else if (iTabIndex == 2) {
        [self turnBackToAnOldViewController:@"MessageVC" withNibName:@"Message"];
//        if (![strViewController isEqualToString:@"MessageVC"]) {
//            UIStoryboard *sbMessage = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
//            MessageVC *mvc = [sbMessage instantiateViewControllerWithIdentifier:@"MessageVC"];
//            [self.navigationController pushViewController:mvc animated:NO];
//        }
    } else if (iTabIndex == 3) {
        [self turnBackToAnOldViewController:@"WatchListVC" withNibName:@"Watchlist"];
//        if (![strViewController isEqualToString:@"WatchListVC"]) {
//            UIStoryboard *sbWatchlist = [UIStoryboard storyboardWithName:@"Watchlist" bundle:nil];
//            WatchListVC *wlvc = [sbWatchlist instantiateViewControllerWithIdentifier:@"WatchListVC"];
//            [self.navigationController pushViewController:wlvc animated:NO];
//        }     9135885984
    }

}

- (void)turnBackToAnOldViewController:(NSString *)viewController withNibName:(NSString *)nibName {
    UIViewController *controller;
    for (controller in self.navigationController.viewControllers) {
        NSLog(@"class name:::...%@", controller.class);
        //Do not forget to import AnOldViewController.h
        if ([controller isKindOfClass:[viewController class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            return;
        }
//        else {
//
//            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:nibName bundle:nil];
//            controller = [storyBoard instantiateViewControllerWithIdentifier:viewController];
//            [self.navigationController pushViewController:controller animated:NO];
//
//        }
    }
}

#pragma mark - footer Delegate
-(void)footerButtonTap:(int)iTag {
    
    if (iTotalNewMsg > 0) {
//        viewFooterTab.imgMessage.image = [UIImage imageNamed:@"MessagesNew"];
        viewFooterTab.lblNewMessage.hidden = NO;
    } else {
//        viewFooterTab.imgMessage.image = [UIImage imageNamed:@"Messages"];
        viewFooterTab.lblNewMessage.hidden = YES;
    }
    
    if (iNewLeads > 0) {
//        viewFooterTab.imgMyProject.image = [UIImage imageNamed:@"MyProjectNew"];
        viewFooterTab.lblNewProject.hidden = NO;
    } else {
//        viewFooterTab.imgMyProject.image = [UIImage imageNamed:@"MyProject"];
        viewFooterTab.lblNewProject.hidden = YES;
    }
    
    if (iTag == 1) {
        
        [viewFooterTab.btnDashboard setTintColor:COLOR_BUTTON_TAB];
        [viewFooterTab.btnProjects setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnMessages setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnWatchlist setTintColor:[UIColor darkGrayColor]];
        
        viewFooterTab.lblDashboard.textColor = COLOR_THEME;
        viewFooterTab.lblWatchlist.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblMyProject.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblMessage.textColor = [UIColor darkGrayColor];
        
        viewFooterTab.imgDashboard.image = [UIImage imageNamed:@"Dashboard_Selected"];
        viewFooterTab.imgWatchlist.image = [UIImage imageNamed:@"Watchlist"];
        
        if (![strClassName isEqualToString:@"DashboardVC"]) {
            UIStoryboard *sbDashboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
            DashboardVC *dvc = [sbDashboard instantiateViewControllerWithIdentifier:@"DashboardVC"];
            [self.navigationController pushViewController:dvc animated:NO];
        }
    } else if (iTag == 2) {
        
        [viewFooterTab.btnDashboard setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnProjects setTintColor:COLOR_BUTTON_TAB];
        [viewFooterTab.btnMessages setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnWatchlist setTintColor:[UIColor darkGrayColor]];
        
        viewFooterTab.lblDashboard.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblMyProject.textColor = COLOR_THEME;
        viewFooterTab.lblMessage.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblWatchlist.textColor = [UIColor darkGrayColor];
        
        viewFooterTab.imgDashboard.image = [UIImage imageNamed:@"Dashboard"];
        viewFooterTab.imgWatchlist.image = [UIImage imageNamed:@"Watchlist"];
        
        if (iNewLeads > 0) {
            viewFooterTab.imgMyProject.image = [UIImage imageNamed:@"MyProjectNew_Selected"];
        } else {
            viewFooterTab.imgMyProject.image = [UIImage imageNamed:@"MyProject_Selected"];
        }
        
        if (![strClassName isEqualToString:@"MyProjectVC"]) {
            UIStoryboard *sbDashboard = [UIStoryboard storyboardWithName:@"Projects" bundle:nil];
            MyProjectVC *dvc = [sbDashboard instantiateViewControllerWithIdentifier:@"MyProjectVC"];
            [self.navigationController pushViewController:dvc animated:NO];
        }
    } else if (iTag == 3) {
        
        [viewFooterTab.btnDashboard setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnProjects setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnMessages setTintColor:COLOR_BUTTON_TAB];
        [viewFooterTab.btnWatchlist setTintColor:[UIColor darkGrayColor]];
        
        viewFooterTab.lblDashboard.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblMyProject.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblMessage.textColor = COLOR_THEME;
        viewFooterTab.lblWatchlist.textColor = [UIColor darkGrayColor];
        
        viewFooterTab.imgDashboard.image = [UIImage imageNamed:@"Dashboard"];
        viewFooterTab.imgWatchlist.image = [UIImage imageNamed:@"Watchlist"];
        
        if (iTotalNewMsg > 0) {
            viewFooterTab.imgMessage.image = [UIImage imageNamed:@"MessagesNew_Selected"];
        } else {
            viewFooterTab.imgMessage.image = [UIImage imageNamed:@"Messages_Selected"];
        }
        
        if (![strClassName isEqualToString:@"MessageVC"]) {
            UIStoryboard *sbDashboard = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
            MessageVC *dvc = [sbDashboard instantiateViewControllerWithIdentifier:@"MessageVC"];
            [self.navigationController pushViewController:dvc animated:NO];
        }
    } else if (iTag == 4) {
        
        [viewFooterTab.btnDashboard setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnProjects setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnMessages setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnWatchlist setTintColor:COLOR_BUTTON_TAB];
        
        viewFooterTab.lblDashboard.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblMyProject.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblMessage.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblWatchlist.textColor = COLOR_THEME;
        
        viewFooterTab.imgDashboard.image = [UIImage imageNamed:@"Dashboard"];
        viewFooterTab.imgWatchlist.image = [UIImage imageNamed:@"Watchlist_Selected"];
        
        if (![strClassName isEqualToString:@"WatchListVC"]) {
            UIStoryboard *sbDashboard = [UIStoryboard storyboardWithName:@"Watchlist" bundle:nil];
            WatchListVC *dvc = [sbDashboard instantiateViewControllerWithIdentifier:@"WatchListVC"];
            [self.navigationController pushViewController:dvc animated:NO];
        }
    }
}

-(void)footerButtonDesignWithTag:(int)iTag {
    
    if (iTotalNewMsg > 0) {
//        viewFooterTab.imgMessage.image = [UIImage imageNamed:@"MessagesNew"];
        viewFooterTab.lblNewMessage.hidden = NO;
    } else {
//        viewFooterTab.imgMessage.image = [UIImage imageNamed:@"Messages"];
        viewFooterTab.lblNewMessage.hidden = YES;
    }
    
    if (iNewLeads > 0) {
//        viewFooterTab.imgMyProject.image = [UIImage imageNamed:@"MyProjectNew"];
        viewFooterTab.lblNewProject.hidden = NO;
    } else {
//        viewFooterTab.imgMyProject.image = [UIImage imageNamed:@"MyProject"];
        viewFooterTab.lblNewProject.hidden = YES;
    }
    
    if (iTag == 1) {
        
        [viewFooterTab.btnDashboard setTintColor:COLOR_BUTTON_TAB];
        [viewFooterTab.btnProjects setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnMessages setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnWatchlist setTintColor:[UIColor darkGrayColor]];
        
        viewFooterTab.lblDashboard.textColor = COLOR_THEME;
        viewFooterTab.lblWatchlist.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblMyProject.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblMessage.textColor = [UIColor darkGrayColor];
        
        viewFooterTab.imgDashboard.image = [UIImage imageNamed:@"Dashboard_Selected"];
//        viewFooterTab.imgMyProject.image = [UIImage imageNamed:@"MyProject"];
//        viewFooterTab.imgMessage.image = [UIImage imageNamed:@"Messages"];
        viewFooterTab.imgWatchlist.image = [UIImage imageNamed:@"Watchlist"];
    } else if (iTag == 2) {
        
        [viewFooterTab.btnDashboard setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnProjects setTintColor:COLOR_BUTTON_TAB];
        [viewFooterTab.btnMessages setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnWatchlist setTintColor:[UIColor darkGrayColor]];
        
        viewFooterTab.lblDashboard.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblMyProject.textColor = COLOR_THEME;
        viewFooterTab.lblMessage.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblWatchlist.textColor = [UIColor darkGrayColor];
        
        viewFooterTab.imgDashboard.image = [UIImage imageNamed:@"Dashboard"];
//        viewFooterTab.imgMessage.image = [UIImage imageNamed:@"Messages"];
        viewFooterTab.imgWatchlist.image = [UIImage imageNamed:@"Watchlist"];
        
        if (iNewLeads > 0) {
            viewFooterTab.imgMyProject.image = [UIImage imageNamed:@"MyProjectNew_Selected"];
        } else {
            viewFooterTab.imgMyProject.image = [UIImage imageNamed:@"MyProject_Selected"];
        }
    } else if (iTag == 3) {
        
        [viewFooterTab.btnDashboard setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnProjects setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnMessages setTintColor:COLOR_BUTTON_TAB];
        [viewFooterTab.btnWatchlist setTintColor:[UIColor darkGrayColor]];
        
        viewFooterTab.lblDashboard.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblMyProject.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblMessage.textColor = COLOR_THEME;
        viewFooterTab.lblWatchlist.textColor = [UIColor darkGrayColor];
        
        viewFooterTab.imgDashboard.image = [UIImage imageNamed:@"Dashboard"];
//        viewFooterTab.imgMyProject.image = [UIImage imageNamed:@"MyProject"];
        viewFooterTab.imgWatchlist.image = [UIImage imageNamed:@"Watchlist"];
        
        if (iTotalNewMsg > 0) {
            viewFooterTab.imgMessage.image = [UIImage imageNamed:@"MessagesNew_Selected"];
        } else {
            viewFooterTab.imgMessage.image = [UIImage imageNamed:@"Messages_Selected"];
        }
    } else if (iTag == 4) {
        
        [viewFooterTab.btnDashboard setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnProjects setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnMessages setTintColor:[UIColor darkGrayColor]];
        [viewFooterTab.btnWatchlist setTintColor:COLOR_BUTTON_TAB];
        
        viewFooterTab.lblDashboard.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblMyProject.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblMessage.textColor = [UIColor darkGrayColor];
        viewFooterTab.lblWatchlist.textColor = COLOR_THEME;
        
        viewFooterTab.imgDashboard.image = [UIImage imageNamed:@"Dashboard"];
//        viewFooterTab.imgMyProject.image = [UIImage imageNamed:@"MyProject"];
//        viewFooterTab.imgMessage.image = [UIImage imageNamed:@"Messages"];
        viewFooterTab.imgWatchlist.image = [UIImage imageNamed:@"Watchlist_Selected"];
    }
}

#pragma mark - sideMenu Delegate
// NSLog(@"CCKFNavDrawerSelection = %ld and row number = %ld", selectionIndex, rowIndex);
//-(void)ADSideMenuDrawerSelection
- (void)ADSideMenuDrawerSelection:(NSInteger)selectedSection withRow:(NSInteger)selectedRow andTitle:(NSString *)title {
    
    NSLog(@"Side Menu Drawer Selection = %ld and row number = %ld", selectedSection, selectedRow);

    [self tapToDismissView];
    self.tabBarController.tabBar.hidden = YES;
    
    UIStoryboard *sbMain = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIStoryboard *sbDashboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    UIStoryboard *sbDetails = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
    
    if (selectedSection == 1) {
        
        self.tabBarController.tabBar.hidden = NO;
        
        if ([title isEqualToString:@"User Information"]) {
            
            UserInfoVC *uivc = [sbDashboard instantiateViewControllerWithIdentifier:@"UserInfoVC"];
            [self.navigationController pushViewController:uivc animated:YES];
        } else if ([title isEqualToString:@"Company"]) {
            
            CompanyProfileVC *uivc = [sbDashboard instantiateViewControllerWithIdentifier:@"CompanyProfileVC"];
            [self.navigationController pushViewController:uivc animated:YES];
        } else if ([title isEqualToString:@"Services"]) {
            
            ServicesVC *svc = [sbDashboard instantiateViewControllerWithIdentifier:@"ServicesVC"];
            [self.navigationController pushViewController:svc animated:YES];
            
        } else if ([title isEqualToString:@"Licenses"]) {
            
            LicenceVC *uivc = [sbDashboard instantiateViewControllerWithIdentifier:@"LicenceVC"];
            [self.navigationController pushViewController:uivc animated:YES];
        } else if ([title isEqualToString:@"Portfolio"]) {
            
            PortfolioVC *pvc = [sbDashboard instantiateViewControllerWithIdentifier:@"PortfolioVC"];
            [self.navigationController pushViewController:pvc animated:YES];
        } else if ([title isEqualToString:@"Campaign Summary"] || [title isEqualToString:@"Campaigns"]) {
            
            CampaignSummaryVC *csvc = [sbDashboard instantiateViewControllerWithIdentifier:@"CampaignSummaryVC"];
            [self.navigationController pushViewController:csvc animated:YES];
        } else if ([title isEqualToString:@"Login Settings"]) {
            
            LoginSettingsVC *lsvc = [sbDashboard instantiateViewControllerWithIdentifier:@"LoginSettingsVC"];
            [self.navigationController pushViewController:lsvc animated:YES];
        } else if ([title isEqualToString:@"Notifications"]) {
            
            NotificationVC *nvc = [sbDashboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
            [self.navigationController pushViewController:nvc animated:YES];
        } else if ([title isEqualToString:@"Quick Reply"]) {
            
            QuickReplyVC *qrvc = [sbDashboard instantiateViewControllerWithIdentifier:@"QuickReplyVC"];
            [self.navigationController pushViewController:qrvc animated:YES];
        } else if ([title isEqualToString:@"Availability"]) {
            
            AvailalabilityVC *avc = [sbDashboard instantiateViewControllerWithIdentifier:@"AvailalabilityVC"];
            [self.navigationController pushViewController:avc animated:YES];
        } else if ([title isEqualToString:@"Social Media"]) {
            
            SocialMediaVC *smvc = [sbDashboard instantiateViewControllerWithIdentifier:@"SocialMediaVC"];
            [self.navigationController pushViewController:smvc animated:YES];
        } else if ([title isEqualToString:@"Payment Methods"]) {
            
            CampaignUpgradeVC *csvc = [sbDashboard instantiateViewControllerWithIdentifier:@"CampaignUpgradeVC"];
            csvc.viewHeader.hidden = YES;
            csvc.viewPaymentMethod.hidden = NO;
            csvc.strPlanId = [[NSUserDefaults standardUserDefaults] objectForKey:@"plan_id"];
            [self.navigationController pushViewController:csvc animated:YES];
        } else if ([title isEqualToString:@"Transaction History"]) {
            
            TransactionHistoryVC *thvc = [sbDashboard instantiateViewControllerWithIdentifier:@"TransactionHistoryVC"];
            [self.navigationController pushViewController:thvc animated:YES];
        } else if ([title isEqualToString:@"Service Area"]) {
            
            ServiceAreaVC *savc = [sbDashboard instantiateViewControllerWithIdentifier:@"ServiceAreaVC"];
            [self.navigationController pushViewController:savc animated:YES];
        } else if ([title isEqualToString:@"Business Hours"]) {
            
            BusinessHoursVC *bhvc = [sbDashboard instantiateViewControllerWithIdentifier:@"BusinessHoursVC"];
            [self.navigationController pushViewController:bhvc animated:YES];
        } else if ([title isEqualToString:@"Request Reviews"]) {
            
            RequestReviewVC *rrvc = [sbDashboard instantiateViewControllerWithIdentifier:@"RequestReviewVC"];
            rrvc.strRequestingReview = @"Requesting a review from request review page";
            [self.navigationController pushViewController:rrvc animated:YES];
        } else if ([title isEqualToString:@"Invite Friends"]) {
            
            InviteFriendVC *ifvc = [sbDashboard instantiateViewControllerWithIdentifier:@"InviteFriendVC"];
            [self.navigationController pushViewController:ifvc animated:YES];
        } else if ([title isEqualToString:@"Analytics"]) {
            
            [self connectionForGetAnalytics];
        } else if ([title isEqualToString:@"Share Profile"]) {
            self.tabBarController.tabBar.hidden = NO;
//            NSString *textToShare = @"";//[[dictProInfoDetails objectForKey:@"info"] objectForKey:@"url"];
            NSLog(@"SHARE TEXT:::...%@", strShareUrl);
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:[NSArray arrayWithObjects:strShareUrl, nil] applicationActivities:nil];
            activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll]; //Exclude whichever aren't relevant
            [self presentViewController:activityVC animated:YES completion:NULL];
        } else if ([title isEqualToString:@"My Profile"]) {
            
            ProDetailsVC *pdvc = [sbDetails instantiateViewControllerWithIdentifier:@"ProDetailsVC"];
            [self.navigationController pushViewController:pdvc animated:YES];
        } else if ([title isEqualToString:@"Log out"]) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Do you want to log out?" message:@"Are you sure you want to log out of your account?" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Log out" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                
                [self connectionForLogOut];
            }];
            
            UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            }];
            
            [alertController addAction:noAction];
            [alertController addAction:yesAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        }
    } else if (selectedSection == 2) {
        
        if (selectedRow == 0) {
            
            [self emailSupport];
        } else if (selectedRow == 1) {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            TermsAndConditionVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditionVC"];
            svc.strTitle = @"Faq";
            svc.strApi = @"app_faq_page";
            [self.navigationController pushViewController:svc animated:YES];
        } else if (selectedRow == 2) {
            
            [self provideFeedback];
        }
    } else if (selectedSection == 3) {
        
        if (selectedRow == 0) {
            
            TermsAndConditionVC *svc = [sbMain instantiateViewControllerWithIdentifier:@"TermsAndConditionVC"];
            svc.strTitle = @"TERMS OF SERVICES";
            svc.strApi = @"app_term";
            [self.navigationController pushViewController:svc animated:YES];
        } else if (selectedRow == 1) {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            TermsAndConditionVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditionVC"];
            svc.strTitle = @"Privacy Policy";
            svc.strApi = @"app_privacy_policy";
            [self.navigationController pushViewController:svc animated:YES];
        }
    }
    
}

-(void)ADSideMenuDrawerSelection:(NSInteger)selectedSection {
    if (selectedSection == 0) {
        
        [self tapToDismissView];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchProjectVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SearchProjectVC"];
        [self.navigationController pushViewController:svc animated:YES];
    }
}

#pragma mark - UITextField Design
-(void) paddingForTextField:(UITextField *)textField {
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(30, 0, 10, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    textField.layer.masksToBounds = NO;
    textField.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    textField.layer.shadowRadius = 0.5;
    textField.layer.shadowOpacity = 0.2;
    textField.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    
    textField.layer.borderWidth = 1.0;
    textField.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
}

-(void)shadowForTextField:(UITextField *)textfield {
    
    UIColor *color = [UIColor blackColor];
    textfield.layer.shadowColor = [color CGColor];
    textfield.layer.shadowRadius = 1.5f;
    textfield.layer.shadowOpacity = 0.2;
    textfield.layer.shadowOffset = CGSizeZero;
    textfield.layer.masksToBounds = NO;
}

#pragma mark - UITextView Design
-(void) paddingForTextView:(UITextView *)textView {
    
//    textView.backgroundColor = [UIColor whiteColor];
//    textView.contentInset = UIEdgeInsetsMake(0, 10, 10, 30);
    textView.contentInset = UIEdgeInsetsMake(3, 5, 10, 5);
    
//    UIColor *color = [UIColor blackColor];
//    textView.layer.shadowColor = [color CGColor];
    
    textView.layer.masksToBounds = NO;
    textView.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    textView.layer.shadowRadius = 1.5;
    textView.layer.shadowOpacity = 0.2;
    textView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
}

-(void)shadowForTextView:(UITextView *)textView {
    
    textView.layer.borderWidth = 1.5;
    textView.layer.borderColor = COLOR_TABLE_HEADER.CGColor;
//    textView.backgroundColor = [UIColor whiteColor];
}

#pragma mark - UITextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    textField.layer.borderColor = COLOR_THEME.CGColor;
    textField.layer.borderWidth = 1.5f;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self paddingForTextField:textField];
    [self shadowForTextField:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITextView Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    textView.layer.borderColor = COLOR_THEME.CGColor;
    textView.layer.borderWidth = 1.5f;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    [self paddingForTextView:textView];
    [self shadowForTextView:textView];
}

#pragma mark - UITextField Validation
-(BOOL) validateEmail:(NSString*) emailString {
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    }
    else
        return YES;
}

#pragma mark - Web Service
-(void)connectionForGetDashboardData {
    
    if ([strClassName isEqualToString:@"VerifyBusinessAddressVC"]) {
        [YXSpritesLoadingView show];
    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_DASHBOARD];
    
    NSDictionary *params = @{@"user_id": [userDefault objectForKey:@"userId"]};
    
    strUserId = [NSString stringWithFormat:@"%@", [userDefault objectForKey:@"userId"]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE FROM BASE CLASS::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            self->dictInfoArray = [[responseObject objectForKey:@"info_array"] objectAtIndex:0];
            self->strShareUrl = [NSString stringWithFormat:@"%@%@", [self->dictInfoArray objectForKey:@"url"], [userDefault objectForKey:@"userId"]];
            UIImage *icon = [[UIImage imageNamed:@"MyProjectNew"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            UIImage *iconSelected = [[UIImage imageNamed:@"MyProjectNew_Selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            
            self->strLongitude = [self->dictInfoArray objectForKey:@"longitude"];
            self->strLatitude = [self->dictInfoArray objectForKey:@"latitude"];
            
            if ([self->strClassName isEqualToString:@"ViewAllReviewVC"]) {
                self.navigationItem.title = [self->dictInfoArray objectForKey:@"company_name"];
            }
            self->iPlanId = [[self->dictInfoArray objectForKey:@"plan_id"] intValue];
            self->iNewLeads = [[self->dictInfoArray objectForKey:@"newLead_count"] intValue];
            self->iTotalNewMsg = [[self->dictInfoArray objectForKey:@"total_msg"] intValue];
            self->iRateFlag = [[self->dictInfoArray objectForKey:@"rate_flag"] intValue];
            self->iRateStatus = [[self->dictInfoArray objectForKey:@"app_rate_status"] intValue];
            
            [userDefault setObject:[self->dictInfoArray objectForKey:@"rate_flag"] forKey:@"rate_flag"];
            [userDefault setObject:[self->dictInfoArray objectForKey:@"total_noti"] forKey:@"badge"];
            
            if (self->iTotalNewMsg > 0) {
                self->viewFooterTab.lblNewMessage.hidden = NO;
            } else {
                self->viewFooterTab.lblNewMessage.hidden = YES;
            }

            if (self->iNewLeads > 0) {
                self->viewFooterTab.lblNewProject.hidden = NO;
            } else {
                self->viewFooterTab.lblNewProject.hidden = YES;
            }
            
//            if (self->iTotalNewMsg > 0) {
//                self->viewFooterTab.imgMessage.image = [UIImage imageNamed:@"MessagesNew"];
//            } else {
//                self->viewFooterTab.imgMessage.image = [UIImage imageNamed:@"Messages"];
//            }
//
//            if (self->iNewLeads > 0) {
//                self->viewFooterTab.imgMyProject.image = [UIImage imageNamed:@"MyProjectNew"];
//            } else {
//                self->viewFooterTab.imgMyProject.image = [UIImage imageNamed:@"MyProject"];
//            }
            
            int iTag = [[userDefault objectForKey:@"footer"] intValue];
            
            [self footerButtonDesignWithTag:iTag];
            self->strUserId = [self->dictInfoArray objectForKey:@"pro_id"];
            [userDefault setObject:[self->dictInfoArray objectForKey:@"plan_id"] forKey:@"plan_id"];
            [userDefault setObject:[self->dictInfoArray objectForKey:@"email"] forKey:@"email"];
            [userDefault setObject:[self->dictInfoArray objectForKey:@"pro_id"] forKey:@"user_id"];
            
//            NSLog(@"INFO DICT:::...%@", dictInfoArray);
            
            self->iPaymentOption = [[self->dictInfoArray objectForKey:@"payment_option"] intValue];
            self->iPremiumStatus = [[self->dictInfoArray objectForKey:@"pro_premium_status"] intValue];
            
            [userDefault setObject:[self->dictInfoArray objectForKey:@"address"] forKey:@"address"];
            [userDefault setObject:[self->dictInfoArray objectForKey:@"city"] forKey:@"city"];
            [userDefault setObject:[self->dictInfoArray objectForKey:@"country_name"] forKey:@"country_name"];
            [userDefault setObject:[self->dictInfoArray objectForKey:@"state"] forKey:@"state"];
            [userDefault setObject:[self->dictInfoArray objectForKey:@"zipcode"] forKey:@"zipcode"];
            [userDefault setObject:[self->dictInfoArray objectForKey:@"latitude"] forKey:@"latitude"];
            [userDefault setObject:[self->dictInfoArray objectForKey:@"longitude"] forKey:@"longitude"];
            
            [userDefault setObject:[NSNumber numberWithInt:self->iPremiumStatus] forKey:@"pro_premium_status"];
            [userDefault setObject:[NSNumber numberWithInt:self->iPaymentOption] forKey:@"payment_option"];
//            [userDefault setObject:[self->dictInfoArray objectForKey:@"profile_img"] forKey:@"profile_img"];
            [userDefault synchronize];
            
            NSLog(@"INFO DICT FROM BASE CLASS:::...%d %d", [[userDefault objectForKey:@"payment_option"] intValue], [[userDefault objectForKey:@"pro_premium_status"] intValue]);
            
            [self->arrAdvViewStatus removeAllObjects];
            [self->arrAdvViewStatus addObjectsFromArray:[responseObject objectForKey:@"adv_array"]];
            
            [self->arrInterstitialAd removeAllObjects];
            [self->arrInterstitialAd addObjectsFromArray:[responseObject objectForKey:@"interstitial_ads_array"]];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TestNotification" object:self];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationLoginSettings" object:self];
        }
        if ([self->strClassName isEqualToString:@"VerifyBusinessAddressVC"]) {
            [YXSpritesLoadingView dismiss];
        }
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForGetAnalytics {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_ANALYTICS];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSDictionary *params = @{@"user_id": [userDefault objectForKey:@"userId"]};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
//            NSDictionary *dictInfo = [responseObject objectForKey:@"info_array"];
            
            UIStoryboard *sbDashboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
//            AnalyticsViewController *avc = [sbDashboard instantiateViewControllerWithIdentifier:@"AnalyticsViewController"];
//            ReviewListVC *avc = [sbDashboard instantiateViewControllerWithIdentifier:@"ReviewListVC"];
            AnalyticsVC *avc = [sbDashboard instantiateViewControllerWithIdentifier:@"AnalyticsVC"];
            avc.dictCartInfo = [[NSMutableDictionary alloc] init];
            [avc.dictCartInfo addEntriesFromDictionary:[responseObject objectForKey:@"info_array"]];
            [self.navigationController pushViewController:avc animated:YES];
        }
        [YXSpritesLoadingView dismiss];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void) connectionForLogOut {
    
    [YXSpritesLoadingView show];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",ROOT_URL, @"app_logout"];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSDictionary *params = @{@"user_id": [userDefault valueForKey:@"userId"],
                             @"ios_status": @"1",
                             };
    
    NSLog(@"%@", strUrl);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:strUrl parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Successfully logged out" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self logOutProRinger];
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            alertController.view.tintColor = COLOR_THEME;
            
        }
        [YXSpritesLoadingView dismiss];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

-(void)loginProRinger {
    
    UIViewController *controller;
    BOOL isFound = NO;
    
    for (controller in self.navigationController.viewControllers) {
        
        if ([controller isKindOfClass:[LoginVC class]]) {
            [self.navigationController popToViewController:controller animated:YES];
            isFound = YES;
            return;
        } else {
            isFound = NO;
        }
    }
    
    if (!isFound) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginVC *lvc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        
        CATransition* transition = [CATransition animation];
        transition.duration = 0.65;
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:lvc animated:NO];
    }
}

-(void)logOutProRinger {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    [userDefault setBool:NO forKey:@"isLoggedIn"];
    [userDefault removeObjectForKey:@"email"];
    [userDefault removeObjectForKey:@"firstName"];
    [userDefault removeObjectForKey:@"lastName"];
    [userDefault removeObjectForKey:@"userId"];
    [userDefault removeObjectForKey:@"date"];
    [userDefault setObject:[NSString stringWithFormat:@"%d", 0] forKey:@"badge"];
    
    self.strUserId = @"";
    
    self.tabBarController.tabBar.hidden = YES;
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HomeVC *lvc = [storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.6;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:NO];
}

-(void) goToProRingerHome {
    
    UIViewController *controller;
    BOOL isFound = NO;
    
    for (controller in self.navigationController.viewControllers) {
        
        if ([controller isKindOfClass:[HomeVC class]]) {
            [self.navigationController popToViewController:controller animated:YES];
            isFound = YES;
            return;
        } else {
            isFound = NO;
        }
    }
    
    if (!isFound) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        HomeVC *hvc = [storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
        
        CATransition* transition = [CATransition animation];
        transition.duration = 0.65;
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:hvc animated:NO];
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)emailSupport {
    
    NSString *emailTitle = @"Feedback";
    // Email Content
    NSString *messageBody = @"";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"feedback@proringer.com"];
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        [self presentViewController:mc animated:YES completion:nil];
    }
}

-(void)provideFeedback {
    NSString *emailTitle = @"Support";
    // Email Content
    NSString *messageBody = @"";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"support@proringer.com"];
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        [self presentViewController:mc animated:YES completion:nil];
    }
}

#pragma mark - Arrays
// Check to see if arrays contain the same elements, not necessarily in the same order
// This is different from [array isEqualToArray:responseKeys] which demands the same order in both arrays
// ## Does not compensate for duplicate entries in an array

-(NSMutableArray *)doArraysContainTheSameObjects:(NSArray *)firstArray {
    
//    [arrKhistiFound removeAllObjects];
    
    NSMutableArray *mArray = [[NSMutableArray alloc] init];
    for (id myObject in firstArray) {
        NSString *lower = [myObject lowercaseString];
        if ([appDelegate.arrKhistiList containsObject:lower]) {
            [mArray addObject:myObject];
        }
    }
    return mArray;
}

@end
