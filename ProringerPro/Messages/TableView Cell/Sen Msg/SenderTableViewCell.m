//
//  SenderTableViewCell.m
//  ProRinger
//
//  Created by Soma Halder on 14/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import "SenderTableViewCell.h"

@implementation SenderTableViewCell
@synthesize viewBackground, imgUser, lblMessage;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    imgUser.layer.borderWidth = 2.0;
//    imgUser.layer.borderColor = [UIColor whiteColor].CGColor;
    
    viewBackground.layer.cornerRadius = 5.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
