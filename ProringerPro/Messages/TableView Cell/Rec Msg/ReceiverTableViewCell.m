//
//  ReceiverTableViewCell.m
//  ProRinger
//
//  Created by Soma Halder on 14/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import "ReceiverTableViewCell.h"

@implementation ReceiverTableViewCell
@synthesize viewBackgroundLabel, imgUser, lblMessage;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    viewBackgroundLabel.backgroundColor = [UIColor colorWithRed:241.0f/255 green:89.0f/255 blue:42.0f/255 alpha:1];
//    imgUser.layer.borderWidth = 2.0;
//    imgUser.layer.borderColor = [UIColor colorWithRed:241.0f/255 green:89.0f/255 blue:42.0f/255 alpha:1].CGColor;
    
    lblMessage.textColor = [UIColor whiteColor];
    viewBackgroundLabel.layer.cornerRadius = 5.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
