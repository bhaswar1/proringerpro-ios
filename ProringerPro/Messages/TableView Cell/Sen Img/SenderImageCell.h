//
//  SenderImageCell.h
//  ProRinger
//
//  Created by Soma Halder on 14/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>

//@protocol didSelectImageViewDelegate <NSObject>
//
//-(void)didSelectImageView:(NSInteger)selectedSection withRow:(NSInteger)selectedRow;
//
//@end

NS_ASSUME_NONNULL_BEGIN

@interface SenderImageCell : UITableViewCell <UIGestureRecognizerDelegate>

//@property (weak, nonatomic) id <didSelectImageViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imgUser;

@property (weak, nonatomic) IBOutlet UIImageView *imgMessage;

@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (weak, nonatomic) IBOutlet UIView *viewBackgroundImage;
@property (weak, nonatomic) IBOutlet UIView *viewBackgroundLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constImageHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constImageWidth;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *longPressed;

//-(void)handleLongPressedGesture: (UIImageView *)imageView;
-(void)handleLongPressedGesture:(UITapGestureRecognizer *)recognizer;

@end

NS_ASSUME_NONNULL_END
