//
//  SenderImageCell.m
//  ProRinger
//
//  Created by Soma Halder on 14/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import "SenderImageCell.h"

@implementation SenderImageCell
@synthesize imgUser, viewBackgroundImage, viewBackgroundLabel, lblMessage, imgMessage, longPressed;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    imgUser.layer.borderWidth = 2.0;
//    imgUser.layer.borderColor = [UIColor whiteColor].CGColor;
    
    imgMessage.layer.borderWidth = 2.0;
    imgMessage.layer.borderColor = [UIColor whiteColor].CGColor;
    
//    viewBackgroundImage.backgroundColor = [UIColor clearColor];
    
    imgMessage.userInteractionEnabled = YES;
    
//    longPressed = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressedGesture:)];
//    
//    [imgMessage addGestureRecognizer:longPressed];
    
    viewBackgroundLabel.layer.cornerRadius = 5.0;
    viewBackgroundImage.layer.cornerRadius = 5.0;
}
/*
-(void)handleLongPressedGesture:(UITapGestureRecognizer *)recognizer {
    
    UIImage *snapshot = imgMessage.image;
    
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetChangeRequest *changeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:snapshot];
        changeRequest.creationDate = [NSDate date];
    } completionHandler:^(BOOL success, NSError *error) {
        if (success) {
            NSLog(@"successfully saved");
            [[[UIAlertView alloc] initWithTitle:@"" message:@"successfully saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        else {
            NSLog(@"error saving to photos: %@", error);
        }
    }];
}
*/
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
