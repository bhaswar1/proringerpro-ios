//
//  UserNotificationTableViewCell.m
//  ProRinger
//
//  Created by Soma Halder on 18/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import "UserNotificationTableViewCell.h"

@implementation UserNotificationTableViewCell
@synthesize viewNotification;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    viewNotification.backgroundColor = [UIColor whiteColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
