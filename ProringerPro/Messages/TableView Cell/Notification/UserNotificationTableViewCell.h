//
//  UserNotificationTableViewCell.h
//  ProRinger
//
//  Created by Soma Halder on 18/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserNotificationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@property (weak, nonatomic) IBOutlet UIView *viewNotification;

@end

NS_ASSUME_NONNULL_END
