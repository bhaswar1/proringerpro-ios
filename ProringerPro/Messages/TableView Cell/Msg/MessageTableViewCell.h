//
//  MessageTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 01/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constUnreadHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constStatusLead;

@property (weak, nonatomic) IBOutlet UIImageView *imgProject;

@property (weak, nonatomic) IBOutlet UILabel *lblProjectName;
@property (weak, nonatomic) IBOutlet UILabel *lblNew;
@property (weak, nonatomic) IBOutlet UILabel *lblProjectStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

-(void)configureCellWith:(NSDictionary *)dictProject;

@end

NS_ASSUME_NONNULL_END
