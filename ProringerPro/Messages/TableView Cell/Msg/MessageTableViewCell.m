//
//  MessageTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 01/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "MessageTableViewCell.h"

@implementation MessageTableViewCell
@synthesize constUnreadHeight, imgProject, lblNew, lblDate, lblProjectName, lblProjectStatus, constStatusLead;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
}

-(void)configureCellWith:(NSDictionary *)dictProject {
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd-YYYY"];
    NSString *today = [formatter stringFromDate:date];
    
    if ([[dictProject objectForKey:@"project_applied_date"] isEqualToString:today]) {
        lblDate.text = @"Today";
    } else {
        lblDate.text = [dictProject objectForKey:@"project_applied_date"];
    }
    
    lblProjectName.text = [dictProject objectForKey:@"category_name"];
    
    [imgProject sd_setImageWithURL:[dictProject objectForKey:@"proj_image"]];
    
    if ([[dictProject objectForKey:@"read_status"] intValue] == 1) {
        constStatusLead.constant = constUnreadHeight.constant = 5.0;
        lblNew.text = @" NEW ";
    } else {
        constStatusLead.constant = constUnreadHeight.constant = 0.0;
        lblNew.text = @"";
    }
    
    if ([[dictProject objectForKey:@"project_applied_status"] isEqualToString:@"Y"]) {
        lblProjectStatus.text = @"Pending";
        lblProjectStatus.textColor = COLOR_THEME;
    } else if ([[dictProject objectForKey:@"project_applied_status"] isEqualToString:@"A"]) {
        lblProjectStatus.text = @"Accepted";
        lblProjectStatus.textColor = COLOR_GREEN;
    } else if ([[dictProject objectForKey:@"project_applied_status"] isEqualToString:@"AW"]) {
        lblProjectStatus.text = @"Awaiting Response";
        lblProjectStatus.textColor = COLOR_BLUE;
    } else if ([[dictProject objectForKey:@"project_applied_status"] isEqualToString:@"F"]) {
        lblProjectStatus.text = @"Completed";
        lblProjectStatus.textColor = COLOR_BLUE;
    } else if ([[dictProject objectForKey:@"project_applied_status"] isEqualToString:@"R"]) {
        lblProjectStatus.text = @"Closed";
        lblProjectStatus.textColor = [UIColor lightGrayColor];
    }
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
