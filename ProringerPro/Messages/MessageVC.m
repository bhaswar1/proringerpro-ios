//
//  MessageVC.m
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "MessageVC.h"
#import "MessageTableViewCell.h"

@interface MessageVC () <UISearchBarDelegate>
{
    int iStartForm, iNextData, iSenderTag, iHireRateFlag;
    NSMutableArray *arrProjectMessage;
}

@end

@implementation MessageVC
@synthesize tblMessageList, strProjectId, searchBar, lblNoMsg, viewPopUp, viewBackground, lblRemainingLeads;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon"]];
//    self.tabBarController.tabBar.hidden = NO;
    
//    [self.view addGestureRecognizer:self.panGesture];
    
    viewBackground.hidden = viewPopUp.hidden = YES;
    
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.viewFooterTab.lblNewProject.backgroundColor = self.viewFooterTab.lblNewMessage.backgroundColor = COLOR_THEME;
    self.viewFooterTab.lblNewMessage.layer.borderColor = self.viewFooterTab.lblNewProject.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewFooterTab.lblNewProject.layer.borderWidth = self.viewFooterTab.lblNewMessage.layer.borderWidth = 0.5;
    
    [self.view addSubview:self.viewFooterTab];
    
    iNextData = iStartForm = 0;
    
    arrProjectMessage = [[NSMutableArray alloc] init];
    
    tblMessageList.dataSource = self;
    tblMessageList.delegate = self;
    searchBar.delegate = self;
    
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    [tblMessageList registerNib:[UINib nibWithNibName:@"MessageTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellProjectMessage"];
    
    [self connectionForGetDashboardData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
    
    [self footerButtonTap:3];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [arrProjectMessage removeAllObjects];
    [tblMessageList reloadData];

    self.tabBarController.tabBar.hidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    
    viewBackground.hidden = viewPopUp.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshScroll) name:@"reload_data" object:nil];
    iNextData = iStartForm = 0;
    arrProjectMessage = [[NSMutableArray alloc] init];
    [arrProjectMessage removeAllObjects];
    [tblMessageList reloadData];
    
//    [self connectionForGetDashboardData];
    [self connectionForGetMessageList];
    
    NSInteger iTag = [[[NSUserDefaults standardUserDefaults] objectForKey:@"footer"] integerValue];
    [self footerButtonDesignWithTag:iTag];
    [self footerButtonTap:3];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
     iNextData = iStartForm = 0;
    [arrProjectMessage removeAllObjects];
    [tblMessageList reloadData];
    
    viewBackground.hidden = viewPopUp.hidden = YES;
    
    [self connectionForGetDashboardData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(counterUpdate) name:@"reload_data" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) counterUpdate {
    
    iNextData = iStartForm = 0;
    
    arrProjectMessage = [[NSMutableArray alloc] init];
    [self connectionForGetDashboardData];
    [self connectionForGetMessageList];
}

-(void)MessageNotificationTap {
    
    [self.appDelegate MessageNotificationTap];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
//    self.tabBarController.tabBar.hidden = YES;
}

- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    adView.alpha = 0;
    [UIView animateWithDuration:1.0 animations:^{
        adView.alpha = 1;
//        [self addBannerViewToView:self.bannerView];
    }];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSDate *datePrevious = [userDefault objectForKey:@"date"];
    NSDate *today = [NSDate date];
    
    [userDefault synchronize];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    if (self->iHireRateFlag == 1) {
        if (self.iRateStatus == 0) {
            [[AppManager sharedDataAccess] showAlertWithTitle];
        } else if (self.iRateStatus == 2) {
            if (datePrevious == nil) {
                [[AppManager sharedDataAccess] showAlertWithTitle];
            } else {
                NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:datePrevious toDate:today options:0];
                NSLog(@"Remaining days:::...%ld", [components day]);
                
                if ([components day] > 5) {
                    [[AppManager sharedDataAccess] showAlertWithTitle];
                }
            }
        }
    }
    
    NSLog(@"TOTAL NEW MESSAGE:::...%d", self.iTotalNewMsg);
    
    lblRemainingLeads.text = [self.dictInfoArray objectForKey:@"no_of_leads"];
    
    if (self.iTotalNewMsg > 0) {
        self.viewFooterTab.lblNewMessage.hidden = NO;
    } else {
        self.viewFooterTab.lblNewMessage.hidden = YES;
    }
    
    if (self.iNewLeads > 0) {
        self.viewFooterTab.lblNewProject.hidden = NO;
    } else {
        self.viewFooterTab.lblNewProject.hidden = YES;
    }
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Message"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
}

-(void)refreshScroll {
    iStartForm = 0;
    
    arrProjectMessage = [NSMutableArray array];
    
    iNextData = 1;
    
    [self connectionForGetMessageList];
}

#pragma mark - UINavBar Button
-(void)rightBarButtonPressed:(id)sender {
    
    NSLog(@"Right button pressed in base class");
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchProjectVC *svc = [storyboard instantiateViewControllerWithIdentifier:@"SearchProjectVC"];
    [self.navigationController pushViewController:svc animated:YES];
}

#pragma mark - UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrProjectMessage.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellProjectMessage";
    
    MessageTableViewCell *cell = (MessageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell configureCellWith:[arrProjectMessage objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 90.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dictMessage = [arrProjectMessage objectAtIndex:indexPath.row];
    //https://www.proringer.com/app_pro_project_message?user_id=82&project_id=1749&start_from=0&per_page=30&
    NSLog(@"DICTIONARY FROM SELECTED ROW:::...%@", dictMessage);
    
    iSenderTag = indexPath.row;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    if ([[dictMessage objectForKey:@"read_status"] intValue] == 1) {
        if (self.iPremiumStatus == 0) {
            if ([[dictMessage objectForKey:@"activeProject_Status"] intValue] == 0) {
                viewBackground.hidden = viewPopUp.hidden = NO;
                lblRemainingLeads.text = [dictMessage objectForKey:@"no_of_leads"];
            } else {
                int iBadge = [[userDefault objectForKey:@"badge"] intValue];
                if (iBadge > 0) {
                    iBadge = iBadge -1;
                }
                [userDefault setObject:[NSString stringWithFormat:@"%d", iBadge] forKey:@"badge"];
                [userDefault synchronize];
                
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:iBadge];
                
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
                MessageDetailsVC *mdvc = [storyBoard instantiateViewControllerWithIdentifier:@"MessageDetailsVC"];
                mdvc.strProjectId = [dictMessage objectForKey:@"proj_id"];
                mdvc.isFromProject = NO;
                [self.navigationController pushViewController:mdvc animated:YES];
            }
        } else {
            int iBadge = [[userDefault objectForKey:@"badge"] intValue];
            if (iBadge > 0) {
                iBadge = iBadge -1;
            }
            [userDefault setObject:[NSString stringWithFormat:@"%d", iBadge] forKey:@"badge"];
            [userDefault synchronize];
            
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:iBadge];
            
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
            MessageDetailsVC *mdvc = [storyBoard instantiateViewControllerWithIdentifier:@"MessageDetailsVC"];
            mdvc.strProjectId = [dictMessage objectForKey:@"proj_id"];
            mdvc.isFromProject = NO;
            [self.navigationController pushViewController:mdvc animated:YES];
        }
    } else {
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
        MessageDetailsVC *mdvc = [storyBoard instantiateViewControllerWithIdentifier:@"MessageDetailsVC"];
        mdvc.strProjectId = [dictMessage objectForKey:@"proj_id"];
        mdvc.isFromProject = NO;
        [self.navigationController pushViewController:mdvc animated:YES];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict = [arrProjectMessage objectAtIndex:indexPath.row];
  NSString *deleteString = [NSString stringWithFormat:@"\n\n%@\n%@", @"Delete", @"Conversation"];
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Conversati" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        UIAlertController *deleteAlert = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to delete permanently to remove conversation?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * alert_action){
            
            [tableView beginUpdates];
            
            [self->tblMessageList deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            [self->arrProjectMessage removeObjectAtIndex:indexPath.row];
            [self connectionForDeleteMessage:[dict objectForKey:@"proj_id"]];
            [tableView endUpdates];
            
            if (self->arrProjectMessage > 0) {
                self->lblNoMsg.hidden = YES;
            } else {
                self->lblNoMsg.hidden = NO;
            }
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        }];
        
        [deleteAlert addAction:cancelAction];
        [deleteAlert addAction:okAction];
        [self presentViewController:deleteAlert animated:YES completion:nil];
        deleteAlert.view.tintColor = COLOR_THEME;
    }];
    
    MessageTableViewCell *commentCell = (MessageTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    CGFloat height = commentCell.frame.size.height;
    
    UIImage *backgroundImage = [self deleteImageForHeight:height];
    
    
    deleteAction.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"DeleteConversation"]];
//     deleteAction.backgroundColor= [UIColor colorWithRed:241.0f/255 green:89.0f/255 blue:42.0f/255 alpha:1];
//    deleteAction.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    [[UIButton appearance] setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    return @[deleteAction];
}
/*
- (UISwipeActionsConfiguration *)tableView:(UITableView *)tableView trailingSwipeActionsConfigurationForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIContextualAction *cntextualAction = [UIContextualAction contextualActionWithStyle:UIContextualActionStyleNormal title:nil handler:^(UIContextualAction *action, __kindof UIView *sourceView, void (^ _Nonnull completionHandler)(BOOL)) {
        NSLog(@"DELETE BUTTON PRESSED");
    }];
    
    cntextualAction.image = [UIImage imageNamed:@"DeleteConversation"];
    
    return cntextualAction;
}


- (UISwipeActionsConfiguration *)tableView:(UITableView *)tableView trailingSwipeActionsConfigurationForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (@available(iOS 11.0, *)) {
        UIContextualAction *action = [UIContextualAction contextualActionWithStyle:UIContextualActionStyleNormal title:@"Files" handler:^(UIContextualAction *action, __kindof UIView *sourceView, void (^ _Nonnull completionHandler)(BOOL)) {
            
            return completionHandler;
        }];
    } else {
        // Fallback on earlier versions
    }
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewCellEditingStyleDelete;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict = [arrProjectMessage objectAtIndex:indexPath.row];
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self->arrProjectMessage removeObjectAtIndex:indexPath.row];
        [self->tblMessageList deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self connectionForDeleteMessage:[dict objectForKey:@"proj_id"]];
        [tblMessageList reloadData];
    }
}
*/
- (UIImage*)deleteImageForHeight:(CGFloat)height{
    
    CGRect frame = CGRectMake(0, 0, 60, height);
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(60, height), NO, [UIScreen mainScreen].scale);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [UIColor redColor].CGColor);
    CGContextFillRect(context, frame);
    
    UIImage *image = [UIImage imageNamed:@"CrossRound"];
    
    [image drawInRect:CGRectMake(frame.size.width/2.0, frame.size.height/2.0 - 10, 18, 20)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

/*
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat height = scrollView.frame.size.height;
    CGFloat contentYoffset = scrollView.contentOffset.y;
    CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
    
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 10;
    if(y > h + reload_distance) {
        NSLog(@"load more rows");
    }
    
    if (distanceFromBottom < height) {
//        NSLog(@"you reached end of the table");
        if (iNextData == 1) {
            [self connectionForGetMessageList];
        }
    }
}
*/

#pragma mark - UIScrollView Delegate
//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//    
//    CGFloat height = scrollView.frame.size.height;
//    CGFloat contentYoffset = scrollView.contentOffset.y;
//    CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
//    
//    if (distanceFromBottom == height) {
//        NSLog(@"you reached end of the table");
//        if (iNextData == 1) {
//            [self connectionForGetMessageList];
//        }
//    }
//}



- (void)scrollViewDidScroll: (UIScrollView *)scrollView {
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
//    NSLog(@"scrollViewDidScroll");
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 10.0) {
//        [self methodThatAddsDataAndReloadsTableView];
        NSLog(@"scrollViewDidScroll in IF condition");
        if (iNextData == 1) {
            [self connectionForGetMessageList];
        }
    }
}

#pragma mark - UISearchBar Delegate
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    [self connectionForGetMessageList];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
    [self connectionForGetMessageList];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Web Service
-(void)connectionForGetMessageList {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_MESSAGE_LIST];
    
    if ([strProjectId isEqual:[NSNull null]] || strProjectId == nil) {
        strProjectId = @"";
    }
    
    NSLog(@"USER ID FROM MSG DETAILS:::...%@", self.strUserId);
    
    NSDictionary *param = @{@"user_id": self.strUserId,
                             @"project_id": strProjectId,
                             @"list_search": searchBar.text,
                             @"start_from": [NSString stringWithFormat:@"%d", iStartForm],
                             @"per_page": @"10"};
    
    NSLog(@"PARAMETER FROM MSG DETAILS:::...%@", param);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            if (self->iStartForm == 0) {
                [self->arrProjectMessage removeAllObjects];
            }
            
            [self->arrProjectMessage addObjectsFromArray:[responseObject objectForKey:@"info_array"]];
            
            self->iHireRateFlag = [[responseObject objectForKey:@"msg_rate_flag"] intValue];
            
            
            
            self->iNextData = [[responseObject objectForKey:@"next_data"] intValue];
            if (self->iNextData == 1) {
                self->iStartForm = self->iStartForm + 10;
            }
            
//            if ([[responseObject objectForKey:@"msg_rate_flag"] intValue] == 1) {
//                [[AppManager sharedDataAccess] showAlertWithTitle];
//            }
            
            if (self->arrProjectMessage.count > 0) {
                [self->tblMessageList reloadData];
                self->lblNoMsg.hidden = YES;
            } else {
                self->lblNoMsg.hidden = NO;
            }
        }
        
        self.appDelegate.MessageNotificationgesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MessageNotificationTap)];
        [self.appDelegate.notificationView addGestureRecognizer:self.appDelegate.MessageNotificationgesture];
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForDeleteMessage:(NSString *)projectId {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_MESSAGE_DELETE];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"],
                             @"project_id": projectId};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:[responseObject objectForKey:@"message"]];
            [self connectionForGetDashboardData];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

#pragma mark - UIButton Action
- (IBAction)ClosedButtonPressed:(id)sender {
    
    viewPopUp.hidden = viewBackground.hidden = YES;
}

- (IBAction)ShowMessage:(id)sender {
    
    if (self.arrInterstitialAd.count > 0) {
        for (int i = 0; i < self.arrInterstitialAd.count; i++) {
            NSDictionary *dict = [self.arrInterstitialAd objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Direct Lead see message"]) {
                if ([[dict objectForKey:@"ios_interstitial_ads"] intValue] == 1) {
                    if (self.interstitial.isReady) {
                        [self.interstitial presentFromRootViewController:self];
                    }
                }
            }
        }
    }
    
    NSDictionary *dict = [arrProjectMessage objectAtIndex:iSenderTag];
    
    if ([lblRemainingLeads.text intValue] > 0) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        int iBadge = [[userDefault objectForKey:@"badge"] intValue];
        if (iBadge > 0) {
            iBadge = iBadge -1;
        }
        [userDefault setObject:[NSString stringWithFormat:@"%d", iBadge] forKey:@"badge"];
        [userDefault synchronize];
        
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:iBadge];
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
        MessageDetailsVC *mdvc = [storyBoard instantiateViewControllerWithIdentifier:@"MessageDetailsVC"];
        mdvc.strProjectId = [dict objectForKey:@"proj_id"];
        mdvc.isFromProject = NO;
        [self.navigationController pushViewController:mdvc animated:YES];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"SORRY!!!" message:@"You don't have any free leads / responses remaining" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            self->viewBackground.hidden = self->viewPopUp.hidden = YES;
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        [alertController.view setTintColor:COLOR_THEME];
    }
}

- (IBAction)GoPremium:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    PremiumVC *pvc = [storyboard instantiateViewControllerWithIdentifier:@"PremiumVC"];
    [self.navigationController pushViewController:pvc animated:YES];
}

@end
