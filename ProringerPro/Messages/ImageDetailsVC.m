//
//  ImageDetailsVC.m
//  ProRinger
//
//  Created by Soma Halder on 29/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import "ImageDetailsVC.h"

@interface ImageDetailsVC () {
    
    UITapGestureRecognizer *tapGesture;
}

@end

@implementation ImageDetailsVC
@synthesize imgDetails, imageData;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.hidden = YES;
    
    UIImage *image = [[UIImage alloc]initWithData:imageData];
    imgDetails.image = image;
    
    tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapToDismissView)];
    
    [self.view addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if (CGRectContainsPoint(imgDetails.bounds, [touch locationInView:imgDetails])) {
        return NO;
    } else
        return YES;
}

-(void)tapToDismissView {
    
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
//    [self.navigationController popViewControllerAnimated:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)DismissButtonPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:NO];
}
@end
