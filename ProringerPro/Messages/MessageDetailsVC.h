//
//  MessageDetailsVC.h
//  ProRinger
//
//  Created by Soma Halder on 14/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface MessageDetailsVC : ProRingerProBaseVC <UIDocumentPickerDelegate, UIDocumentMenuDelegate, UIDocumentBrowserViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblMessageDetails;

//@property (weak, nonatomic) IBOutlet UIView *viewUserInput;
@property (weak, nonatomic) IBOutlet UIView *viewUserInput;
@property (strong, nonatomic) IBOutlet UIView *viewAccessory;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (assign, nonatomic) BOOL isFromProject;

@property (weak, nonatomic) IBOutlet UITextView *txtChatView;
@property (weak, nonatomic) IBOutlet UITextView *txtChatAccessory;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constUserInputHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constUserInputBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewTop;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (strong, nonatomic) NSString *strTitle, *strProjectId, *strHomeOwnerId;

//@property (weak, nonatomic) IBOutlet UITextView *txtChatView;

@property (weak, nonatomic) IBOutlet UIButton *btnAttachment;
@property (weak, nonatomic) IBOutlet UIButton *btnSendMsg;
@property (weak, nonatomic) IBOutlet UIButton *btnAccessorySendMsg;

@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserImage;
@property (weak, nonatomic) IBOutlet UIView *viewNavigation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constNavBarHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constBackButtonTop;

- (IBAction)AddAttachment:(id)sender;
- (IBAction)SendMessage:(id)sender;
- (IBAction)BackButtonPressed:(id)sender;

@end

NS_ASSUME_NONNULL_END
