//
//  MessageVC.h
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UITableView *tblMessageList;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UILabel *lblNoMsg;

@property (strong, nonatomic) NSString *strProjectId;

@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIView *viewPopUp;

@property (weak, nonatomic) IBOutlet UILabel *lblRemainingLeads;

- (IBAction)ClosedButtonPressed:(id)sender;
- (IBAction)ShowMessage:(id)sender;
- (IBAction)GoPremium:(id)sender;
@end

NS_ASSUME_NONNULL_END
