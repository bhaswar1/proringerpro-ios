//
//  ImageDetailsVC.h
//  ProRinger
//
//  Created by Soma Halder on 29/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageDetailsVC : UIViewController <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imgDetails;
@property (strong, nonatomic) NSData *imageData;

- (IBAction)DismissButtonPressed:(id)sender;


@end

NS_ASSUME_NONNULL_END
