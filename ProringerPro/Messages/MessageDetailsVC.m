//
//  MessageDetailsVC.m
//  ProRinger
//
//  Created by Soma Halder on 14/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import "MessageDetailsVC.h"
#import "ReceiverImageCell.h"
#import "ReceiverTableViewCell.h"
#import "SenderImageCell.h"
#import "SenderTableViewCell.h"
#import "UserNotificationTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "IQKeyboardManager.h"
#import "ImageDetailsVC.h"
#import "AppDelegate.h"

//#define BORDER_COLOR [UIColor colorWithRed:241.0f/255 green:89.0f/255 blue:42.0f/255 alpha:1];

@interface MessageDetailsVC () {
    
    NSMutableArray *arrMessage, *arrRestrictedWordsFound;
    
    UIRefreshControl *refreshController;
    NSData *imageData;
    int iStartForm, iNextData, iCountProsReviewRating, iHireRateFlag;
    NSString *strApplyJobStatus, *strTimeZone;
    UIDocumentPickerViewController *docPicker;
    UIImagePickerController *imagePicker;
    NSString *uploadType, *strHomeDeleteStatus;
    NSURL *urlPDF;
    AppDelegate *appDelegate;
    
    UIImage *chosenImage;
    CGFloat yOffset;
    BOOL isFirstTime, isKeyboardAppeared;
    
    float newImageHeight, newImageWidth;
}

@end

@implementation MessageDetailsVC
@synthesize tblMessageDetails, viewUserInput, constUserInputBottom, constUserInputHeight, strTitle, strProjectId, txtChatView, btnSendMsg, btnAttachment, lblUserName, imgUserImage, constNavBarHeight, viewNavigation, constBackButtonTop, txtChatAccessory, viewAccessory, strHomeOwnerId, constViewTop, lblDate, btnAccessorySendMsg, isFromProject;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    isFirstTime = YES;
    isKeyboardAppeared = NO;
    
    [self connectionForGetDashboardData];
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    
//    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon"]];
    txtChatView.delegate = self;
    txtChatAccessory.delegate = self;
    txtChatView.frame = txtChatAccessory.frame = CGRectMake(10, 50, 200, 44);
    
    self.navigationController.navigationBar.hidden = YES;
    yOffset = 0;
    iStartForm = iNextData = 0;
    
//    if (@available(iOS 11.0, *)) {
//        constNavBarHeight.constant = self.navigationController.navigationBar.frame.size.height + [[[UIApplication sharedApplication] delegate] window].safeAreaInsets.top;
//    } else {
//        // Fallback on earlier versions
//    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height >= 812.0f) {
            constUserInputBottom.constant = 34.0;
        } else {
            constUserInputBottom.constant = 0.0;
        }
    }
    
    viewNavigation.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    viewNavigation.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    viewNavigation.layer.shadowRadius = 2.0f;
    viewNavigation.layer.shadowOpacity = 1.0f;
    viewNavigation.layer.masksToBounds = NO;
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    arrRestrictedWordsFound = [[NSMutableArray alloc] init];
    arrMessage = [[NSMutableArray alloc] init];
    
    imageData = nil;
    
    btnAttachment.hidden = NO;
    btnSendMsg.hidden = NO;
    
    tblMessageDetails.dataSource = self;
    tblMessageDetails.delegate = self;
    tblMessageDetails.backgroundColor = [UIColor clearColor];
    tblMessageDetails.estimatedRowHeight = 66.0;
    tblMessageDetails.rowHeight = UITableViewAutomaticDimension;
    
    [tblMessageDetails registerNib:[UINib nibWithNibName:@"ReceiverImageCell" bundle:nil] forCellReuseIdentifier:@"cellRecImg"];
    [tblMessageDetails registerNib:[UINib nibWithNibName:@"ReceiverTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellRecText"];
    [tblMessageDetails registerNib:[UINib nibWithNibName:@"SenderImageCell" bundle:nil] forCellReuseIdentifier:@"cellSenImg"];
    [tblMessageDetails registerNib:[UINib nibWithNibName:@"SenderTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellSenText"];
    [tblMessageDetails registerNib:[UINib nibWithNibName:@"SenderDocTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellSenderDoc"];
    [tblMessageDetails registerNib:[UINib nibWithNibName:@"ReceiverDocTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellRecDoc"];
    [tblMessageDetails registerNib:[UINib nibWithNibName:@"UserNotificationTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellUserNotification"];
    
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults synchronize];
    
    [self connectionForGetAllMessageList];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshScroll) name:@"reload_data" object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if (iNextData > 0) {
        [tblMessageDetails reloadData];
        
//        refreshController = [[UIRefreshControl alloc] init];
//        [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    }
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSDate *datePrevious = [userDefault objectForKey:@"date"];
    NSDate *today = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    [userDefault synchronize];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//    NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:datePrevious toDate:today options:0];
//    NSLog(@"Remaining days:::...%ld", [components day]);
    
    NSLog(@"RATE FLAG:::...%d\nRATE STATUS:::...%d", self.iRateFlag, self.iRateStatus);
    
    if (iStartForm <= 10) {
        if (self.iRateFlag == 1) {
            if (self.iRateStatus == 0) {
                [[AppManager sharedDataAccess] showAlertWithTitle];
            } else if (self.iRateStatus == 2) {
                if (datePrevious == nil) {
                    [[AppManager sharedDataAccess] showAlertWithTitle];
                } else {
                    NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:datePrevious toDate:today options:0];
                    NSLog(@"Remaining days:::...%ld", (long)[components day]);
                    
                    if ([components day] > 5) {
                        [[AppManager sharedDataAccess] showAlertWithTitle];
                    }
                }
            } /* else if (self.iRateStatus == 3) {
               [[AppManager sharedDataAccess] showAlertWithTitle];
               } */
        }
    }
    
//    [self scrollToBottom];
    
    if (tblMessageDetails.contentSize.height > tblMessageDetails.bounds.size.height) {
        yOffset = tblMessageDetails.contentSize.height - tblMessageDetails.bounds.size.height;
    }
    
    [tblMessageDetails setContentOffset:CGPointMake(0, yOffset) animated:NO];
    [tblMessageDetails setNeedsLayout];
}

-(void)refreshScroll {
    
    iStartForm = iNextData = 0;
    
    arrMessage = [NSMutableArray array];
    
    [self connectionForGetAllMessageList];
}

- (void)scrollToBottom {
    
    NSUInteger sectionCout = 0;
    NSUInteger rowCount = 0;
    
//    if (self->iStartForm > 0) {
    
//    }
    
    [tblMessageDetails reloadData];
    
    if (self->arrMessage.count > 0) {
//        if (self->iStartForm == 0) {
//
//            if ([[[self->arrMessage objectAtIndex:self->arrMessage.count -1] valueForKey:@"info"] count] >= 1) {
//                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[[[self->arrMessage objectAtIndex:self->arrMessage.count -1] objectForKey:@"info"] count] -1 inSection:self->arrMessage.count -1];
//                [self->tblMessageDetails scrollToRowAtIndexPath:indexPath
//                                               atScrollPosition:UITableViewScrollPositionBottom
//                                                       animated:NO];
//            } else {
//                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[[[self->arrMessage objectAtIndex:self->arrMessage.count -1] objectForKey:@"info"] count] inSection:self->arrMessage.count -1];
//                [self->tblMessageDetails scrollToRowAtIndexPath:indexPath
//                                               atScrollPosition:UITableViewScrollPositionBottom
//                                                       animated:NO];
//            }
//        } else {
        
        sectionCout = self->arrMessage.count;
//        NSArray *arrTemp = [[self->arrMessage objectAtIndex:0] valueForKey:@"info"];
//        rowCount = arrTemp.count;
        
            NSArray *arrTemp = [[self->arrMessage objectAtIndex:sectionCout -1] valueForKey:@"info"];
            rowCount = arrTemp.count;
        
        NSLog(@"LAST ROW::::...%@", [[[self->arrMessage objectAtIndex:sectionCout -1] valueForKey:@"info"] objectAtIndex:rowCount -1]);
        
        if (sectionCout > 0) {
            NSIndexPath *topPath = [NSIndexPath indexPathForRow:rowCount -1 inSection:sectionCout -1];
            [self->tblMessageDetails scrollToRowAtIndexPath:topPath
                                           atScrollPosition:UITableViewScrollPositionBottom
                                                   animated:NO];
        }
            
        
//        }
    }
    
    
    CGPoint bottomOffset = CGPointMake(0, tblMessageDetails.contentSize.height - tblMessageDetails.bounds.size.height);
    if ( bottomOffset.y > 0 ) {
        [tblMessageDetails setContentOffset:bottomOffset animated:YES];
    }
    
    /*
    if (tblMessageDetails.contentSize.height > tblMessageDetails.bounds.size.height) {
        yOffset = tblMessageDetails.contentSize.height - tblMessageDetails.bounds.size.height;
    }
    
    [tblMessageDetails setContentOffset:CGPointMake(0, yOffset) animated:NO];
    [tblMessageDetails setNeedsLayout];
     */
}

-(void)handleRefresh:(id)sender {
    
    NSLog (@"Pull To Refresh Method Called");
    
    [self connectionForGetAllMessageList];
    
    [refreshController endRefreshing];
}

#pragma mark - Header button action
-(void)hiredButtonPressed {
    
    if ([strApplyJobStatus isEqualToString:@"A"]) {
        ;
    } else {
//        [self connectionForHirePro];
    }
}

#pragma mark - UITableView DataSource (Count)
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return arrMessage.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[[arrMessage objectAtIndex:section] valueForKey:@"info"] count];
}

#pragma mark - UITableView DataSource
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tblMessageDetails.frame.size.width, 40)];
//    headerView.backgroundColor = [UIColor cyanColor];
    UILabel *headerTitle = [[UILabel alloc]init];//WithFrame:CGRectMake(0, 0, headerView.frame.size.width, headerView.frame.size.height)];
    headerTitle.font = [UIFont fontWithName:@"OpenSans" size:13];
    //      [[arrMessage objectAtIndex:section] objectForKey:@"date"]
    float widthIs =  [[[arrMessage objectAtIndex:section] objectForKey:@"date"] boundingRectWithSize:headerTitle.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:headerTitle.font } context:nil].size.width+20;
    
    headerTitle.frame = CGRectMake(headerView.frame.size.width/2 - widthIs/2, headerView.frame.origin.y, widthIs +5, 20);
    
    headerTitle.backgroundColor = [UIColor colorWithRed:241.0f/255 green:89.0f/255 blue:42.0f/255 alpha:1];
    
    NSString *strCurrentDate = [[arrMessage objectAtIndex:section] objectForKey:@"date"];
    
    if (section == 0) {
        NSLog(@"SECTION NUMBER :::...%ld", (long)section);
    }
    
    if (section > 1 && section < arrMessage.count-1) {
        NSString *strPreviousDate = [[arrMessage objectAtIndex:section -1] objectForKey:@"date"];
        NSString *strNextDate = [[arrMessage objectAtIndex:section +1] objectForKey:@"date"];

        if ([strCurrentDate isEqualToString:strPreviousDate] || [strCurrentDate isEqualToString:strNextDate]) {
            headerTitle.hidden = YES;
        } else {
            headerTitle.hidden = NO;
        }
    }
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd-YYYY"];
    NSString *today = [formatter stringFromDate:date];
//    NSString *yesterday = [formatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:-24*60*60]];
    if ([strCurrentDate isEqualToString:today]) {
        headerTitle.text = @"Today";
    } else {
        headerTitle.text = strCurrentDate;
    }
    
    headerTitle.textAlignment = NSTextAlignmentCenter;
    
    headerTitle.clipsToBounds = YES;
    headerTitle.layer.cornerRadius = 10;
    headerTitle.textColor = [UIColor whiteColor];
    
    [headerView addSubview:headerTitle];
    
    return headerView;
}

//

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdSendText = @"cellSenText";
    static NSString *cellIdSendImage = @"cellSenImg";
    static NSString *cellIdRecvText = @"cellRecText";
    static NSString *cellIdRecvImage = @"cellRecImg";
    static NSString *cellIdNotification = @"cellUserNotification";
    static NSString *cellIdSendDoc = @"cellSenderDoc";
    static NSString *cellIdRecDoc = @"cellRecDoc";
    
    NSDictionary *dictMessage = [[[arrMessage objectAtIndex:indexPath.section] valueForKey:@"info"] objectAtIndex:indexPath.row];
    
    SenderTableViewCell *cellSendText = (SenderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdSendText];// forIndexPath:indexPath];
    SenderImageCell *cellSendImg = (SenderImageCell *)[tableView dequeueReusableCellWithIdentifier:cellIdSendImage];// forIndexPath:indexPath];
    ReceiverTableViewCell *cellRecText = (ReceiverTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdRecvText];// forIndexPath:indexPath];
    ReceiverImageCell *cellRecImg = (ReceiverImageCell *)[tableView dequeueReusableCellWithIdentifier:cellIdRecvImage];// forIndexPath:indexPath];
    UserNotificationTableViewCell *cellNotify = (UserNotificationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdNotification];
    SenderDocTableViewCell *cellSendDoc = (SenderDocTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdSendDoc];
    ReceiverDocTableViewCell *cellRecDoc = (ReceiverDocTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdRecDoc];
    
    //    float fWidth = (300.0/375.0)*[[UIScreen mainScreen] bounds].size.width;// [[UIScreen mainScreen] bounds].size.width -55;
    float fWidth = [[UIScreen mainScreen] bounds].size.width-70;
    
    NSString *strMsg = [dictMessage objectForKey:@"message_info"];
    strMsg = [strMsg stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    strMsg = [strMsg stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
    
    if ([[dictMessage objectForKey:@"msg_system_status"] isEqualToString:@"S"]) {
        
        cellNotify.lblTime.text = [NSString stringWithFormat:@"%@ %@", [dictMessage objectForKey:@"time_status"], [dictMessage objectForKey:@"time_show"]];
        
        cellNotify.lblMessage.text = [dictMessage objectForKey:@"message_info"];
        
        return cellNotify;
        
    } else {
        
        if ([[dictMessage objectForKey:@"sender_id"] isEqualToString:self.strUserId]) {
            
            if ([[dictMessage objectForKey:@"message_type"] isEqualToString:@"0"] || [[dictMessage objectForKey:@"message_type"] isEqualToString:@""]) {
                
                cellRecText.lblDate.text = [dictMessage objectForKey:@"time_show"];
                cellRecText.lblMessage.text = strMsg;
                
                [cellRecText.imgUser sd_setImageWithURL:[NSURL URLWithString:[dictMessage objectForKey:@"usersimage"]] placeholderImage:[UIImage imageNamed:@""]];
                
                return cellRecText;
                
            } else {
                
                if ([[dictMessage objectForKey:@"filetypename"] isEqualToString:@"pdf"]) {
                    
                    if ([[dictMessage objectForKey:@"message_type"] isEqualToString:@"1"]) {
                        
                        cellRecDoc.viewBackgroundLabel.hidden = YES;
                    } else {
                        
                        cellRecDoc.viewBackgroundLabel.hidden = NO;
                    }
                    
                    cellRecDoc.lblDate.text = [dictMessage objectForKey:@"time_show"];
                    cellRecDoc.lblMessage.text = strMsg;
                    cellRecDoc.lblFileName.text = [dictMessage objectForKey:@"other_originalName"];
                    cellRecDoc.lblFileSize.text = [dictMessage objectForKey:@"filesize"];
                    [cellRecDoc.imgUser sd_setImageWithURL:[NSURL URLWithString:[dictMessage objectForKey:@"usersimage"]] placeholderImage:[UIImage imageNamed:@""]];
                    cellRecDoc.btnDownload.tag = indexPath.row;
                    cellRecDoc.btnDownload.restorationIdentifier = [NSString stringWithFormat:@"%ld", (long)indexPath.section];
                    
                    return cellRecDoc;
                    
                } else {
                    
                    if ([[dictMessage objectForKey:@"message_type"] isEqualToString:@"1"]) {
                        
                        cellRecImg.viewBackgroundLabel.hidden = YES;
                        
                    } else {
                        
                        cellRecImg.viewBackgroundLabel.hidden = NO;
                    }
                    
                    cellRecImg.lblDate.text = [dictMessage objectForKey:@"time_show"];
                    cellRecImg.lblMessage.text = strMsg;
                    cellRecImg.longPressed.delegate = self;
                    cellRecImg.imgMessage.tag = indexPath.row;
                    cellRecImg.imgMessage.restorationIdentifier = [NSString stringWithFormat:@"%ld", (long)indexPath.section];
                    cellRecImg.longPressed = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressedGesture:)];
                    
                    [cellRecImg.imgMessage addGestureRecognizer:cellRecImg.longPressed];
                    
                    [cellRecImg.imgUser sd_setImageWithURL:[NSURL URLWithString:[dictMessage objectForKey:@"usersimage"]] placeholderImage:[UIImage imageNamed:@""]];
                    
                    [cellRecImg.imgMessage  sd_setImageWithURL:[NSURL URLWithString:[dictMessage objectForKey:@"msg_attachment"]] placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                        
                        NSLog(@"Size of my Image from receive => %f, %f ", [[cellRecImg.imgMessage image] size].width, [[cellRecImg.imgMessage image] size].height) ;
                        
                        CGFloat imageHeight = cellRecImg.imgMessage.image.size.height;
                        CGFloat imageWidth = cellRecImg.imgMessage.image.size.width;
                        
                        if (imageWidth > fWidth) {
                            
//                            self->newImageWidth = fWidth;
//                            self->newImageHeight = (imageHeight / imageWidth) * self->newImageWidth;
                            
                            self->newImageWidth =imageWidth;
                            
                            while ( self->newImageWidth > fWidth) {
                                self->newImageWidth =  (fWidth/[[UIScreen mainScreen] bounds].size.width)*   self->newImageWidth;
                            }
                            
                            self->newImageHeight = (imageHeight / imageWidth) * self->newImageWidth;
                            
                        } else {
                            
                            self->newImageWidth = imageWidth;
                            self->newImageHeight = imageHeight;
                        }
                        
                        NSLog(@"NEW image width and height: %f == %f", self->newImageWidth, self->newImageHeight);
                        
                        cellRecImg.constImageHeight.constant = self->newImageHeight;
                        cellRecImg.constImageWidth.constant = self->newImageWidth;
                        
                        CGRect frameRecView = cellRecImg.viewBackgroundImage.frame;
                        //                        frameRecView.size.height = self->newImageHeight +10;
                        //                        frameRecView.size.width = self->newImageWidth +10;
                        cellRecImg.viewBackgroundImage.frame = frameRecView;
                        
                        [tableView beginUpdates];
                        [tableView setNeedsLayout];
                        [tableView layoutIfNeeded];
                        [tableView endUpdates];
                    }];
                    
                    return cellRecImg;
                }
            }
            
        } else {
            
            if ([[dictMessage objectForKey:@"message_type"] isEqualToString:@"0"] || [[dictMessage objectForKey:@"message_type"] isEqualToString:@""]) {
                
                cellSendText.lblDate.text = [dictMessage objectForKey:@"time_show"];
                cellSendText.lblMessage.text = [dictMessage objectForKey:@"message_info"];
                
                [cellSendText.imgUser sd_setImageWithURL:[NSURL URLWithString:[dictMessage objectForKey:@"usersimage"]] placeholderImage:[UIImage imageNamed:@""]];
                
                return cellSendText;
                
            } else {
                
                if ([[dictMessage objectForKey:@"filetypename"] isEqualToString:@"pdf"]) {
                    
                    if ([[dictMessage objectForKey:@"message_type"] isEqualToString:@"1"]) {
                        
                        cellSendDoc.viewBackgroundLabel.hidden = YES;
                    } else {
                        
                        cellSendDoc.viewBackgroundLabel.hidden = NO;
                    }
                    cellSendDoc.lblMessage.text = strMsg;
                    cellSendDoc.lblDate.text = [dictMessage objectForKey:@"time_show"];
                    cellSendDoc.lblFileName.text = [dictMessage objectForKey:@"other_originalName"];
                    cellSendDoc.lblFileSize.text = [dictMessage objectForKey:@"filesize"];
                    
                    [cellSendDoc.imgUser sd_setImageWithURL:[NSURL URLWithString:[dictMessage objectForKey:@"usersimage"]] placeholderImage:[UIImage imageNamed:@""]];
                    
                    cellSendDoc.btnDownload.tag = indexPath.row;
                    cellSendDoc.btnDownload.restorationIdentifier = [NSString stringWithFormat:@"%ld", (long)indexPath.section];
                    
                    return cellSendDoc;
                    
                } else {
                    
                    if ([[dictMessage objectForKey:@"message_type"] isEqualToString:@"1"]) {
                        
                        cellSendImg.viewBackgroundLabel.hidden = YES;
                        
                    } else {
                        cellSendImg.viewBackgroundLabel.hidden = NO;
                        
                    }
                    cellSendImg.lblMessage.text = strMsg;
                    cellSendImg.lblDate.text = [dictMessage objectForKey:@"time_show"];
                    cellSendImg.longPressed.delegate = self;
                    cellSendImg.imgMessage.tag = indexPath.row;
                    cellSendImg.imgMessage.restorationIdentifier = [NSString stringWithFormat:@"%ld", (long)indexPath.section];
                    
                    cellSendImg.longPressed = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressedGesture:)];
                    
                    [cellSendImg.imgMessage addGestureRecognizer:cellSendImg.longPressed];
                    
                    [cellSendImg.imgUser sd_setImageWithURL:[NSURL URLWithString:[dictMessage objectForKey:@"usersimage"]] placeholderImage:[UIImage imageNamed:@""]];
                    
                    [cellSendImg.imgMessage sd_setImageWithURL:[NSURL URLWithString:[dictMessage objectForKey:@"msg_attachment"]] placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                        
                        NSLog(@"Size of my Image from Sent => %f, %f ", [[cellSendImg.imgMessage image] size].width, [[cellSendImg.imgMessage image] size].height) ;
                        
                        CGFloat imageHeight = cellSendImg.imgMessage.image.size.height;
                        CGFloat imageWidth = cellSendImg.imgMessage.image.size.width;
                        
                        if (imageWidth > fWidth) {
                            
                            self->newImageWidth =imageWidth;
                            
//                            self->newImageWidth = fWidth;
//                            self->newImageHeight = (imageHeight / imageWidth) * self->newImageWidth;
                            while ( self->newImageWidth > fWidth) {
                                self->newImageWidth =  (fWidth/[[UIScreen mainScreen] bounds].size.width)*   self->newImageWidth;
                            }
                        
                        self->newImageHeight = (imageHeight / imageWidth) * self->newImageWidth;
                        } else {
                            
                            self->newImageWidth = imageWidth;
                            self->newImageHeight = imageHeight;
                        }
                        
                        NSLog(@"NEW image width and height: %f == %f", self->newImageWidth, self->newImageHeight);
                        
                        cellSendImg.constImageHeight.constant = self->newImageHeight;
                        cellSendImg.constImageWidth.constant = self->newImageWidth;
                        
                        CGRect frameSendView = cellSendImg.viewBackgroundImage.frame;
                        //                        frameSendView.size.height = self->newImageHeight +10;
                        //                        frameSendView.size.width = self->newImageWidth +10;
                        cellSendImg.viewBackgroundImage.frame = frameSendView;
                        
                        [tableView beginUpdates];
                        [tableView setNeedsLayout];
                        [tableView layoutIfNeeded];
                        [tableView endUpdates];
                    }];
                    
                    return cellSendImg;
                }
            }
        }
    }
}

-(void)imageLoadFromURL:(NSString*)url withContainer:(UIImageView*)imgView {
   
    NSURL *imageURL = [NSURL URLWithString:url];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            imgView.image = [UIImage imageWithData:imageData];
        });
    });
}

#pragma mark - UITableView Image Action
-(void)handleLongPressedGesture:(UITapGestureRecognizer *)recognizer {
    
//    [YXSpritesLoadingView show];
    
    UIView *view = recognizer.view;
    
//    NSDictionary *dictImage = [arrMessage objectAtIndex:imageView.tag];
    int iSectionNumber = [[NSString stringWithFormat:@"%@", view.restorationIdentifier] intValue];
    NSDictionary *dictImage = [[[arrMessage objectAtIndex:iSectionNumber] valueForKey:@"info"] objectAtIndex:view.tag];
    
    if ([[dictImage objectForKey:@"message_type"] isEqualToString:@"1"] || [[dictImage objectForKey:@"message_type"] isEqualToString:@"2"]) {
        
//        UIImage *snapshot = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dictImage objectForKey:@"msg_attachment"]]]];
        
//        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
//            PHAssetChangeRequest *changeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:snapshot];
//            changeRequest.creationDate = [NSDate date];
//        } completionHandler:^(BOOL success, NSError *error) {
//            if (success) {
//                NSLog(@"successfully saved");
//                
//                [YXSpritesLoadingView dismiss];
//                
////            [[[UIAlertView alloc] initWithTitle:@"" message:@"successfully saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
////                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
////                ImageDetailsVC *idvc = [storyBoard instantiateViewControllerWithIdentifier:@"ImageDetailsVC"];
////                idvc.imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[dictImage objectForKey:@"msg_attachment"]]];
////                [self.navigationController pushViewController:idvc animated:NO];
//            }
//            else {
//                NSLog(@"error saving to photos: %@", error);
//            }
//        }];
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
        ImageDetailsVC *idvc = [storyBoard instantiateViewControllerWithIdentifier:@"ImageDetailsVC"];
        idvc.imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[dictImage objectForKey:@"msg_attachment"]]];
        [self.navigationController pushViewController:idvc animated:NO];
    }
}

#pragma mark - UITableView Delegate (Height)
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 66.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dictMessage = [[[arrMessage objectAtIndex:indexPath.section] valueForKey:@"info"] objectAtIndex:indexPath.row];
    
    if ([[dictMessage objectForKey:@"message_type"] isEqualToString:@"1"] || [[dictMessage objectForKey:@"message_type"] isEqualToString:@"2"]) {
        
//        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
//        ImageDetailsVC *idvc = [storyBoard instantiateViewControllerWithIdentifier:@"ImageDetailsVC"];
//        idvc.imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[dictMessage objectForKey:@"msg_attachment"]]];
//        [self.navigationController pushViewController:idvc animated:NO];
    }
}

#pragma mark - UIScrollView Delegate
//- (void)scrollViewDidScroll: (UIScrollView *)scrollView {
//    // UITableView only moves in one direction, y axis
//    CGFloat currentOffset = scrollView.contentOffset.y;
//    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
//    //    NSLog(@"scrollViewDidScroll");
//    // Change 10.0 to adjust the distance from bottom
//    if (maximumOffset - currentOffset == 0.0) {
//        //        [self methodThatAddsDataAndReloadsTableView];
//        NSLog(@"scrollViewDidScroll in IF condition");
//        if (iNextData == 1) {
//            [self connectionForGetAllMessageList];
//        }
//    }
//}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    CGFloat currentOffset = scrollView.contentOffset.y;
    
    if (currentOffset <= 0) {
        NSLog(@"scrollViewDidScroll in IF condition");
        if (iNextData == 1) {
            isFirstTime = NO;
            [self connectionForGetAllMessageList];
        }
    }
}

#pragma mark - TextView Delegate

- (void)keyboardWillShow:(NSNotification*)notification {
    
    NSDictionary *info = [notification userInfo];
    NSValue* keyboardFrame = [info valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [keyboardFrame CGRectValue].size;
    float keyboardHeight = keyboardSize.height;
    
    if (constUserInputBottom.constant == 0) {
        constUserInputBottom.constant = keyboardHeight +44;
    }
    
//    if (isFirstTime) {
//        [self scrollToBottom];
//    } else {
//        CGSize tableSize = tblMessageDetails.contentSize; [tblMessageDetails setContentOffset:CGPointMake(0, tableSize.height)];
//    }
}

//-(void)keyboardWillHide:(NSNotification*)notification {
//
//    constUserInputBottom.constant = 0;
//
//    tblMessageDetails.scrollsToTop = YES;
//}
//
//- (void)textViewDidChange:(UITextView *)textView {
//
//    // Figure out the frame size of the text in the textView so we know how much to grow the textView's height by.
//    CGRect frame = [textView.text boundingRectWithSize:CGSizeMake(textView.frame.size.width - 10.0, 160)
//                                               options:NSStringDrawingUsesLineFragmentOrigin
//                                            attributes:@{ NSFontAttributeName:textView.font }
//                                               context:nil];
//
//    // Take the height of the text from the frame
//    float textHeight = frame.size.height + 10.0;
//
//    // If you start with a large textView height initally it will shrink when typing begins because
//    // the text height is intially small.  To prevent this, we add the conditional below.  If the textHeight
//    // is greater than the initial height, we know the textHeight has now outgrown the size of the textView and it's time
//    // to increase the textView's height.
//    if (textHeight > 44)
//        [textView setFrame:CGRectMake(textView.frame.origin.x, textView.frame.origin.y, textView.frame.size.width, textHeight)];
//
//}


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    return YES;
}


- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    return YES;
}

// 875081994

- (void)keyboardDidShow:(NSNotification *)notification {
    // Assign new frame to your view
    //    [self.view setFrame:CGRectMake(0, -230, self.view.frame.size.width, self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    //    [self.view endEditing:NO];
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //    scrollView.scrollEnabled = NO;
    NSLog(@"KEYBOARD HEIGHT keyboardDidShow:::...%f", keyboardSize.height);
    
//    KEYBOARD HEIGHT keyboardDidShow:::...271.000000   7+
//    KEYBOARD HEIGHT keyboardDidShow:::...346.000000   XR
//    SCREEN HEIGHT keyboardDidShow:::...896.000000     XR
//    KEYBOARD HEIGHT keyboardDidShow:::...260.000000   8
//    SCREEN HEIGHT keyboardDidShow:::...667.000000     8
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        NSLog(@"SCREEN HEIGHT keyboardDidShow:::...%f", screenSize.height);
        if (screenSize.height == 896.0f) {
//            constUserInputBottom.constant = keyboardSize.height +34;
            if (keyboardSize.height == 346.0) {
                constUserInputBottom.constant = keyboardSize.height +34;
            } else {
                constUserInputBottom.constant = 346;
            }
        } else if (screenSize.height == 667.0f) {
//            constUserInputBottom.constant = keyboardSize.height;
            if (keyboardSize.height == 260.0) {
                constUserInputBottom.constant = keyboardSize.height;
            } else {
                constUserInputBottom.constant = 260;
            }
        } else if (screenSize.height == 736) {
            if (keyboardSize.height == 271.0) {
                constUserInputBottom.constant = keyboardSize.height;
            } else {
                constUserInputBottom.constant = 271;
            }
        } else {
            constUserInputBottom.constant = keyboardSize.height;
        }
    }
    
//    [self scrollToBottom];
    
    CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height + self.scrollView.contentInset.bottom);
    [self.scrollView setContentOffset:bottomOffset animated:NO];

}

-(void)keyboardDidHide:(NSNotification *)notification {
//    [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    //    [self.view endEditing:NO];
    //    scrollView.scrollEnabled = YES;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height >= 812.0f) {
            constUserInputBottom.constant = 34.0;
        } else {
            constUserInputBottom.constant = 0.0;
        }
    }
    
    CGPoint bottomOffset = CGPointMake(0, 0);
    [self.scrollView setContentOffset:bottomOffset animated:NO];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
//    [self scrollToBottom];
    
    CGRect frame = textView.frame;
    frame.size.height = textView.contentSize.height;
    NSLog(@"TEXTVIEW HEIGHT:::...%f", frame.size.height);
    
//    if (textView == txtChatView) {
//        txtChatAccessory.text = txtChatView.text;
//    }
    
    if([text isEqualToString:@""]) {
        
        NSLog(@"%@",textView.text);
        
        if (textView.text.length == 1) {
//            [btnSendMsg setTintColor:COLOR_DARK_BACK];
            [btnSendMsg setImage:[UIImage imageNamed:@"Send"] forState:UIControlStateNormal];
//            [btnAccessorySendMsg setImage:[UIImage imageNamed:@"Send"] forState:UIControlStateNormal];
        }
    } else if ([NSString stringWithFormat:@"%@%@",textView.text,text].length > 0) {
        
//        [btnSendMsg setTintColor:COLOR_THEME];
        [btnSendMsg setImage:[UIImage imageNamed:@"SendActive"] forState:UIControlStateNormal];
//        [btnAccessorySendMsg setImage:[UIImage imageNamed:@"SendActive"] forState:UIControlStateNormal];
    } else {
//        [btnSendMsg setTintColor:COLOR_DARK_BACK];
        [btnSendMsg setImage:[UIImage imageNamed:@"Send"] forState:UIControlStateNormal];
//        [btnAccessorySendMsg setImage:[UIImage imageNamed:@"Send"] forState:UIControlStateNormal];
    }
    
    if(frame.size.height < 120) {
        textView.scrollEnabled = NO;
    } else {
        textView.scrollEnabled = YES;
        frame.size.height = 120;
    }
    textView.frame = frame;
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    isKeyboardAppeared = YES;
    [self scrollToBottom];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    isKeyboardAppeared = NO;
    isFirstTime = NO;
    [arrRestrictedWordsFound removeAllObjects];
    NSArray *arrNewWords = [textView.text componentsSeparatedByString:@" "];
    NSLog(@"WORDS ARRAY FROM TEXTVIEW:::...%@", arrNewWords);
//    arrRestrictedWordsFound = [appDelegate doArraysContainTheSameObjects:arrNewWords];
    
}

-(void)findTimeZoneFromSelectedAddressWithLat:(double)latitude andLon:(double)longitude {
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        
        self->strTimeZone = placemark.timeZone.name;
    }];
}

#pragma mark - Web Service
-(void)connectionForGetAllMessageList {
    
    [YXSpritesLoadingView show];
    
    NSString *strStartForm = [NSString stringWithFormat:@"%i", iStartForm];
    //https://www.proringer.com/app_pro_project_message?user_id=82&project_id=1749&start_from=0&per_page=30&
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_MESSAGE_DETAILS];
    
    NSLog(@"USER ID FROM MSG DETAILS:::...%@", self.strUserId);
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:strProjectId forKey:@"project_id"];
    [userDefault synchronize];
    
    NSDictionary *params = @{@"user_id": self.strUserId,
                             @"project_id": strProjectId,
                             @"list_search": @"",
                             @"start_from": strStartForm,
                             @"per_page": @"10"};
    
    NSLog(@"PARAMETER FROM MSG DETAILS:::...%@", params);
    
    //    NSDictionary *param = @{@"per_page": @"10", @"start_from": strStartForm};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            NSUInteger sectionCout = 0;
            NSUInteger rowCount = 0;
            
            self->iHireRateFlag = [[responseObject objectForKey:@"hire_rate_flag"] intValue];
            
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            
            NSDate *datePrevious = [userDefault objectForKey:@"date"];
            NSDate *today = [NSDate date];
            
            [userDefault synchronize];
            
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//            NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:datePrevious toDate:today options:0];
//            NSLog(@"Remaining days:::...%ld", (long)[components day]);
            
            if (self->iStartForm > 0) {
                
                if (self->iHireRateFlag == 1) {
                    if (self.iRateStatus == 0) {
                        [[AppManager sharedDataAccess] showAlertWithTitle];
                    } else if (self.iRateStatus == 2) {
                        if (datePrevious == nil) {
                            [[AppManager sharedDataAccess] showAlertWithTitle];
                        } else {
                            NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:datePrevious toDate:today options:0];
                            NSLog(@"Remaining days:::...%ld", [components day]);
                            if ([components day] > 5) {
                                [[AppManager sharedDataAccess] showAlertWithTitle];
                            }
                        }
                    }
                }
                
                sectionCout = self->arrMessage.count;
                NSArray *arrTemp = [[self->arrMessage objectAtIndex:0] valueForKey:@"info"];
                rowCount = arrTemp.count;
            } else {
                [self->arrMessage removeAllObjects];
            }
            
            NSArray *arrInfoArray = [responseObject objectForKey:@"info_array"];
            NSMutableArray *arrM = [NSMutableArray new];
//            NSMutableArray *arrInfo = [NSMutableArray new];
            
            for (int i = 0; i < arrInfoArray.count; i++) {
//                NSArray *arrUserList = [[arrInfoArray objectAtIndex:i] objectForKey:@"all_pro_user_list"];
                self->strApplyJobStatus = [[arrInfoArray objectAtIndex:0] objectForKey:@"applyJobStatus"];
                self->iCountProsReviewRating = [[[arrInfoArray objectAtIndex:0] objectForKey:@"count_pros_Reviewrating"] intValue];
                self->strHomeDeleteStatus = [[arrInfoArray objectAtIndex:0] objectForKey:@"home_delete_status"];
                
                if (([self->strApplyJobStatus isEqualToString:@"A"] || [self->strApplyJobStatus isEqualToString:@"P"] || [self->strApplyJobStatus isEqualToString:@"Y"]) && [self->strHomeDeleteStatus isEqualToString:@"N"] && self->iCountProsReviewRating == 0) {
//                    constUserInputHeight.constant = 0;
                    self->btnAttachment.hidden = NO;
                    self->btnSendMsg.hidden = NO;
                } else {
                    
                    self->constUserInputHeight.constant = 0;
                    self->btnAttachment.hidden = YES;
                    self->btnSendMsg.hidden = YES;
                }
                
                [self imageLoadFromURL:[[arrInfoArray objectAtIndex:0] objectForKey:@"homeowner_user_image"] withContainer:self->imgUserImage];
                [self->imgUserImage sd_setImageWithURL:[[arrInfoArray objectAtIndex:0] objectForKey:@"homeowner_user_image"]];
                self->strHomeOwnerId = [[arrInfoArray objectAtIndex:0] objectForKey:@"homeowner_id"];
                
                self->lblUserName.text = [[arrInfoArray objectAtIndex:0] objectForKey:@"homeowner_name"];
                for (NSDictionary *dictList in  arrInfoArray) {
                    NSArray *arrMsgList = [dictList objectForKey:@"msg_list"];
                    
                    for (NSDictionary *dictMsg in arrMsgList) {
                        [arrM addObject:dictMsg];
                    }
                }
            }
            
//      NSArray *reverse = [[arrM reverseObjectEnumerator] allObjects];
            
            
            
            //   NSLog(@"SHOW DETAILS ARRAY:::....%@", arrMessage);
            
            NSRange range = NSMakeRange(0, arrM.count);
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:range];
            
            [self->arrMessage insertObjects:arrM atIndexes:indexSet];
//            [self scrollToBottom];
            
            self->lblDate.text = [NSString stringWithFormat:@"   %@   ", [[self->arrMessage objectAtIndex:0] objectForKey:@"date"]];
            
            NSLog(@"%lu\n%@", (unsigned long)self->arrMessage.count, self->arrMessage);
            
            if (self->arrMessage.count > 1 ) {
                for (int i = 1; i < self->arrMessage.count; i++) {
                    if ([[[self->arrMessage objectAtIndex:i] objectForKey:@"date"] isEqualToString:[[self->arrMessage objectAtIndex:i -1] objectForKey:@"date"]]) {
                        
                        NSMutableArray *arrTemp = [NSMutableArray new];
                        [arrTemp addObjectsFromArray:[[self->arrMessage objectAtIndex:i -1] objectForKey:@"info"]];
                        NSMutableDictionary *dictTemp = [NSMutableDictionary new];
                        [dictTemp addEntriesFromDictionary:[self->arrMessage objectAtIndex:i]];
                        NSMutableArray *arrInfo = [NSMutableArray new];
                        [arrInfo addObjectsFromArray:[dictTemp objectForKey:@"info"]];
                        [arrTemp addObjectsFromArray:arrInfo];
                        [dictTemp removeObjectForKey:@"info"];
                        [dictTemp setObject:arrTemp forKey:@"info"];
                        [self->arrMessage replaceObjectAtIndex:i withObject:dictTemp];
                        [self->arrMessage removeObjectAtIndex:i -1];
                    }
                }
            }
            
            [self->tblMessageDetails reloadData];
            
            if (self->isFirstTime) {
                if (self->arrMessage.count > 0) {
                    if (self->iStartForm == 0) {
                        
                        if ([[[self->arrMessage objectAtIndex:self->arrMessage.count -1] valueForKey:@"info"] count] >= 1) {
                            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[[[self->arrMessage objectAtIndex:self->arrMessage.count -1] objectForKey:@"info"] count] -1 inSection:self->arrMessage.count -1];
                            [self->tblMessageDetails scrollToRowAtIndexPath:indexPath
                                                           atScrollPosition:UITableViewScrollPositionBottom
                                                                   animated:NO];
                        } else {
                            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[[[self->arrMessage objectAtIndex:self->arrMessage.count -1] objectForKey:@"info"] count] inSection:self->arrMessage.count -1];
                            [self->tblMessageDetails scrollToRowAtIndexPath:indexPath
                                                           atScrollPosition:UITableViewScrollPositionBottom
                                                                   animated:NO];
                        }
                    } else {
                        
                        NSArray *arrTemp = [[self->arrMessage objectAtIndex:sectionCout -1] valueForKey:@"info"];
                        rowCount = arrTemp.count;
                        
                        NSIndexPath *topPath = [NSIndexPath indexPathForRow:rowCount -1 inSection:sectionCout -1];
                        [self->tblMessageDetails scrollToRowAtIndexPath:topPath
                                                       atScrollPosition:UITableViewScrollPositionBottom
                                                               animated:NO];
                    }
                }
            }
            
            self->iNextData = [[responseObject objectForKey:@"next_data"] intValue];
            if (self->iNextData == 1) {
                self->iStartForm = self->iStartForm + 10;
//                [self->tblMessageDetails addSubview:self->refreshController];
            } else {
//                [self->refreshController removeFromSuperview];
//                self->refreshController = nil;
            }
            
//            if ([[responseObject objectForKey:@"hire_rate_flag"] intValue] == 1) {
//                [[AppManager sharedDataAccess] showAlertWithTitle];
//            }
            
            double latitude = [self.strLatitude doubleValue];
            double longitude = [self.strLongitude doubleValue];
            
            [self findTimeZoneFromSelectedAddressWithLat:latitude andLon:longitude];
            
//            [self scrollToBottom];
            
            
        }
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void) connectionForSendImageAndMessage:(NSString *)message andType:(NSString *)type {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_MESSAGE_SEND];
    
    NSDictionary *params = @{@"user_id": self.strUserId,
                             @"project_id": strProjectId,
                             @"receiver_users_id": strHomeOwnerId,
                             @"message": message,
                             @"message_type": type,
                             @"ios_mod": @"I",
//                             @"attach_file": @"",
                             @"user_timezone": strTimeZone
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:self->imageData
                                    name:@"attach_file"
                                fileName:@"image.jpg"
                                mimeType:@"image/jpeg"];
    } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [YXSpritesLoadingView dismiss];
        
        NSLog(@"RESPONSE AFTER IMAGE SEND:::...%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] ==1) {
            
            [self->btnSendMsg setImage:[UIImage imageNamed:@"Send"] forState:UIControlStateNormal];
            self->imageData = nil;
            
            if (message.length > 0 || self->txtChatView.text.length > 0) {
                
                self->txtChatView.text = self->txtChatAccessory.text = @"";
                
//                CGRect frame = self->viewUserInput.frame;
//                frame.size.height = 55.5;
//                self->viewUserInput.frame = frame;
//                
//                CGRect textFrame = self->txtChatView.frame;
//                textFrame.size.height = 45;
//                self->txtChatView.frame = textFrame;
            }
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                CGSize screenSize = [[UIScreen mainScreen] bounds].size;
                if (screenSize.height >= 812.0f) {
                    constUserInputBottom.constant = 34.0;
                } else {
                    constUserInputBottom.constant = 0.0;
                }
            }
            
//            [self connectionForGetAllMessageList];
            
            
            NSMutableDictionary *customDict =[[NSMutableDictionary alloc]init];
            NSMutableArray *arrInfo = [NSMutableArray new];
            [arrInfo removeAllObjects];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"MM-dd-YYYY"];
            NSString *today = [formatter stringFromDate:[NSDate date]];
            
            NSString *strCheckingDate = [[self->arrMessage lastObject] objectForKey:@"date"];
            
            if ([strCheckingDate isEqualToString:today]) {
                
                [arrInfo addObjectsFromArray:[[self->arrMessage lastObject] objectForKey:@"info"]];
                [self->arrMessage removeLastObject];
            }
            
            [arrInfo addObject:[responseObject objectForKey:@"info_array"]];
            [customDict setObject:arrInfo forKey:@"info"];
            [customDict setObject:today forKey:@"date"];
            [self->arrMessage addObject:customDict];
            
            NSLog(@"SHOW LATEST MESSAGE:::...%@", [self->arrMessage objectAtIndex:self->arrMessage.count -1]);
            [self->tblMessageDetails reloadData];
            [self->tblMessageDetails setContentOffset:CGPointMake(0, CGFLOAT_MAX)];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[[[self->arrMessage objectAtIndex:self->arrMessage.count -1] valueForKey:@"info"] count] -1 inSection:self->arrMessage.count -1];
            [self->tblMessageDetails scrollToRowAtIndexPath:indexPath
                                   atScrollPosition:UITableViewScrollPositionBottom
                                           animated:YES];
            
            if (self->tblMessageDetails.contentSize.height > self->tblMessageDetails.bounds.size.height) {
                self->yOffset = self->tblMessageDetails.contentSize.height - self->tblMessageDetails.bounds.size.height;
            }
            
            [self->tblMessageDetails setContentOffset:CGPointMake(0, self->yOffset) animated:NO];
            [self->tblMessageDetails setNeedsLayout];
            
            [self connectionForGetDashboardData];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
            
//            [self configureCellAfterSendDataWith:responseObject];
            
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        
        NSLog(@"%@", task.error);
    }];
}

-(void)connectionForSendMessage:(NSString *)message andType:(NSString *)type {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_MESSAGE_SEND];
    
    NSDictionary *params = @{@"user_id": self.strUserId,
                             @"project_id": strProjectId,
                             @"receiver_users_id": strHomeOwnerId,
                             @"message": message,
                             @"message_type": type,
                             @"Ios_mod": @"I",
                             @"attach_file": @"",
                             @"user_timezone": strTimeZone
                             };
    
    NSLog(@"%@", url);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"RESPONSE OBJECT:::...%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {

            [self->btnSendMsg setImage:[UIImage imageNamed:@"Send"] forState:UIControlStateNormal];
            self->imageData = nil;
            
            if (message.length > 0 || self->txtChatView.text.length > 0) {
                
                self->txtChatView.text = self->txtChatAccessory.text = @"";
                
//                CGRect frame = self->viewUserInput.frame;
//                frame.size.height = 55.5;
//                self->viewUserInput.frame = frame;
//
//                CGRect textFrame = self->txtChatView.frame;
//                textFrame.size.height = 45;
//                self->txtChatView.frame = textFrame;
            }
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                CGSize screenSize = [[UIScreen mainScreen] bounds].size;
                if (screenSize.height >= 812.0f) {
                    constUserInputBottom.constant = 34.0;
                } else {
                    constUserInputBottom.constant = 0.0;
                }
            }
            
//            [self connectionForGetAllMessageList];
            
            
            
            NSMutableDictionary *customDict =[[NSMutableDictionary alloc]init];
            NSMutableArray *arrInfo = [NSMutableArray new];
            [arrInfo removeAllObjects];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"MM-dd-YYYY"];
            NSString *today = [formatter stringFromDate:[NSDate date]];
            
            NSString *strCheckingDate = [[self->arrMessage lastObject] objectForKey:@"date"];
            
            if ([strCheckingDate isEqualToString:today]) {
                
                [arrInfo addObjectsFromArray:[[self->arrMessage lastObject] objectForKey:@"info"]];
                [self->arrMessage removeLastObject];
            }
            
            [arrInfo addObject:[responseObject objectForKey:@"info_array"]];
            [customDict setObject:arrInfo forKey:@"info"];
            [customDict setObject:today forKey:@"date"];
            [self->arrMessage addObject:customDict];
            
            NSLog(@"SHOW LATEST MESSAGE:::...%@", [self->arrMessage objectAtIndex:self->arrMessage.count -1]);
            [self->tblMessageDetails reloadData];
            [self->tblMessageDetails setContentOffset:CGPointMake(0, CGFLOAT_MAX)];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[[[self->arrMessage objectAtIndex:self->arrMessage.count -1] valueForKey:@"info"] count] -1 inSection:self->arrMessage.count -1];
            [self->tblMessageDetails scrollToRowAtIndexPath:indexPath
                                           atScrollPosition:UITableViewScrollPositionBottom
                                                   animated:YES];
            
            if (self->tblMessageDetails.contentSize.height > self->tblMessageDetails.bounds.size.height) {
                self->yOffset = self->tblMessageDetails.contentSize.height - self->tblMessageDetails.bounds.size.height;
            }
            
            [self->tblMessageDetails setContentOffset:CGPointMake(0, self->yOffset) animated:NO];
            [self->tblMessageDetails setNeedsLayout];
            
            [self connectionForGetDashboardData];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
            
//            [self configureCellAfterSendDataWith:responseObject];
             
        }
    }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              [YXSpritesLoadingView dismiss];
              NSLog(@"ERROR:::...%@", task.error);
          }];
}

-(void)configureCellAfterSendDataWith:(NSDictionary *)response {
    
    txtChatView.text = txtChatAccessory.text = @"";
    
    CGRect frameTextView = txtChatView.frame;
//    CGRect frameTextView = txtChatAccessory.frame;
    NSLog(@"TEXTVIEW HEIGHT:::...%f", frameTextView.size.height);
    frameTextView.size.height = 35.0;
    txtChatView.frame = txtChatAccessory.frame = frameTextView;
    
    CGRect frameView = viewUserInput.frame;
//    CGRect frameView = viewAccessory.frame;
    NSLog(@"TEXTVIEW HEIGHT:::...%f", frameView.size.height);
    frameView.size.height = 55.0;
    viewUserInput.frame = viewAccessory.frame = frameView;
    
    imageData = nil;
    
    NSMutableDictionary *customDict =[[NSMutableDictionary alloc]init];
    NSMutableArray *arrInfo = [NSMutableArray new];
    [arrInfo removeAllObjects];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd-YYYY"];
    NSString *today = [formatter stringFromDate:[NSDate date]];
    
    NSString *strCheckingDate = [[self->arrMessage lastObject] objectForKey:@"date"];
    
    if ([strCheckingDate isEqualToString:today]) {
        
        [arrInfo addObjectsFromArray:[[self->arrMessage lastObject] objectForKey:@"info"]];
        [self->arrMessage removeLastObject];
    }
    
    [arrInfo addObject:[response objectForKey:@"info_array"]];
    [customDict setObject:arrInfo forKey:@"info"];
    [customDict setObject:today forKey:@"date"];
    [self->arrMessage addObject:customDict];
    
    NSLog(@"SHOW LATEST MESSAGE:::...%@", [arrMessage objectAtIndex:arrMessage.count -1]);
    [tblMessageDetails reloadData];
    [tblMessageDetails setContentOffset:CGPointMake(0, CGFLOAT_MAX)];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[[[arrMessage objectAtIndex:arrMessage.count -1] valueForKey:@"info"] count] -1 inSection:arrMessage.count -1];
    [self->tblMessageDetails scrollToRowAtIndexPath:indexPath
                                   atScrollPosition:UITableViewScrollPositionBottom
                                           animated:YES];
}


/*
-(void)connectionForHirePro {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@homeowner_pro_hired", ROOT_URL];
    
    NSDictionary *params = @{@"user_id": strUserId,
                             @"project_id": strProjectId,
                             @"pro_id": strProUserId
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:[NSSet setWithObjects:@"text/json", @"text/html", @"application/json", nil]];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE OBJECT:::...%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            [self connectionForGetAllMessageList];
        }
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"ERROR:::...%@", task.error);
        [YXSpritesLoadingView dismiss];
    }];
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    ImageDetailsVC *idvc = [segue destinationViewController];
    idvc.imageData = imageData;
    [idvc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.providesPresentationContextTransitionStyle = YES;
    self.definesPresentationContext = YES;
}


#pragma mark - Button Action
- (IBAction)AddAttachment:(id)sender {
    
    [txtChatView resignFirstResponder];
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Select Source" message:@"You need to select source to add your attachment" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"NEED TO OPEN CAMERA");
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            picker.delegate = self;
            picker.allowsEditing = NO;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:^{
                [alertController dismissViewControllerAnimated:YES completion:nil];
            }];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Your device camera is not supported"
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        }
    }];
    
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"NEED TO OPEN PHOTO LIBRARY");
        
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:^{
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
    }];
    
    UIAlertAction *documentsAction = [UIAlertAction actionWithTitle:@"Document" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"NEED TO OPEN DOCUMENT");
        /*
        [self showDocumentPickerInMode:UIDocumentPickerModeOpen];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths firstObject];
        NSLog(@"%@\n%@", paths, documentsDirectory);
         */
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"NEED TO CANCEL ACTION");
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    
    [alertController addAction:cameraAction];
    [alertController addAction:photoAction];
    [alertController addAction:documentsAction];
    [alertController addAction:cancelAction];
    [alertController.view setTintColor:[UIColor colorWithRed:241.0f/255 green:89.0f/255 blue:42.0f/255 alpha:1]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)SendMessage:(id)sender {
    
    [txtChatView resignFirstResponder];
    
    NSString *type = @"";
    NSString *message = txtChatView.text;
    message = [message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([[AppManager sharedDataAccess] isEmptyString:txtChatView.text] && imageData == nil) {
        [[[UIAlertView alloc]initWithTitle:@"" message:@"Please type your message or select image" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    } else {
        
        [arrRestrictedWordsFound removeAllObjects];
        NSArray *arrNewWords = [txtChatView.text componentsSeparatedByString:@" "];
        NSLog(@"WORDS ARRAY FROM TEXTVIEW:::...%@", arrNewWords);
        
        if (arrRestrictedWordsFound.count > 0) {
            NSString *strKhisti = [arrRestrictedWordsFound componentsJoinedByString:@", "];
            [[[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"Invalid term: %@ - Please be respectful of others. Remove and try again", strKhisti] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            [txtChatView becomeFirstResponder];
        } else if ([[AppManager sharedDataAccess] detectType:txtChatView.text]) {
            [[[UIAlertView alloc]initWithTitle:@"" message:self.strRestrictionMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            [txtChatView becomeFirstResponder];
        } else {
            if (![txtChatView.text isEqualToString: @""]) {
                type = @"0";
//                message = txtChatView.text;
            }
            if (imageData != nil) {
                type = @"1";
            }
            if (![txtChatView.text isEqualToString: @""] && imageData != nil) {
                type = @"2";
//                message = txtChatView.text;
            }
            if ([type isEqualToString:@"0"]) {
                [self connectionForSendMessage:message andType:type];
            } else {
                [self connectionForSendImageAndMessage:message andType:type];
            }
        }
    }
}

- (IBAction)BackButtonPressed:(id)sender {
    
    if (isFromProject) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self turnBackToAnOldViewController];
    }
}

- (void)turnBackToAnOldViewController{
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        if ([controller isKindOfClass:[MessageVC class]]) {
            
            [self.navigationController popToViewController:controller animated:YES];
            return;
        } else {
//            [self.navigationController popViewControllerAnimated:NO];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
            MessageVC *lvc = [storyboard instantiateViewControllerWithIdentifier:@"MessageVC"];
            
            CATransition* transition = [CATransition animation];
            transition.duration = 0.5;
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromLeft;
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:lvc animated:NO];
        }
    }
}

#pragma mark - Picker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    chosenImage = info[UIImagePickerControllerOriginalImage];
    imageData = UIImagePNGRepresentation(chosenImage);
    [picker dismissViewControllerAnimated:YES completion:nil];
//    [self resizeImage:chosenImage];
    
//    chosenImage = scaledImage;
    [self SendMessage:nil];
}

-(void)showDocumentPickerInMode:(UIDocumentPickerMode)mode {
    
    UIDocumentMenuViewController *picker = [[UIDocumentMenuViewController alloc]initWithDocumentTypes:@[@"com.adobe.pdf"] inMode:UIDocumentPickerModeImport];
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
    
    [self SendMessage:nil];
}

-(void)documentMenu:(UIDocumentMenuViewController *)documentMenu didPickDocumentPicker:(UIDocumentPickerViewController *)documentPicker {
    
    documentPicker.delegate = self;
    [self presentViewController:documentPicker animated:YES completion:nil];
}

-(void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    
    urlPDF = url;
    uploadType = @"PDF";
    NSLog(@"%@", uploadType);
}

#pragma mark - UIImage Cropper
-(UIImage *)centerCropImage:(UIImage *)image {
    // Use smallest side length as crop square length
    CGFloat squareLength = MIN(image.size.width, image.size.height);
    // Center the crop area
    CGRect clippedRect = CGRectMake((image.size.width - squareLength) / 2, (image.size.height - squareLength) / 2, squareLength, squareLength);
    // Crop logic
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], clippedRect);
    UIImage * croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return croppedImage;
}

#pragma mark - UIImagePicker Delegate
//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
//
//    // output image
//    chosenImage = info[UIImagePickerControllerOriginalImage];
//
//    [picker dismissViewControllerAnimated:YES completion:^{
////        [self openEditor];
//    }];
//}

-(void)openEditor {
    self.cropController.image = chosenImage;
    self.cropController.keepingCropAspectRatio = YES;
    self.cropController.cropAspectRatio = 1.0;
    
    CGFloat ratio = 1.0f / 1.0f;
    CGRect cropRect = self.cropController.cropView.cropRect;
    CGFloat widthCrop = CGRectGetWidth(cropRect);
    cropRect.size = CGSizeMake(widthCrop, widthCrop * ratio);
    self.cropController.cropView.cropRect = cropRect;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.cropController];
    
    [self presentViewController:navigationController animated:YES completion:^{
        [self.cropController clickedButtonAtIndex:1];
    }];
}

- (void)cancelButtonDidPush:(id)sender {
    
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - PECropViewControllerDelegate methods
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
    chosenImage = croppedImage;
    
    //    [self resizeAndConvertImageTodata];
//    [self resizeImage:chosenImage];
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
    chosenImage = croppedImage;
    
    //    [self resizeAndConvertImageTodata];
//    [self resizeImage:chosenImage];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:^{
//        self->imgLicence.image = self->chosenImage;
    }];
}

-(void)resizeAndConvertImageTodata {
    
    imageData = UIImagePNGRepresentation(chosenImage);
//    dataLicenseImage = UIImageJPEGRepresentation(chosenImage, 1.0);
//    dataLicenseImage = [NSData dataWithData:dataLicenseImage];
    NSLog(@"Size of Image(bytes):%lu",(unsigned long)[imageData length]);
    
    NSUInteger iDataSize = [imageData length];
    /*
     if (iDataSize > 4000000) {
     //        [self centerCropImage:chosenImage];
     //        dataLicenseImage = UIImagePNGRepresentation(chosenImage);
     dataLicenseImage = UIImageJPEGRepresentation(chosenImage, 0.6);
     dataLicenseImage = [NSData dataWithData:dataLicenseImage];
     NSLog(@"Size of Image after resize(bytes):%lu",(unsigned long)[dataLicenseImage length]);
     }
     */

}

- (void)resizeImage:(UIImage*)image {
    
    NSData *finalData = nil;
    NSData *unscaledData = UIImagePNGRepresentation(image);
    
    if (unscaledData.length > 500000.0f ) {
        
        //if image size is greater than 5KB dividing its height and width maintaining proportions
        
        UIImage *scaledImage = [self imageWithImage:image andWidth:image.size.width/2 andHeight:image.size.height/2];
        finalData = UIImagePNGRepresentation(scaledImage);
        
        if (finalData.length > 500000.0f ) {
            
            [self resizeImage:scaledImage];
        } else {
            //scaled image will be your final image
            imageData = UIImagePNGRepresentation(scaledImage);
            chosenImage = scaledImage;
//            imgLicence.image = chosenImage;
            [self SendMessage:nil];
        }
    }
//    imgLicence.image = chosenImage;
    
}

- (UIImage*)imageWithImage:(UIImage*)image andWidth:(CGFloat)width andHeight:(CGFloat)height {
    
    UIGraphicsBeginImageContext( CGSizeMake(width, height));
    [image drawInRect:CGRectMake(0,0,width,height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext() ;
    return newImage;
}

@end
