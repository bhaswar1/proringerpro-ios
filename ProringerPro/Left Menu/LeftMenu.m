//
//  LeftMenu.m
//  ProringerPro
//
//  Created by Soma Halder on 19/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "LeftMenu.h"


@implementation LeftMenu
@synthesize tblLeftMenu, delegate, iPremiumStatus, audioPlayer, iPaymentOption;

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self = [[[NSBundle mainBundle] loadNibNamed:@"LeftMenu" owner:self options:nil] objectAtIndex:0];
        account = support = about = NO;
        
        iPremiumStatus = iPaymentOption = 0;
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault synchronize];
        
        iPremiumStatus = [[userDefault objectForKey:@"pro_premium_status"] intValue];
        iPaymentOption = [[userDefault objectForKey:@"payment_option"] intValue];
        
        NSLog(@"INFO DICT FROM VIEW CLASS:::...%d %d", [[userDefault objectForKey:@"payment_option"] intValue], [[userDefault objectForKey:@"pro_premium_status"] intValue]);
        
        tblLeftMenu.frame = CGRectMake(0, 0, 250, self.frame.size.height);
        
        tblLeftMenu.backgroundColor = COLOR_TABLE_HEADER;
        
//        arrAccount = [[NSArray alloc] initWithObjects:@"User Information", @"Company", @"Services", @"License", @"Portfolio", @"Campaign Summary", @"Login Settings", @"Notifications", @"Quick Reply", @"Availability", @"Social Media", @"Payment Methods", @"Transaction History", @"Service Area", @"Business Hours", @"Request Reviews", @"Invite Friends", @"Analytics", @"Share Profile", @"My Profile", @"Log out", nil];
        
        if (iPaymentOption > 0) {
            if (iPremiumStatus > 0) {
                arrAccount = [[NSArray alloc] initWithObjects:
                              @"User Information",
                              @"Company",
                              @"Services",
                              @"Licenses",
                              @"Portfolio",
                              @"Campaign Summary",
                              @"Login Settings",
                              @"Notifications",
                              @"Quick Reply",
                              @"Availability",
                              @"Social Media",
                              @"Payment Methods",
                              @"Transaction History",
                              @"Service Area",
                              @"Business Hours",
                              @"Request Reviews",
                              @"Invite Friends",
                              @"Analytics",
                              @"Share Profile",
                              @"My Profile",
                              @"Log out", nil];
            } else {
                arrAccount = [[NSArray alloc] initWithObjects:@"User Information", @"Company", @"Services", @"Licenses", @"Portfolio", @"Campaign Summary", @"Login Settings", @"Notifications", @"Availability", @"Social Media", @"Payment Methods", @"Transaction History", @"Service Area", @"Business Hours", @"Request Reviews", @"Invite Friends", @"Analytics", @"Share Profile", @"My Profile", @"Log out", nil];
            }
        } else {
            arrAccount = [[NSArray alloc] initWithObjects:@"User Information", @"Company", @"Services", @"Licenses", @"Portfolio", @"Login Settings", @"Notifications", @"Availability", @"Social Media", @"Transaction History", @"Service Area", @"Business Hours", @"Request Reviews", @"Invite Friends", @"Analytics", @"Share Profile", @"My Profile", @"Log out", nil];
        }
        
        arrSupport = [[NSArray alloc] initWithObjects:@"Email Support", @"FAQ", @"Provide Feedback", nil];
        
        arrAbout = [[NSArray alloc] initWithObjects:@"Terms of Service", @"Privacy Policy", nil];
        
        arrHeaderTitle = [[NSArray alloc] initWithObjects:
                          @{@"title": @"Find local Projects", @"imageSelected": @"Search"},
                          @{@"title": @"Account",             @"imageSelected": @"Account"},
                          @{@"title": @"Support",             @"imageSelected": @"Support"},
                          @{@"title": @"About",               @"imageSelected": @"About"},nil];
        
        [tblLeftMenu registerNib:[UINib nibWithNibName:@"LeftMenuTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellLeftMenu"];
        tblLeftMenu.dataSource = self;
        tblLeftMenu.delegate = self;
        [tblLeftMenu reloadData];
    }
    return self;
}

#pragma mark - TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (section == 1) {
        
        if (account) {
            return arrAccount.count;
        } else {
            return 0;
        }
    } else if (section == 2) {
        
        if (support) {
            return arrSupport.count;
        } else {
            return 0;
        }
    } else if (section == 3) {
        
        if (about) {
            return arrAbout.count;
        } else {
            return 0;
        }
    }
    else {
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tblLeftMenu.frame.size.width, 60)];
    
    UIButton *btnHeader = [[UIButton alloc] initWithFrame:CGRectMake(headerView.frame.origin.x, headerView.frame.origin.y, headerView.frame.size.width, headerView.frame.size.height -1)];
    
    UILabel *lblSeparator = [[UILabel alloc] initWithFrame:CGRectMake(headerView.frame.origin.x, btnHeader.frame.size.height, headerView.frame.size.width, 1)];
    lblSeparator.backgroundColor = [UIColor whiteColor];
    
    [headerView addSubview:btnHeader];
    [headerView addSubview:lblSeparator];
    
    [btnHeader addTarget:self action:@selector(headerButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    btnHeader.tag = section;
    NSDictionary *dict = [arrHeaderTitle objectAtIndex:section];
//    UIImage *imgHeader = [UIImage imageNamed:[dict objectForKey:@"imageSelected"]];
    UIImage *imgHeader = [[UIImage imageNamed:[dict objectForKey:@"imageSelected"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    btnHeader.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btnHeader.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    [btnHeader setTitle:[dict objectForKey:@"title"] forState:UIControlStateNormal];
    [btnHeader.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Semibold" size:16.0]];
    [btnHeader setImage:imgHeader forState:UIControlStateNormal];
    btnHeader.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    btnHeader.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    
    if (section == 1) {
        if (account) {
            
            [btnHeader setBackgroundColor:COLOR_DARK_BACK];
            [btnHeader setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            btnHeader.tintColor = [UIColor whiteColor];
            lblSeparator.backgroundColor = COLOR_DARK_BACK;
        } else {
            
            [btnHeader setBackgroundColor:COLOR_TABLE_HEADER];
            [btnHeader setTitleColor:COLOR_DARK_BACK forState:UIControlStateNormal];
            [btnHeader setTintColor:COLOR_DARK_BACK];
            lblSeparator.backgroundColor = [UIColor whiteColor];
        }
    } else if (section == 2) {
        
        if (support) {
            btnHeader.tintColor = [UIColor whiteColor];
            [btnHeader setBackgroundColor:COLOR_DARK_BACK];
            [btnHeader setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            lblSeparator.backgroundColor = COLOR_DARK_BACK;
        } else {
            
            [btnHeader setBackgroundColor:COLOR_TABLE_HEADER];
            [btnHeader setTitleColor:COLOR_DARK_BACK forState:UIControlStateNormal];
            [btnHeader setTintColor:COLOR_DARK_BACK];
            lblSeparator.backgroundColor = [UIColor whiteColor];
        }
    } else if (section == 3) {
        
        if (about) {
            
            btnHeader.tintColor = [UIColor whiteColor];
            [btnHeader setBackgroundColor:COLOR_DARK_BACK];
            [btnHeader setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            lblSeparator.backgroundColor = COLOR_DARK_BACK;
        } else {
            
            [btnHeader setBackgroundColor:COLOR_TABLE_HEADER];
            [btnHeader setTitleColor:COLOR_DARK_BACK forState:UIControlStateNormal];
            [btnHeader setTintColor:COLOR_DARK_BACK];
            lblSeparator.backgroundColor = [UIColor whiteColor];
        }
    } else {
        
        [btnHeader setBackgroundColor:COLOR_TABLE_HEADER];
        [btnHeader setTitleColor:COLOR_DARK_BACK forState:UIControlStateNormal];
        [btnHeader setTintColor:COLOR_DARK_BACK];
    }
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tblLeftMenu.frame.size.width, 60)];
    footerView.backgroundColor = [UIColor clearColor];
    
    return footerView;
}

-(void)headerButtonPressed:(UIButton *)sender {
    
    NSLog(@"SECTION NUMBER:::...%ld", sender.tag);
    
    [delegate ADSideMenuDrawerSelection:sender.tag];
    
    sender.tintColor = [UIColor whiteColor];
    
    if (sender.tag == 0) {
        
        account = support = about = NO;
        
    } else if (sender.tag == 1) {
        if (account) {
            account = support = about = NO;
        } else {
            account = YES;
            support = about = NO;
        }
    } else if (sender.tag == 2) {
        
        if (support) {
            account = support = about = NO;
        } else {
            support = YES;
            account = about = NO;
        }
    } else if (sender.tag == 3) {
        
        if (about) {
            account = support = about = NO;
        } else {
            about = YES;
            account = support = NO;
        }
    }
    
    [UIView transitionWithView: tblLeftMenu
                      duration: 0.45f
                       options: UIViewAnimationOptionTransitionCrossDissolve
                    animations: ^(void) {
                        [self->tblLeftMenu reloadData];
                    }
                    completion: nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if (section == 3) {
        return 60;
    } else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    LeftMenuTableViewCell *cell = (LeftMenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellLeftMenu" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[LeftMenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellLeftMenu"];
//    cell.contentView.backgroundColor = COLOR_DARK_BACK;
//    if (indexPath.section == 1) {
//        cell.lblTitle.text = [arrAccount objectAtIndex:indexPath.row];
//    } else if (indexPath.section == 2) {
//        cell.lblTitle.text = [arrSupport objectAtIndex:indexPath.row];
//    } else if (indexPath.section == 3) {
//        cell.lblTitle.text = [arrAbout objectAtIndex:indexPath.row];
//    }
//    if (indexPath.row == 8) {
//        if (iPremiumStatus > 0) {
//            cell.btnAccessory.hidden = NO;
//        } else {
//            cell.btnAccessory.hidden = YES;
//        }
//    }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(LeftMenuTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 1) {
        cell.lblTitle.text = [arrAccount objectAtIndex:indexPath.row];
    } else if (indexPath.section == 2) {
        cell.lblTitle.text = [arrSupport objectAtIndex:indexPath.row];
    } else if (indexPath.section == 3) {
        cell.lblTitle.text = [arrAbout objectAtIndex:indexPath.row];
    }
    
    cell.contentView.backgroundColor = COLOR_DARK_BACK;
    /*
    if (indexPath.section == 1) {
        if (indexPath.row == 8) {
            if (iPremiumStatus > 0) {
                cell.btnAccessory.hidden = NO;
//                [cell.btnAccessory setImage:[UIImage imageNamed:@"ArrowAccessory"] forState:UIControlStateNormal];
            } else {
                cell.btnAccessory.hidden = YES;
//                [cell.btnAccessory setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            }
        }
//        else if (indexPath.row == 5 || indexPath.row == 11) {
//            if (iPaymentOption > 0) {
////                cell.btnAccessory.hidden = NO;
////                [cell.btnAccessory setImage:[UIImage imageNamed:@"ArrowAccessory"] forState:UIControlStateNormal];
//            } else {
////                cell.btnAccessory.hidden = YES;
////                [cell.btnAccessory setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//            }
//        }
    }
    */
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60;
    
//    if (indexPath.section == 1) {
//        if (indexPath.row == 8) {
//            if (iPremiumStatus > 0) {
//                return 60;
//            } else {
//                return 0;
//            }
//        }
//        else if (indexPath.row == 5 || indexPath.row == 11) {
//            if (iPaymentOption > 0) {
//                return 60;
//            } else {
//                return 0;
//            }
//        }
//        else {
//            return 60;
//        }
//    }
//    else {
//        return 60;
//    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *title;
    
    if (indexPath.section == 1) {
        title = [arrAccount objectAtIndex:indexPath.row];
    } else if (indexPath.section == 2) {
        title = [arrSupport objectAtIndex:indexPath.row];
    } else if (indexPath.section == 3) {
        title = [arrAbout objectAtIndex:indexPath.row];
    }
    
    [delegate ADSideMenuDrawerSelection:indexPath.section withRow:indexPath.row andTitle:title];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


@end
