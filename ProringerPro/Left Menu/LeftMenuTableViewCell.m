//
//  LeftMenuTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 22/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "LeftMenuTableViewCell.h"

@implementation LeftMenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
