//
//  LeftMenu.h
//  ProringerPro
//
//  Created by Soma Halder on 19/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ADSideMenuDrawerDelegate <NSObject>

- (void)ADSideMenuDrawerSelection:(NSInteger)selectedSection withRow:(NSInteger)selectedRow andTitle:(NSString *)title;
- (void)ADSideMenuDrawerSelection:(NSInteger)selectedSection;

@end

NS_ASSUME_NONNULL_BEGIN

@interface LeftMenu : UIView <UITableViewDataSource, UITableViewDelegate> {
    
    NSArray *arrAccount, *arrSupport, *arrAbout, *arrHeaderTitle;
    BOOL account, support, about;
}

@property (nonatomic) int iPremiumStatus, iPaymentOption;
@property (weak, nonatomic) id <ADSideMenuDrawerDelegate> delegate;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) IBOutlet UITableView *tblLeftMenu;

- (id)initWithFrame:(CGRect)frame;

@end

NS_ASSUME_NONNULL_END
