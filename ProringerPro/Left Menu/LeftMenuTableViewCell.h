//
//  LeftMenuTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 22/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LeftMenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnAccessory;

@end

NS_ASSUME_NONNULL_END
