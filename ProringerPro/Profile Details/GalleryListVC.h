//
//  GalleryListVC.h
//  ProringerPro
//
//  Created by Soma Halder on 19/09/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GalleryListVC : ProRingerProBaseVC

@property (weak, nonatomic) IBOutlet UICollectionView *collGalleryList;

@property (nonatomic) NSUInteger tagNumber;
@property (strong, nonatomic) NSMutableArray *arrGalleryList;
@property (strong, nonatomic) NSString *strPortfolioId;

- (IBAction)BackButtonPressed:(id)sender;


@end

NS_ASSUME_NONNULL_END
