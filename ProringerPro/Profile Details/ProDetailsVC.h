//
//  ProDetailsVC.h
//  ProRinger
//
//  Created by Soma Halder on 09/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProDetailsVC : ProRingerProBaseVC <UITableViewDataSource, UITableViewDelegate, TPFloatRatingViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (strong, nonatomic) NSString *strProsId;
@property (strong, nonatomic) NSString *strProCompanyName;
@property (strong, nonatomic) NSString *strFavourite;

@property (weak, nonatomic) IBOutlet UITableView *tblProDetails;
@property (weak, nonatomic) IBOutlet UIView *viewFooter;

@property (weak, nonatomic) IBOutlet UIButton *btnFavouritePros;

@property (weak, nonatomic) IBOutlet UIImageView *imgHeader;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfRating;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfReviews;

@property (weak, nonatomic) IBOutlet TPFloatRatingView *viewRating;

@property (weak, nonatomic) IBOutlet UIButton *btnViewAll;

- (IBAction)ViewAllReviews:(id)sender;
- (IBAction)RequestReview:(id)sender;
- (IBAction)EditProfile:(id)sender;
- (IBAction)UpdateHeaderImage:(id)sender;
- (IBAction)ViewLicenceDetails:(id)sender;

@end

NS_ASSUME_NONNULL_END
