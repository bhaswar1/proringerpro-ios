//
//  GalleryVC.m
//  ProRinger
//
//  Created by Soma Halder on 22/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import "GalleryVC.h"
#import "GalleryDetailsCollectionViewCell.h"
//#import <SDWebImage/UIImageView+WebCache.h>

@interface GalleryVC () {
    
    UITapGestureRecognizer *tapGesture;
}

@end

@implementation GalleryVC
@synthesize imgGallery, strImage, arrGallery, tagNumber, indexPathNumber, scrollView, containerView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.navigationController.navigationBar.hidden = YES;
    
    scrollView.delegate = self;
    scrollView.minimumZoomScale = 1.0;
    scrollView.maximumZoomScale = 5.0;
    scrollView.clipsToBounds = YES;
//    scrollView.contentSize = imgGallery.frame.size;
//    imgGallery.frame = CGRectMake(60, 100, self.view.frame.size.width -120, self.view.frame.size.height -200);
//    imgGallery.contentMode = UIViewContentModeCenter;
//    containerView.contentMode = UIViewContentModeCenter;
    
//    scrollView.contentSize = [UIScreen mainScreen].bounds.size;
    scrollView.bounces = NO;
    
    tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapToDismissView)];
    
    [self.view addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;
    
    [imgGallery sd_setImageWithURL:[NSURL URLWithString:strImage]];
    
    NSDictionary *dict = [arrGallery objectAtIndex:indexPathNumber];
    
    if (tagNumber == 7) {
        [imgGallery sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"portfolio_img"]]
                      placeholderImage:[UIImage imageNamed:@"Image-21"]];
    } else {
        [imgGallery sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"license_file"]]
                      placeholderImage:[UIImage imageNamed:@"Image-21"]];
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return containerView;
}

//-(void)setupConstraintsToImageSize:(CGSize)imageSize {
//    [self.imageHConstraint setConstant:imageSize.height];
//    [self layoutIfNeeded];
//}

//-(void)scrollViewDidZoom:(UIScrollView *)scrollView {
//    CGFloat diffHeight = imgGallery.frame.size.height - imgGallery.image.size.height;
////    CGFloat diffWidth = imgGallery.frame.size.width - imgGallery.image.size.width;
//    if(imgGallery.frame.size.height >= self.containerView.frame.size.height) {
//        [self.centerYConstraint setConstant:-diffHeight/2];
//        return;
//    }/* else if (imgGallery.frame.size.width >= self.containerView.frame.size.width) {
//        [self.centerXConstraint setConstant:-diffWidth/2];
////        return;
//    } */
//}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.navigationController.navigationBar.hidden = YES;
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.hidden = NO;
}

//-(void)imageLoadFromURL:(NSString*)url withContainer:(UIImageView*)imgView {
//    
//    NSURL *imageURL = [NSURL URLWithString:url];
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // Update the UI
//            imgView.image = [UIImage imageWithData:imageData];
//        });
//    });
//}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if (CGRectContainsPoint(imgGallery.bounds, [touch locationInView:imgGallery])) {
        return NO;
    } else
        return YES;
}

-(void)tapToDismissView {
    self.navigationController.navigationBar.hidden = NO;
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)DismissButtonPressed:(id)sender {
    
    [self tapToDismissView];
}
@end
