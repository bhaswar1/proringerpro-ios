//
//  GalleryListVC.m
//  ProringerPro
//
//  Created by Soma Halder on 19/09/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "GalleryListVC.h"
#import "GalleryCollectionViewCell.h"
#import "GalleryVC.h"

@interface GalleryListVC ()
{
    int iLicenceNumber;
}

@end

@implementation GalleryListVC
@synthesize collGalleryList, arrGalleryList, tagNumber, strPortfolioId;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon"]];
    self.navigationItem.rightBarButtonItem = nil;
    
    collGalleryList.dataSource = self;
    collGalleryList.delegate = self;
    
    [collGalleryList registerNib:[UINib nibWithNibName:@"GalleryCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cellGallery"];
    
    arrGalleryList = [[NSMutableArray alloc] init];
    
    [self connectionForGetImageList];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    self.navigationController.navigationBar.hidden = YES;
}

-(void)viewDidDisappear:(BOOL)animated {
    
    self.navigationController.navigationBar.hidden = YES;
}

#pragma mark - UICollectioView DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrGalleryList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellGallery";
    
    NSDictionary *dict = [arrGalleryList objectAtIndex:indexPath.item];
    
    GalleryCollectionViewCell *cell = (GalleryCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (tagNumber == 7) {
        
        [cell.imgGallery sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"portfolio_img"]]
                           placeholderImage:[UIImage imageNamed:@"Image-21"]];
    } else if (tagNumber == 8) {
        
        [cell.imgGallery sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"badges_image"]]
                           placeholderImage:[UIImage imageNamed:@"Image-21"]];
    }
    
    return cell;
}

#pragma mark - UICollectioView Delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    iLicenceNumber = (int)indexPath.item;
    
    [self performSegueWithIdentifier:@"galleryDetails" sender:nil];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake((collGalleryList.frame.size.width /3), (collGalleryList.frame.size.width /3) -5);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}

-(void)crossButtonPressed:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"galleryDetails"]) {
        
        GalleryVC *gvc = [segue destinationViewController];
        gvc.arrGallery = [[NSMutableArray alloc]init];
        gvc.arrGallery = arrGalleryList;
        gvc.indexPathNumber = iLicenceNumber;
        gvc.tagNumber = 7;
        [gvc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    }
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.providesPresentationContextTransitionStyle = YES;
    self.definesPresentationContext = YES;
}

#pragma mark - Web Service
-(void)connectionForGetImageList {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@app_individual_portfolio_image", ROOT_URL];
    
    NSDictionary *params = @{@"portfolio_id": strPortfolioId,
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            [self->arrGalleryList removeAllObjects];
            [self->arrGalleryList addObjectsFromArray:[responseObject objectForKey:@"info_array"]];
            [self->collGalleryList reloadData];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

- (IBAction)BackButtonPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
