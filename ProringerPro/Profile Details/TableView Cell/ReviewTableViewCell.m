//
//  ReviewTableViewCell.m
//  ProRinger
//
//  Created by Soma Halder on 13/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import "ReviewTableViewCell.h"
//#import <SDWebImage/UIImageView+WebCache.h>

@implementation ReviewTableViewCell
@synthesize viewShadow, lblDate, lblReview, lblUserName, viewRating, imgReviewUser, btnViewMore, constViewMore;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIColor *color = [UIColor blackColor];
    viewShadow.layer.shadowColor = [color CGColor];
    viewShadow.layer.shadowRadius = 1.5f;
    viewShadow.layer.shadowOpacity = 1.2;
    viewShadow.layer.shadowOffset = CGSizeZero;
    viewShadow.layer.masksToBounds = NO;
    
    viewRating.starImage = [UIImage imageNamed:@"Image-29"];
    viewRating.starHighlightedImage = [UIImage imageNamed:@"Image-28"];
    viewRating.maxRating = 5.0;
    viewRating.delegate = self;
    viewRating.horizontalMargin = 0;
    viewRating.editable = NO;
    viewRating.displayMode = EDStarRatingDisplayAccurate;
    viewRating.rating = 2.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCellFrom:(NSDictionary *)dictReview {
    
    NSString *strReview = [dictReview objectForKey:@"review_reply_content"];
    
    if (strReview.length >50) {
        strReview = [strReview substringFromIndex:50];
        btnViewMore.hidden = NO;
        constViewMore.constant = 30;
    } else {
        btnViewMore.hidden = YES;
        constViewMore.constant = 0;
    }
    
    lblDate.text = [dictReview objectForKey:@"date_time"];
    lblReview.text = strReview;
    lblUserName.text = [dictReview objectForKey:@"homeowner_name"];
    
    viewRating.rating = [[dictReview objectForKey:@"avg_rating"] floatValue];
    
//    [imgReviewUser sd_setImageWithURL:[NSURL URLWithString:[dictReview objectForKey:@"profile_img"]]];
}

@end
