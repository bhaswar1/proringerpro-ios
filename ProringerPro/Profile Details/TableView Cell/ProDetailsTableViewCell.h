//
//  ProDetailsTableViewCell.h
//  ProRinger
//
//  Created by Soma Halder on 09/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProDetailsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblCompanyName;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDesignation;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;

@end

NS_ASSUME_NONNULL_END
