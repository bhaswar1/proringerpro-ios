//
//  ReviewTableViewCell.h
//  ProRinger
//
//  Created by Soma Halder on 13/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReviewTableViewCell : UITableViewCell <EDStarRatingProtocol>

@property (weak, nonatomic) IBOutlet UIView *viewShadow;
@property (weak, nonatomic) IBOutlet EDStarRating *viewRating;

@property (weak, nonatomic) IBOutlet UIImageView *imgReviewUser;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (weak, nonatomic) IBOutlet UILabel *lblReview;

@property (weak, nonatomic) IBOutlet UIButton *btnViewMore;
@property (weak, nonatomic) IBOutlet UIButton *btnReport;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewMore;

-(void)configureCellFrom:(NSDictionary *)dictReview;

@end

NS_ASSUME_NONNULL_END
