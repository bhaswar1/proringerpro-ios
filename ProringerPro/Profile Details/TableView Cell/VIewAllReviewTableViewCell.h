//
//  VIewAllReviewTableViewCell.h
//  ProringerPro
//
//  Created by Soma Halder on 15/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ADReviewDescriptionDelegate <NSObject>

-(void)navigateToReadFullReviewWithString:(NSString *)selectedString;

@end

NS_ASSUME_NONNULL_BEGIN

@interface VIewAllReviewTableViewCell : UITableViewCell <EDStarRatingProtocol>

@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) id <ADReviewDescriptionDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet FRHyperLabel *lblReviewDescription;
@property (weak, nonatomic) IBOutlet FRHyperLabel *lblReply;

@property (weak, nonatomic) IBOutlet UIButton *btnReport;
@property (weak, nonatomic) IBOutlet UIButton *btnReply;

@property (weak, nonatomic) IBOutlet EDStarRating *viewRating;

@property (weak, nonatomic) IBOutlet UIView *viewReply;
@property (weak, nonatomic) IBOutlet UIView *viewShadow;
@property (weak, nonatomic) IBOutlet UIView *viewSeparator;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constTopReply;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constHeightReply;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constImageHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constImageTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constTopLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constBottomLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constLeadReport;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constTrailingReport;

@property (strong, nonatomic) NSString *strSelectedDescription, *strSelectedReply;
@property (weak, nonatomic) IBOutlet UIButton *btnReadFullReview;
@property (weak, nonatomic) IBOutlet UIButton *btnReadFullReply;

-(void)navigateToReadFullReview;
-(void)configureReviewListCellWith:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
