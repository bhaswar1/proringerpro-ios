//
//  ServiceInfoTableViewCell.m
//  ProRinger
//
//  Created by Soma Halder on 09/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import "ServiceInfoTableViewCell.h"
#import "ServiceInfoCollectionViewCell.h"
#import "GalleryCollectionViewCell.h"
#import "BusinessCollectionViewCell.h"
//#import <SDWebImage/UIImageView+WebCache.h>


@implementation ServiceInfoTableViewCell
@synthesize collServiceInfo, sectionNumber, customDelegate, arrGalleryInfo, arrContactInfo;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    arrGalleryInfo = [[NSMutableArray alloc]init];
    
    arrContactInfo = [[NSMutableArray alloc] init];//WithObjects:
    [arrContactInfo addObject:@"Business Since"];
    [arrContactInfo addObject:@"No. of employee"];
    [arrContactInfo addObject:@"ProRinger awarded"];
    [arrContactInfo addObject:@"Business Review"];
    [arrContactInfo addObject:@"Last Verified on"];
    
    collServiceInfo.dataSource = self;
    collServiceInfo.delegate = self;
    
    [collServiceInfo registerNib:[UINib nibWithNibName:@"GalleryCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cellGallery"];
    [collServiceInfo registerNib:[UINib nibWithNibName:@"ServiceInfoCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cellServiceInfo"];
    [collServiceInfo registerNib:[UINib nibWithNibName:@"BusinessCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cellBusiness"];
}

-(void)cellArrayConfiguration:(NSArray *)arrDataSet withOptional:(nonnull NSDictionary *)dictCompanyInfo {
    
    if (sectionNumber == 6) {
        
        [arrContactInfo addObject:[dictCompanyInfo objectForKey:@"business_since"]];
        [arrContactInfo addObject:[dictCompanyInfo objectForKey:@"no_of_employee"]];
        [arrContactInfo addObject:[dictCompanyInfo objectForKey:@"proringer_awarded"]];
        [arrContactInfo addObject:[dictCompanyInfo objectForKey:@"business_review"]];
        [arrContactInfo addObject:[dictCompanyInfo objectForKey:@"last_verified_on"]];
        
    } else {
        
        [arrGalleryInfo removeAllObjects];
        [arrGalleryInfo addObjectsFromArray:arrDataSet];
    }
    [collServiceInfo reloadData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (sectionNumber == 7) {
        return arrGalleryInfo.count;
    }
    else if (sectionNumber == 8) {
        return arrGalleryInfo.count;
    }
    else if (sectionNumber == 2) {
        if (arrGalleryInfo.count > 14) {
            return 14;
        } else {
            return arrGalleryInfo.count;
        }
    }
    else if (sectionNumber == 5) {
        return arrGalleryInfo.count;
    }
    else if (sectionNumber == 4) {
        if (arrGalleryInfo.count > 14) {
            return 14;
        } else {
            return arrGalleryInfo.count;
        }
    }
    else if (sectionNumber == 6) {
        return arrContactInfo.count;
    }
    else if (sectionNumber == 3) {
        return arrGalleryInfo.count;
    }
    else {
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict;// = [arrGalleryInfo objectAtIndex:indexPath.item];
    if (sectionNumber == 6) {
        dict = [arrContactInfo objectAtIndex:indexPath.row];
    } else {
        dict = [arrGalleryInfo objectAtIndex:indexPath.row];
    }
    
    GalleryCollectionViewCell *cellGallery = (GalleryCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellGallery" forIndexPath:indexPath];
    
    ServiceInfoCollectionViewCell *cellInfo = (ServiceInfoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellServiceInfo" forIndexPath:indexPath];
    
    BusinessCollectionViewCell *cellBusi = (BusinessCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellBusiness" forIndexPath:indexPath];
    
    if (sectionNumber == 2) {
        
        cellInfo.lblInfo.text = [dict objectForKey:@"service_name"];
       // NSLog(@"service_name%@", [dict objectForKey:@"service_name"]);
        return cellInfo;
        
    } else if (sectionNumber == 3) {
        
        cellBusi.lblDays.text = [dict objectForKey:@"day"];
        cellBusi.lblHours.text = [dict objectForKey:@"hours"];
        
        return cellBusi;
        
    } else if (sectionNumber == 4) {
        
        cellInfo.lblInfo.text = [dict objectForKey:@"city_services"];
        
        return cellInfo;
        
    } else if (sectionNumber == 5) {
        
      //  cellInfo.lblInfo.text = [dict objectForKey:@"city_services"];
        cellInfo.lblInfo.text = [NSString stringWithFormat:@"%@\n%@\n%@\n%@", [dict objectForKey:@"category_name"], [dict objectForKey:@"licence_issuer"], [dict objectForKey:@"licence_no"], [dict objectForKey:@"license_expire"]];
        //  NSLog(@"city_services%@", [dict objectForKey:@"city_services"]);
        
        return cellInfo;
        
    } else if (sectionNumber == 6) {
        
      //  cellInfo.lblInfo.text = [arrContactInfo objectAtIndex:indexPath.row];
        cellInfo.lblInfo.text = [NSString stringWithFormat:@"%@", [arrContactInfo objectAtIndex:indexPath.row]];
        return cellInfo;
        
    } else if (sectionNumber == 7) {
        cellGallery.imgGallery.contentMode = UIViewContentModeScaleAspectFill;
        [cellGallery.imgGallery sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"portfolio_image"]]];

        return cellGallery;
        
    } else if (sectionNumber == 8) {
        cellGallery.imgGallery.contentMode = UIViewContentModeScaleAspectFit;
        [cellGallery.imgGallery sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"badges_image"]]];
   
        return cellGallery;
        
    } else {
        return nil;
    }
}

-(void)imageLoadFromURL:(NSString*)url withContainer:(UIImageView*)imgView {
    
    NSURL *imageURL = [NSURL URLWithString:url];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            
            imgView.image = [UIImage imageWithData:imageData];
        });
    });
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [customDelegate didSelectedItem:sectionNumber at:(int)indexPath.item];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (sectionNumber == 7) {
        
        return CGSizeMake((collServiceInfo.frame.size.width /4) +25, collServiceInfo.frame.size.height);
    } else if (sectionNumber == 8) {
        
        return CGSizeMake((collServiceInfo.frame.size.width /4) -10, collServiceInfo.frame.size.height);
    } else if (sectionNumber == 3) {
        
        return CGSizeMake(collServiceInfo.frame.size.width -4, 20);
    }/* else if (sectionNumber == 4) {
        return CGSizeMake((collServiceInfo.frame.size.width /2) -3, 22);
    } */else if (sectionNumber == 5) {
        
        return CGSizeMake((collServiceInfo.frame.size.width /2) -4, 100);
    } else {
        
        return CGSizeMake((collServiceInfo.frame.size.width /2) -4, 20);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    if (sectionNumber == 7 || sectionNumber == 8) {
        return 10.0;
    } else
        return 0.0;
}

@end
