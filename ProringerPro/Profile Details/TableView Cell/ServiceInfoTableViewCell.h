//
//  ServiceInfoTableViewCell.h
//  ProRinger
//
//  Created by Soma Halder on 09/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ADCustomDelegate <NSObject>

-(void)didSelectedItem:(int)section at:(int)indexPath;

@end

NS_ASSUME_NONNULL_BEGIN

@interface ServiceInfoTableViewCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collServiceInfo;
@property (weak, nonatomic) id <ADCustomDelegate> customDelegate;

@property (nonatomic, assign) int sectionNumber;
@property (strong, nonatomic) NSMutableArray *arrGalleryInfo;

@property (strong, nonatomic) NSMutableArray *arrContactInfo;

-(void)cellArrayConfiguration:(NSArray *)arrDataSet withOptional:(NSDictionary *)dictCompanyInfo;

@end

NS_ASSUME_NONNULL_END
