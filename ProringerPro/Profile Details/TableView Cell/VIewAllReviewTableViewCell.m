//
//  VIewAllReviewTableViewCell.m
//  ProringerPro
//
//  Created by Soma Halder on 15/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "VIewAllReviewTableViewCell.h"

@implementation VIewAllReviewTableViewCell
@synthesize btnReply, btnReport, viewRating, lblDate, lblUserName, lblReviewDescription, imgProfile, viewReply, viewShadow, constTopReply, constHeightReply, lblReply, constImageTop, constImageHeight, constTopLabel, constBottomLabel, delegate, strSelectedDescription, viewSeparator, strSelectedReply, btnReadFullReview, constLeadReport, constTrailingReport;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    viewRating.maxRating = 5.0;
    viewRating.delegate = self;
    viewRating.horizontalMargin = 0;
    viewRating.editable = NO;
    viewRating.displayMode = EDStarRatingDisplayAccurate;
//    viewRating.rating = fltRating;
    viewRating.starImage = [UIImage imageNamed:@"StarEmpty"];
    viewRating.starHighlightedImage = [UIImage imageNamed:@"StarFull"];
    
    UIColor *color = [UIColor blackColor];
    viewShadow.layer.shadowColor = [color CGColor];
    viewShadow.layer.shadowRadius = 1.5f;
    viewShadow.layer.shadowOpacity = 0.1;
    viewShadow.layer.cornerRadius = 2.0;
    viewShadow.layer.shadowOffset = CGSizeZero;
    viewShadow.layer.masksToBounds = NO;
    
    viewShadow.layer.shadowColor = COLOR_TABLE_HEADER.CGColor;
    viewShadow.layer.shadowOffset = CGSizeMake(0, 3);
    viewShadow.layer.shadowOpacity = 0.5;
    viewShadow.layer.shadowRadius = 0.6;
}

-(void)configureReviewListCellWith:(NSDictionary *)dict {
    
    viewRating.rating = [[dict objectForKey:@"avg_rating"] floatValue];
    
    if ([[dict objectForKey:@"review_reply"] intValue] == 1) {
        btnReply.hidden = YES;
        [btnReply setTitle:@"" forState:UIControlStateNormal];
        constTopReply.constant = 5.0;
        constHeightReply.constant = 20.0;
        viewSeparator.hidden = viewReply.hidden = NO;
        constImageHeight.constant = 15.0;
        constBottomLabel.constant = constTopLabel.constant = constImageTop.constant = 8.0;
    } else {
        btnReply.hidden = NO;
        [btnReply setTitle:@" Reply " forState:UIControlStateNormal];
        constImageHeight.constant = constImageTop.constant = constHeightReply.constant = constTopReply.constant = constBottomLabel.constant = constTopLabel.constant = 0.0;
        viewSeparator.hidden = viewReply.hidden = YES;
        CGRect frame = viewReply.frame;
        frame.size.height = 0.0;
        viewReply.frame = frame;
    }
    
    if ([[dict objectForKey:@"review_report_status"] intValue] == 1) {
        btnReport.hidden = YES;
        [btnReport setTitle:@"" forState:UIControlStateNormal];
        constLeadReport.constant = constTrailingReport.constant = -30.0;
    } else {
        btnReport.hidden = NO;
        [btnReport setTitle:@" Report " forState:UIControlStateNormal];
        constLeadReport.constant = 15.0;
        constTrailingReport.constant = 8;
    }
    
    [imgProfile sd_setImageWithURL:[dict objectForKey:@"profile_img"]];
    
    strSelectedReply = [dict objectForKey:@"review_reply_content"];
    strSelectedDescription = [dict objectForKey:@"rater_description"];
    lblUserName.text = [dict objectForKey:@"homeowner_name"];
    lblDate.text = [dict objectForKey:@"date_time"];
    
//    NSString *strDescription = [dict objectForKey:@"rater_description"];
//    NSString *strReply = [dict objectForKey:@"review_reply_content"];
//    lblReviewDescription.text = [dict objectForKey:@"rater_description"];
//    lblReply.text = [dict objectForKey:@"review_reply_content"];
    
    strSelectedDescription = [strSelectedDescription stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
    strSelectedDescription = [strSelectedDescription stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    lblReviewDescription.text = strSelectedDescription;
    
    strSelectedReply = [strSelectedReply stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
    strSelectedReply = [strSelectedReply stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    lblReply.text = strSelectedReply;
    
    /*
    if (strDescription.length > 60) {
        NSRange stringRange = {0, MIN([strDescription length], 60)};
        
        // adjust the range to include dependent chars
        stringRange = [strDescription rangeOfComposedCharacterSequencesForRange:stringRange];
        
        // Now you can create the short string
        NSString *shortString = [strDescription substringWithRange:stringRange];
        
        shortString = [NSString stringWithFormat:@"%@... Read Full Review", shortString];
        
        NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor darkGrayColor],NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote]};
        lblReviewDescription.attributedText = [[NSAttributedString alloc]initWithString:shortString attributes:attributes];
        
        void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
            if ([substring isEqualToString:@"Read Full Review"]) {
                [self navigateToReadFullReview];
            }
        };
        
        //Step 3: Add link substrings
        [lblReviewDescription setLinksForSubstrings:@[@"Read Full Review"] withLinkHandler:handler];
        
        NSLog(@"SUBSTRING:::...%@", shortString);
    } else {
        lblReviewDescription.text = strSelectedDescription;
    }
    
    if (strReply.length > 0) {
        if (strReply.length > 60) {
            NSRange stringRange = {0, MIN([strReply length], 60)};
            
            // adjust the range to include dependent chars
            stringRange = [strReply rangeOfComposedCharacterSequencesForRange:stringRange];
            
            // Now you can create the short string
            NSString *shortString = [strReply substringWithRange:stringRange];
            
            shortString = [NSString stringWithFormat:@"%@... Read Full Reply", shortString];
            
            NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor darkGrayColor],NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote]};
            lblReply.attributedText = [[NSAttributedString alloc]initWithString:shortString attributes:attributes];
            
            void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
                if ([substring isEqualToString:@"Read Full Reply"]) {
                    [self navigateToReadFullReply];
                }
            };
            
            //Step 3: Add link substrings
            [lblReply setLinksForSubstrings:@[@"Read Full Reply"] withLinkHandler:handler];
            
            NSLog(@"SUBSTRING:::...%@", shortString);
        } else {
            lblReply.text = strSelectedReply;
        }
    } else {
        lblReply.text = @"";
    }
     */
}

-(void)navigateToReadFullReview {
    
    NSLog(@"navigateToTermsOfUse");
    [delegate navigateToReadFullReviewWithString:strSelectedDescription];
}

-(void)navigateToReadFullReply {
    
    NSLog(@"navigateToTermsOfUse");
    [delegate navigateToReadFullReviewWithString:strSelectedReply];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
