//
//  ProDetailsVC.m
//  ProRinger
//
//  Created by Soma Halder on 09/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import "ProDetailsVC.h"
#import "ProDetailsTableViewCell.h"
#import "ServiceInfoTableViewCell.h"
#import "ServiceAboutListVC.h"
#import "GalleryVC.h"
#import "NSMutableAttributedString+Color.h"


@interface ProDetailsVC () <ADCustomDelegate>
{
    NSArray *arrHeaderTitle;
    NSString *strAbout, *strCompanyName, *strUserName, *strDesignation, *strAddress, *strGalleryImage;
    NSMutableDictionary *dictProInfoDetails;
    NSData *dataImgHeader;
    UIImage *chosenImage;
    int rowHeight, licenceHeight, tag, serviceRowHeight, indexPathNumber, iLicenceNumber;
}

@end

@implementation ProDetailsVC
@synthesize strProsId, strProCompanyName, tblProDetails, imgHeader, imgProfileImage, lblNumberOfRating, lblNumberOfReviews, viewRating, btnViewAll, strFavourite, btnFavouritePros, viewFooter;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    self.title = @"PROFILE";
    
//    self.tabBarController.tabBar.hidden = NO;
    
    arrHeaderTitle = [[NSArray alloc] initWithObjects:@"", @"About", @"Service", @"Business Hours", @"Service Area", @"License", @"Company info", @"Project Gallery", @"Milestones", nil];
    
    dictProInfoDetails = [NSMutableDictionary dictionary];
    
    rowHeight = 0;
    licenceHeight = 20;
    tag = 0;
    serviceRowHeight = 0;
    
    [tblProDetails registerNib:[UINib nibWithNibName:@"ProDetailsTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellProDetails"];
    [tblProDetails registerNib:[UINib nibWithNibName:@"ServiceInfoTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellServiceInfo"];
    
    tblProDetails.rowHeight = UITableViewAutomaticDimension;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    [self connectionForGetProDetils];
    
    viewRating.delegate = self;
    viewRating.emptySelectedImage = [UIImage imageNamed:@"StarEmpty"];
    viewRating.fullSelectedImage = [UIImage imageNamed:@"StarFull"];
    viewRating.contentMode = UIViewContentModeScaleAspectFill;
    viewRating.maxRating = 5.0;
    viewRating.minRating = 0.0;
    viewRating.rating = 0.0;
    viewRating.editable = NO;
    viewRating.halfRatings = YES;
    viewRating.floatRatings = YES;
    viewRating.hidden = YES;
    
//    int xMargin = 30;
//    int marginBottom = 25;
//    CGFloat btnWidth = self.view.frame.size.width - xMargin * 2;
//    int btnHeight = 42;
//
//    UIButton* kakaoLoginButton = [[UIButton alloc] initWithFrame:CGRectMake(xMargin, self.view.frame.size.height-btnHeight-marginBottom, btnWidth, btnHeight)];
//    kakaoLoginButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
//    kakaoLoginButton.backgroundColor = [UIColor redColor];
//
//    [self.view addSubview:kakaoLoginButton];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self setupADBanner];
}

-(void)setupADBanner {
    
//    for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
//        NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
//        if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Profile"]) {
//            if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
//                [self addBannerViewToView:self.bannerView];
//            }
//        }
//    }
    
    if (self.arrAdvViewStatus.count > 0) {
        for (int i = 0; i < self.arrAdvViewStatus.count; i++) {
            NSDictionary *dict = [self.arrAdvViewStatus objectAtIndex:i];
            if ([[dict objectForKey:@"title"] isEqualToString:@"Pro Profile"]) {
                if ([[dict objectForKey:@"iosadv_viewstatus"] intValue] == 1) {
                    [self addBannerViewToView:self.bannerView];
                } else {
                    [self.bannerView removeFromSuperview];
                }
            }
        }
    } else {
        [self.bannerView removeFromSuperview];
    }
}

-(void)rightBarButtonPressed:(UIButton *)sender {
    NSLog(@"SHARE BUTTON PRESSED from vc");
    
    NSString *textToShare = [NSString stringWithFormat:@"%@%@", [[dictProInfoDetails objectForKey:@"info"] objectForKey:@"url"], [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:[NSArray arrayWithObjects:textToShare, nil] applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll]; //Exclude whichever aren't relevant
    [self presentViewController:activityVC animated:YES completion:NULL];
}

#pragma mark - UITableView DataSource (Section)
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return arrHeaderTitle.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 250, viewHeader.frame.size.height)];
    UILabel *separetor = [[UILabel alloc] initWithFrame:CGRectMake(viewHeader.frame.origin.x +5, viewHeader.frame.size.height -5, viewHeader.frame.size.width -10, 1)];
    UIButton *btnViewAll = [[UIButton alloc] init];//WithFrame:CGRectMake(viewHeader.frame.size.width -75, 0, 68, viewHeader.frame.size.height)];
    separetor.backgroundColor = [UIColor lightGrayColor];
    
    viewHeader.backgroundColor = [UIColor clearColor];
    
    lblTitle.text = [arrHeaderTitle objectAtIndex:section];
    lblTitle.font = [UIFont fontWithName:@"OpenSans" size:16.0];
    lblTitle.textColor = [UIColor darkGrayColor];
    //    -Semibold
    btnViewAll.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:15.0];
    [btnViewAll setTitleColor:[UIColor colorWithRed:241.0f/255 green:89.0f/255 blue:42.0f/255 alpha:1] forState:UIControlStateNormal];
    btnViewAll.tag = section;
    [btnViewAll addTarget:self action:@selector(ViewMoreOrViewAllButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    btnViewAll.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    if (section == 1) {
        NSString *strAbout = [[dictProInfoDetails objectForKey:@"about"] objectForKey:@"description"];
        if (strAbout.length > 0) {
            [btnViewAll setTitle:@"View More" forState:UIControlStateNormal];
            btnViewAll.frame = CGRectMake(viewHeader.frame.size.width -100, 0, 90, viewHeader.frame.size.height);
        } else {
            [btnViewAll setTitle:@"" forState:UIControlStateNormal];
            btnViewAll.frame = CGRectMake(0, 0, 0, 0);
        }
        
    } else if (section == 2) {
        NSArray *arrServices = [dictProInfoDetails objectForKey:@"services"];
        if (arrServices.count > 14) {
            [btnViewAll setTitle:@"View All" forState:UIControlStateNormal];
            btnViewAll.frame = CGRectMake(viewHeader.frame.size.width -100, 0, 90, viewHeader.frame.size.height);
        } else {
            [btnViewAll setTitle:@"" forState:UIControlStateNormal];
            btnViewAll.frame = CGRectMake(0, 0, 0, 0);
        }
    } else if (section == 4) {
        NSArray *arrServicesArea = [dictProInfoDetails objectForKey:@"service_area"];
        if (arrServicesArea.count > 14) {
            [btnViewAll setTitle:@"View All" forState:UIControlStateNormal];
            btnViewAll.frame = CGRectMake(viewHeader.frame.size.width -100, 0, 90, viewHeader.frame.size.height);
        } else {
            [btnViewAll setTitle:@"" forState:UIControlStateNormal];
            btnViewAll.frame = CGRectMake(0, 0, 0, 0);
        }
    } else if (section == 7) {
        
        NSString *strTitle = [NSString stringWithFormat:@"%@ Projects  %@ Pictures", [dictProInfoDetails objectForKey:@"total_project"], [dictProInfoDetails objectForKey:@"total_picture"]];
        
        [btnViewAll setTitle:strTitle forState:UIControlStateNormal];
        
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:btnViewAll.titleLabel.text];
        [string setColorForText:@"Projects" withColor:COLOR_TEXT];
        [string setColorForText:@"Pictures" withColor:COLOR_TEXT];
        btnViewAll.titleLabel.attributedText = string;
        
        btnViewAll.frame = CGRectMake(viewHeader.frame.size.width -200, 0, 190, viewHeader.frame.size.height);
    } else if (section == 8) {
        
        NSString *strTitle = [NSString stringWithFormat:@"%@ Total", [dictProInfoDetails objectForKey:@"total_achievement"]];
        
        [btnViewAll setTitle:strTitle forState:UIControlStateNormal];
        
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:btnViewAll.titleLabel.text];
        [string setColorForText:@"Total" withColor:[UIColor colorWithRed:49.0f/255 green:49.0f/255 blue:49.0f/255 alpha:1.0]];
        btnViewAll.titleLabel.attributedText = string;
        
        btnViewAll.frame = CGRectMake(viewHeader.frame.size.width -130, 0, 120, viewHeader.frame.size.height);
    } else {
        [btnViewAll setTitle:@"" forState:UIControlStateNormal];
        btnViewAll.frame = CGRectMake(0, 0, 0, 0);
    }
    
    [viewHeader addSubview:btnViewAll];
    [viewHeader addSubview:separetor];
    [viewHeader addSubview:lblTitle];
    
    return viewHeader;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *viewFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    
    UIButton *btnViewFirst = [[UIButton alloc] initWithFrame:CGRectMake(10, 5, 80, 40)];
    [btnViewFirst setTitle:@"VIEW" forState:UIControlStateNormal];
    [btnViewFirst setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnViewFirst setBackgroundColor:COLOR_THEME];
    [btnViewFirst addTarget:self action:@selector(firstViewButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnViewSec = [[UIButton alloc] initWithFrame:CGRectMake((viewFooter.frame.size.width /2) -10, 5, 80, 40)];
    [btnViewSec setTitle:@"VIEW" forState:UIControlStateNormal];
    [btnViewSec setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnViewSec setBackgroundColor:COLOR_THEME];
    [btnViewSec addTarget:self action:@selector(secondViewButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnViewFirst.titleLabel.font = btnViewSec.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:13.0];
    
    NSArray *arrGallery = [dictProInfoDetails objectForKey:@"licence"];
    
    if (section == 5) {
        if (arrGallery.count > 0) {
            if (arrGallery.count == 1) {
                btnViewSec.hidden = YES;
            } else {
                btnViewSec.hidden = NO;
            }
        }  else {
            btnViewFirst.hidden = btnViewSec.hidden = YES;
        }
    } else {
        btnViewFirst.hidden = btnViewSec.hidden = YES;
    }
    
    [viewFooter addSubview:btnViewFirst];
    [viewFooter addSubview:btnViewSec];
    
    return viewFooter;
}

#pragma mark - UITableView DataSource (Row)
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProDetailsTableViewCell *cellPro = (ProDetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellProDetails"];
    
    ServiceInfoTableViewCell *cellInfo = (ServiceInfoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellServiceInfo"];
    
    cellInfo.sectionNumber = (int)indexPath.section;
    cellInfo.customDelegate = self;
    
    if (indexPath.section == 8) {
        
        UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)[cellInfo.collServiceInfo collectionViewLayout];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        
        cellInfo.collServiceInfo.userInteractionEnabled = YES;
        NSArray *arrGallery = [dictProInfoDetails objectForKey:@"achievement"];
        [cellInfo cellArrayConfiguration:arrGallery withOptional:nil];
        
        return cellInfo;
        
    } else if (indexPath.section == 2) {
        
        NSArray *arrGallery = [dictProInfoDetails objectForKey:@"services"];
        
        if (arrGallery.count > 0) {
            
            UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)[cellInfo.collServiceInfo collectionViewLayout];
            [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
            
            cellInfo.collServiceInfo.userInteractionEnabled = NO;
            if (arrGallery.count > 14) {
                serviceRowHeight = 150;
            } else {
                serviceRowHeight = (int)((arrGallery.count * 20) /2) +15;
            }
            [cellInfo cellArrayConfiguration:arrGallery withOptional:nil];
            return cellInfo;
        } else {
            serviceRowHeight = 25;
            cellPro.lblUserName.text = @"";
            cellPro.lblCompanyName.text = @"";
            cellPro.lblDesignation.text = @"";
            cellPro.lblAddress.text = @"No documents provided";
            
            return cellPro;
        }
        
        return cellInfo;
        
    } else if (indexPath.section == 4) {
        
        NSArray *arrGallery = [dictProInfoDetails objectForKey:@"service_area"];
        
        if (arrGallery.count > 0) {
            
            UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)[cellInfo.collServiceInfo collectionViewLayout];
            [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
            
            cellInfo.collServiceInfo.userInteractionEnabled = NO;
            if (arrGallery.count > 14) {
                rowHeight = 150;
            } else {
                rowHeight = (int)((arrGallery.count * 20) /2) +15;
            }
            [cellInfo cellArrayConfiguration:arrGallery withOptional:nil];
            return cellInfo;
        } else {
            rowHeight = 25;
            cellPro.lblUserName.text = @"";
            cellPro.lblCompanyName.text = @"";
            cellPro.lblDesignation.text = @"";
            cellPro.lblAddress.text = @"No documents provided";
            
            return cellPro;
        }
        
        return cellInfo;
        
    } else if (indexPath.section == 6) {
        
        UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)[cellInfo.collServiceInfo collectionViewLayout];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        
        cellInfo.collServiceInfo.userInteractionEnabled = NO;
        [cellInfo cellArrayConfiguration:nil withOptional:[dictProInfoDetails objectForKey:@"company_info"]];
        return cellInfo;
        
    } else if (indexPath.section == 7) {
        
        NSArray *arrGallery = [dictProInfoDetails objectForKey:@"project_gallery"];
        
        if (arrGallery.count > 0) {
//            cellInfo.collServiceInfo.scrollDirection = UICollectionViewScrollDirectionHorizontal;
            UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)[cellInfo.collServiceInfo collectionViewLayout];
            [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
            
            cellInfo.collServiceInfo.userInteractionEnabled = YES;
            [cellInfo cellArrayConfiguration:arrGallery withOptional:nil];
            return cellInfo;
        } else {
            cellPro.lblUserName.text = @"";
            cellPro.lblCompanyName.text = @"";
            cellPro.lblDesignation.text = @"";
            cellPro.lblAddress.text = @"No documents provided";
            
            return cellPro;
        }
        
    } else if (indexPath.section == 0) {
        
        NSString *strCity = [[dictProInfoDetails objectForKey:@"info"] objectForKey:@"city"];
        strCity = [strCity stringByReplacingOccurrencesOfString:@"-" withString:@" "];
        
        cellPro.lblCompanyName.text = [[dictProInfoDetails objectForKey:@"info"] objectForKey:@"company_name"];
        cellPro.lblDesignation.text = [[dictProInfoDetails objectForKey:@"info"] objectForKey:@"designation"];
        cellPro.lblAddress.text = [NSString stringWithFormat:@"%@\n%@, %@ %@",
                                   [[dictProInfoDetails objectForKey:@"info"] objectForKey:@"address"],
                                   strCity,
                                   [[dictProInfoDetails objectForKey:@"info"] objectForKey:@"state"],
                                   [[dictProInfoDetails objectForKey:@"info"] objectForKey:@"zipcode"]];
        cellPro.lblUserName.text = [[dictProInfoDetails objectForKey:@"info"] objectForKey:@"user_name"];
        
        return cellPro;
        
    } else if (indexPath.section == 1) {
        
        cellPro.lblCompanyName.text = @"";
        NSString *strAbout = [[dictProInfoDetails objectForKey:@"about"] objectForKey:@"description"];
        if (strAbout.length > 0) {
            strAbout = [strAbout stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
            strAbout = [strAbout stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
            cellPro.lblAddress.text = strAbout;
            cellPro.lblAddress.textAlignment = NSTextAlignmentLeft;
        } else {
            cellPro.lblAddress.textAlignment = NSTextAlignmentCenter;
            cellPro.lblAddress.text = @"No Data to show";
        }
        cellPro.lblUserName.text = @"";
        cellPro.lblDesignation.text = @"";
        
        return cellPro;
    } else if (indexPath.section == 3) {
        
        id object = [dictProInfoDetails objectForKey:@"business_hours"];
        
        if ([object isKindOfClass:[NSString class]]) {
            cellPro.lblCompanyName.text = @"";
            cellPro.lblAddress.text = [dictProInfoDetails objectForKey:@"business_hours"];
            cellPro.lblUserName.text = @"";
            cellPro.lblDesignation.text = @"";
            
            rowHeight = UITableViewAutomaticDimension;
            
        } else if ([object isKindOfClass:[NSArray class]]) {
            
            NSLog(@"OBJECT IS AN ARRAY:::...%@", object);
            
            NSArray *arrGallery = [dictProInfoDetails objectForKey:@"business_hours"];
            
            if (arrGallery.count > 0) {
                
                UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)[cellInfo.collServiceInfo collectionViewLayout];
                [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
                
                rowHeight = 160;
                cellInfo.collServiceInfo.userInteractionEnabled = NO;
                [cellInfo cellArrayConfiguration:arrGallery withOptional:nil];
                
            } else {
                rowHeight = UITableViewAutomaticDimension;
                cellPro.lblUserName.text = @"";
                cellPro.lblCompanyName.text = @"";
                cellPro.lblDesignation.text = @"";
                cellPro.lblAddress.text = @"No documents provided";
                
                return cellPro;
            }
            
            return cellInfo;
        }
        
        return cellPro;
        
    } else if (indexPath.section == 5) {
        
        NSArray *arrGallery = [dictProInfoDetails objectForKey:@"licence"];
        
        if (arrGallery.count > 0) {
            
            UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)[cellInfo.collServiceInfo collectionViewLayout];
            [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
            
            cellInfo.collServiceInfo.userInteractionEnabled = NO;
            [cellInfo cellArrayConfiguration:arrGallery withOptional:nil];
            NSLog(@"LICENCE ARRAY COUNT:::...%ld", arrGallery.count);
            
            licenceHeight = 100;
            return cellInfo;
        } else {
            cellPro.lblUserName.text = @"";
            cellPro.lblCompanyName.text = @"";
            cellPro.lblDesignation.text = @"";
            cellPro.lblAddress.text = @"No documents provided";
            licenceHeight = UITableViewAutomaticDimension;
            
            return cellPro;
        }
        return cellPro;
    }
    return nil;
}

#pragma mark - UITableView Delegate (Height)
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 7 || indexPath.section == 8) {
        return self.view.frame.size.width /4;
    } else if (indexPath.section == 2) {
        
        return serviceRowHeight;
    } else if (indexPath.section == 3) {
        
        return rowHeight;
    } else if (indexPath.section == 5) {
          
          return licenceHeight;
      } else if (indexPath.section == 6) {
          return 110;
      } else if (indexPath.section == 4) {
          return rowHeight;
      } else
          return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return 0;
    } else {
        return 40;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if (section == 5) {
        
        NSArray *arrGallery = [dictProInfoDetails objectForKey:@"licence"];
        
        if (arrGallery.count > 0) {
            return 50;
        }  else {
            return 0;
        }
    } else {
        return 0;
    }
}

#pragma mark - Web Service

-(void) connectionForGetProDetils {
    
    [YXSpritesLoadingView show];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_PROFILE_DETAILS];
    
    NSDictionary *param = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    NSLog(@"%@", strUrl);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:strUrl parameters:param progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            self.title = [[[responseObject objectForKey:@"info_array"] objectForKey:@"info"] objectForKey:@"company_name"];
            
            [self->imgHeader sd_setImageWithURL:[NSURL URLWithString:[[[responseObject objectForKey:@"info_array"] objectForKey:@"info"] objectForKey:@"header_image"]]];
            [self->imgProfileImage sd_setImageWithURL:[NSURL URLWithString:[[[responseObject objectForKey:@"info_array"] objectForKey:@"info"] objectForKey:@"profile_image"]]];
            
            [self->dictProInfoDetails addEntriesFromDictionary:[responseObject objectForKey:@"info_array"]];
            
            if ([[self->dictProInfoDetails objectForKey:@"total_review"] intValue] == 0) {
                self->btnViewAll.hidden = YES;
            }
            self->lblNumberOfReviews.text = [NSString stringWithFormat:@"%@", [self->dictProInfoDetails objectForKey:@"total_review"]];
            self->lblNumberOfRating.text = [NSString stringWithFormat:@"%@", [self->dictProInfoDetails objectForKey:@"total_avg_review"]];
            self->viewRating.rating = [[self->dictProInfoDetails objectForKey:@"total_avg_review"] floatValue];
            self->viewRating.hidden = NO;
        }
        
        if ([self->dictProInfoDetails count] == 0) {
            self->tblProDetails.dataSource = nil;
            self->tblProDetails.delegate = nil;
        } else {
            self->tblProDetails.dataSource = self;
            self->tblProDetails.delegate = self;
        }
        
        [self->tblProDetails reloadData];
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

-(void)connectionForUploadHeaderImage {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_HEADER_IMAGE];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] valueForKey:@"userId"],
                             @"header_bg_image": @""
                             };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:self->dataImgHeader name:@"header_bg_image" fileName:@"Image.png" mimeType:@"image/png"];
        
    } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [YXSpritesLoadingView dismiss];
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //  LoginViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
                
                //  [self.navigationController pushViewController:obj animated:YES];
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", task.error);
        [YXSpritesLoadingView dismiss];
    }];
}

-(void)imageLoadFromURL:(NSString*)url withContainer:(UIImageView*)imgView withFlag:(NSString *)location {

    NSURL *imageURL = [NSURL URLWithString:url];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *dataImgHeader = [NSData dataWithContentsOfURL:imageURL];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults synchronize];
        
        if ([location isEqualToString:@"profile"]) {
            [userDefaults setObject:dataImgHeader forKey:@"dataImgProfile"];
        } else {
            [userDefaults setObject:dataImgHeader forKey:@"dataImgHeader"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            imgView.image = [UIImage imageWithData:dataImgHeader];
        });
    });
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
  if ([[segue identifier] isEqualToString:@"about"]) {
        ServiceAboutListVC *savc = [segue destinationViewController];
        
        if (tag == 1) {
            savc.strAbout = [[dictProInfoDetails objectForKey:@"about"] objectForKey:@"description"];
            savc.strHeader = [arrHeaderTitle objectAtIndex:1];
        } else if (tag == 2) {
            savc.strHeader = [arrHeaderTitle objectAtIndex:2];
            savc.arrServiceList = [[NSMutableArray alloc] init];
            [savc.arrServiceList removeAllObjects];
            [savc.arrServiceList addObjectsFromArray:[dictProInfoDetails objectForKey:@"services"]];
        } else if (tag == 4) {
            savc.strHeader = [arrHeaderTitle objectAtIndex:4];
            savc.arrServiceList = [[NSMutableArray alloc] init];
            [savc.arrServiceList removeAllObjects];
            [savc.arrServiceList addObjectsFromArray:[dictProInfoDetails objectForKey:@"service_area"]];
        }
        [savc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    } else if ([[segue identifier] isEqualToString:@"gallery"]) {
        
        GalleryVC *gvc = [segue destinationViewController];
        gvc.arrGallery = [[NSMutableArray alloc]init];
        gvc.arrGallery = [dictProInfoDetails objectForKey:@"licence"];
        gvc.indexPathNumber = iLicenceNumber;
        gvc.tagNumber = tag;
        [gvc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    }
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.providesPresentationContextTransitionStyle = YES;
    self.definesPresentationContext = YES;
}


- (void)didSelectedItem:(int)section at:(int)indexPath {
    NSLog(@"SECTION NUMBER IS:::...%d and INDEX PATH NUMBER IS:::...%d", section, indexPath);
    tag = section;
    indexPathNumber = indexPath;

//    NSArray *arrGallery = [[NSArray alloc] init];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
    if (tag == 7) {
        GalleryListVC *glvc = (GalleryListVC *)[storyboard instantiateViewControllerWithIdentifier:@"GalleryListVC"];
        glvc.arrGalleryList = [[NSMutableArray alloc] init];
        glvc.tagNumber = tag;
        glvc.strPortfolioId = [[[dictProInfoDetails objectForKey:@"project_gallery"] objectAtIndex:indexPathNumber] objectForKey:@"portfolio_id"];
        [self.navigationController pushViewController:glvc animated:YES];
    } else if (tag == 8) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[[[dictProInfoDetails objectForKey:@"achievement"] objectAtIndex:indexPath] objectForKey:@"achievement_type"] message:[[[dictProInfoDetails objectForKey:@"achievement"] objectAtIndex:indexPath] objectForKey:@"batch_award_date"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        alertController.view.tintColor = COLOR_THEME;
    }
}

#pragma mark - UITableView Button Action
-(void)ViewMoreOrViewAllButtonPressed:(UIButton *)sender {

    NSLog(@"SECCTION NUMBER:::...%ld", sender.tag);
    tag = (int)sender.tag;
    
    if (sender.tag == 1 || sender.tag == 2 || sender.tag == 4) {
        [self performSegueWithIdentifier:@"about" sender:nil];
    }
}

-(void)firstViewButtonPressed:(UIButton *)sender {
    
    iLicenceNumber = 0;
    tag = 5;
    NSLog(@"FIRST VIEW BUTTON PRESSED");
    [self performSegueWithIdentifier:@"gallery" sender:nil];
}

-(void)secondViewButtonPressed:(UIButton *)sender {
    
    iLicenceNumber = 1;
    tag = 5;
    NSLog(@"SECOND VIEW BUTTON PRESSED");
    [self performSegueWithIdentifier:@"gallery" sender:nil];
}

#pragma mark - UIButton Action
- (IBAction)ViewAllReviews:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
    ViewAllReviewVC *varvc = [storyboard instantiateViewControllerWithIdentifier:@"ViewAllReviewVC"];
    varvc.strTitle = [[dictProInfoDetails objectForKey:@"info"] objectForKey:@"company_name"];
    varvc.strProfileImage = [[dictProInfoDetails objectForKey:@"info"] objectForKey:@"profile_image"];
    varvc.strNumberOfReview = [dictProInfoDetails objectForKey:@"total_review"];
    varvc.strNumberOfRate = [dictProInfoDetails objectForKey:@"total_avg_review"];
    varvc.fltRating = [[dictProInfoDetails objectForKey:@"total_avg_review"] floatValue];
    varvc.isProfileDetails = YES;
//    varvc.arrServiceList = [[NSMutableArray alloc]init];
//    [varvc.arrServiceList addObjectsFromArray:[dictProInfoDetails objectForKey:@"services"]];
    [self.navigationController pushViewController:varvc animated:YES];
}

- (IBAction)RequestReview:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    RequestReviewVC *rrvc = [storyboard instantiateViewControllerWithIdentifier:@"RequestReviewVC"];
    rrvc.strRequestingReview = @"Requesting a review from Pros profile";
    [self.navigationController pushViewController:rrvc animated:YES];
    
//    [self ADSideMenuDrawerSelection:1 withRow:15 andTitle:@"Request Reviews"];
}

- (IBAction)EditProfile:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    CompanyProfileVC *rvc = [storyboard instantiateViewControllerWithIdentifier:@"CompanyProfileVC"];
    [self.navigationController pushViewController:rvc animated:YES];
}

- (IBAction)UpdateHeaderImage:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"upload background picture" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Take Photo using Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:nil];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Your device has no camera or camera is not supported" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }];
    UIAlertAction *photo = [UIAlertAction actionWithTitle:@"Choose from Photos" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
    }];
    
    [alertController addAction:camera];
    [alertController addAction:photo];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
    [alertController.view setTintColor:COLOR_THEME];
}

- (IBAction)ViewLicenceDetails:(id)sender {
    NSArray *arrGallery = [dictProInfoDetails objectForKey:@"licence"];
    strGalleryImage = [[arrGallery objectAtIndex:0] objectForKey:@"license_file"];
    
    tag = 5;
    
    [self performSegueWithIdentifier:@"gallery" sender:nil];
}

#pragma mark - UIImage Navigation
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    // output image
//    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
//    imgHeader.image = chosenImage;
//    [picker dismissViewControllerAnimated:YES completion:nil];
//
//    dataImgHeader = UIImagePNGRepresentation(chosenImage);
//
//    dataImgHeader = [NSData dataWithData:dataImgHeader];
//
//    [self connectionForUploadHeaderImage];
    
    chosenImage = info[UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        [self openEditor];
    }];
}

-(void)openEditor {
    self.cropController.image = chosenImage;
    self.cropController.keepingCropAspectRatio = YES;
    self.cropController.cropAspectRatio = 1.0;
    
    CGFloat ratio = 3.0f / 4.0f;
    CGRect cropRect = self.cropController.cropView.cropRect;
    CGFloat widthCrop = CGRectGetWidth(cropRect);
    cropRect.size = CGSizeMake(widthCrop, widthCrop * ratio);
    self.cropController.cropView.cropRect = cropRect;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.cropController];
    
    [self presentViewController:navigationController animated:YES completion:^{
        [self.cropController clickedButtonAtIndex:1];
    }];
}

- (void)cancelButtonDidPush:(id)sender {
    
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - PECropViewControllerDelegate methods
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
    chosenImage = croppedImage;
    
    [self resizeImage:chosenImage];
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage {
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
    chosenImage = croppedImage;
    
//    [self resizeAndConvertImageTodata];
    [self resizeImage:chosenImage];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:^{
//        self->imgLicence.image = self->chosenImage;
    }];
}

- (void)resizeImage:(UIImage*)image {
    
    NSData *finalData = nil;
    NSData *unscaledData = UIImagePNGRepresentation(image);
    
    if (unscaledData.length > 5000.0f ) {
        
        //if image size is greater than 5KB dividing its height and width maintaining proportions
        
        UIImage *scaledImage = [self imageWithImage:image andWidth:image.size.width/2 andHeight:image.size.height/2];
        finalData = UIImagePNGRepresentation(scaledImage);
        
        if (finalData.length > 500000.0f ) {
            
            [self resizeImage:scaledImage];
        } else {
            //scaled image will be your final image
            dataImgHeader = UIImagePNGRepresentation(scaledImage);
            chosenImage = scaledImage;
            //            imgLicence.image = chosenImage;
        }
    }
    //    imgLicence.image = chosenImage;
    [self connectionForUploadHeaderImage];
}

- (UIImage*)imageWithImage:(UIImage*)image andWidth:(CGFloat)width andHeight:(CGFloat)height {
    
    UIGraphicsBeginImageContext( CGSizeMake(width, height));
    [image drawInRect:CGRectMake(0,0,width,height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext() ;
    return newImage;
}

@end
