//
//  ViewAllReviewVC.h
//  ProringerPro
//
//  Created by Soma Halder on 15/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ProRingerProBaseVC.h"
#import "VIewAllReviewTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ViewAllReviewVC : ProRingerProBaseVC <ADReviewDescriptionDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblReviewList;

@property (weak, nonatomic) IBOutlet UIView *viewHeader;

@property (weak, nonatomic) IBOutlet UIImageView *imgUser;

@property (weak, nonatomic) IBOutlet EDStarRating *ratingView;

@property (weak, nonatomic) IBOutlet UILabel *lblNoOfRate;
@property (weak, nonatomic) IBOutlet UILabel *lblNoOfReview;

@property (assign, nonatomic) BOOL isProfileDetails;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constHeaderHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constHeaderTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constHeaderBottom;


@property (strong, nonatomic) NSString *strTitle, *strNumberOfRate, *strNumberOfReview, *strProfileImage;
@property (strong, nonatomic) NSData *dataProfileImage;
@property (nonatomic) float fltRating;

- (IBAction)RequestAReview:(id)sender;

@end

NS_ASSUME_NONNULL_END
