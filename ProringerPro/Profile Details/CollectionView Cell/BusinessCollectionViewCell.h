//
//  BusinessCollectionViewCell.h
//  ProRinger
//
//  Created by Soma Halder on 12/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BusinessCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDays;
@property (weak, nonatomic) IBOutlet UILabel *lblHours;

@end

NS_ASSUME_NONNULL_END
