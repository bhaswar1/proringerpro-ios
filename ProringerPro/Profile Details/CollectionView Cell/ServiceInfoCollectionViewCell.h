//
//  ServiceInfoCollectionViewCell.h
//  ProRinger
//
//  Created by Soma Halder on 09/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServiceInfoCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;

@end

NS_ASSUME_NONNULL_END
