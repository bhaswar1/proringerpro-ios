//
//  GalleryCollectionViewCell.h
//  ProRinger
//
//  Created by Soma Halder on 11/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GalleryCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgGallery;

@end

NS_ASSUME_NONNULL_END
