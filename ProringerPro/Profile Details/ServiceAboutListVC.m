//
//  ServiceAboutListVC.m
//  ProRinger
//
//  Created by Soma Halder on 12/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import "ServiceAboutListVC.h"
#import "ServiceListCollectionViewCell.h"

@interface ServiceAboutListVC () {
    
    UITapGestureRecognizer *tapGesture;
}

@end

@implementation ServiceAboutListVC
@synthesize collServiceList, lblAbout, lblHeader, strAbout, strHeader, arrServiceList, constViewHeight, txtAbout, constViewTop;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    lblHeader.text = strHeader;
    
    if ([strAbout isEqual:[NSNull null]] || strAbout == nil) {
        lblAbout.text = strAbout;
    } else {
        lblAbout.text = [NSString stringWithFormat:@"%@\n", strAbout];
    }
    
    if ([strAbout isEqualToString:@""] || [strAbout isEqual:[NSNull null]] || strAbout == nil) {
        collServiceList.hidden = NO;
    } else {
        collServiceList.hidden = YES;
    }
    
    collServiceList.dataSource = self;
    collServiceList.delegate = self;
    
    tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapToDismissView)];
    
    [self.view addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;
    
    if (arrServiceList == nil || arrServiceList.count == 0) {
        constViewHeight.constant = 0;
    } else {
        if ((arrServiceList.count % 2) > 0)
            constViewHeight.constant = ((arrServiceList.count /2) *25) +25;
        else
        constViewHeight.constant = ((arrServiceList.count /2) *25);
    }
    
    if (strAbout.length > 0) {
        strAbout = [strAbout stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
        strAbout = [strAbout stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
//        strAbout = [NSString stringWithFormat:@"\n%@\n", strAbout];
        txtAbout.text = strAbout;
//        if (constViewTop.constant == 50) {
//            txtAbout.scrollEnabled = YES;
//        } else {
//            txtAbout.scrollEnabled = NO;
//        }
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if (CGRectContainsPoint(lblAbout.bounds, [touch locationInView:lblAbout])) {
        return NO;
    } else if (CGRectContainsPoint(lblHeader.bounds, [touch locationInView:lblHeader])) {
        return NO;
    } else if (CGRectContainsPoint(collServiceList.bounds, [touch locationInView:collServiceList])) {
        return NO;
    } else
        return YES;
}

-(void)tapToDismissView {
    
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrServiceList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellServices";
    
    NSDictionary *dict = [arrServiceList objectAtIndex:indexPath.row];
    
    ServiceListCollectionViewCell *cell = (ServiceListCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSString *strText = [dict objectForKey:@"service_name"];
    
    if (strText == nil || [strText isEqual:[NSNull null]]) {
        strText = [dict objectForKey:@"city_services"];
    }
    
    cell.lblServiceList.text = strText;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake((collServiceList.frame.size.width /2) -3, 22);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)CloseView:(id)sender {
    
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
}
@end
