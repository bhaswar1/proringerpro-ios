//
//  ReplyReportVC.m
//  ProringerPro
//
//  Created by Soma Halder on 15/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ReplyReportVC.h"
#import "IQKeyboardManager.h"

@interface ReplyReportVC () {
    
    NSMutableArray *arrRestrictedWordsFound;
    
    UITapGestureRecognizer *gesture;
}

@end

@implementation ReplyReportVC
@synthesize viewContent, lblPlaceholder, strApi, strNavigationTitle, strPlaceHolder, txtReplyReview, constBottomSubmit, strHomeownerId, strReviewReplyId, strProId, scrollView, toolBar, viewAccessory, constHeightSubmit;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = strNavigationTitle;
    
    [self connectionForGetDashboardData];
    
    gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapToDismissKeyboard)];
    
    lblPlaceholder.text = strPlaceHolder;
    
    txtReplyReview.delegate = self;
//    txtReplyReview.inputAccessoryView = viewAccessory;
    
    arrRestrictedWordsFound = [[NSMutableArray alloc] init];
    
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    txtReplyReview.inputAccessoryView = viewAccessory;
    viewAccessory.backgroundColor = COLOR_LIGHT_BACK;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if (CGRectContainsPoint(txtReplyReview.bounds, [touch locationInView:txtReplyReview])) {
        return NO;
    } else
        return YES;
}

-(void) tapToDismissKeyboard {
    
    [self doneButtonPressed];
}

-(void)doneButtonPressed {
    
    [txtReplyReview resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@""]) {
        
        NSLog(@"%@",textView.text);
        
        if (textView.text.length == 1) {
            lblPlaceholder.hidden = NO;
        }
    } else if ([NSString stringWithFormat:@"%@%@",textView.text,text].length > 0) {
        
        lblPlaceholder.hidden = YES;
    } else {
        lblPlaceholder.hidden = NO;
    }
    
    return YES;
}

#pragma mark - UITextView Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    textView.layer.borderColor = [UIColor clearColor].CGColor;
    textView.layer.borderWidth = 1.5f;
    
    constHeightSubmit.constant = 0.0;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    constHeightSubmit.constant = 45.0;
    
    [arrRestrictedWordsFound removeAllObjects];
    
    NSString *strText = [textView.text stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    NSArray *arrNewWords = [strText componentsSeparatedByString:@" "];
    NSLog(@"WORDS ARRAY FROM TEXTVIEW:::...%@", arrNewWords);
    arrRestrictedWordsFound = [self doArraysContainTheSameObjects:arrNewWords];
    
    if ([textView.text isEqualToString:@""]) {
        lblPlaceholder.hidden = NO;
    } else {
        lblPlaceholder.hidden = YES;
    }
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}


- (BOOL)textViewShouldEndEditing:(UITextView *)textView {

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    return YES;
}

// Bike insurence 875081994

- (void)keyboardDidShow:(NSNotification *)notification {
    // Assign new frame to your view
//    [self.view setFrame:CGRectMake(0, -230, self.view.frame.size.width, self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    //    [self.view endEditing:NO];
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    scrollView.scrollEnabled = NO;
    NSLog(@"KEYBOARD HEIGHT:::...%f", keyboardSize.height);
    constBottomSubmit.constant = keyboardSize.height;
}

-(void)keyboardDidHide:(NSNotification *)notification {
//    [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    //    [self.view endEditing:NO];
    scrollView.scrollEnabled = YES;
    constBottomSubmit.constant = 15.0;
}

-(void)connectionForReplyOrReport {
    
    [YXSpritesLoadingView show];
    
    NSLog(@"INTERSTITIAL AD:::...%@", self.arrInterstitialAd);
    
    NSString *strComment = [NSString stringWithFormat:@"%@", txtReplyReview.text];
    strComment = [strComment stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, strApi];
    NSDictionary *params;
    
    if ([strApi isEqualToString:@"app_homeowner_reportreview"]) {
        params = @{@"user_id": strProId,
                   @"review_report_id": strReviewReplyId,
                   @"report_comment": strComment};
    } else {
        params = @{@"user_id": strHomeownerId,
                   @"pro_id": strProId,
                   @"review_reply_id": strReviewReplyId,
                   @"reply_comment": strComment};
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[responseObject objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                if ([self->strNavigationTitle isEqualToString:@"Report Abuse"]) {
                    if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
                        if (self.arrInterstitialAd.count > 0) {
                            for (int i = 0; i < self.arrInterstitialAd.count; i++) {
                                NSDictionary *dict = [self.arrInterstitialAd objectAtIndex:i];
                                if ([[dict objectForKey:@"title"] isEqualToString:@"Report abuse"]) {
                                    if ([[dict objectForKey:@"ios_interstitial_ads"] intValue] == 1) {
                                        if (self.interstitial.isReady) {
                                            [self.interstitial presentFromRootViewController:self];
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if ([self->strNavigationTitle isEqualToString:@"Reply"]) {
                    if ([[self.dictInfoArray objectForKey:@"pro_premium_status"] intValue] == 0) {
                        if (self.arrInterstitialAd.count > 0) {
                            for (int i = 0; i < self.arrInterstitialAd.count; i++) {
                                NSDictionary *dict = [self.arrInterstitialAd objectAtIndex:i];
                                if ([[dict objectForKey:@"title"] isEqualToString:@"Respond to review"]) {
                                    if ([[dict objectForKey:@"ios_interstitial_ads"] intValue] == 1) {
                                        if (self.interstitial.isReady) {
                                            [self.interstitial presentFromRootViewController:self];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
            [alertController.view setTintColor:COLOR_THEME];
        } else {
            [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:[responseObject objectForKey:@"message"]];
        }
        
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)SubmitButtonPressed:(id)sender {
    
//    NSArray *arr = [[NSArray alloc] initWithObjects:@"", @"About", @"Services", @"Business Hours", @"Service Area", @"License", @"Company info", @"Project Gallery", @"Milestones", nil];
//
//    [arrRestrictedWordsFound addObjectsFromArray:arr];
    
    [txtReplyReview resignFirstResponder];
    
    [self textViewDidEndEditing:txtReplyReview];
    
    if (txtReplyReview.text.length > 30) {
        if ([[AppManager sharedDataAccess] detectType:txtReplyReview.text]) {
            [[[UIAlertView alloc]initWithTitle:@"" message:self.strRestrictionMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            [txtReplyReview becomeFirstResponder];
        } else if (arrRestrictedWordsFound.count > 0) {
            NSString *strKhisti = [arrRestrictedWordsFound componentsJoinedByString:@", "];
            [[[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"Invalid term: %@ - Please be respectful of others. Remove and try again", strKhisti] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            [txtReplyReview becomeFirstResponder];
        } else {
            [self connectionForReplyOrReport];
        }
    } else {
        [[AppManager sharedDataAccess] showAlertWithTitle:@"" andMessage:@"Please tell us more than 30 characters"];
    }
}

- (IBAction)doneButtonPressed:(id)sender {
    
    [self doneButtonPressed];
}
@end
