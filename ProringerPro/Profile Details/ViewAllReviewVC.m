//
//  ViewAllReviewVC.m
//  ProringerPro
//
//  Created by Soma Halder on 15/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ViewAllReviewVC.h"
#import "VIewAllReviewTableViewCell.h"
#import "ServiceAboutListVC.h"

@interface ViewAllReviewVC ()
{
    NSMutableArray *arrReviewList;
    int iStartForm, iNextData;
    NSString *strSelectedDescription;
}

@end

@implementation ViewAllReviewVC

@synthesize tblReviewList, viewHeader, lblNoOfRate, lblNoOfReview, strTitle, imgUser, ratingView, fltRating, strProfileImage, strNumberOfRate, strNumberOfReview, isProfileDetails, constHeaderTop, constHeaderBottom, constHeaderHeight;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectionForGetDashboardData];
    
    self.navigationItem.title = strTitle;
    
    tblReviewList.dataSource = self;
    tblReviewList.delegate = self;
    
    iNextData = iStartForm = 0;
    
    arrReviewList = [[NSMutableArray alloc] init];
    
    if (isProfileDetails) {
        lblNoOfRate.text = strNumberOfRate;
        lblNoOfReview.text = strNumberOfReview;
        
        [imgUser sd_setImageWithURL:[NSURL URLWithString:strProfileImage]];
        
        ratingView.maxRating = 5.0;
        ratingView.delegate = self;
        ratingView.horizontalMargin = 0;
        ratingView.editable = NO;
        ratingView.displayMode = EDStarRatingDisplayAccurate;
        ratingView.rating = fltRating;
        ratingView.starImage = [UIImage imageNamed:@"StarEmpty"];
        ratingView.starHighlightedImage = [UIImage imageNamed:@"StarFull"];
        
        constHeaderHeight.constant = 120.0;
        constHeaderBottom.constant = constHeaderTop.constant = 8.0;
        viewHeader.hidden = NO;
    } else {
        constHeaderHeight.constant = 0.0;
        constHeaderBottom.constant = constHeaderTop.constant = 0.0;
        viewHeader.hidden = YES;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callDashboardWebService:) name:@"TestNotification" object:nil];
    
//    [self connectionForGetReviewList];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    iNextData = iStartForm = 0;
    [self connectionForGetReviewList];
}

- (void) callDashboardWebService:(NSNotification *) notification {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSDate *datePrevious = [userDefault objectForKey:@"date"];
    NSDate *today = [NSDate date];
    
    [userDefault synchronize];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//    NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:datePrevious toDate:today options:0];
//    NSLog(@"Remaining days:::...%ld", [components day]);
    
    if (self.iRateFlag == 1) {
        if (self.iRateStatus == 0) {
            [[AppManager sharedDataAccess] showAlertWithTitle];
        } else if (self.iRateStatus == 2) {
            if (datePrevious == nil) {
                [[AppManager sharedDataAccess] showAlertWithTitle];
            } else {
                NSDateComponents *components = [calendar components:NSCalendarUnitDay fromDate:datePrevious toDate:today options:0];
                NSLog(@"Remaining days:::...%ld", [components day]);
                
                if ([components day] > 5) {
                    [[AppManager sharedDataAccess] showAlertWithTitle];
                }
            }
        }
    }
}

#pragma mark - UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrReviewList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellReview";
    
    VIewAllReviewTableViewCell *cell = (VIewAllReviewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.delegate = self;
    cell.btnReply.tag = cell.btnReport.tag = cell.btnReadFullReview.tag = cell.btnReadFullReply.tag = indexPath.row;
    
    [cell.btnReport addTarget:self action:@selector(reportButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnReply addTarget:self action:@selector(replyButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnReadFullReview addTarget:self action:@selector(readFullReviewButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnReadFullReply addTarget:self action:@selector(readFullReplyButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([[[arrReviewList objectAtIndex:indexPath.row] objectForKey:@"rater_description"] length] > 120) {
        cell.btnReadFullReview.hidden = NO;
    } else {
        cell.btnReadFullReview.hidden = YES;
    }
    
    if ([[[arrReviewList objectAtIndex:indexPath.row] objectForKey:@"review_reply_content"] length] > 150) {
        cell.btnReadFullReply.hidden = NO;
    } else {
        cell.btnReadFullReply.hidden = YES;
    }
    
    [cell configureReviewListCellWith:[arrReviewList objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    CGFloat height = scrollView.frame.size.height;
    CGFloat contentYoffset = scrollView.contentOffset.y;
    CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
    
    if (distanceFromBottom < height) {
        if (iNextData == 1) {
            [self connectionForGetReviewList];
        }
    }
}

#pragma mark - UITableView Button Action
-(void)replyButtonPressed:(UIButton *)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
    ReplyReportVC *rrvc = [storyboard instantiateViewControllerWithIdentifier:@"ReplyReportVC"];
    rrvc.strPlaceHolder = @"What is the reply?";
    rrvc.strNavigationTitle = @"Reply";
    rrvc.strApi = API_REPLY;
    rrvc.strHomeownerId = [[arrReviewList objectAtIndex:sender.tag] objectForKey:@"user_id"];
    rrvc.strProId = [[arrReviewList objectAtIndex:sender.tag] objectForKey:@"pros_id"];
    rrvc.strReviewReplyId = [[arrReviewList objectAtIndex:sender.tag] objectForKey:@"id"];
    [self.navigationController pushViewController:rrvc animated:YES];
}

-(void)reportButtonPressed:(UIButton *)sender {
    
    NSLog(@"DICTIONARY:::...%@", [arrReviewList objectAtIndex:sender.tag]);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
    ReplyReportVC *rrvc = [storyboard instantiateViewControllerWithIdentifier:@"ReplyReportVC"];
    rrvc.strPlaceHolder = @"What is the abuse?";
    rrvc.strNavigationTitle = @"Report Abuse";
    rrvc.strApi = API_REPORT;
    rrvc.strHomeownerId = [[arrReviewList objectAtIndex:sender.tag] objectForKey:@"user_id"];
    rrvc.strProId = [[arrReviewList objectAtIndex:sender.tag] objectForKey:@"pros_id"];
    rrvc.strReviewReplyId = [[arrReviewList objectAtIndex:sender.tag] objectForKey:@"id"];
    [self.navigationController pushViewController:rrvc animated:YES];
}

-(void)readFullReviewButtonPressed:(UIButton *)sender {
    
    [self navigateToReadFullReviewWithString:[[arrReviewList objectAtIndex:sender.tag] objectForKey:@"rater_description"]];
}

-(void)readFullReplyButtonPressed:(UIButton *)sender {
    
    [self navigateToReadFullReviewWithString:[[arrReviewList objectAtIndex:sender.tag] objectForKey:@"review_reply_content"]];
}

-(void)navigateToReadFullReviewWithString:(NSString *)selectedString {
    
    strSelectedDescription = selectedString;
    NSLog(@"STRING FROM MAIN CLASS:::...%@", selectedString);
    [self performSegueWithIdentifier:@"about" sender:nil];
}

#pragma mark - Web Service
-(void)connectionForGetReviewList {
    
    [YXSpritesLoadingView show];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", ROOT_URL, API_ALL_REVIEW_LIST];
    
    NSDictionary *params = @{@"user_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"pro_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                             @"start_from": [NSString stringWithFormat:@"%d", iStartForm],
                             @"perpage": @"10"};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet *contentTypes = [[NSMutableSet alloc]initWithSet:[NSSet setWithObject:@"text/json"]];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"RESPONSE FROM REVIEW LIST::::....%@", responseObject);
        
        if ([[responseObject objectForKey:@"response"] intValue] == 1) {
            
            if (self->iStartForm == 0) {
                
                [self->arrReviewList removeAllObjects];
            }
            [self->arrReviewList addObjectsFromArray:[responseObject objectForKey:@"info_array"]];
            self->iNextData = [[responseObject objectForKey:@"next_data"] intValue];
            if (self->iNextData == 1) {
                self->iStartForm = self->iStartForm +10;
            }
            [self->tblReviewList reloadData];
        }
        [YXSpritesLoadingView dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [YXSpritesLoadingView dismiss];
        NSLog(@"ERROR:::%@", task.error);
    }];
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"about"]) {
        ServiceAboutListVC *savc = [segue destinationViewController];
        
        savc.strAbout = strSelectedDescription;
        savc.strHeader = @"Review";
        [savc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    }
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.providesPresentationContextTransitionStyle = YES;
    self.definesPresentationContext = YES;
}

#pragma mark - UIButton Action
- (IBAction)RequestAReview:(id)sender {
    
    [self ADSideMenuDrawerSelection:1 withRow:15 andTitle:@"Request Reviews"];
}
@end
