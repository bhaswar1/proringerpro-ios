//
//  ServiceAboutListVC.h
//  ProRinger
//
//  Created by Soma Halder on 12/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServiceAboutListVC : UIViewController <UIGestureRecognizerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UICollectionView *collServiceList;
@property (weak, nonatomic) IBOutlet UILabel *lblAbout;
@property (weak, nonatomic) IBOutlet UITextView *txtAbout;

@property (strong, nonatomic) NSString *strAbout;
@property (strong, nonatomic) NSString *strHeader;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewTop;

@property (strong, nonatomic) NSMutableArray *arrServiceList;

- (IBAction)CloseView:(id)sender;
@end

NS_ASSUME_NONNULL_END
