//
//  ReplyReportVC.h
//  ProringerPro
//
//  Created by Soma Halder on 15/10/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "ProRingerProBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReplyReportVC : ProRingerProBaseVC <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceholder;
@property (weak, nonatomic) IBOutlet UITextView *txtReplyReview;
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;

@property (strong, nonatomic) IBOutlet UIView *viewAccessory;

@property (strong, nonatomic) NSString *strPlaceHolder, *strNavigationTitle, *strApi, *strProId, *strReviewReplyId, *strHomeownerId;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constBottomSubmit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constHeightSubmit;


- (IBAction)SubmitButtonPressed:(id)sender;
- (IBAction)doneButtonPressed:(id)sender;

@end

NS_ASSUME_NONNULL_END
