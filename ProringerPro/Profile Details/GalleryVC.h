//
//  GalleryVC.h
//  ProRinger
//
//  Created by Soma Halder on 22/03/19.
//  Copyright © 2019 Kausik Jati. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GalleryVC : ProRingerProBaseVC <UIGestureRecognizerDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
//centerYConstraint

@property (weak, nonatomic) IBOutlet UIImageView *imgGallery;

@property (strong, nonatomic) NSString *strImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerXConstraint;

@property (nonatomic) NSUInteger indexPathNumber;
@property (nonatomic) NSUInteger tagNumber;
@property (strong, nonatomic) NSMutableArray *arrGallery;

- (IBAction)DismissButtonPressed:(id)sender;

@end

NS_ASSUME_NONNULL_END
