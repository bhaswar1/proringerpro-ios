//
//  Footer.h
//  ProRinger
//
//  Created by Kausik Jati on 10/06/17.
//  Copyright © 2017 Kausik Jati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol ADFooterDelegate <NSObject>

-(void)footerButtonTap:(int)iTag;

@end

@interface Footer : UIView
{
    
    
}
@property (strong, nonatomic) IBOutlet UIView *footer_contentview;
@property (weak, nonatomic) IBOutlet UIView *viewShadow;

@property (weak, nonatomic) IBOutlet UILabel *lblDashboard;
@property (weak, nonatomic) IBOutlet UILabel *lblMyProject;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblWatchlist;

@property (weak, nonatomic) IBOutlet UILabel *lblNewProject;
@property (weak, nonatomic) IBOutlet UILabel *lblNewMessage;

@property (weak, nonatomic) IBOutlet UIImageView *imgDashboard;
@property (weak, nonatomic) IBOutlet UIImageView *imgMyProject;
@property (weak, nonatomic) IBOutlet UIImageView *imgMessage;
@property (weak, nonatomic) IBOutlet UIImageView *imgWatchlist;

@property (weak, nonatomic) IBOutlet UIButton *btnDashboard;
@property (weak, nonatomic) IBOutlet UIButton *btnProjects;
@property (weak, nonatomic) IBOutlet UIButton *btnMessages;
@property (weak, nonatomic) IBOutlet UIButton *btnWatchlist;

- (IBAction)footerBtnTap:(UIButton *)sender;

@property(assign) id <ADFooterDelegate> footerDelegate;

@end
