//
//  Footer.m
//  ProRinger
//
//  Created by Kausik Jati on 10/06/17.
//  Copyright © 2017 Kausik Jati. All rights reserved.
//

#import "Footer.h"

@implementation Footer
{
    
}
@synthesize footer_contentview, footerDelegate, lblMessage, lblDashboard, lblMyProject, lblWatchlist, imgMessage, imgDashboard, imgMyProject, imgWatchlist, btnMessages, btnProjects, btnDashboard, btnWatchlist, lblNewProject, lblNewMessage;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"Footer" owner:self options:nil]objectAtIndex:0];
    }
    
    footer_contentview.layer.masksToBounds = NO;
    footer_contentview.layer.shadowOffset = CGSizeMake(3, 5);
    footer_contentview.layer.shadowRadius = 1.5;
    footer_contentview.layer.shadowOpacity = 0.5;
    footer_contentview.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    
    lblNewProject.backgroundColor = lblNewMessage.backgroundColor = COLOR_THEME;
    lblNewMessage.layer.borderColor = lblNewProject.layer.borderColor = [UIColor whiteColor].CGColor;
    lblNewProject.layer.borderWidth = lblNewMessage.layer.borderWidth = 1.5;
//    lblNewProject.layer.masksToBounds = lblNewMessage.layer.masksToBounds = YES;
    
    return self;
}

- (IBAction)footerBtnTap:(UIButton *)sender {
    
    lblNewProject.backgroundColor = lblNewMessage.backgroundColor = COLOR_THEME;
    lblNewMessage.layer.borderColor = lblNewProject.layer.borderColor = [UIColor whiteColor].CGColor;
    lblNewProject.layer.borderWidth = lblNewMessage.layer.borderWidth = 1.5;
    
    NSString *strTag = [NSString stringWithFormat:@"%ld", (long)sender.tag];
    
    [[NSUserDefaults standardUserDefaults] setObject:strTag forKey:@"footer"];
    
//    if (sender.tag == 1) {
//        [btnDashboard setTintColor:COLOR_THEME];
//        btnDashboard.tintColor = COLOR_THEME;
//    } else if (sender.tag == 2) {
//        [btnProjects setTintColor:COLOR_THEME];
//    } else if (sender.tag == 3) {
//        [btnMessages setTintColor:COLOR_THEME];
//    } else if (sender.tag == 4) {
//        [btnWatchlist setTintColor:COLOR_THEME];
//    }
    
    [self footerDesignWith:sender.tag];
}

-(void)footerDesignWith:(int)iTag {
    
    [footerDelegate footerButtonTap:iTag];
}

@end
