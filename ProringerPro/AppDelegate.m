//
//  AppDelegate.m
//  ProringerPro
//
//  Created by Soma Halder on 18/04/19.
//  Copyright © 2019 Soma Halder. All rights reserved.
//

#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "firebase.h"
#import <FirebaseCore/FIRApp.h>
#import <FirebaseCore/FirebaseCore.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@import GoogleMaps;
@import GooglePlaces;
@import Firebase;
@import UserNotifications;
@import FirebaseAnalytics;
//@import FirebaseMessaging;
@import GoogleMobileAds;

@interface AppDelegate () <UIApplicationDelegate, UNUserNotificationCenterDelegate, UIResponderStandardEditActions, FIRMessagingDelegate, FIRMessagingDelegate, ADCustomNavigation>
{
    NSDictionary *dictFromData;
    NSString *strMsgProjectId, *strProProjectId;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    int iBadge = [[userDefault objectForKey:@"badge"] intValue];
    [userDefault setObject:[NSString stringWithFormat:@"%d", iBadge] forKey:@"badge"];
    [userDefault synchronize];
    
    if (([[userDefault objectForKey:@"isLoggedIn"] boolValue] == YES)) {
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:iBadge];
    } else {
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }
    
    strProProjectId = strMsgProjectId = [[NSString alloc] init];
    
    [Fabric with:@[[Crashlytics class]]];
    
    [[UITextField appearance] setTintColor: COLOR_THEME];
    [[UITextView appearance] setTintColor: COLOR_THEME];
    
    [GMSServices provideAPIKey:@"AIzaSyDSYXgSTcdBaf0fZjOrbaC2UEePVrYVNq8"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyDSYXgSTcdBaf0fZjOrbaC2UEePVrYVNq8"];
    
    [[GADMobileAds sharedInstance] startWithCompletionHandler:nil];
    [[GADMobileAds sharedInstance]
     startWithCompletionHandler:^(GADInitializationStatus *_Nonnull status) {
         GADAdapterInitializationState adapterState = status.adapterStatusesByClassName[@"SampleAdapter"].state;
         
         if (adapterState == GADAdapterInitializationStateReady) {
             NSLog(@"Sample adapter was successfully initialized.");
         } else {
             NSLog(@"Sample adapter is not ready.");
         }
     }];
    
//    http://www.danstools.com/email-validator/?apikey=API-KEY&apiid=API-ID&email=EMAIL_TO_VERIFY
    
//    [self.window addSubview:viewController.view];
    [self.window makeKeyAndVisible];
    NSLog(@"Registering for push notifications...");
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    [self registerForRemoteNotifications];
    
//    [GMSServices provideAPIKey:@"AIzaSyBIGY1ZzRFrtpS8Zb_j2JX7nCQdbw83gJI"];
//    [GMSPlacesClient provideAPIKey:@"AIzaSyBIGY1ZzRFrtpS8Zb_j2JX7nCQdbw83gJI"];
    
     [FIRApp configure];
    
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             // ...
         }];
    } else {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    [application registerForRemoteNotifications];

   
    [FIRMessaging messaging].delegate = self;
    [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
    
//    [application registerForRemoteNotifications];
//    [self configureFCM:application launchingWithOptions:launchOptions];
    
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        
        [application registerUserNotificationSettings:[UIUserNotificationSettings
                                                       settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|
                                                       UIUserNotificationTypeSound categories:nil]];
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    
    return YES;
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
//    NSString *Tokenstr = [NSString stringWithFormat:@"%@",deviceToken];
//    Tokenstr = [Tokenstr stringByReplacingOccurrencesOfString:@"<" withString:@""];
//    Tokenstr = [Tokenstr stringByReplacingOccurrencesOfString:@">" withString:@""];
//    Tokenstr = [Tokenstr stringByReplacingOccurrencesOfString:@" " withString:@""];
//
//    [[NSUserDefaults standardUserDefaults]setObject:Tokenstr forKey:@"deviceToken"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [FIRMessaging messaging].APNSToken = deviceToken;
//    [[NSUserDefaults standardUserDefaults] setValue:deviceToken forKey:@"deviceToken"];
//    NSLog(@"Device Token=%@",deviceToken);
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    //_apns = userInfo;
//    [self handleRemoteNotifications:userInfo withApplication:application];
    NSLog(@"NOTIFICATION DETAILS:::...%@", userInfo);
    
    NSString *strBadge = [[userInfo objectForKey:@"aps"] objectForKey:@"badge"];
    [[NSUserDefaults standardUserDefaults] setObject:strBadge forKey:@"badge"];
    
    [application setApplicationIconBadgeNumber:[strBadge intValue]];
}


-(void)handleRemoteNotifications:(NSDictionary*)userInfo withApplication:(UIApplication*)application {
    //userInfo
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    //    UIAlertView *new = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@",userInfo ] message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //    [new show];
    //
    
//    NSLog(@"NOTIFICATION DETAILS:::...%@\nMESSAGE:::...%@", userInfo, notifMessage);
    
    NSString *strBadge = [[userInfo objectForKey:@"aps"] objectForKey:@"badge"];
    [[NSUserDefaults standardUserDefaults] setObject:strBadge forKey:@"badge"];
    
    [application setApplicationIconBadgeNumber:[strBadge intValue]];
    
    NSLog(@"Badge %d",[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue]);
    NSLog(@"SHOW BADGE VALLUE FROM USER DEFAULT:::...%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"badge"]);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationMessageEvent" object:nil userInfo:userInfo];
    
    NSString *num_str = [[NSUserDefaults standardUserDefaults] valueForKey:@"userunreadmessage"];
    
    NSUInteger countnumber = [num_str integerValue] +1;
    
    
    [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%lu",(unsigned long)countnumber] forKey:@"userunreadmessage"];
    
    UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound  categories:nil];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    
    //    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:123];
    
    //here start
    
    if (application.applicationState == UIApplicationStateActive) {
        //
        //        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"alert" message:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",userInfo ]] delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
        //        [alert show];
        
        //start new
        
        NSString *notifMessage = [NSString stringWithFormat:@"%@",[userInfo valueForKeyPath:@"aps.alert.msg_body.message_info.message_info"]];
        
        NSString *username = [NSString stringWithFormat:@"%@",[userInfo valueForKeyPath:@"aps.alert.msg_body.message_info.usersname"]];
        
        NSLog(@"NOTIFICATION DETAILS:::...%@\nMESSAGE:::...%@", userInfo, notifMessage);
        
        //Define notifView as UIView in the header file
        //        [notifView removeFromSuperview]; //If already existing
        
        //        notifView = [[UIView alloc] initWithFrame:CGRectMake(10, -50, self.window.frame.size.width-20, 80)];
        //        notifView.layer.cornerRadius=8.0f;
        //        [notifView setBackgroundColor:[UIColor colorWithRed:189.0f/255 green:78.0f/255 blue:83.0f/255 alpha:1]];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10,20,30,30)];
        imageView.image = [UIImage imageNamed:@"Image"];
        
        UILabel *usernameLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 10, self.window.frame.size.width - 100 , 30)];
        
        UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 27, self.window.frame.size.width - 100 , 30)];
        
        imageView.layer.cornerRadius = 7.0f;
        imageView.clipsToBounds = YES;
        
        myLabel.font = [UIFont fontWithName:@"OpenSans" size:14.0];
        myLabel.text = notifMessage;
        
        [myLabel setTextColor:[UIColor whiteColor]];
        [myLabel setNumberOfLines:0];
        
        usernameLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:10.0];
        usernameLabel.text = username;
        
        [usernameLabel setTextColor:[UIColor whiteColor]];
        [usernameLabel setNumberOfLines:0];
        
        //        [notifView setAlpha:0.95];
        
        //The Icon
        //        [notifView addSubview:imageView];
        
        //The Text
        //        [notifView addSubview:myLabel];
        //        [notifView addSubview:usernameLabel];
        
        //The View
        //        [self.window addSubview:notifView];
        
        UITapGestureRecognizer *tapToDismissNotif = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                            action:@selector(dismissNotifFromScreen)];
        tapToDismissNotif.numberOfTapsRequired = 1;
        tapToDismissNotif.numberOfTouchesRequired = 1;
        
        //        [notifView addGestureRecognizer:tapToDismissNotif];
        
        [UIView animateWithDuration:1.0 delay:.1 usingSpringWithDamping:0.5 initialSpringVelocity:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            //            [notifView setFrame:CGRectMake(0, 0, self.window.frame.size.width, 60)];
            
        } completion:^(BOOL finished) {
        }];
        
        //Remove from top view after 5 seconds
        [self performSelector:@selector(dismissNotifFromScreen) withObject:nil afterDelay:5.0];
        
        return;
    } else if (application.applicationState == UIApplicationStateBackground || application.applicationState == UIApplicationStateInactive) {
        // Do something else rather than showing an alert view, because it won't be displayed.
        //        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"alert" message:[NSString stringWithFormat:@"%d",[[[userInfo objectForKey:@"aps"] objectForKey: @"badge"] intValue]] delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
        //        [alert show];
        
//        [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo objectForKey:@"aps"] objectForKey: @"badge"] intValue];
    }
    
//    [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo objectForKey:@"aps"] objectForKey: @"badge"] intValue];

    //here end
    NSLog(@"%@", userInfo);
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)dismissNotifFromScreen{
    
    [UIView animateWithDuration:1.0 delay:.1 usingSpringWithDamping:0.5 initialSpringVelocity:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
//        [notifView setFrame:CGRectMake(0, -70, self.window.frame.size.width, 60)];
        
    } completion:^(BOOL finished) {
        
    }];
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
    
    if ([fcmToken length] == 0 || [fcmToken isKindOfClass:[NSNull class]] || [fcmToken isEqual:@"<null>"] || fcmToken==nil || [fcmToken isEqual:@"(null)"]) {
        fcmToken = @"cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e";
        [[NSUserDefaults standardUserDefaults]setValue:fcmToken forKey:@"deviceToken"];
    } else {
        [[NSUserDefaults standardUserDefaults]setValue:fcmToken forKey:@"deviceToken"];
        
        //        UIAlertView *Alert = [[UIAlertView alloc]initWithTitle:@"Alert!!!" message:[[NSUserDefaults standardUserDefaults]objectForKey:DEVICETOKEN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //        [Alert show];
    }
    
    NSLog(@"device token here: %@",fcmToken);
    [[NSUserDefaults standardUserDefaults] synchronize];

    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}



- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
//    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
//    NSLog(@"InstanceID token: %@", refreshedToken);
    //dC5qKQBu9XA:APA91bGN2wA3WFMymrGCXVjFiIKQb9TT7m4CyRYVT4fLvxe0wxxIVsmC1rji3d0SP-sy01v7eey9MamLw0_Dd9Q1_aqj8W-I5exvZaAwlFNpFVrFbhTf29Ufzfv2tUlX_biYVHUUNvp8
    
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    
    
//    if([refreshedToken length] > 1) {
//        [[NSUserDefaults standardUserDefaults] setObject:refreshedToken forKey:@"FCM_Token"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
    
        [self connectToFcm];
        //[self post_push_token];
//    }
    
    // TODO: If necessary send token to application server.
}

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    
    NSLog(@"willPresentNotification User Info : %@",notification.request.content.userInfo);
    
    NSInteger iBadge = [[[notification.request.content.userInfo objectForKey:@"aps"] objectForKey:@"badge"] integerValue];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld", (long)iBadge] forKey:@"badge"];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:iBadge];
    
    NSData *dataFromDict = [[notification.request.content.userInfo objectForKey:@"gcm.notification.info"] dataUsingEncoding:NSUTF8StringEncoding];
    
    dictFromData = [NSJSONSerialization JSONObjectWithData:dataFromDict
                                                   options:0
                                                     error:nil];
    
    strProProjectId = [dictFromData objectForKey:@"project_id"];
    
    NSLog(@"Dictionary...%@",dictFromData);
    
    UINavigationController *navController1 = (UINavigationController *)self.window.rootViewController;
    
    [self.notificationView removeFromSuperview];
    self.notificationView = [[UIView alloc]init];
    self.notificationView.backgroundColor = [UIColor whiteColor];
    self.notificationView.frame = CGRectMake(12, 30, navController1.visibleViewController.navigationController.view.frame.size.width - 24, 90);   //Anupam
    self.notificationView.layer.cornerRadius = 10.0f;
    self.notificationView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.notificationView.layer.borderWidth = 1.0f;
    
    [UIView animateWithDuration:0.5f delay:0.0 options: UIViewAnimationOptionCurveEaseOut animations:^{
        
        self.notificationView.frame = CGRectMake(10, 12,  navController1.visibleViewController.navigationController.view.frame.size.width - 24, 90);
        
    }
                     completion:^(BOOL finished)
     {
     }];
    
    UIImageView *iconImg = [[UIImageView alloc] initWithFrame:CGRectMake(20, 10, 30, 30)];
    iconImg.image = [UIImage imageNamed:@"AppIcon"];
    iconImg.contentMode = UIViewContentModeScaleAspectFit;
    iconImg.layer.cornerRadius = 6.0;
    iconImg.clipsToBounds = YES;
    [self.notificationView addSubview:iconImg];
    
    UILabel *applbl = [[UILabel alloc] initWithFrame:CGRectMake(20, iconImg.frame.origin.y + iconImg.frame.size.height +5, navController1.visibleViewController.navigationController.view.frame.size.width -40, 20)];
    [self.notificationView addSubview:applbl];
    applbl.text = [NSString stringWithFormat:@"%@", [[[notification.request.content.userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"title"]];
    applbl.textAlignment = NSTextAlignmentLeft;
    applbl.numberOfLines = 0;
    applbl.textColor = [UIColor blackColor];
    //            [applbl setFont:[UIFont fontWithName:applbl.font.fontName size:12]];
    applbl.font = [UIFont systemFontOfSize:14 weight:UIFontWeightSemibold];
    
    UILabel *applbl2 = [[UILabel alloc] initWithFrame:CGRectMake(20, applbl.frame.origin.y + applbl.frame.size.height, navController1.visibleViewController.navigationController.view.frame.size.width - 40, 20)];
    [self.notificationView addSubview:applbl2];
    applbl2.text = [NSString stringWithFormat:@"%@",[[[notification.request.content.userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"body"]];
    applbl2.textAlignment = NSTextAlignmentLeft;
    applbl2.numberOfLines = 0;
    applbl2.textColor = [UIColor blackColor];
    [applbl2 setFont:[UIFont fontWithName:applbl2.font.fontName size:14]];
    
    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(applbl2.frame.origin.x + applbl2.frame.size.width + 10, 40, 20, 20)];
    [self.notificationView addSubview:img];
    [img sd_setImageWithURL:[dictFromData objectForKey:@"icon_link"]];
    
    UILabel *applbl3 = [[UILabel alloc] initWithFrame:CGRectMake(iconImg.frame.origin.x + iconImg.frame.size.width + 10, iconImg.frame.origin.y +5, 100, 20)];
    [self.notificationView addSubview:applbl3];
    //            NSString *strIconName = [NSString stringWithFormat:@"%@", [dictFromData objectForKey:@"icon_name"]];
    NSString *strIconName = @"ProRinger Pro";
    NSLog(@"ICON NAME:::...%@", strIconName);
    if ([strIconName isEqualToString:@"<null>"] || [strIconName isEqual:[NSNull null]]) {
        applbl3.text = @"";
    } else {
        applbl3.text = strIconName;
    }
    
    applbl3.textAlignment = NSTextAlignmentLeft;
    applbl3.numberOfLines = 0;
    applbl3.textColor = COLOR_THEME;
    [applbl3 setFont:[UIFont fontWithName:applbl3.font.fontName size:14]];
    
    UITapGestureRecognizer *MessageNotificationgesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MessageNotificationTap)];
    [self.notificationView addGestureRecognizer:MessageNotificationgesture];
    
    
    UISwipeGestureRecognizer *recognizer;
    
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.notificationView addGestureRecognizer:recognizer];
    
    UISwipeGestureRecognizer *recognizer1;
    
    recognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer1 setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.notificationView addGestureRecognizer:recognizer1];
    
    [NSTimer scheduledTimerWithTimeInterval:15.0 target:self selector:@selector(RemoveNotificationView) userInfo:nil repeats: NO];
    
    if ([[dictFromData objectForKey:@"click_action"] isEqualToString:@"Message"]) {
        if ([navController1.visibleViewController isKindOfClass:[MessageDetailsVC class]]/* && ![strProProjectId isEqualToString:[dictFromData objectForKey:@"project_id"]]*/) {
            
//            [[NSUserDefaults standardUserDefaults] setObject:strProProjectId forKey:@"project_id"];
            
            if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"project_id"] isEqualToString:strProProjectId]) {
                [navController1.visibleViewController.navigationController.view addSubview:self.notificationView];
            } else {
                [self.notificationView removeFromSuperview];
            }
        } else {
            [navController1.visibleViewController.navigationController.view addSubview:self.notificationView];
        }
    } else {
        [navController1.visibleViewController.navigationController.view addSubview:self.notificationView];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reload_data" object:self];
}

-(void)MessageNotificationTap {
    
    [self.notificationView removeFromSuperview];
    
    UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
    int iPremiumStatus = [[dictFromData objectForKey:@"pro_premium_status"] intValue];
    
    if ([[dictFromData objectForKey:@"click_action"] isEqualToString:@"new_project_post"]) {

            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Watchlist" bundle: nil];
            ProjectDetailsVC *controller = (ProjectDetailsVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"ProjectDetailsVC"];
        
            controller.strProjectId = [dictFromData objectForKey:@"project_id"];
            
            [navigationController pushViewController:controller animated:YES];
    } else if ([[dictFromData objectForKey:@"click_action"] isEqualToString:@"Message"]) {
        
        if (iPremiumStatus == 0) {
            if ([[dictFromData objectForKey:@"new_lead_status"] isEqualToString:@"D"]) {
                
                if ([[dictFromData objectForKey:@"project_status"] intValue] == 0) {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Projects" bundle:nil];
                    LeadResponseVC *szvc = [storyboard instantiateViewControllerWithIdentifier:@"LeadResponseVC"];
                    
                    szvc.strLeadsRemaining = [NSString stringWithFormat:@"%d", [[dictFromData objectForKey:@"no_of_leads"] intValue]];
                    szvc.strProjectOwnerId = [dictFromData objectForKey:@"sender_id"];
                    szvc.strProjectId = [dictFromData objectForKey:@"project_id"];
                    szvc.isMessage = YES;
                    szvc.customNavigation = self;
                    
                    [navigationController presentViewController:szvc animated:NO completion:nil];
                } else {
                    
                    if (![navigationController.visibleViewController isKindOfClass:[MessageDetailsVC class]]) {
                        
                        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Message" bundle: nil];
                        MessageDetailsVC *controller = (MessageDetailsVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MessageDetailsVC"];
                        controller.isFromProject = NO;
                        controller.strProjectId = [dictFromData objectForKey:@"project_id"];
                        controller.strUserId = [dictFromData objectForKey:@"receiver_id"];
                        controller.strHomeOwnerId = [dictFromData objectForKey:@"sender_id"];
                        
                        [navigationController pushViewController:controller animated:YES];
                    } else {
                        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Message" bundle: nil];
                        MessageDetailsVC *controller = (MessageDetailsVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MessageDetailsVC"];
                        
                        controller.isFromProject = NO;
                        controller.strProjectId = [dictFromData objectForKey:@"project_id"];
                        controller.strUserId = [dictFromData objectForKey:@"receiver_id"];
                        controller.strHomeOwnerId = [dictFromData objectForKey:@"sender_id"];
                        
                        [navigationController pushViewController:controller animated:YES];
                    }
                }
                
            } else {
                
                if (![navigationController.visibleViewController isKindOfClass:[MessageDetailsVC class]]) {
                    
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Message" bundle: nil];
                    MessageDetailsVC *controller = (MessageDetailsVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MessageDetailsVC"];
                    
                    controller.isFromProject = NO;
                    controller.strProjectId = [dictFromData objectForKey:@"project_id"];
                    controller.strUserId = [dictFromData objectForKey:@"receiver_id"];
                    controller.strHomeOwnerId = [dictFromData objectForKey:@"sender_id"];
                    
                    [navigationController pushViewController:controller animated:YES];
                } else {
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Message" bundle: nil];
                    MessageDetailsVC *controller = (MessageDetailsVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MessageDetailsVC"];
                    
                    controller.isFromProject = NO;
                    controller.strProjectId = [dictFromData objectForKey:@"project_id"];
                    controller.strUserId = [dictFromData objectForKey:@"receiver_id"];
                    controller.strHomeOwnerId = [dictFromData objectForKey:@"sender_id"];
                    
                    [navigationController pushViewController:controller animated:YES];
                }
            }
        } else {
            
            if (![navigationController.visibleViewController isKindOfClass:[MessageDetailsVC class]]) {
                
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Message" bundle: nil];
                MessageDetailsVC *controller = (MessageDetailsVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MessageDetailsVC"];
                
                controller.isFromProject = NO;
                controller.strProjectId = [dictFromData objectForKey:@"project_id"];
                controller.strUserId = [dictFromData objectForKey:@"receiver_id"];
                controller.strHomeOwnerId = [dictFromData objectForKey:@"sender_id"];
                
                [navigationController pushViewController:controller animated:YES];
            } else {
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Message" bundle: nil];
                MessageDetailsVC *controller = (MessageDetailsVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MessageDetailsVC"];
                
                controller.isFromProject = NO;
                controller.strProjectId = [dictFromData objectForKey:@"project_id"];
                controller.strUserId = [dictFromData objectForKey:@"receiver_id"];
                controller.strHomeOwnerId = [dictFromData objectForKey:@"sender_id"];
                
                [navigationController pushViewController:controller animated:YES];
            }
        }
    } else if ([[dictFromData objectForKey:@"click_action"] isEqualToString:@"premium_subscription_cancel"]) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle: nil];
        PremiumVC *controller = (PremiumVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"PremiumVC"];
        
        [navigationController pushViewController:controller animated:YES];
        
    } else if ([[dictFromData objectForKey:@"click_action"] isEqualToString:@"Hire_pros"]) {
        
        if (![navigationController.visibleViewController isKindOfClass:[MessageDetailsVC class]]) {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Message" bundle: nil];
            MessageDetailsVC *controller = (MessageDetailsVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MessageDetailsVC"];
            
            controller.isFromProject = NO;
            controller.strProjectId = [dictFromData objectForKey:@"project_id"];
            controller.strUserId = [dictFromData objectForKey:@"receiver_id"];
            controller.strHomeOwnerId = [dictFromData objectForKey:@"sender_id"];
            
            [navigationController pushViewController:controller animated:YES];
        } else {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Message" bundle: nil];
            MessageDetailsVC *controller = (MessageDetailsVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MessageDetailsVC"];
            
            controller.isFromProject = NO;
            controller.strProjectId = [dictFromData objectForKey:@"project_id"];
            controller.strUserId = [dictFromData objectForKey:@"receiver_id"];
            controller.strHomeOwnerId = [dictFromData objectForKey:@"sender_id"];
            
            [navigationController pushViewController:controller animated:YES];
        }
    } else {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle: nil];
        DashboardVC *controller = (DashboardVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
        
        [navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - Custom Navigation
-(void)navigateToMessage:(int)indexPath {
    
    LeadResponseVC *lrvc = [[LeadResponseVC alloc] init];
    [lrvc tapToDismissView];
    
    UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Message" bundle: nil];
    MessageDetailsVC *controller = (MessageDetailsVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MessageDetailsVC"];
    controller.isFromProject = NO;
    controller.strProjectId = [dictFromData objectForKey:@"project_id"];
    controller.strUserId = [dictFromData objectForKey:@"receiver_id"];
    controller.strHomeOwnerId = [dictFromData objectForKey:@"sender_id"];
    
    [navigationController pushViewController:controller animated:YES];
}

-(void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer
{
    NSLog(@"Swipe received.");
    [self.notificationView removeFromSuperview];
    
}

-(void)RemoveNotificationView
{
    [self.notificationView removeFromSuperview];
}

//Called to let your app know which action was selected by the user for a given notification.
// Handle notification messages after display notification is tapped by the user.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
        withCompletionHandler:(void(^)())completionHandler {
    
//    [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
    
    NSLog(@"didReceiveNotificationResponse User Info : %@",response.notification.request.content.userInfo);
    
    NSInteger iBadge = [[[response.notification.request.content.userInfo objectForKey:@"aps"] objectForKey:@"badge"] integerValue];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", iBadge] forKey:@"badge"];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:iBadge];
    
    UINavigationController *navController1 = (UINavigationController *)self.window.rootViewController;
    
    
    NSData *dataFromDict = [[response.notification.request.content.userInfo objectForKey:@"gcm.notification.info"] dataUsingEncoding:NSUTF8StringEncoding];
    
    
    dictFromData = [NSJSONSerialization JSONObjectWithData:dataFromDict
                                                   options:0
                                                     error:nil];
    
    NSLog(@"Dictionary...%@",dictFromData);
    
    [self MessageNotificationTap];
    
//    if ([[dictFromData objectForKey:@"click_action"] isEqualToString:@"Message"]) {
//
//        if (![navController1.visibleViewController isKindOfClass:[MessageDetailsVC class]]/* && ![strProProjectId isEqualToString:[dictFromData objectForKey:@"project_id"]]*/) {
//
//            [[NSUserDefaults standardUserDefaults] setObject:strProProjectId forKey:@"project_id"];
//
////            [self MessageNotificationTap];
//
////            UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
////            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Message" bundle: nil];
////            MessageDetailsVC *controller = (MessageDetailsVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MessageDetailsVC"];
////
////
////            controller.strProjectId = [dictFromData objectForKey:@"project_id"];
////            controller.strUserId = [dictFromData objectForKey:@"receiver_id"];
////            controller.strHomeOwnerId = [dictFromData objectForKey:@"sender_id"];
////
////            [navigationController pushViewController:controller animated:YES];
//        }
//    } else if ([[dictFromData objectForKey:@"click_action"] isEqualToString:@"new_project_post"]) {
//
//        if (![navController1.visibleViewController isKindOfClass:[ProjectDetailsVC class]]) {
//            UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
//            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Watchlist" bundle: nil];
//            ProjectDetailsVC *controller = (ProjectDetailsVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"ProjectDetailsVC"];
//
//            controller.strProjectId = [dictFromData objectForKey:@"project_id"];
//
//            [navigationController pushViewController:controller animated:YES];
//        }
//    } else {
//
//    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reload_data" object:self];
}
#endif
// [END ios_10_message_handling]

- (void)registerForRemoteNotifications {
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else
    {
        // Code for old versions
    }
}

// [START ios_10_data_message_handling]
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Receive data message on iOS 10 devices while app is in the foreground.
- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    // Print full message
    NSLog(@"NOTIFICATION MESSAGE:::...%@", remoteMessage.appData);
    
}
#endif

// [START connect_to_fcm]
- (void)connectToFcm {
    
    //[[FIRMessaging messaging] disconnect];
    
    
    // Disconnect previous FCM connection if it exists.
    [[FIRMessaging messaging] setShouldEstablishDirectChannel:NO];
    [[FIRMessaging messaging] setShouldEstablishDirectChannel:YES];
    
    
    /*
     [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
     if (error != nil) {
     NSLog(@"Unable to connect to FCM. %@", error);
     } else {
     NSLog(@"Connected to FCM.");
     }
     }];*/
}

-(void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"Error %@",str);
}

//- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
//    if (notificationSettings.types != UIUserNotificationTypeNone) {
//        NSLog(@"didRegisterUser is called");
//        [application registerForRemoteNotifications];
//    }
//}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    int iBadge = [[userDefault objectForKey:@"badge"] intValue];
    [userDefault setObject:[NSString stringWithFormat:@"%d", iBadge] forKey:@"badge"];
    [userDefault synchronize];
    
    if (([[userDefault objectForKey:@"isLoggedIn"] boolValue] == YES)) {
        [application setApplicationIconBadgeNumber:iBadge];
    } else {
        [application setApplicationIconBadgeNumber:0];
    }
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    int iBadge = [[userDefault objectForKey:@"badge"] intValue];
    [userDefault setObject:[NSString stringWithFormat:@"%d", iBadge] forKey:@"badge"];
    [userDefault synchronize];
    
    if (([[userDefault objectForKey:@"isLoggedIn"] boolValue] == YES)) {
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:iBadge];
    } else {
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
//    [self MessageNotificationTap];
    
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    int iBadge = [[userDefault objectForKey:@"badge"] intValue];
//    [userDefault setObject:[NSString stringWithFormat:@"%d", iBadge] forKey:@"badge"];
//    [userDefault synchronize];
    
//    [application setApplicationIconBadgeNumber:iBadge];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    int iBadge = [[userDefault objectForKey:@"badge"] intValue];
//    [userDefault setObject:[NSString stringWithFormat:@"%d", iBadge] forKey:@"badge"];
//    [userDefault synchronize];
//
//    [application setApplicationIconBadgeNumber:iBadge];
    
//    [self connectToFcm];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    int iBadge = [[userDefault objectForKey:@"badge"] intValue];
    [userDefault setObject:[NSString stringWithFormat:@"%d", iBadge] forKey:@"badge"];
    [userDefault synchronize];
    
    if (([[userDefault objectForKey:@"isLoggedIn"] boolValue] == YES)) {
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:iBadge];
    } else {
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }
}


@end
